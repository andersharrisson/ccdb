/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.ent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Entity
@Table(name = "slot_relationship_label", indexes = { @Index(columnList = "label") })
@NamedQueries({
        @NamedQuery(name =
                "SlotRelationshipLabel.findByLabelPartStarting", query = "SELECT l FROM SlotRelationshipLabel l " +
                "WHERE LOWER(l.label) LIKE LOWER(CONCAT(:query, '%')) ORDER BY l.label"),
        @NamedQuery(name =
                "SlotRelationshipLabel.findByLabelPartContaining", query = "SELECT l FROM SlotRelationshipLabel l " +
                "WHERE LOWER(l.label) LIKE LOWER(CONCAT('%', :query, '%'))" +
                " AND LOWER(l.label) NOT LIKE LOWER(CONCAT(:query, '%')) ORDER BY l.label"),
        @NamedQuery(name = "SlotRelationshipLabel.findByLabel", query = "SELECT l FROM SlotRelationshipLabel l " +
                "WHERE l.label = :label")
})
public class SlotRelationshipLabel implements EntityWithId {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Size(max = 255)
    @Column(name = "label")
    private String label;

    /**
     * Constructor for persistence framework
     */
    public SlotRelationshipLabel() {}

    /**
     * Constructor
     * @param label label value
     */
    public SlotRelationshipLabel(final String label) {
        this.label = label;
    }

    /**
     * @return id from database table
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * @return label value
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets label value
     * @param label label value
     */
    public void setLabel(String label) {
        this.label = label;
    }
}
