/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.openepics.discs.conf.ent.dto;

import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotPair;
import org.openepics.discs.conf.ent.SlotRelationName;

import java.util.Objects;

/**
 * This class represents slot relationships.Every relationship contains source and target slot and a relationship name
 * (contains, controls, powers, ...).
 * 
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class SlotRelationship {
    private String id;
    private String relationshipName;
    private Slot sourceSlot;
    private Slot targetSlot;
    private SlotPair slotPair;

    /**
     * @return relationship id.
     */
    public String getId() {
        return id;
    }

    /**
     * @param id relationship id.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return relationship name (contains, controls, ...).
     */
    public String getRelationshipName() {
        return relationshipName;
    }

    /**
     * @param relationshipName relationship name (contains, controls, ...).
     */
    public void setRelationshipName(String relationshipName) {
        this.relationshipName = relationshipName;
    }

    /**
     * @return source slot.
     */
    public Slot getSourceSlot() {
        return sourceSlot;
    }

    /**
     * @param sourceSlot source slot.
     */
    public void setSourceSlot(Slot sourceSlot) {
        this.sourceSlot = sourceSlot;
    }

    /**
     * @return target slot.
     */
    public Slot getTargetSlot() {
        return targetSlot;
    }

    /**
     * @param targetSlot target slot.
     */
    public void setTargetSlot(Slot targetSlot) {
        this.targetSlot = targetSlot;
    }

    /**
     * @return parent-child slot pair.
     */
    public SlotPair getSlotPair() {
        return slotPair;
    }

    /**
     * @param slotPair parent-child slot pair.
     */
    public void setSlotPair(SlotPair slotPair) {
        this.slotPair = slotPair;
    }

    /**
     * @see Object#equals(Object) 
     */
    @Override 
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        SlotRelationship that = (SlotRelationship) o;
        return Objects.equals(id, that.id) && 
                Objects.equals(relationshipName, that.relationshipName) && 
                Objects.equals(sourceSlot, that.sourceSlot) && 
                Objects.equals(targetSlot, that.targetSlot) && 
                Objects.equals(slotPair, that.slotPair);
    }

    /**
     * @see Objects#hashCode(Object)
     */
    @Override 
    public int hashCode() {
        return Objects.hash(id, relationshipName, sourceSlot, targetSlot, slotPair);
    }

    /**
     * @see Object#toString() 
     */
    @Override 
    public String toString() {
        return "SlotRelationship{" + "id='" + id + '\'' + ", relationshipName='" + relationshipName + '\''
                + ", sourceSlot=" + sourceSlot + ", targetSlot=" + targetSlot + ", slotPair=" + slotPair + '}';
    }

    public static boolean isRelationNameInverse(String slotRelationName) {
        return slotRelationName.equals(SlotRelationName.CONTAINS.inverseName()) ||
                slotRelationName.equals(SlotRelationName.CONTROLS.inverseName()) ||
                slotRelationName.equals(SlotRelationName.POWERS.inverseName());
    }
}
