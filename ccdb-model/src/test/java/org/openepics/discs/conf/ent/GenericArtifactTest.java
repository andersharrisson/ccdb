/*
 *  Copyright (C) 2018 European Spallation Source ERIC.
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.ent;

import java.util.Date;
import java.util.UUID;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author georgweiss
 * Created Sep 17, 2018
 */
public class GenericArtifactTest {

    @Test
    public void testNewCopy(){
        GenericArtifact source = new GenericArtifact();
        source.setId(UUID.randomUUID().toString());
        source.setName("name");
        source.setDescription("desc");
        source.setContent(new byte[]{1});
        Date now = new Date();
        source.setModifiedAt(now);
        source.setModifiedBy("user");
        
        GenericArtifact copy = GenericArtifact.createNewCopy(source);
        
        assertEquals("name", copy.getName());
        assertEquals("desc", copy.getDescription());
        assertEquals("user", copy.getModifiedBy());
        assertEquals(now, copy.getModifiedAt());
        assertArrayEquals(new byte[]{1}, copy.getContent());
        assertNotEquals(source.getId(), copy.getId());
    }
}
