<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.openepics.discs</groupId>
    <artifactId>ccdb</artifactId>
    <version>1.7.8-SNAPSHOT</version>
    <packaging>pom</packaging>

    <name>CCDB master project</name>

    <parent>
        <groupId>se.esss.ics</groupId>
        <artifactId>ess-java-config</artifactId>
        <version>2.5.1</version>
    </parent>

    <properties>
        <version.jacoco>0.7.4.201502262128</version.jacoco>
        <app.version>1.7.8-SNAPSHOT</app.version>
        <jackson.version>2.8.5</jackson.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.openepics.discs</groupId>
                <artifactId>ccdb-jaxb</artifactId>
                <version>${app.version}</version>
            </dependency>
            <dependency>
                <groupId>org.openepics.discs</groupId>
                <artifactId>ccdb-client-api</artifactId>
                <version>${app.version}</version>
            </dependency>
            <dependency>
                <groupId>org.openepics.discs</groupId>
                <artifactId>ccdb-model</artifactId>
                <version>${app.version}</version>
            </dependency>
            <dependency>
                <groupId>org.openepics.discs</groupId>
                <artifactId>ccdb-business</artifactId>
                <version>${app.version}</version>
            </dependency>

            <dependency>
                <groupId>se.esss.ics.rbac</groupId>
                <artifactId>loginmodules</artifactId>
                <version>2.0.8</version>
            </dependency>
            <dependency>
                <groupId>org.openepics</groupId>
                <artifactId>seds-japi</artifactId>
                <version>${app.version}</version>
            </dependency>

            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>19.0</version>
            </dependency>
            <dependency>
                <groupId>javax</groupId>
                <artifactId>javaee-web-api</artifactId>
                <version>7.0</version>
            </dependency>
            <dependency>
                <groupId>com.google.code.findbugs</groupId>
                <artifactId>jsr305</artifactId>
                <version>2.0.2</version>
            </dependency>

            <dependency>
              <groupId>dom4j</groupId>
              <artifactId>dom4j</artifactId>
              <version>1.6.1</version>
            </dependency>

            <dependency>
              <groupId>io.swagger</groupId>
              <artifactId>swagger-jaxrs</artifactId>
              <version>1.5.10</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <modules>
        <module>conf-JAXB</module>
        <module>ccdb-client-api</module>
        <module>ccdb-model</module>
        <module>ccdb-business</module>
        <module>conf-core</module>
        <module>ccdb-ws</module>
        <module>ccdb-db-maint</module>
        <module>ccdb-ear</module>
        <module>contrib/seds-japi</module>
    </modules>

    <repositories>
        <repository>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <id>central</id>
            <name>libs-release</name>
            <url>https://artifactory.esss.lu.se:443/artifactory/libs-release</url>
        </repository>
        <repository>
            <snapshots />
            <id>snapshots</id>
            <name>libs-snapshot</name>
            <url>https://artifactory.esss.lu.se:443/artifactory/libs-snapshot</url>
        </repository>
    </repositories>
    <pluginRepositories>
        <pluginRepository>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <id>central</id>
            <name>plugins-release</name>
            <url>https://artifactory.esss.lu.se:443/artifactory/plugins-release</url>
        </pluginRepository>
        <pluginRepository>
            <snapshots />
            <id>snapshots</id>
            <name>plugins-release</name>
            <url>https://artifactory.esss.lu.se:443/artifactory/plugins-release</url>
        </pluginRepository>
    </pluginRepositories>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-war-plugin</artifactId>
                    <version>2.6</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-dependency-plugin</artifactId>
                    <version>2.10</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-deploy-plugin</artifactId>
                    <version>2.8.2</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.3</version>
                    <configuration>
                        <source>1.8</source>
                        <target>1.8</target>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>2.19.1</version>
                </plugin>
                <!-- This plugin's configuration is used to store Eclipse m2e settings only.
                     It has no influence on the Maven build itself.-->
                <plugin>
                    <groupId>org.eclipse.m2e</groupId>
                    <artifactId>lifecycle-mapping</artifactId>
                    <version>1.0.0</version>
                    <configuration />
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>build-helper-maven-plugin</artifactId>
                    <version>1.7</version>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.jfrog.buildinfo</groupId>
                <artifactId>artifactory-maven-plugin</artifactId>
                <version>2.6.1</version>
                <inherited>false</inherited>
                <executions>
                    <execution>
                        <id>build-info</id>
                        <goals>
                            <goal>publish</goal>
                        </goals>
                        <configuration>
                            <publisher>
                                <contextUrl>${artifactory.url}</contextUrl>
                                <username>${artifactory.username}</username>
                                <password>${artifactory.password}</password>
                                <repoKey>libs-release-local</repoKey>
                                <snapshotRepoKey>libs-snapshot-local</snapshotRepoKey>
                            </publisher>
                            <buildInfo>
                                <buildNumber>{{BUILD_NUMBER}}</buildNumber>
                                <buildUrl>{{BUILD_URL}}</buildUrl>
                            </buildInfo>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>
