/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.auditlog;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.ComptypeArtifact;
import org.openepics.discs.conf.ent.ComptypePropertyValue;
import org.openepics.discs.conf.ent.EntityTypeOperation;
import org.openepics.discs.conf.ent.ExternalLink;
import org.openepics.discs.conf.ent.Property;
import org.openepics.discs.conf.ent.values.IntValue;

public class ComponentTypeEntityLoggerTest {

    private final Property prop1 = new Property("DETER", "deter");
    private final ComptypePropertyValue compTypePropVal1 = new ComptypePropertyValue(false);
    private final Property prop2 = new Property("APERTURE", "aperture");
    private final ComptypePropertyValue compTypePropVal2 = new ComptypePropertyValue(false);
    private final ComponentType compType = new ComponentType("Deteriorator");
    private final ComptypeArtifact artifact1 = new ComptypeArtifact("CAT Image", "CAT image");
    private final ComptypeArtifact artifact2 = new ComptypeArtifact("Manual", "Users manual");
    private final ExternalLink externalLink1 = new ExternalLink();
    private final ExternalLink externalLink2 = new ExternalLink();

    private final ComponentTypeEntityLogger ctel = new ComponentTypeEntityLogger();

    @Before
    public void setUp() throws Exception {
        artifact1.setName("CAT Image");
        artifact1.setDescription("Simple CAT image");
        artifact2.setName("Manual");
        artifact2.setDescription("Users manual");
        externalLink1.setName("Link 1");
        externalLink1.setDescription("Test link 1");
        externalLink1.setUri("http://test-link-1.com");
        externalLink2.setName("Link 2");
        externalLink2.setDescription("Test link 2");
        externalLink2.setUri("http://test-link-2.com");
        compTypePropVal1.setPropValue(new IntValue(15));
        compTypePropVal1.setProperty(prop1);
        compTypePropVal2.setPropValue(new IntValue(10));
        compTypePropVal2.setProperty(prop2);

        compType.getComptypePropertyList().add(compTypePropVal1);
        compType.getComptypePropertyList().add(compTypePropVal2);

        compType.getEntityArtifactList().add(artifact1);
        compType.getEntityArtifactList().add(artifact2);

        compType.getExternalLinkList().add(externalLink1);
        compType.getExternalLinkList().add(externalLink2);
    }

    @Test
    public void testGetType() {
        assertTrue(ComponentType.class.equals(ctel.getType()));
    }

    @Test
    public void testSerializeEntity() {
        final String RESULT =
                "{\"name\":\"Deteriorator\",\"deviceTypePropertyList\":[{\"APERTURE\":\"10\"},{\"DETER\":\"15\"}]," +
                "\"deviceTypeArtifactList\":[{\"name\":\"CAT Image\",\"description\":\"Simple CAT image\"}," +
                        "{\"name\":\"Manual\",\"description\":\"Users manual\"}],\"deviceTypeExternalLinkList\":[{" +
                        "\"name\":\"Link 1\"," +
                        "\"description\":\"Test link 1\"," +
                        "\"url\":\"http://test-link-1.com\"" +
                        "},{" +
                        "\"name\":\"Link 2\"," +
                        "\"description\":\"Test link 2\"," +
                        "\"url\":\"http://test-link-2.com\"" +
                        "}]}";
        assertEquals(RESULT, ctel.auditEntries(compType, EntityTypeOperation.CREATE).get(0).getEntry());
    }

}
