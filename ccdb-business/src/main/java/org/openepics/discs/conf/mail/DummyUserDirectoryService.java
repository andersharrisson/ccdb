/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 * Controls Configuration Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.mail;

import java.io.Serializable;
import java.util.Collections;
import java.util.Set;

import javax.annotation.Priority;
import javax.ejb.Singleton;
import javax.enterprise.inject.Alternative;
import javax.interceptor.Interceptor;

/**
 * A dummy implementation of user directory service. Returns no data on any users.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Singleton
@Alternative
@Priority(Interceptor.Priority.APPLICATION)
public class DummyUserDirectoryService implements UserDirectoryService, Serializable {

    private static final long serialVersionUID = 4222990370703984622L;

    @Override
    public Set<String> getAllUsernames() {
        return Collections.emptySet();
    }

    @Override
    public Set<String> getAllAdministratorUsernames() {
        return Collections.emptySet();
    }

    @Override
    public String getEmail(String username) {
        return null;
    }

    @Override
    public String getUserFullName(String username) {
        return null;
    }

    @Override
    public String getUserFullNameAndEmail(String username) {
        return null;
    }
}
