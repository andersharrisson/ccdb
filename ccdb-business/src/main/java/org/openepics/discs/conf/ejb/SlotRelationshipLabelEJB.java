/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.ejb;

import com.google.common.collect.ImmutableMap;
import org.openepics.discs.conf.ent.SlotRelationshipLabel;
import org.openepics.discs.conf.util.AutoCompleteQuery;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Stateless
public class SlotRelationshipLabelEJB extends DAO<SlotRelationshipLabel> {
    private final AutoCompleteDatabaseQuery<SlotRelationshipLabel> labelAutoCompDatabaseQuery
            = new AutoCompleteDatabaseQuery<SlotRelationshipLabel>() {
        @Override
        public String getQueryNameOfFindStarting() {
            return "SlotRelationshipLabel.findByLabelPartStarting";
        }

        @Override
        public String getQueryNameOfFindContaining() {
            return "SlotRelationshipLabel.findByLabelPartContaining";
        }

        @Override
        public Class<SlotRelationshipLabel> getAutoCompleteEntityClass() {
            return SlotRelationshipLabel.class;
        }

        @Override
        public EntityManager getEntityManager() {
            return em;
        }
    };

    /**
     * Finds the {@link SlotRelationshipLabel} which has the specific label value matching to query.
     *
     * @return A list of all {@link SlotRelationshipLabel}s in the database ordered by creation date */
    public List<SlotRelationshipLabel> findByLabelPart(final String query, final int limit) {
        return labelAutoCompDatabaseQuery.autoCompleteResult(
                ImmutableMap.of(AutoCompleteQuery.QUERY_PARAM, query), limit);
    }

    /**
     * Finds the {@link SlotRelationshipLabel} which has the specific label value
     * @param label label value
     * @return the {@link SlotRelationshipLabel} which has the label value or <code>null<code/> if no
     * matching {@link SlotRelationshipLabel}
     */
    public SlotRelationshipLabel findByLabel(final String label) {
        return em.createNamedQuery("SlotRelationshipLabel.findByLabel", SlotRelationshipLabel.class)
                .setParameter("label", label)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    protected Class<SlotRelationshipLabel> getEntityClass() {
        return SlotRelationshipLabel.class;
    }
}
