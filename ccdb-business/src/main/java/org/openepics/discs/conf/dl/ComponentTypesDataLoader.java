/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.dl;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.openepics.discs.conf.dl.annotations.ComponentTypesLoader;
import org.openepics.discs.conf.dl.common.AbstractDataLoader;
import org.openepics.discs.conf.dl.common.AbstractEntityWithPropertiesDataLoader;
import org.openepics.discs.conf.dl.common.DataLoader;
import org.openepics.discs.conf.dl.common.DataLoaderResult;
import org.openepics.discs.conf.dl.common.ErrorMessage;
import org.openepics.discs.conf.ejb.ComptypeEJB;
import org.openepics.discs.conf.ejb.DAO;
import org.openepics.discs.conf.ejb.SlotEJB;
import org.openepics.discs.conf.ent.*;
import org.openepics.discs.conf.mail.MailService;
import org.openepics.discs.conf.security.SSOSessionService;
import org.openepics.discs.conf.security.SecurityPolicy;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import org.openepics.discs.conf.util.ConversionException;
import org.openepics.discs.conf.util.DefaultValueNotAllowedException;
import org.openepics.discs.conf.util.PropertyAliasDefaultValueException;

/**
 * Implementation of data loader for device types.
 *
 * @author <a href="mailto:andraz.pozar@cosylab.com">Andraž Požar</a>
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
@Stateless
@ComponentTypesLoader
public class ComponentTypesDataLoader extends AbstractEntityWithPropertiesDataLoader<ComptypePropertyValue>
        implements DataLoader {

    private static final Logger LOGGER = Logger.getLogger(ComponentTypesDataLoader.class.getCanonicalName());

    // Header column name constants
    protected static final String HDR_NAME = "NAME";
    protected static final String HDR_DESC = "DESCRIPTION";
    protected static final String HDR_PROP_TYPE = "PROPERTY TYPE";
    protected static final String HDR_PROP_NAME = "PROPERTY NAME";
    protected static final String HDR_PROP_VALUE = "PROPERTY VALUE";

    private static  final String DEFAULT_VALUE_ERROR = "Default value not allowed for entered type";
    private static  final String CONVERSION_ERROR = "Conversion error";

    private static final int COL_INDEX_NAME = 1;
    private static final int COL_INDEX_DESC = 2;
    private static final int COL_INDEX_PROP_TYPE = 3;
    private static final int COL_INDEX_PROP_NAME = 4;
    private static final int COL_INDEX_PROP_VALUE = 5;

    // Fields for row cells
    private String nameFld, descriptionFld, propTypeFld, propNameFld, propValueFld;

    @Inject
    private ComptypeEJB comptypeEJB;
    @Inject
    private SecurityPolicy securityPolicy;
    @Inject
    private SSOSessionService sessionService;
    @Inject
    private MailService mailService;

    @Override
    protected @Nullable Integer getUniqueColumnIndex() {
        return COL_INDEX_NAME;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected DAO<ComponentType> getDAO() {
        return comptypeEJB;
    }

    @Override
    protected void assignMembersForCurrentRow() {
        nameFld = readCurrentRowCellForHeader(COL_INDEX_NAME);
        descriptionFld = readCurrentRowCellForHeader(COL_INDEX_DESC);
        propTypeFld = readCurrentRowCellForHeader(COL_INDEX_PROP_TYPE);
        propNameFld = readCurrentRowCellForHeader(COL_INDEX_PROP_NAME);
        propValueFld = readCurrentRowCellForHeader(COL_INDEX_PROP_VALUE);
    }

    @Override
    protected void handleUpdate(String actualCommand) {
        final ComponentType componentTypeToUpdate = comptypeEJB.findByName(nameFld);
        if (componentTypeToUpdate != null) {
            if (componentTypeToUpdate.getName().equals(SlotEJB.ROOT_COMPONENT_TYPE)) {
                result.addRowMessage(ErrorMessage.NOT_AUTHORIZED, AbstractDataLoader.HDR_OPERATION, actualCommand);
                return;
            }
            try {
                if (DataLoader.CMD_UPDATE_DEVICE_TYPE.equals(actualCommand)) {
                    if (StringUtils.isNotEmpty(descriptionFld)) {
                        componentTypeToUpdate.setDescription(descriptionFld);
                        comptypeEJB.explicitAuditLog(componentTypeToUpdate, EntityTypeOperation.UPDATE);
                    } else {
                        result.addRowMessage(ErrorMessage.REQUIRED_FIELD_MISSING, HDR_DESC);
                    }
                } else {
                    if (propNameFld != null) {
                        if (DataLoader.PROP_EXTERNAL_LINK.equals(propTypeFld)) {
                            updateExternalLink(componentTypeToUpdate);
                        }
                        // Disable to update device property (after device tab was hidden)
                        else if (DataLoader.PROP_TYPE_DEV_INSTANCE.equals(propTypeFld)) {
                            result.addRowMessage(ErrorMessage.ATTEMPT_TO_ADD_OR_UPDATE_DEVICE_PROPERTY, HDR_PROP_NAME,
                                    propNameFld);
                        } else {
                            final ComptypePropertyValue comptypePropertyValue =
                                    (ComptypePropertyValue) getPropertyValue(
                                            componentTypeToUpdate, propNameFld, HDR_PROP_NAME);
                            //do not allow to enter default value for SlotReference
                            if (comptypePropertyValue != null) {
                                Property slotProp = propertyEJB.findByName(propNameFld);
                                if ((DataLoader.CMD_UPDATE_PROPERTY.equals(actualCommand)) &&
                                        (slotProp != null) &&
                                        (BooleanUtils.toBoolean(slotProp.getSlotReference())) &&
                                        (StringUtils.isNotEmpty(propValueFld))) {

                                    throw new DefaultValueNotAllowedException(
                                            "Default value not allowed for type: " + propTypeFld);
                                }

                                updateProperty(componentTypeToUpdate, propNameFld, propValueFld, HDR_PROP_NAME,
                                        HDR_PROP_VALUE);
                            }
                        }
                    } else {
                        result.addRowMessage(ErrorMessage.REQUIRED_FIELD_MISSING, HDR_PROP_NAME);
                    }
                }
            } catch (final ConversionException e) {
                LOGGER.log(Level.FINE, CONVERSION_ERROR, e);
                result.addRowMessage(ErrorMessage.CONVERSION_ERROR, HDR_PROP_VALUE,
                        propValueFld);
            } catch (final DefaultValueNotAllowedException e) {
                LOGGER.log(Level.FINE, DEFAULT_VALUE_ERROR, e);
                result.addRowMessage(ErrorMessage.DEFAULT_VALUE_NOT_ALLOWED, HDR_PROP_VALUE,
                        propValueFld);
            } catch (final PropertyAliasDefaultValueException pade) {
                result.addRowMessage(ErrorMessage.PROPERTY_ALIAS_DEFAULT_VALUE, HDR_PROP_VALUE, propValueFld);
            } catch (final EJBTransactionRolledbackException e) {
                handleLoadingError(LOGGER, e);
            }
        } else {
            result.addRowMessage(ErrorMessage.ENTITY_NOT_FOUND, HDR_NAME, nameFld);
        }
    }

    @Override
    protected void handleCreate(String actualCommand) {
        final ComponentType componentTypeToUpdate = comptypeEJB.findByName(nameFld);
        if (DataLoader.CMD_CREATE_DEVICE_TYPE.equals(actualCommand)) {
            if (componentTypeToUpdate == null) {
                try {
                    final ComponentType compTypeToAdd = new ComponentType(nameFld);
                    if (StringUtils.isNotEmpty(descriptionFld)) {
                        compTypeToAdd.setDescription(descriptionFld);
                        comptypeEJB.add(compTypeToAdd);
                    } else {
                        result.addRowMessage(ErrorMessage.REQUIRED_FIELD_MISSING, HDR_DESC);
                    }
                } catch (EJBTransactionRolledbackException e) {
                    handleLoadingError(LOGGER, e);
                }
            } else {
                result.addRowMessage(ErrorMessage.NAME_ALREADY_EXISTS, HDR_NAME, nameFld);
            }
        } else {
            if (componentTypeToUpdate != null) {
                if (DataLoader.PROP_EXTERNAL_LINK.equals(propTypeFld)) {
                    addExternalLink(componentTypeToUpdate);
                } else {
                    addPropertyValue(componentTypeToUpdate);
                }
            } else {
                result.addRowMessage(ErrorMessage.ENTITY_NOT_FOUND, HDR_NAME, nameFld);
            }
        }
    }

    @Override
    protected void handleDelete(String actualCommand) {
        final ComponentType componentTypeToDelete = comptypeEJB.findByName(nameFld);
        try {
            if (componentTypeToDelete == null) {
                result.addRowMessage(ErrorMessage.ENTITY_NOT_FOUND, HDR_NAME, nameFld);
            } else {
                if (SlotEJB.ROOT_COMPONENT_TYPE.equals(componentTypeToDelete.getName())) {
                    result.addRowMessage(ErrorMessage.NOT_AUTHORIZED, AbstractDataLoader.HDR_OPERATION, actualCommand);
                    return;
                }

                if (DataLoader.CMD_DELETE_DEVICE_TYPE.equals(actualCommand)) {
                    if (comptypeEJB.isComponentTypeUsed(componentTypeToDelete)) {
                        result.addRowMessage(ErrorMessage.DELETE_IN_USE, HDR_NAME, nameFld);
                    } else {
                        comptypeEJB.delete(componentTypeToDelete);
                    }
                } else {
                    if (propNameFld != null) {
                        if (DataLoader.PROP_EXTERNAL_LINK.equals(propTypeFld)) {
                            deleteExternalLink(componentTypeToDelete);
                        } else {
                            final ComptypePropertyValue comptypePropertyValue =
                                    (ComptypePropertyValue) getPropertyValue(
                                            componentTypeToDelete, propNameFld, HDR_PROP_NAME);
                            // Property defs can only be deleted through UI to properly handle already
                            // assigned prop values
                            if (comptypePropertyValue != null) {
                                if (!comptypePropertyValue.isPropertyDefinition()) {
                                    comptypeEJB.deleteChild(comptypePropertyValue);
                                } else {
                                    result.addRowMessage(
                                            ErrorMessage.PROPERTY_TYPE_INCORRECT, HDR_PROP_TYPE, propTypeFld);
                                }
                            }
                        }
                    } else {
                        result.addRowMessage(ErrorMessage.REQUIRED_FIELD_MISSING, HDR_PROP_NAME);
                    }
                }
            }
        } catch (EJBTransactionRolledbackException e) {
            handleLoadingError(LOGGER, e);
        }
    }

    @Override
    protected void setUpIndexesForFields() {
        final Builder<String, Integer> mapBuilder = ImmutableMap.builder();

        mapBuilder.put(HDR_NAME, COL_INDEX_NAME);
        mapBuilder.put(HDR_DESC, COL_INDEX_DESC);
        mapBuilder.put(HDR_PROP_TYPE, COL_INDEX_PROP_TYPE);
        mapBuilder.put(HDR_PROP_NAME, COL_INDEX_PROP_NAME);
        mapBuilder.put(HDR_PROP_VALUE, COL_INDEX_PROP_VALUE);

        indicies = mapBuilder.build();
    }

    @Override
    public int getImportDataStartIndex() {
        // the new template starts with data in row 11 (0 based == 10)
        return 10;
    }

    private void addExternalLink(ComponentType comptypeToUpdate) {
        if (Strings.isNullOrEmpty(propNameFld)) {
            result.addRowMessage(ErrorMessage.REQUIRED_FIELD_MISSING, HDR_PROP_NAME, propNameFld);
            return;
        }
        if (Strings.isNullOrEmpty(propValueFld)) {
            result.addRowMessage(ErrorMessage.REQUIRED_FIELD_MISSING, HDR_PROP_VALUE, propValueFld);
            return;
        }

        ExternalLink externalLink = new ExternalLink();
        externalLink.setName(propNameFld.trim());
        externalLink.setUri(propValueFld);
        externalLink.setDescription("Created from import");

        if (comptypeToUpdate.getExternalLinkList().contains(externalLink)) {
            result.addRowMessage(ErrorMessage.EXTERNAL_LINK_NAME_NOT_UNIQUE, HDR_PROP_NAME);
            return;
        }

        comptypeEJB.addExternalLink(comptypeToUpdate, externalLink);

    }

    private void updateExternalLink(ComponentType comptypeToUpdate) {
        if (Strings.isNullOrEmpty(propNameFld)) {
            result.addRowMessage(ErrorMessage.REQUIRED_FIELD_MISSING, HDR_PROP_NAME, propNameFld);
            return;
        }
        if (Strings.isNullOrEmpty(propValueFld)) {
            result.addRowMessage(ErrorMessage.REQUIRED_FIELD_MISSING, HDR_PROP_VALUE, propValueFld);
            return;
        }

        for(ExternalLink externalLink : comptypeToUpdate.getExternalLinkList()) {
            if(externalLink.getName().equals(propNameFld.trim())){
                externalLink.setUri(propValueFld);
                comptypeEJB.save(comptypeToUpdate);
                return;
            }
        }

        // If an external link with the specified name has not been found, create an error message.
        result.addRowMessage(ErrorMessage.EXTERNAL_LINK_NAME_NOT_FOUND, HDR_PROP_NAME, propNameFld);
    }

    private void deleteExternalLink(ComponentType comptypeToUpdate) {
        if(Strings.isNullOrEmpty(propNameFld)) {
            result.addRowMessage(ErrorMessage.REQUIRED_FIELD_MISSING, HDR_PROP_NAME);
            return;
        }

        for(ExternalLink externalLink : comptypeToUpdate.getExternalLinkList()) {
            if(externalLink.getName().equals(propNameFld)) {
                comptypeEJB.deleteExternalLink(comptypeToUpdate, externalLink);
                return;
            }
        }

        // If an external link with the specified name has not been found, create an error message.
        result.addRowMessage(ErrorMessage.EXTERNAL_LINK_NAME_NOT_FOUND, HDR_PROP_NAME, propNameFld);
    }

    private void addPropertyValue(ComponentType comptypeToUpdate) {
        if (Strings.isNullOrEmpty(propNameFld)) {
            result.addRowMessage(ErrorMessage.REQUIRED_FIELD_MISSING, HDR_PROP_NAME, propNameFld);
            return;
        }

        if (Strings.isNullOrEmpty(propTypeFld)) {
            result.addRowMessage(ErrorMessage.REQUIRED_FIELD_MISSING, HDR_PROP_TYPE, propTypeFld);
            return;
        }

        // does property exist
        final @Nullable Property property = propertyEJB.findByName(propNameFld);
        if (property == null) {
            result.addRowMessage(ErrorMessage.ENTITY_NOT_FOUND, HDR_PROP_NAME, propNameFld);
            return;
        }

        // is this property value already added
        for (final ComptypePropertyValue value : comptypeToUpdate.getComptypePropertyList()) {
            if (value.getProperty().equals(property)) {
                result.addRowMessage(ErrorMessage.NAME_ALREADY_EXISTS, HDR_PROP_NAME, propNameFld);
                return;
            }
        }

        final ComptypePropertyValue propertyValue = new ComptypePropertyValue(false);
        propertyValue.setProperty(property);
        propertyValue.setComponentType(comptypeToUpdate);
        boolean isAlias = propertyValue.getProperty().getAliasOf() != null;
        switch (propTypeFld) {
        case DataLoader.PROP_TYPE_DEV_TYPE:
            if (isAlias) {
                result.addRowMessage(ErrorMessage.PROPERTY_ALIAS_ALLOWED_ONLY_FOR_SLOT_PROPERTY, HDR_PROP_NAME,
                        propNameFld);
                return;
            }
            if (Strings.isNullOrEmpty(propValueFld)) {
                result.addRowMessage(ErrorMessage.REQUIRED_FIELD_MISSING, HDR_PROP_VALUE);
                return;
            }

            if (BooleanUtils.toBoolean(property.getSlotReference())) {
                result.addRowMessage(ErrorMessage.SLOT_REFERENCE_IN_DEVICE, HDR_PROP_VALUE);
                return;
            }
            break;
        case DataLoader.PROP_TYPE_SLOT:
            propertyValue.setPropertyDefinition(true);
            propertyValue.setDefinitionTargetSlot(true);
            break;
        case DataLoader.PROP_TYPE_DEV_INSTANCE:
            // Disable to add new device property (after device tab was hidden)
            result.addRowMessage(ErrorMessage.ATTEMPT_TO_ADD_OR_UPDATE_DEVICE_PROPERTY, HDR_PROP_NAME,
                    propNameFld);
            break;
        default:
            result.addRowMessage(ErrorMessage.COMMAND_NOT_VALID, HDR_PROP_TYPE, propTypeFld);
            return;
        }

        if ((BooleanUtils.toBoolean(propertyValue.getProperty().getSlotReference())) &&
                (StringUtils.isNotEmpty(propValueFld))) {
            result.addRowMessage(ErrorMessage.DEFAULT_VALUE_NOT_ALLOWED, HDR_PROP_NAME, propValueFld);
            return;
        }

        try {
            // Slot and device property values can have defaults
            addProperty(true, propertyValue, propValueFld, HDR_PROP_VALUE);
        } catch (final ConversionException e) {
            LOGGER.log(Level.FINE, CONVERSION_ERROR, e);
            result.addRowMessage(ErrorMessage.CONVERSION_ERROR, HDR_PROP_VALUE,
                    propValueFld);
        } catch (DefaultValueNotAllowedException e) {
            LOGGER.log(Level.FINE, DEFAULT_VALUE_ERROR, e);
            result.addRowMessage(ErrorMessage.DEFAULT_VALUE_NOT_ALLOWED, HDR_PROP_VALUE,
                    propValueFld);
        } catch (PropertyAliasDefaultValueException pade) {
            result.addRowMessage(ErrorMessage.PROPERTY_ALIAS_DEFAULT_VALUE, HDR_PROP_VALUE, propValueFld);
        }
    }

    @Override
    public DataLoaderResult loadDataToDatabase(List<Pair<Integer, List<String>>> inputRows,
            Map<String, Object> contextualData, ByteArrayInputStream inputStream) {
        DataLoaderResult result = super.loadDataToDatabase(inputRows, contextualData, inputStream);
        // Mail notification
        if (!result.isError()) {
            inputStream.reset();
            mailService.sendMail(securityPolicy.getAllAdministratorUsernames(), sessionService.getLoggedInName(),
                    "Device Types imported notification",
                    "Device Types have been imported in the CCDB" + " (by "
                            + securityPolicy.getUserFullNameAndEmail(sessionService.getLoggedInName()) + ")",
                    Arrays.asList(inputStream), Arrays.asList("imported_component_types.xlsx"), true, true);
        }
        return result;
    }
}
