/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.dl.common;

import org.apache.commons.lang.StringUtils;

/**
 * This enumeration contains all possible error messages for data loaders.
 *
 * @author <a href="mailto:andraz.pozar@cosylab.com">Andraž Požar</a>
 */
public enum ErrorMessage {
    NAME_ALREADY_EXISTS("Entity with this name already exists"),
    NAME_ALREADY_EXISTS_UNDER_PARENT("Entity with this name already exists under this parent"),
    NOT_AUTHORIZED("You are not authorized to perform this action"),
    COMMAND_NOT_VALID("Command is not valid"),
    RENAME_MISFORMAT("Rename syntax is not correct"),
    ENTITY_NOT_FOUND("Entity to be affected was not found"),
    UNKNOWN_SLOT_RELATION_TYPE("Slot Relation specified is of unknown type"),
    REQUIRED_FIELD_MISSING("Required field is missing"),
    SHOULD_BE_NUMERIC_VALUE("The value of this field should be numeric"),
    SHOULD_BE_BOOLEAN_VALUE("The value of this field should be TRUE or FALSE"),
    CANT_ADD_PARENT_TO_ROOT("Can't add parent to root node"),
    INSTALL_CANT_CONTAIN_CONTAINER("Slot can't be the parent of container"),
    SLOT_RELATIONSHIP_NOT_FOUND("This slot relationship does not exist"),
    POWER_RELATIONSHIP_RESTRICTIONS("\"Powers\" slot relationship can only be set up between two slots"),
    CONTROL_RELATIONSHIP_RESTRICTIONS("\"Controls\" slot relationship can only be set up between two slots"),
    ORPHAN_SLOT("Specified slot was not assigned a parent"),
    SAME_CHILD_AND_PARENT("Loop relationship. Child and parent can't be the same slot"),
    PROPERTY_NOT_FOUND("Property with this name was not found"),
    PROPERTY_TYPE_INCORRECT("The command is incorrect for this type of property"),
    SYSTEM_EXCEPTION("Internal error occurred"),
    CREATE_VALUE_EXISTS("Cannot create a property value, because it already exists"),
    MODIFY_VALUE_MISSING("Cannot modify a property value, because it was not defined"),
    MODIFY_IN_USE("Cannot modify entity field because it is in use"),
    DELETE_IN_USE("Cannot delete entity because it is in use"),
    UNIQUE_INCORRECT("The value of the uniqueness is incorrect"),
    REGEXP_INCORRECT("The regular expression syntax is invalid"),
    ENUM_NOT_ENOUGH_ELEMENTS("The enumeration needs at least two elements"),
    ENUM_INVALID_CHARACTERS("Enumeration value can only contain alphanumerical characters"),
    INSTALLATION_SLOT_REQUIRED("This operation is only valid for an slot"),
    AMBIGUOUS_PARENT_SLOT("There is more than one parent matching provided name"),
    CONVERSION_ERROR("The property value cannot be converted to required type"),
    INVALID_SLOT_PROPERTY_ALIAS("Invalid slot name for slot property alias"),
    PROPERTY_ALIAS_DEFAULT_VALUE("Setting default value for property alias not allowed"),
    PROPERTY_ALIAS_ALLOWED_ONLY_FOR_SLOT_PROPERTY("Only slot property can be alias of other property"),
    NOT_UNIQUE("The property value is not unique"),
    INSTALLATION_EXISTING("The slot already has a device installed"),
    DEVICE_TYPE_ERROR("The device type is incorrect for requested operation"),
    VALUE_NOT_IN_DATABASE("The value of the field does not match the database"),
    ORPHAN_CREATED_BY_DELETE("Removing this entity would create an orphan"),
    ORPHAN_CREATED_BY_EDIT("Editing the entity would create an orphan"),
    EXTERNAL_LINK_NAME_NOT_UNIQUE("Name of the external link is not unique"),
    DEV_TYPE_SLOT_EXTERNAL_LINK_NAME_NOT_UNIQUE("At least one Device type external link and Slot external link have" +
            " the same name, external link update is not allowed in this case"),
    EXTERNAL_LINK_NAME_NOT_FOUND("External link not found"),
    UNKNOWN_SLOT("Slot not found in Naming"),
    UNKNOWN_SLOT_REFERENCE("Unknown Slot to refer to"),
    DEFAULT_VALUE_NOT_ALLOWED("Default value not allowed for that Type"),
    SLOT_REFERENCE_IN_DEVICE("Only slot property can be a Slot reference"),
    PROPERTY_ALIAS_UPDATE_NOT_ALLOWED("Cannot modify this field of property alias"),
    SLOT_REFERENCE_UPDATE_NOT_ALLOWED("Cannot modify this field of slot reference"),
    FBS_TAG_NOT_MATCH("Slot FBS tag from input doesn't match to FBS tag from service"),
    INVALID_HOSTNAME("Invalid Hostname"),
    ATTEMPT_TO_ADD_OR_UPDATE_DEVICE_PROPERTY("Invalid operation: Attempt to add or update device property." +
            " (Device and Device property is about to be taken of completely.)"),
    ATTEMPT_TO_INSTALL_DEVICE("Invalid operation: Attempt to install device." +
            " (Device and Device property is about to be taken of completely.)");

    private final String text;

    private String additionalInfo;

    /**
     * @param text
     */
    ErrorMessage(final String text) {
        this.text = text;
    }

    public ErrorMessage additionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
        return this;
    }

    @Override
    public String toString() {
        return text + (StringUtils.isNotEmpty(additionalInfo) ? " " + additionalInfo : "");
    }
}
