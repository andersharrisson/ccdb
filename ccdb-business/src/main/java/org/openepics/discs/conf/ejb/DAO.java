/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.ejb;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.openepics.discs.conf.auditlog.Audit;
import org.openepics.discs.conf.auditlog.AuditEntity;
import org.openepics.discs.conf.auditlog.AuditLogEntryCreator;
import org.openepics.discs.conf.auditlog.AuditUserName;
import org.openepics.discs.conf.ent.Artifact;
import org.openepics.discs.conf.ent.AuditRecord;
import org.openepics.discs.conf.ent.EntityTypeOperation;
import org.openepics.discs.conf.ent.EntityWithArtifacts;
import org.openepics.discs.conf.ent.EntityWithId;
import org.openepics.discs.conf.ent.EntityWithProperties;
import org.openepics.discs.conf.ent.EntityWithExternalLinks;
import org.openepics.discs.conf.ent.ExternalLink;
import org.openepics.discs.conf.ent.Property;
import org.openepics.discs.conf.ent.PropertyValue;
import org.openepics.discs.conf.ent.values.Value;
import org.openepics.discs.conf.security.Authorized;
import org.openepics.discs.conf.util.CCDBRuntimeException;
import org.openepics.discs.conf.util.CRUDOperation;
import org.openepics.discs.conf.util.ParentEntityResolver;
import org.openepics.discs.conf.util.PropertyNotUniqueException;
import org.openepics.discs.conf.util.PropertyValueNotUniqueException;
import org.openepics.discs.conf.util.UnhandledCaseException;

import com.google.common.base.Preconditions;

/**
 * Abstract generic DAO used for all entities.
 *
 * It uses the concept of Parent and optional Child entities in {@link List} collections. This one extends the read-only
 * {@link ReadOnlyDAO}.
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 * @param <T>
 *            The entity type for which this DAO is defined.
 */
public abstract class DAO<T extends EntityWithId> extends ReadOnlyDAO<T> {

    private static final String GET_PARENT_CALLED_ON_ENTITY_THAT_HAS_NEITHER_PROPERTIES_NOR_ARTIFACTS =
            "getParent called on entity that has neither properties nor artifacts.";
    private static final String ATTEMPT_TO_SAVE_SLOT_NO_USER_INFO =
            "Attempt to save slot without user name information";
    private static final String PROPERTY_PARAM = "property";
    private static final String PROPERTY_VALUE_PARAM = "propValue";

    @Inject
    private AuditLogEntryCreator auditLogEntryCreator;

    /**
     * Adds a new entity to the database
     *
     * @param entity
     *            the entity to be added
     */
    @CRUDOperation(operation = EntityTypeOperation.CREATE)
    @Audit
    @Authorized
    public void add(@AuditEntity T entity) {
        Preconditions.checkNotNull(entity);
        entityUtility.setModified(entity);
        em.persist(entity);
    }

    /**
     * Updates an entity in the database
     *
     * @param entity
     *            the entity to be updated
     */
    @CRUDOperation(operation = EntityTypeOperation.UPDATE)
    @Audit
    @Authorized
    public void save(@AuditEntity T entity) {
        Preconditions.checkNotNull(entity);
        entityUtility.setModified(entity);
        em.merge(entity);
    }

    /**
     * Updates an entity in the database. This method is intended for use with updates not issued by logged-in users and
     * does not check for authorization permissions.
     *
     * @param entity the entity to be updated
     * @param username The username to be associated with the operation
     */
    @CRUDOperation(operation = EntityTypeOperation.UPDATE)
    @Audit
    public void saveInternal(@AuditEntity T entity, @AuditUserName String username) {
        Preconditions.checkNotNull(entity);
        if (StringUtils.isEmpty(username)) {
            throw new CCDBRuntimeException(ATTEMPT_TO_SAVE_SLOT_NO_USER_INFO);
        }

        entityUtility.setModified(entity, username);
        em.merge(entity);
    }

    /**
     * Deletes entity from the database
     *
     * @param entity
     *            the entity to be deleted
     */
    @CRUDOperation(operation = EntityTypeOperation.DELETE)
    @Audit
    @Authorized
    public void delete(@AuditEntity T entity) {
        Preconditions.checkNotNull(entity);
        if (entity instanceof EntityWithArtifacts
                && ((EntityWithArtifacts) entity).getEntityArtifactList() != null) {
            for(Artifact artifact : ((EntityWithArtifacts) entity).getEntityArtifactList()) {
                checkArtifactBinaryDataRelation(artifact);
            }
        }
        em.remove(em.merge(entity));
    }

    /**
     * Adds a child entity to a parent and the database
     *
     * @param <S>
     *            the child entity type
     * @param child
     *            the child entity to be added
     * @param log if <code>true</code> then explicit log entry will be created for this operation
     *
     *
     */
    @CRUDOperation(operation = EntityTypeOperation.UPDATE)
    @Authorized
    public <S> void addChild(S child, boolean log) {
        Preconditions.checkNotNull(child);

        // Fresh state of parent is necessary for correct checking if property or artifact will be duplicated.
        final T freshParent = findById(getParent(child).getId());

        uniquePropertyCheck(child, freshParent);
        uniquePropertyValueCheck(child, freshParent);

        entityUtility.setModified(freshParent, child);

        if (child instanceof PropertyValue) {
            getPropertyValueParentChildren(freshParent).add((PropertyValue) child);
        } else if (child instanceof Artifact) {
            // fresh state of children (artifacts) necessary or artifact will be duplicated
            ((EntityWithArtifacts) freshParent).getEntityArtifactList().size();

            getArtifactParentChildren(freshParent).add((Artifact) child);
        } else {
            throw new IllegalStateException(GET_PARENT_CALLED_ON_ENTITY_THAT_HAS_NEITHER_PROPERTIES_NOR_ARTIFACTS);
        }

        em.merge(freshParent);
        if (log) {
            explicitAuditLog(freshParent, EntityTypeOperation.UPDATE);
        }
    }

    /**
     * Updates a child entity of a parent and in the database
     *
     * @param <S>
     *            the child entity type
     * @param child
     *            the child entity to be updated
     */
    @CRUDOperation(operation = EntityTypeOperation.UPDATE)
    @Audit
    @Authorized
    public <S> void saveChild(@AuditEntity S child) {
        saveChild(child, false);
    }

    /**
     * Updates a child entity of a parent and in the database
     *
     * @param <S>
     *            the child entity type
     * @param child
     *            the child entity to be updated
     */
    @CRUDOperation(operation = EntityTypeOperation.UPDATE)
    @Authorized
    public <S> void saveChild(@AuditEntity S child, boolean log) {
        Preconditions.checkNotNull(child);

        T parent = getParent(child);

        uniquePropertyValueCheck(child, parent);

        final S mergedChild = em.merge(child);
        entityUtility.setModified(getParent(child), mergedChild);
        if (log) {
            final T freshParent = findById(getParent(child).getId());
            explicitAuditLog(freshParent, EntityTypeOperation.UPDATE);
        }
    }

    @CRUDOperation(operation = EntityTypeOperation.UPDATE)
    @Authorized
    public void addExternalLink(EntityWithExternalLinks entity, ExternalLink externalLink) {
        Preconditions.checkNotNull(externalLink);

        externalLink.setId(UUID.randomUUID().toString());

        entityUtility.setModified(externalLink);
        entityUtility.setModified(entity);

        entity.getExternalLinkList().add(externalLink);

        modifyExternalLink(entity);
    }

    private void modifyExternalLink(EntityWithExternalLinks entity) {
        Preconditions.checkNotNull(entity);
        em.merge(entity);
        explicitAuditLog(entity, EntityTypeOperation.UPDATE);
    }

    @CRUDOperation(operation = EntityTypeOperation.UPDATE)
    @Authorized
    public void updateExternalLink(EntityWithExternalLinks entity, ExternalLink externalLink) {
        Preconditions.checkNotNull(externalLink);

        entityUtility.setModified(externalLink);
        entityUtility.setModified(entity);

        ExternalLink externalLinkToRemove = null;
        for(ExternalLink existingExternalLink : entity.getExternalLinkList()){
            if(existingExternalLink.getId().equals(externalLink.getId())){
                externalLinkToRemove = existingExternalLink;
                break;
            }
        }

        if(externalLinkToRemove != null){
            entity.getExternalLinkList().remove(externalLinkToRemove);
        }

        entity.getExternalLinkList().add(externalLink);
        modifyExternalLink(entity);
    }

    /**
     * Checks that child entity of a parent follows unique value rules for property.
     *
     * @param <S>
     *            the child entity type
     * @param child
     *            the child entity to be updated
     *
     * @return true if the property value follows unique rules, else false
     */
    public <S> boolean checkPropertyValueUnique(S child) {
        Preconditions.checkNotNull(child);
        try {
            uniquePropertyValueCheck(child, getParent(child));
        } catch (PropertyValueNotUniqueException e) {
            return false;
        }
        return true;
    }

    protected <S> void uniquePropertyValueCheck(final S child, final T parent) {
        if (child instanceof PropertyValue) {
            final PropertyValue propVal = (PropertyValue) child;
            switch (propVal.getProperty().getValueUniqueness()) {
            case NONE:
                break;
            case TYPE:
                if (!isPropertyValueTypeUnique(propVal, parent)) {
                    throw new PropertyValueNotUniqueException(propVal.getProperty());
                }
                break;
            case UNIVERSAL:
                if (!isPropertyValueUniversallyUnique(propVal)) {
                    throw new PropertyValueNotUniqueException();
                }
                break;
            default:
                throw new UnhandledCaseException();
            }
        }
    }

    protected <S> void uniquePropertyCheck(S child, T parent) {
        if (child instanceof PropertyValue && parent instanceof EntityWithProperties) {
            final String propName = ((PropertyValue) child).getProperty().getName();
            final List<PropertyValue> parentProperties = ((EntityWithProperties)parent).getEntityPropertyList();

            if (parentProperties.stream().anyMatch(property -> property.getProperty().getName().equals(propName))) {
                throw new PropertyNotUniqueException("Property already exists.");
            }
        }
    }

    /**
     * Deletes a child entity from parent collection and removes from the database
     *
     * @param <S>
     *            the child entity type
     * @param child
     *            the child entity to be removed
     */
    @CRUDOperation(operation = EntityTypeOperation.UPDATE)
    @Audit
    @Authorized
    public <S> void deleteChild(@AuditEntity S child) {
        deleteChild(child, false);
    }

    /**
     * Deletes a child entity from parent collection and removes from the database
     *
     * @param <S>
     *            the child entity type
     * @param child
     *            the child entity to be removed
     * @param log if <code>true</code> then explicit log entry will be created for this operation
     */
    @CRUDOperation(operation = EntityTypeOperation.UPDATE)
    @Authorized
    public <S> void deleteChild(@AuditEntity S child, boolean log) {
        Preconditions.checkNotNull(child);

        final T parent = getParent(child);

        if (child instanceof PropertyValue) {
            getPropertyValueParentChildren(parent).remove((PropertyValue) child);
        } else if (child instanceof Artifact) {
            checkArtifactBinaryDataRelation((Artifact) child);
            getArtifactParentChildren(parent).remove((Artifact) child);
        } else {
            throw new IllegalStateException(GET_PARENT_CALLED_ON_ENTITY_THAT_HAS_NEITHER_PROPERTIES_NOR_ARTIFACTS);
        }

        final S mergedChild = em.merge(child);
        em.remove(mergedChild);
        if (log) {
            explicitAuditLog(parent, EntityTypeOperation.UPDATE);
        }
    }

    @CRUDOperation(operation = EntityTypeOperation.UPDATE)
    @Authorized
    public void deleteExternalLink(EntityWithExternalLinks entity, ExternalLink child) {
        Preconditions.checkNotNull(child);

        if(entity.getExternalLinkList().contains(child)){
            entityUtility.setModified(entity);
            entity.getExternalLinkList().remove(child);
            modifyExternalLink(entity);
        }
    }

    protected void flushAndClear() {
        em.flush();
        em.clear();
    }

    @SuppressWarnings("unchecked")
    private <S> T getParent(S child) {
        Preconditions.checkNotNull(child);
        if (child instanceof PropertyValue) {
            return (T) ((PropertyValue) child).getPropertiesParent();
        } else if (child instanceof Artifact) {
            return (T) ((Artifact) child).getArtifactsParent();
        } else {
            throw new IllegalStateException(GET_PARENT_CALLED_ON_ENTITY_THAT_HAS_NEITHER_PROPERTIES_NOR_ARTIFACTS);
        }
    }

     @SuppressWarnings("unchecked")
     private <S> T getParent(Class<T> parentClass, S child) {
        Preconditions.checkNotNull(child);
        if (child instanceof PropertyValue) {
            return (T) ((PropertyValue) child).getPropertiesParent();
        } else if (child instanceof Artifact) {
            return (T) ((Artifact) child).getArtifactsParent();
        } else {
            throw new IllegalStateException(GET_PARENT_CALLED_ON_ENTITY_THAT_HAS_NEITHER_PROPERTIES_NOR_ARTIFACTS);
        }
    }

    protected List<PropertyValue> getPropertyValueParentChildren(T parent) {
        Preconditions.checkNotNull(parent);

        return ((EntityWithProperties) parent).getEntityPropertyList();
    }

    protected List<Artifact> getArtifactParentChildren(T parent) {
        Preconditions.checkNotNull(parent);

        return ((EntityWithArtifacts) parent).getEntityArtifactList();
    }

    private void checkArtifactBinaryDataRelation(Artifact artifact) {
        if (artifact.getBinaryData().getArtifacts() != null
                && artifact.getBinaryData().getArtifacts().size() > 1) {
            artifact.setBinaryData(null);
        }
    }

        /**
         * <p>
         * Default implementation of the of the property value uniqueness check.
         * This implementation only throws an exception, since default functionality is only
         * intended to provide implementation to entities without a property value child.
         * </p>
         * <p>
         * Every EJB that supports property value children must override this method.
         * </p>
         *
         * @param child
         *            the child entity @link {@link PropertyValue}
         * @param parent
         *            the parent entity
         * @return <code>true</code> if the property values is unique or <code>null</code>,
         *         <code>false</code> otherwise.
         *         <code>null</code> value can only be achieved through adding a property definition.
         */
    protected boolean isPropertyValueTypeUnique(PropertyValue child, T parent) {
        throw new UnhandledCaseException();
    }

    private boolean isPropertyValueUniversallyUnique(PropertyValue child) {
        Preconditions.checkNotNull(child);
        final Property property = Preconditions.checkNotNull(child.getProperty());
        final Value value = child.getPropValue();
        if (value == null) {
            return true;
        }

        List<PropertyValue> results = em
                .createNamedQuery("ComptypePropertyValue.findSamePropertyValue", PropertyValue.class)
                .setParameter(PROPERTY_PARAM, property).setParameter(PROPERTY_VALUE_PARAM, value).setMaxResults(2)
                .getResultList();
        // value is unique if there is no property value with the same value, or the only one found us the entity itself
        boolean valueUnique = (results.size() < 2) && (results.isEmpty() || results.get(0).equals(child));
        if (valueUnique) {
            results = em.createNamedQuery("SlotPropertyValue.findSamePropertyValue", PropertyValue.class)
                    .setParameter(PROPERTY_PARAM, property).setParameter(PROPERTY_VALUE_PARAM, value).setMaxResults(2)
                    .getResultList();
            // value is unique if there is no same property value, or the only one found us the entity itself
            valueUnique = (results.size() < 2) && (results.isEmpty() || results.get(0).equals(child));
        }
        if (valueUnique) {
            results = em.createNamedQuery("DevicePropertyValue.findSamePropertyValue", PropertyValue.class)
                    .setParameter(PROPERTY_PARAM, property).setParameter(PROPERTY_VALUE_PARAM, value).setMaxResults(2)
                    .getResultList();
            // value is unique if there is no same property value, or the only one found us the entity itself
            valueUnique = (results.size() < 2) && (results.isEmpty() || results.get(0).equals(child));
        }
        return valueUnique;
    }

    /**
     * Create an explicit audit log entry for the database entity.
     *
     * @param entity
     *            the entity to create the audit log for
     * @param operation
     *            the type of database operation
     */
    public void explicitAuditLog(final Object entity, final EntityTypeOperation operation) {
        final List<AuditRecord> auditRecords = auditLogEntryCreator
                .auditRecords(ParentEntityResolver.resolveParentEntity(entity), operation);

        long now = new Date().getTime();
        for (AuditRecord auditRecord : auditRecords) {
            auditRecord.setUser(entityUtility.getUserId());
            auditRecord.setLogTime(new Date(now++));

            em.persist(auditRecord);
        }
    }
}
