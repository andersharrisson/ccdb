/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.auditlog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openepics.discs.conf.ent.Artifact;
import org.openepics.discs.conf.ent.EntityWithArtifacts;
import org.openepics.discs.conf.ent.EntityWithExternalLinks;
import org.openepics.discs.conf.ent.ExternalLink;
import org.openepics.discs.conf.ent.Tag;
import org.openepics.discs.conf.util.AppProperties;
import org.openepics.discs.conf.util.HelperProperties;

/**
 * Class with static methods that are used in many entity loggers
 *
 * @author <a href="mailto:andraz.pozar@cosylab.com">Andraž Požar</a>
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
public class EntityLoggerUtil {
    /**
     * Represents a property bag which contains all properties to log for an entity.
     */
    static class EntityPropertyMap  {
        private final Map<String, Object> propertyMap = new HashMap<>();

        public Map<String, Object> getPropertyMap() {
            return propertyMap;
        }

        EntityPropertyMap put(String key, Object value) {
            propertyMap.put(key, value);
            return this;
        }
    }

    private static final Logger LOGGER = Logger.getLogger(EntityLoggerUtil.class.getCanonicalName());
    private static final String NAME_PROPERTY = "name";
    private static final String URL_PROPERTY = "url";
    private static final String DESCRIPTION_PROPERTY = "description";

    /** If the number of rows is smaller than this value, all the rows are logged into the audit log (100) */
    public static final int AUDIT_LOG_ROWS;
    /** If the number of columns is smaller than this value, all the rows are logged into the audit log (50) */
    public static final int AUDIT_LOG_COLUMNS;

    static {
        // user can redefine defaults using the system properties
        final int rowsValue = HelperProperties.getProperty(AppProperties.AUDIT_LOG_ROWS_PROPERTY_NAME, 100);
        final int colsValue = HelperProperties.getProperty(AppProperties.AUDIT_LOG_COLUMNS_PROPERTY_NAME, 50);
        AUDIT_LOG_ROWS = Math.max(rowsValue, 10);
        AUDIT_LOG_COLUMNS = Math.max(colsValue, 10);
        LOGGER.log(Level.INFO, "Table/Vector audit logging row and column values: ROWS=" + AUDIT_LOG_ROWS
                                                                            + ", COLUMNS=" + AUDIT_LOG_COLUMNS);
    }

    private EntityLoggerUtil() {
        // utility class. No public constructor.
    }

    /**
     * Creates and returns a {@link List} of tag names from given {@link Set} of tags
     *
     * @param tagsSet   {@link Set} of tags for certain entity
     * @return          {@link List} of tag names for certain entity
     */
    public static List<String> getTagNamesFromTagsSet(Set<Tag> tagsSet) {
        final List<String> tags = new ArrayList<>();
        for (final Tag tag : tagsSet) {
            tags.add(tag.getName());
        }
        return tags;
    }

    /**
     * @param entityWithArtifacts the entity containing artifacts.
     * @return {@link List} of artifact object's property bags
     */
    public static List<EntityPropertyMap> getArtifactMap(final EntityWithArtifacts entityWithArtifacts) {
        final List<EntityPropertyMap> artifactsMap = new ArrayList<>();
        if (entityWithArtifacts.getEntityArtifactList() != null) {
            for (Artifact artifact : entityWithArtifacts.getEntityArtifactList()) {
                artifactsMap.add(new EntityPropertyMap()
                        .put(NAME_PROPERTY, artifact.getName())
                        .put(DESCRIPTION_PROPERTY, artifact.getDescription()));
            }
        }
        return artifactsMap;
    }

    /**
     * @param entityWithExternalLinks the entity containing external links.
     * @return {@link List} of external link object's property bags
     */
    public static List<EntityPropertyMap> getExternalLinkMap(final EntityWithExternalLinks entityWithExternalLinks) {
        final List<EntityPropertyMap> externalLinkMap = new ArrayList<>();
        if (entityWithExternalLinks.getExternalLinkList() != null) {
            for (ExternalLink externalLink : entityWithExternalLinks.getExternalLinkList()) {
                externalLinkMap.add(new EntityPropertyMap()
                        .put(NAME_PROPERTY, externalLink.getName())
                        .put(URL_PROPERTY, externalLink.getUri())
                        .put(DESCRIPTION_PROPERTY, externalLink.getDescription()));
            }
        }
        return externalLinkMap;
    }
}
