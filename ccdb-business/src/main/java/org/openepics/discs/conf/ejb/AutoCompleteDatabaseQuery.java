/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.ejb;

import org.openepics.discs.conf.util.AutoCompleteQuery;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public interface AutoCompleteDatabaseQuery<R> extends AutoCompleteQuery<R> {
    String getQueryNameOfFindStarting();
    String getQueryNameOfFindContaining();
    Class<R> getAutoCompleteEntityClass();
    EntityManager getEntityManager();

    @Override
    default List<R> findStartingWithQuery(final Map<String, Object> params, Integer limit) {
        return findByQueryContaining(getQueryNameOfFindStarting(), params, limit);
    }

    @Override
    default List<R> findContainingButNotStartingWithQuery(Map<String, Object> params, Integer limit) {
        return findByQueryContaining(getQueryNameOfFindContaining(), params, limit);
    }

    default List<R> findByQueryContaining(String queryName, Map<String, Object> params, Integer limit) {
        TypedQuery<R> typedQuery = getEntityManager().createNamedQuery(queryName, getAutoCompleteEntityClass());
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            typedQuery.setParameter(entry.getKey(), entry.getValue());
        }
        if (limit != null) {
            typedQuery.setMaxResults(limit);
        }
        return typedQuery.getResultList();
    }
}
