/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.dl.common;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

/**
 * Interface for all data loaders
 *
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 * @author <a href="mailto:andraz.pozar@cosylab.com">Andraž Požar</a>
 *
 */
public interface DataLoader {

    // Command string constants
    String CMD_CREATE = "CREATE";
    String CMD_UPDATE = "UPDATE";
    String CMD_DELETE = "DELETE";
    String CMD_END = "END";

    String CMD_CREATE_DEVICE = "CREATE DEVICE";
    String CMD_ADD_PROPERTY = "ADD PROPERTY";
    String CMD_CREATE_DEVICE_TYPE = "CREATE DEVICE TYPE";
    String CMD_CREATE_ENTITY = "CREATE ENTITY";
    String CMD_CREATE_RELATIONSHIP_IF_NOT_EXISTS = "CREATE RELATIONSHIP IF NOT EXISTS";
    String CMD_INSTALL = "INSTALL DEVICE";

    String CMD_UPDATE_DEVICE = "UPDATE DEVICE";
    String CMD_UPDATE_PROPERTY = "UPDATE PROPERTY";
    String CMD_UPDATE_DEVICE_TYPE = "UPDATE DEVICE TYPE";
    String CMD_UPDATE_ENTITY = "UPDATE ENTITY";
    String CMD_DELETE_DEVICE = "DELETE DEVICE";
    String CMD_DELETE_PROPERTY = "DELETE PROPERTY";
    String CMD_DELETE_DEVICE_TYPE = "DELETE DEVICE TYPE";
    String CMD_DELETE_ENTITY = "DELETE ENTITY";
    String CMD_DELETE_ENTITY_AND_CHILDREN = "DELETE ENTITY AND CHILDREN";
    String CMD_DELETE_RELATION = "DELETE RELATIONSHIP";
    String CMD_UNINSTALL = "UNINSTALL DEVICE";

    String PROP_TYPE_DEV_TYPE = "DEVICE TYPE";

    // Should be removed after Device and Device property will be taken out completely
    String PROP_TYPE_DEV_INSTANCE = "DEVICE INSTANCE";

    String PROP_TYPE_SLOT = "SLOT";
    String PROP_EXTERNAL_LINK = "EXTERNAL LINK";

    String ENTITY_TYPE_SLOT = "SLOT";
    String ENTITY_TYPE_CONTAINER = "CONTAINER";

    String PATH_SEPARATOR_PATTERN = ">>";

    String CMD_CREATE_EXTERNAL_LINK = "CREATE EXTERNAL LINK";
    String CMD_UPDATE_EXTERNAL_LINK = "UPDATE EXTERNAL LINK";
    String CMD_DELETE_EXTERNAL_LINK = "DELETE EXTERNAL LINK";


    /**
     * Saves data read from input file to the database
     *
     * @param inputRows {@link List} of all rows containing data from input file
     * @param contextualData optional map of objects passed with string keys
     * @param inputStream input file to be send via mail notification
     *
     * @return {@link DataLoaderResult} describing the outcome of the data loading
     */
    DataLoaderResult loadDataToDatabase(List<Pair<Integer, List<String>>> inputRows,
            Map<String, Object> contextualData, ByteArrayInputStream inputStream);

    /**
     * The index is defined by the import data template and is returned by the method because different data types
     * could have a different template and the data could start at a different index.
     * @return The '0 based' index of row at which the loader should expect data.
     */
    int getImportDataStartIndex();
}
