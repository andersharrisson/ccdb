/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.util.csentry;

import com.google.common.collect.ImmutableMap;
import org.openepics.cable.client.impl.ResponseException;
import org.openepics.discs.conf.util.AppProperties;
import org.openepics.discs.conf.util.AutoCompleteQuery;
import org.openepics.discs.conf.util.AutoCompleteWsQuery;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Service that periodically refreshes the CSEntry cache.
 *
 * @author <a href="mailto:marcel.salmic@cosylab.com">Marcel Salmic</a>
 */
@Named("csEntryService")
@Stateless
public class CSEntryService {
    private static final Logger LOGGER = Logger.getLogger(CSEntryService.class.getName());
    private static final int MAX_RESULTS = 20;

    @Inject
    private AppProperties properties;

    public int getMaxResults() {
        return MAX_RESULTS;
    }

    private static class HostNameSearchAction implements AutoCompleteWsQuery {
        private final String url;
        private final String token;
        private List<String> filteredHostNames = Collections.emptyList();

        public HostNameSearchAction(final AppProperties properties) {
            this.url = properties.getProperty(AppProperties.CSENTRY_BASE_URL);
            this.token = properties.getProperty(AppProperties.CSENTRY_TOKEN);
        }

        @Override
        public List<String> getData() {
            return filteredHostNames;
        }

        @Override
        public List<String> autoCompleteResult(Map<String, Object> params, Integer limit) {
            String lowerQuery = params.get(QUERY_PARAM).toString().trim().toLowerCase();
            CSEntryClient client = new CSEntryClient(url, token);
            try {
                filteredHostNames = client.searchHost(lowerQuery, limit);
            } catch (ResponseException e) {
                LOGGER.log(Level.WARNING, "Failed to call CSEntry API (" + e.getMessage() + ")");
            }

            return AutoCompleteWsQuery.super.autoCompleteResult(
                    ImmutableMap.of(AutoCompleteQuery.QUERY_PARAM, lowerQuery), limit);
        }
    }

    /**
     * @param query used to filter host names.
     * @return list of host names that match the specified filter.
     */
    public List<String> filteredHostNames(final String query) {
        return new HostNameSearchAction(properties).autoCompleteResult(
                ImmutableMap.of(AutoCompleteQuery.QUERY_PARAM, query), getMaxResults() + 1);
    }

    /**
     * Checks if host name belongs to an entry in CSEntry service.
     *
     * @param hostname host name as string to check
     * @return <code>false<code/> if hostname belongs to an entry otherwise <code>false<code/>
     */
    public boolean isHostnameInvalid(final String hostname) {
        CSEntryClient client = createClient();
        return client.isHostnameInvalid(hostname);
    }

    /**
     * Checks if host name belongs to an entry in CSEntry service. If not, gives automatic suggestion if possible.
     *
     * @param hostname host name as string to check
     * @return {@link org.openepics.discs.conf.util.csentry.CSEntryClient.HostCheckResult} result
     */
    public CSEntryClient.HostCheckResult checkHostname(final String hostname) {
        CSEntryClient client = createClient();
        return client.checkHostname(hostname);
    }

    private CSEntryClient createClient() {
        final String url = properties.getProperty(AppProperties.CSENTRY_BASE_URL);
        final String token = properties.getProperty(AppProperties.CSENTRY_TOKEN);
        return new CSEntryClient(url, token);
    }
}
