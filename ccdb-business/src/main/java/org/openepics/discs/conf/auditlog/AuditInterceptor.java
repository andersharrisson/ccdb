/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.auditlog;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.openepics.discs.conf.ent.AuditRecord;
import org.openepics.discs.conf.mail.UserDirectoryService;
import org.openepics.discs.conf.security.SecurityPolicy;
import org.openepics.discs.conf.util.CRUDOperation;
import org.openepics.discs.conf.util.ParentEntityResolver;


/**
 * An interceptor that creates an audit log
 *
 * @author <a href="mailto:andraz.pozar@cosylab.com">Andraž Požar</a>
 *
 */
@Audit
@Interceptor
public class AuditInterceptor {
    private static final Logger LOGGER = Logger.getLogger(AuditInterceptor.class.getCanonicalName());
    @PersistenceContext private EntityManager em;
    @Inject private AuditLogEntryCreator auditLogEntryCreator;
    @Inject private Provider<SecurityPolicy> securityPolicy;
    @Inject private UserDirectoryService userDirectoryService;

    /**
     * Creates audit log after the method annotated with this interceptor has finished executing.
     *
     * @param context the context
     * @return the return value from invoking {@link InvocationContext#proceed()}
     * @throws Exception many possible exceptions
     */
    @AroundInvoke
    public Object createAuditLog(InvocationContext context) throws Exception {

        final Object returnContext = context.proceed();
        Method method = context.getMethod();
        if (method.getAnnotation(CRUDOperation.class) != null) {
            final Annotation[][] parameterAnnotations = method.getParameterAnnotations();
            Integer usernameIdx = null;
            Integer entityIdx = null;
            for (int i = 0; i < parameterAnnotations.length; i++) {
                for (Annotation annotation : parameterAnnotations[i]) {
                    if (annotation instanceof AuditUserName) {
                        usernameIdx = i;
                    }
                    if (annotation instanceof AuditEntity) {
                        entityIdx = i;
                    }
                }
            }
            final Object[] parameters = context.getParameters();
            final Object entity = entityIdx != null ? parameters[entityIdx] : null;
            final String username = usernameIdx != null
                    ? parameters[usernameIdx].toString()
                    : securityPolicy.get().getUserId();
            if (!userDirectoryService.getAllUsernames().contains(username)) {
                LOGGER.warning("WARNING user that modified data is not present in the user directory service");
            }
            if (entity != null && StringUtils.isNotEmpty(username)) {
                long now = new Date().getTime();
                final List<AuditRecord> auditRecords = auditLogEntryCreator.auditRecords(
                        ParentEntityResolver.resolveParentEntity(entity),
                        context.getMethod().getAnnotation(CRUDOperation.class).operation());

                for (AuditRecord auditRecord : auditRecords) {
                    auditRecord.setUser(username);
                    auditRecord.setLogTime(new Date(now++));

                    em.persist(auditRecord);
                }
            } else {
                LOGGER.warning("WARNING Failed to create audit entry for "
                        + context.getTarget().getClass().getCanonicalName() + " " + method.getName()
                        + ", params: entity: " + entity + " username: " + username);
            }
        }
        return returnContext;
    }
}
