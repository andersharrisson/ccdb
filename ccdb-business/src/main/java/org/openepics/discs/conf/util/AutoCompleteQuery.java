/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.util;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public interface AutoCompleteQuery<R> {
    String QUERY_PARAM = "query";

    List<R> findStartingWithQuery(final Map<String, Object> params, final Integer limit);
    List<R> findContainingButNotStartingWithQuery(final Map<String, Object> params, final Integer limit);

    default List<R> autoCompleteResult(final Map<String, Object> params, final Integer limit) {
        final String query = params.containsKey(QUERY_PARAM) ? params.get(QUERY_PARAM).toString().trim() : null;
        final List<R> namesStarting = ListUtils.emptyIfNull(findStartingWithQuery(params, limit));
        List<R> namesContaining = Collections.emptyList();
        if (StringUtils.isNotEmpty(query)) {
            if (limit != null) {
                if (namesStarting.size() < limit) {
                    namesContaining = ListUtils.emptyIfNull(
                            findContainingButNotStartingWithQuery(params, limit - namesStarting.size()));
                }
            } else {
                namesContaining = ListUtils.emptyIfNull(findContainingButNotStartingWithQuery(params, null));
            }
        }

        return ListUtils.union(namesStarting, namesContaining);
    }
}
