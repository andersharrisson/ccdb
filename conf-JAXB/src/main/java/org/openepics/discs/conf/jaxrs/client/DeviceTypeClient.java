/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.jaxrs.client;

import java.io.InputStream;
import java.util.List;

import org.openepics.discs.conf.jaxb.DeviceTypeXml;

/**
 * This interface provides methods for clients to access {@link DeviceTypeXml} specific data.
 *
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
public interface DeviceTypeClient {
    /** @return returns all device types in the database. */
    public List<DeviceTypeXml> getAllDeviceTypes();

    /**
     * Returns a specific device type.
     *
     * @param name
     *            the name of the device type to retrieve
     * @return the device type instance data
     */
    public DeviceTypeXml getDeviceType(String name);

    /**
     * Returns a specific device type artifact file.
     *
     * @param name
     *            the name of the device type from which to retrieve artifact
     *            file.
     * @param fileName
     *            the name of the artifact file to retrieve.
     * @return the device type artifact file
     */
    public InputStream getAttachment(String name, String fileName);
}

