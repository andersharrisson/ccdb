/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.jaxb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

import org.openepics.discs.conf.ent.Slot;

/**
 * This is data transfer object representing a CCDB slot for JSON and XML serialization. This object only carries
 * basic information, like slot name.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
@XmlRootElement(name = "name")
@XmlAccessorType(XmlAccessType.FIELD)
public class InstallationSlotName {
	@XmlElement
    private String name;

	@XmlElement(name = "type")
    private SlotType slotType;

    @XmlElementWrapper(name = "parents")
    @XmlElement(name = "name")
    private List<String> parents = new ArrayList<>();

    @XmlElementWrapper(name = "children")
    @XmlElement(name = "name")
    private List<String> children = new ArrayList<>();

    @XmlElementWrapper(name = "powers")
    @XmlElement(name = "name")
    private List<String> powers = new ArrayList<>();

    @XmlElementWrapper(name = "poweredBy")
    @XmlElement(name = "name")
    private List<String> poweredBy = new ArrayList<>();

    @XmlElementWrapper(name = "controls")
    @XmlElement(name = "name")
    private List<String> controls = new ArrayList<>();

    @XmlElementWrapper(name = "controlledBy")
    @XmlElement(name = "name")
    private List<String> controlledBy = new ArrayList<>();
    
    public InstallationSlotName() {}

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public SlotType getSlotType() { return slotType; }
    public void setSlotType(SlotType slotType) { this.slotType = slotType; }

    public List<String> getParents() { return parents; }
    public void setParents(List<String> parents) { this.parents = parents; }

    public List<String> getChildren() { return children; }
    public void setChildren(List<String> children) { this.children = children; }

    public List<String> getPowers() { return powers; }
    public void setPowers(List<String> powers) { this.powers = powers; }

    public List<String> getPoweredBy() { return poweredBy; }
    public void setPoweredBy(List<String> poweredBy) { this.poweredBy = poweredBy; }

    public List<String> getControls() { return controls; }
    public void setControls(List<String> controls) { this.controls = controls; }

    public List<String> getControlledBy() { return controlledBy; }
    public void setControlledBy(List<String> controlledBy) { this.controlledBy = controlledBy; }
    
    @Override
    public String toString() {
        final StringBuilder str = new StringBuilder();
        str.append("InstallationSlotName[");
        str.append("name=").append(name);
        str.append(", slotType=").append(Objects.toString(slotType));
        str.append(", parents=");
        if (parents != null) {
          str.append(Arrays.toString(parents.toArray()));
        } else {
            str.append("null");
        }
        str.append(", children=");
        if (children != null) {
          str.append(Arrays.toString(children.toArray()));
        } else {
            str.append("null");
        }
        str.append(", powers=");
        if (powers != null) {
          str.append(Arrays.toString(powers.toArray()));
        } else {
            str.append("null");
        }
        str.append(", poweredBy=");
        if (poweredBy != null) {
          str.append(Arrays.toString(poweredBy.toArray()));
        } else {
            str.append("null");
        }
        str.append(", controls=");
        if (controls != null) {
          str.append(Arrays.toString(controls.toArray()));
        } else {
            str.append("null");
        }
        str.append(", controlledBy=");
        if (controlledBy != null) {
          str.append(Arrays.toString(controlledBy.toArray()));
        } else {
            str.append("null");
        }
        str.append("]");
        return str.toString();
    }

}


