/*
 * Copyright (c) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.jaxb;

import org.apache.commons.lang3.StringUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is data transfer object representing a referenceb data to the installation slots and slot names
 * for JSON and XML serialization.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@XmlRootElement(name = "name")
@XmlAccessorType(XmlAccessType.FIELD)
public class SlotReferenceXml {
    private Long id;
    private String nameId;
    private String name;
    private String label;

    public SlotReferenceXml() { }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getNameId() { return nameId; }
    public void setNameId(String nameId) { this.nameId = nameId; }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    /**
     * @return label value of slot relationship for referenced slot
     */
    public String getLabel() { return label; }

    /**
     * Sets label value of slot relationship for referenced slot
     * @param label label value of slot relationship for referenced slot
     */
    public void setLabel(String label) { this.label = label; }

    @Override
    public String toString() {
        final StringBuilder str = new StringBuilder();
        str.append("SlotReferenceXml[");
        str.append("id=").append(id);
        str.append(", nameId=").append(nameId);
        str.append(", name=").append(name);
        if (StringUtils.isNotEmpty(label)) {
            str.append(", label=").append(label);
        }
        str.append("]");
        return str.toString();
    }
}
