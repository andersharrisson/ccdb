/*
 *  Copyright (C) 2018 European Spallation Source ERIC.
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.webservice;

import com.google.common.collect.Sets;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.junit.Test;

import static org.junit.Assert.*;
import org.junit.Before;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.ComptypeArtifact;
import org.openepics.discs.conf.ent.Device;
import org.openepics.discs.conf.ent.DeviceArtifact;
import org.openepics.discs.conf.ent.ExternalLink;
import org.openepics.discs.conf.ent.InstallationRecord;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotArtifact;
import org.openepics.discs.conf.jaxb.ArtifactXml;

/**
 *
 * @author georgweiss
 * Created Sep 21, 2018
 */
public class DeviceResourceImplTest {
    
    private final DeviceResourceImpl deviceResourceImpl =
            new DeviceResourceImpl();
    
    private DeviceArtifact ga1;
    private DeviceArtifact ga2;
    private DeviceArtifact ga3;
    
    private ExternalLink el1;
    private ExternalLink el2;
    private ExternalLink el3;
    
    @Before
    public void setUp(){
        ga1 = new DeviceArtifact();
        ga1.setName("ga1");
        ga2 = new DeviceArtifact();
        ga2.setName("ga2");
        ga3 = new DeviceArtifact();
        ga3.setName("ga3");
        
        el1 = new ExternalLink();
        el1.setName("el1");
        el2 = new ExternalLink();
        el2.setName("el2");
        el3 = new ExternalLink();
        el3.setName("el3");
    }
    
    
    @Test
    public void testGetArtifactsNullRecordOneExternalLink(){
        
        Device device = new Device("device1");
        ComponentType componentType = new ComponentType();
        componentType.setExternalLinkList(Sets.newHashSet(el1));
        
        device.setComponentType(componentType);
        
        List<ArtifactXml> artifacts = deviceResourceImpl.getArtifacts(device, null);
        
        assertEquals(1, artifacts.size());
    }
    
    @Test
    public void testGetArtifactsNullRecordTwoExternalLinks(){
        
        Device device = new Device("device1");
        ComponentType componentType = new ComponentType();
        componentType.setExternalLinkList(Sets.newHashSet(el1, el2));
        
        device.setComponentType(componentType);      
        
        List<ArtifactXml> artifacts = deviceResourceImpl.getArtifacts(device, null);
        
        assertEquals(2, artifacts.size());
    }
    
    @Test
    public void testGetArtifactsNullRecordExternalLinksAndArtifacts(){
        
        Device device = new Device("device1");
        ComponentType componentType = new ComponentType();
        componentType.setExternalLinkList(Sets.newHashSet(el1, el2));
        ComptypeArtifact ca1 = new ComptypeArtifact();
        ca1.setName("ca1");
        ComptypeArtifact ca2 = new ComptypeArtifact();
        ca1.setName("ca2");
        componentType.setEntityArtifactList(Arrays.asList(ca1, ca2));
        
        device.setComponentType(componentType);      
        
        List<ArtifactXml> artifacts = deviceResourceImpl.getArtifacts(device, null);
        
        assertEquals(4, artifacts.size());
    }
    
    @Test
    public void testGetArtifactsNullRecordNoArtifacts(){
        
        Device device = new Device("device1");
        ComponentType componentType = new ComponentType();
       
        device.setComponentType(componentType);      
        
        List<ArtifactXml> artifacts = deviceResourceImpl.getArtifacts(device, null);
        
        assertEquals(0, artifacts.size());
    }
    
    @Test
    public void testGetArtifactsElementsInAllNodes(){
        
        Device device = new Device("device1");
        ComponentType componentType = new ComponentType();
        componentType.setExternalLinkList(Sets.newHashSet(el1));
        ComptypeArtifact ca1 = new ComptypeArtifact();
        ca1.setName("ca1");
        componentType.setEntityArtifactList(Arrays.asList(ca1));
        
        InstallationRecord installationRecord = 
                new InstallationRecord("installationRecord", new Date());
        
        Slot slot = new Slot("slot", true);
        
        slot.setExternalLinkList(Sets.newHashSet(el2));
        SlotArtifact sa1 = new SlotArtifact();
        sa1.setName("sa1");
        slot.setEntityArtifactList(Arrays.asList(sa1));
        
        installationRecord.setSlot(slot);
        
        device.setComponentType(componentType);      
        
        List<ArtifactXml> artifacts = deviceResourceImpl.getArtifacts(device, installationRecord);
        
        assertEquals(4, artifacts.size());
    }
    
    @Test
    public void testGetArtifactsElementsInAllNodes2(){
        
        Device device = new Device("device1");
        ComponentType componentType = new ComponentType();
        componentType.setExternalLinkList(Sets.newHashSet(el1));
        ComptypeArtifact ca1 = new ComptypeArtifact();
        ca1.setName("ca1");
        componentType.setEntityArtifactList(Arrays.asList(ca1));
        
        device.setEntityArtifactList(Arrays.asList(ga3));
        device.setExternalLinkList(Sets.newHashSet(el3));
        
        InstallationRecord installationRecord = 
                new InstallationRecord("installationRecord", new Date());
        
        Slot slot = new Slot("slot", true);
        
        slot.setExternalLinkList(Sets.newHashSet(el2));
        SlotArtifact sa1 = new SlotArtifact();
        sa1.setName("sa1");
        slot.setEntityArtifactList(Arrays.asList(sa1));
        
        installationRecord.setSlot(slot);
        
        device.setComponentType(componentType);      
        
        List<ArtifactXml> artifacts = deviceResourceImpl.getArtifacts(device, installationRecord);
        
        assertEquals(6, artifacts.size());
    }
}
