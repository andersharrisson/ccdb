/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.webservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import joptsimple.internal.Strings;

import static org.openepics.discs.conf.webservice.GetAttachmentResourceBase.AttachmentOwnerIdentifierType;
import static org.openepics.discs.conf.webservice.GetAttachmentResourceBase.AttachmentOwnerIdentifier;
import org.openepics.discs.conf.ejb.ComptypeEJB;
import org.openepics.discs.conf.ejb.InstallationEJB;
import org.openepics.discs.conf.ejb.SlotEJB;
import org.openepics.discs.conf.ejb.SlotPairEJB;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.InstallationRecord;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotPair;
import org.openepics.discs.conf.ent.SlotRelationName;
import org.openepics.discs.conf.helpers.ResponseHelper;
import org.openepics.discs.conf.jaxb.ArtifactXml;
import org.openepics.discs.conf.jaxb.SlotXml;
import org.openepics.discs.conf.jaxb.PropertyValueXml;
import org.openepics.discs.conf.jaxb.SlotTypeXml;
import org.openepics.discs.conf.jaxb.lists.SlotListXml;
import org.openepics.discs.conf.jaxrs.SlotResource;
import org.openepics.discs.conf.util.Utility;

/**
 * An implementation of the SlotResource interface.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
public class SlotResourceImpl implements SlotResource, Serializable {
    private static final long serialVersionUID = 3125813190423087085L;

    private static final Logger LOG = Logger.getLogger(SlotResourceImpl.class.getCanonicalName());

    @Inject private SlotEJB slotEJB;
    @Inject private SlotPairEJB slotPairEJB;
    @Inject private ComptypeEJB compTypeEJB;
    @Inject private InstallationEJB installationEJB;
       private SlotHieararchyCache slotHieararchyCache;

    @Override
    public Response getInstallationSlots(String deviceType) {
        try
        {
            slotHieararchyCache = new SlotHieararchyCache(slotPairEJB);

            // Get all slots
            SlotListXml installationSlots;
            if ("undefined".equals(deviceType)) {
                installationSlots = new SlotListXml(slotEJB.findAll().stream().
                            filter(slot -> slot!=null
                            && !slot.getComponentType().getName().equals(SlotEJB.ROOT_COMPONENT_TYPE))
                            .map(slot -> createSlot(slot))
                            .collect(Collectors.toList()));
            } else {
                // Get them filtered by deviceType
                installationSlots = new SlotListXml(getInstallationSlotsForType(deviceType));
            }
            return Response.status(Response.Status.OK).entity(installationSlots).build();
        } finally {
            slotHieararchyCache = null;
        }
    }

    @Override
    public Response getInstallationSlotById(Long id) {
        Slot slot = slotEJB.findById(id);
        SlotXml installationSlot = createSlot(slot);
        return ResponseHelper.createResponse(installationSlot);
    }

    @Override
    public Response getInstallationSlotByNameId(String nameId) {
        Slot slot = slotEJB.findByNameUuid(nameId);
        SlotXml installationSlot = createSlot(slot);
        return ResponseHelper.createResponse(installationSlot);
    }

    @Override
    public Response getInstallationSlotsByName(String name) {
        List<Slot> slots = slotEJB.findAllByName(name);
        SlotListXml installationSlots = new SlotListXml(slots.stream()
                .map(slot -> createSlot(slot))
                .collect(Collectors.toList()));
        return ResponseHelper.createResponse(installationSlots);
    }

    @Override
    public Response getAttachmentById(Long id, String fileName) {
        Slot slot = slotEJB.findById(id);
        AttachmentOwnerIdentifier ownerIdentifier = new AttachmentOwnerIdentifier(AttachmentOwnerIdentifierType.ID, id);
        return GetAttachmentResourceBase.getFileForSlot(slot, installationEJB.getActiveInstallationRecordForSlot(slot),
                ownerIdentifier, fileName);
    }

    @Override
    public Response getAttachmentByNameId(String nameId, String fileName) {
        Slot slot = slotEJB.findByNameUuid(nameId);
        AttachmentOwnerIdentifier ownerIdentifier =
                new AttachmentOwnerIdentifier(AttachmentOwnerIdentifierType.NAME_ID, nameId);
        return GetAttachmentResourceBase.getFileForSlot(slot, installationEJB.getActiveInstallationRecordForSlot(slot),
                ownerIdentifier, fileName);
    }

    @Override
    public Response getAttachmentByName(String name, String fileName) {
        final Slot slot = getFirstSlotByName(name);
        AttachmentOwnerIdentifier ownerIdentifier =
                new AttachmentOwnerIdentifier(AttachmentOwnerIdentifierType.NAME, name);
        return GetAttachmentResourceBase.getFileForSlot(slot, installationEJB.getActiveInstallationRecordForSlot(slot),
                ownerIdentifier, fileName);
    }

    private List<SlotXml> getInstallationSlotsForType(String deviceType) {
        if (Strings.isNullOrEmpty(deviceType)) {
            return new ArrayList<>();
        }

        final ComponentType ct = compTypeEJB.findByName(deviceType);
        if (ct == null) {
            return new ArrayList<>();
        }

        return slotEJB.findByComponentType(ct).stream().
                map(slot -> createSlot(slot)).
                collect(Collectors.toList());
    }

    private Slot getFirstSlotByName(String name) {
        Slot slot = slotEJB.findByNameAndIsHostingSlot(name, true);
        // if hosting slot not found, alternatively return a container
        if (slot == null) {
            slot = slotEJB.findByName(name);
        }
        return slot;
    }

    private SlotXml createSlot(final Slot slot) {
        if (slot == null) {
            return null;
        }

        final SlotXml installationSlot = new SlotXml();
        installationSlot.setId(slot.getId());
        installationSlot.setNameId(slot.getNameUuid());
        installationSlot.setName(slot.getName());
        installationSlot.setSlotType(slot.isHostingSlot() ? SlotTypeXml.SLOT : SlotTypeXml.CONTAINER);
        installationSlot.setDescription(slot.getDescription());
        installationSlot.setDeviceType(slot.getComponentType().getName());
        installationSlot.setArtifacts(
                Utils.emptyToNull(getArtifacts(slot, installationEJB.getLastInstallationRecordForSlot(slot))));

        if (slotHieararchyCache != null) {
            final Long id = slot.getId();
            installationSlot.setParents(Utils.getSlotNames(slotHieararchyCache.getParents(id)));
            installationSlot.setChildren(Utils.getSlotNames(slotHieararchyCache.getChildren(id)));
            installationSlot.setPoweredBy(Utils.getSlotNames(slotHieararchyCache.getPoweredBy(id)));
            installationSlot.setPowers(Utils.getSlotNames(slotHieararchyCache.getPowers(id)));
            installationSlot.setControlledBy(Utils.getSlotNames(slotHieararchyCache.getControlledBy(id)));
            installationSlot.setControls(Utils.getSlotNames(slotHieararchyCache.getControls(id)));
        } else {
            installationSlot.setParents(Utils.getRelatedSlots(slot.getPairsInWhichThisSlotIsAChildList().stream(),
                            SlotRelationName.CONTAINS, pair -> pair.getParentSlot()));
            installationSlot.setChildren(Utils.getRelatedSlots(slot.getPairsInWhichThisSlotIsAParentList().stream(),
                            SlotRelationName.CONTAINS, pair -> pair.getChildSlot()));

            installationSlot.setPoweredBy(Utils.getRelatedSlots(slot.getPairsInWhichThisSlotIsAChildList().stream(),
                            SlotRelationName.POWERS, pair -> pair.getParentSlot()));
            installationSlot.setPowers(Utils.getRelatedSlots(slot.getPairsInWhichThisSlotIsAParentList().stream(),
                            SlotRelationName.POWERS, pair -> pair.getChildSlot()));

            installationSlot.setControlledBy(Utils.getRelatedSlots(slot.getPairsInWhichThisSlotIsAChildList().stream(),
                            SlotRelationName.CONTROLS, pair -> pair.getParentSlot()));
            installationSlot.setControls(Utils.getRelatedSlots(slot.getPairsInWhichThisSlotIsAParentList().stream(),
                            SlotRelationName.CONTROLS, pair -> pair.getChildSlot()));
        }

        installationSlot.setProperties(getPropertyValues(slot));
        return installationSlot;
    }

    private List<PropertyValueXml> getPropertyValues(final Slot slot) {
        final InstallationRecord record = installationEJB.getActiveInstallationRecordForSlot(slot);

        final Stream<? extends PropertyValueXml> externalProps = Stream.concat(
                            slot.getComponentType().getComptypePropertyList().stream().
                                filter(propValue -> !propValue.isPropertyDefinition()).
                                map(propValue -> Utils.createPropertyValue(propValue)),
                            record == null ? Stream.empty() :
                                record.getDevice().getDevicePropertyList().stream().
                                    map(propValue -> Utils.createPropertyValue(propValue)));

        return Stream.concat(slot.getSlotPropertyList().stream().map(propValue -> Utils.createPropertyValue(propValue)),
                                externalProps).
                        collect(Collectors.toList());
    }

    protected List<ArtifactXml> getArtifacts(final Slot slot, InstallationRecord record) {

        List<ArtifactXml> allArtifacts = new ArrayList<>();

        allArtifacts.addAll(Utils.getArtifacts(slot.getComponentType()));
        allArtifacts.addAll(Utils.getExternalLinks(slot.getComponentType()));

        if (record != null) {
            allArtifacts.addAll(Utils.getArtifacts(record.getSlot()));
            allArtifacts.addAll(Utils.getExternalLinks(record.getSlot()));
        }

        allArtifacts.addAll(Utils.getArtifacts(slot));
        allArtifacts.addAll(Utils.getExternalLinks(slot));

        return allArtifacts;
    }

    @Override
    public Response getControlsChildrenById(Long id, boolean transitive, List<String> properties) {
        Slot slot = slotEJB.findById(id);
        return getControlsChildrenForSlot(slot, transitive, properties);
    }

    @Override
    public Response getControlsChildrenByNameId(String nameId, boolean transitive, List<String> properties) {
        Slot slot = slotEJB.findByNameUuid(nameId);
        return getControlsChildrenForSlot(slot, transitive, properties);
    }

    @Override
    public Response getControlsChildrenByName(String name, boolean transitive, List<String> properties) {
        final Slot slot = getFirstSlotByName(name);
        return getControlsChildrenForSlot(slot, transitive, properties);
    }

    private Response getControlsChildrenForSlot(Slot slot, boolean transitive, List<String> properties) {
        try
        {
            slotHieararchyCache = new SlotHieararchyCache(slotPairEJB);

            if (slot == null) {
                return Response.status(Status.NOT_FOUND).build();
            }

            final List<SlotXml> slots = new ArrayList<>();
            try {
                buildControlsSlotList(slots, slot, transitive, properties, new HashSet<Long>());
            } catch (Exception e) {
                LOG.log(Level.WARNING, "Error buidling slot list.", e);
                return Response.status(Status.INTERNAL_SERVER_ERROR).build();
            }

            return Response.status(Status.OK).entity(new SlotListXml(slots)).build();

        } finally {
            slotHieararchyCache = null;
        }
    }

    private void buildControlsSlotList(final List<SlotXml> slots, final Slot parent, final boolean transitive,
                                                                final List<String> properties, Set<Long> addedSlots) {
        for (final SlotPair pair : parent.getPairsInWhichThisSlotIsAParentList()) {
            if ((pair.getSlotRelation().getName() == SlotRelationName.CONTROLS)) {
                final Slot slot = pair.getChildSlot();
                if (isSlotCandidate(slot, properties)) {
                    final SlotXml iSlot = createSlot(slot);

                    if (!addedSlots.contains(slot.getId())) {
                        slots.add(iSlot);
                        addedSlots.add(slot.getId());
                    }
                }
                if (transitive) {
                    buildControlsSlotList(slots, slot, transitive, properties, addedSlots);
                }
            }
        }
    }

    private boolean isSlotCandidate(final Slot slot, final List<String> properties) {
        if (Utility.isNullOrEmpty(properties)) {
            return true;
        }
        final List<String> propCopy = new ArrayList<>(properties);
        // check slot properties
        checkProperties(slot.getSlotPropertyList(), propCopy);
        if (!propCopy.isEmpty()) {
            // check device type properties
            checkProperties(slot.getComponentType().getComptypePropertyList(), propCopy);
        }
        if (!propCopy.isEmpty()) {
            // check installed device properties
            final InstallationRecord rec = installationEJB.getActiveInstallationRecordForSlot(slot);
            if (rec != null) {
                checkProperties(rec.getDevice().getDevicePropertyList(), propCopy);
            }
        }
        return propCopy.isEmpty();
    }

    private <T extends org.openepics.discs.conf.ent.PropertyValue> void checkProperties(final List<T> propVals,
            final List<String> properties) {
        for (T val : propVals) {
            final String propName = val.getProperty().getName();
            if (properties.contains(propName))
                properties.remove(propName);
            if (properties.isEmpty())
                return;
        }
    }
}
