/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.webservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import static org.openepics.discs.conf.webservice.GetAttachmentResourceBase.AttachmentOwnerIdentifierType;
import static org.openepics.discs.conf.webservice.GetAttachmentResourceBase.AttachmentOwnerIdentifier;
import org.openepics.discs.conf.ejb.DeviceEJB;
import org.openepics.discs.conf.ejb.InstallationEJB;
import org.openepics.discs.conf.ent.Device;
import org.openepics.discs.conf.ent.InstallationRecord;
import org.openepics.discs.conf.helpers.ResponseHelper;
import org.openepics.discs.conf.jaxb.ArtifactXml;
import org.openepics.discs.conf.jaxb.DeviceXml;
import org.openepics.discs.conf.jaxb.PropertyValueXml;
import org.openepics.discs.conf.jaxb.lists.DeviceListXml;
import org.openepics.discs.conf.jaxrs.DeviceResource;

/**
 * An implementation of the DeviceResource interface.
 *
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
public class DeviceResourceImpl implements DeviceResource, Serializable {
    private static final long serialVersionUID = 3108793668900422356L;

    @Inject private DeviceEJB deviceEJB;
    @Inject private InstallationEJB installationEJB;

    @Override
    public Response getAllDevices() {
        DeviceListXml devices = new DeviceListXml(
                deviceEJB.findAll().stream()
                        .map(device -> getDevice(device))
                        .collect(Collectors.toList()));
        return Response.status(Response.Status.OK).entity(devices).build();
    }

    @Override
    public Response getDeviceById(Long id) {
        DeviceXml device = getDevice(deviceEJB.findById(id));
        return ResponseHelper.createResponse(device);
    }

    @Override
    public Response getDeviceByName(String name) {
        DeviceXml device = getDevice(deviceEJB.findByName(name));
        return ResponseHelper.createResponse(device);
    }

    @Override public Response getAttachmentById(Long id, String fileName) {
        final Device device = deviceEJB.findById(id);
        AttachmentOwnerIdentifier ownerIdentifier = new AttachmentOwnerIdentifier(AttachmentOwnerIdentifierType.ID, id);
        return GetAttachmentResourceBase.getFileForDevice(device,
                installationEJB.getActiveInstallationRecordForDevice(device), ownerIdentifier, fileName);
    }

    @Override
    public Response getAttachmentByName(String name, String fileName) {
        final Device device = deviceEJB.findByName(name);
        AttachmentOwnerIdentifier ownerIdentifier =
                new AttachmentOwnerIdentifier(AttachmentOwnerIdentifierType.NAME, name);
        return GetAttachmentResourceBase.getFileForDevice(device,
                installationEJB.getActiveInstallationRecordForDevice(device), ownerIdentifier, fileName);
    }

    /**
     * Transforms a CCDB database entity into a REST DTO object. Called from
     * other web service classes as well.
     *
     * @param device
     *            the CCDB database entity to wrap
     * @return REST DTO object
     */
    protected DeviceXml getDevice(org.openepics.discs.conf.ent.Device device) {
        if (device == null) {
            return null;
        } else {
            final DeviceXml deviceJAXB = new DeviceXml();
            deviceJAXB.setId(device.getId());
            deviceJAXB.setInventoryId(device.getSerialNumber());
            deviceJAXB.setDeviceType(device.getComponentType().getName());
            deviceJAXB.setProperties(Utils.emptyToNull(getPropertyValues(device)));
            deviceJAXB.setArtifacts(
                    Utils.emptyToNull(
                            getArtifacts(device, installationEJB.getLastInstallationRecordForDevice(device))));
            return deviceJAXB;
        }
    }

    private List<PropertyValueXml> getPropertyValues(final org.openepics.discs.conf.ent.Device device) {
        final InstallationRecord record = installationEJB .getLastInstallationRecordForDevice(device);

        final Stream<PropertyValueXml> externalProps = Stream.concat(
                device.getComponentType().getComptypePropertyList().stream().
                        filter(propValue -> !propValue.isPropertyDefinition()).
                        map(propValue -> Utils.createPropertyValue(propValue)),
                record == null ? Stream.empty() : record.getSlot().getSlotPropertyList().stream().
                                                                map(propValue -> Utils.createPropertyValue(propValue)));

        return Stream.concat(device.getDevicePropertyList().stream().
                                            map(propValue -> Utils.createPropertyValue(propValue)), externalProps).
                                            collect(Collectors.toList());
    }

    protected List<ArtifactXml> getArtifacts(final Device device, InstallationRecord record) {

        List<ArtifactXml> allArtifacts = new ArrayList<>();

        allArtifacts.addAll(Utils.getArtifacts(device.getComponentType()));
        allArtifacts.addAll(Utils.getExternalLinks(device.getComponentType()));

        if (record != null) {
            allArtifacts.addAll(Utils.getArtifacts(record.getSlot()));
            allArtifacts.addAll(Utils.getExternalLinks(record.getSlot()));
        }

        allArtifacts.addAll(Utils.getArtifacts(device));
        allArtifacts.addAll(Utils.getExternalLinks(device));

        return allArtifacts;
    }
}
