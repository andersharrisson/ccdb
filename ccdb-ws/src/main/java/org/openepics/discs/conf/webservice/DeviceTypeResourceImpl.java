/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.webservice;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import static org.openepics.discs.conf.webservice.GetAttachmentResourceBase.AttachmentOwnerIdentifierType;
import static org.openepics.discs.conf.webservice.GetAttachmentResourceBase.AttachmentOwnerIdentifier;
import org.openepics.discs.conf.ejb.ComptypeEJB;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.helpers.ResponseHelper;
import org.openepics.discs.conf.jaxb.ArtifactXml;
import org.openepics.discs.conf.jaxb.DeviceTypeXml;
import org.openepics.discs.conf.jaxb.lists.DeviceTypeListXml;
import org.openepics.discs.conf.jaxrs.DeviceTypeResource;

/**
 * An implementation of the DeviceTypeResource interface.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
public class DeviceTypeResourceImpl implements DeviceTypeResource, Serializable {
    private static final long serialVersionUID = 2029600617687884478L;

    @Inject private ComptypeEJB comptypeEJB;
  
    @Override
    public Response getAllDeviceTypes() {
        DeviceTypeListXml deviceTypes = new DeviceTypeListXml(
                comptypeEJB.findAll().stream()
                        .map(compType -> getDeviceType(compType))
                        .collect(Collectors.toList()));
        return Response.status(Response.Status.OK).entity(deviceTypes).build();
    }

    @Override
    public Response getDeviceTypeById(Long id) {
        DeviceTypeXml deviceType = getDeviceType(comptypeEJB.findById(id));
        return ResponseHelper.createResponse(deviceType);
    }

    @Override
    public Response getDeviceTypeByName(String name) {
        DeviceTypeXml deviceType = getDeviceType(comptypeEJB.findByName(name));
        return ResponseHelper.createResponse(deviceType);
    }

    @Override public Response getAttachmentById(Long id, String fileName) {
        AttachmentOwnerIdentifier ownerIdentifier = new AttachmentOwnerIdentifier(AttachmentOwnerIdentifierType.ID, id);
        return GetAttachmentResourceBase.getFileForDeviceType(comptypeEJB.findById(id), ownerIdentifier, fileName);
    }

    @Override
    public Response getAttachmentByName(String name, String fileName) {
        AttachmentOwnerIdentifier ownerIdentifier =
                new AttachmentOwnerIdentifier(AttachmentOwnerIdentifierType.NAME, name);
        return GetAttachmentResourceBase.getFileForDeviceType(comptypeEJB.findByName(name), ownerIdentifier, fileName);
    }

    /** Transforms a CCDB database entity into a REST DTO object. Called from other web service classes as well.
     * @param componentType the CCDB database entity to wrap
     * @return REST DTO object
     */
    private DeviceTypeXml getDeviceType(ComponentType componentType) {
        if (componentType == null) {
            return null;
        } else {
            final DeviceTypeXml deviceType = new DeviceTypeXml();
            deviceType.setId(componentType.getId());
            deviceType.setName(componentType.getName());
            deviceType.setDescription(componentType.getDescription());
            List<ArtifactXml> artifacts = Utils.getArtifacts(componentType);
            List<ArtifactXml> externalLinks = Utils.getExternalLinks(componentType);
            artifacts.addAll(externalLinks);
            deviceType.setArtifacts(artifacts);
            return deviceType;
        }
    }
}
