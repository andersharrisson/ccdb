/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.webservice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.BooleanUtils;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.ComptypePropertyValue;
import org.openepics.discs.conf.ent.DevicePropertyValue;
import org.openepics.discs.conf.ent.EntityWithArtifacts;
import org.openepics.discs.conf.ent.EntityWithExternalLinks;
import org.openepics.discs.conf.ent.Property;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotPair;
import org.openepics.discs.conf.ent.SlotPropertyValue;
import org.openepics.discs.conf.ent.SlotRelationName;
import org.openepics.discs.conf.ent.SlotRelationshipLabel;
import org.openepics.discs.conf.ent.values.EnumValue;
import org.openepics.discs.conf.ent.values.StrVectorValue;
import org.openepics.discs.conf.jaxb.ArtifactXml;
import org.openepics.discs.conf.jaxb.PropertyKindXml;
import org.openepics.discs.conf.jaxb.PropertyValueXml;
import org.openepics.discs.conf.jaxb.SlotReferenceXml;
import org.openepics.discs.conf.util.BuiltInDataType;
import org.openepics.discs.conf.util.Conversion;
import org.openepics.discs.conf.util.UnhandledCaseException;

public class Utils {

    @FunctionalInterface
    interface RelatedSlotExtractor {
        public Slot getRelatedSlot(final SlotPair pair);
    }

    public static List<ArtifactXml> getArtifacts(final EntityWithArtifacts entity) {
        final PropertyKindXml kind = getKind(entity);
        return entity.getEntityArtifactList().stream().
                map(ea -> {
                    ArtifactXml a = new ArtifactXml(ea); a.setKind(kind); return a; }).collect(Collectors.toList());
    }

    public static List<ArtifactXml> getExternalLinks(final EntityWithExternalLinks entity) {
        final PropertyKindXml kind = getKind(entity);
        return entity.getExternalLinkList().stream()
                .map(ea -> {ArtifactXml a = new ArtifactXml(ea); a.setKind(kind); return a; })
                .collect(Collectors.toList());
    }

    private static PropertyKindXml getKind(final EntityWithArtifacts entity) {
        if (entity instanceof ComponentType) {
            return PropertyKindXml.TYPE;
        }
        if (entity instanceof Slot) {
            final Slot slot = (Slot) entity;
            return slot.isHostingSlot() ? PropertyKindXml.SLOT : PropertyKindXml.CONTAINER;
        }

        return PropertyKindXml.DEVICE;
    }

    private static PropertyKindXml getKind(final EntityWithExternalLinks entity) {
        if (entity instanceof ComponentType) {
            return PropertyKindXml.TYPE;
        }
        if (entity instanceof Slot) {
            final Slot slot = (Slot) entity;
            return slot.isHostingSlot() ? PropertyKindXml.SLOT : PropertyKindXml.CONTAINER;
        }

        return PropertyKindXml.DEVICE;
    }

    public static <T> List<T> emptyToNull(List<T> list) {
        return list == null ? null :(list.isEmpty() ? null : list);
    }

    static PropertyValueXml createPropertyValue(
            final org.openepics.discs.conf.ent.PropertyValue entityPropertyValue) {
        final PropertyValueXml propertyValue = new PropertyValueXml();
        final Property parentProperty = entityPropertyValue.getProperty();
        propertyValue.setName(parentProperty.getName());
        propertyValue.setDataType(parentProperty.getDataType() != null ? parentProperty.getDataType().getName() : null);
        propertyValue.setUnit(parentProperty.getUnit() != null ? parentProperty.getUnit().getName() : null);
        String propValueString = propertyToString(entityPropertyValue);

        if (entityPropertyValue instanceof SlotPropertyValue) {
            SlotPropertyValue slotPropValue = (SlotPropertyValue)entityPropertyValue;
            SlotPropertyValue aliasedProperty = slotPropValue.getAliasOf();
            if (aliasedProperty != null) {
                Slot referenceToSlot = aliasedProperty.getReferenceToSlot();
                if (referenceToSlot != null) {
                    // If property is alias of a slot reference then use aliased referred slot name
                    propValueString =  referenceToSlot.getName();
                } else {
                    // If property value is alias of other property value
                    // then use the value of the aliased property value
                    propValueString = propertyToString(aliasedProperty);
                }
            } else if (BooleanUtils.toBoolean(slotPropValue.getProperty().getSlotReference())) {
                //Setting the referred SlotName (if the type is a slot ref.) in SlotPropertyValue response
                if (slotPropValue.getReferenceToSlot() == null) {
                    propValueString = "";
                } else {
                    propValueString = slotPropValue.getReferenceToSlot().getName();
                }
            }
        }
        propertyValue.setValue(propValueString);

        if (entityPropertyValue instanceof ComptypePropertyValue) {
            propertyValue.setKind(PropertyKindXml.TYPE);
        } else if (entityPropertyValue instanceof SlotPropertyValue) {
            propertyValue.setKind(PropertyKindXml.SLOT);
        } else if (entityPropertyValue instanceof DevicePropertyValue) {
            propertyValue.setKind(PropertyKindXml.DEVICE);
        } else {
            throw new UnhandledCaseException();
        }

        return propertyValue;
    }

    private static String propertyToString(final org.openepics.discs.conf.ent.PropertyValue entityPropertyValue) {
        final String typeName = entityPropertyValue.getProperty().getDataType().getName();
        switch (typeName) {
            case BuiltInDataType.BOOLEAN_NAME:
                final EnumValue pvEnum = (EnumValue) entityPropertyValue.getPropValue();
                if (pvEnum == null) {
                    return "null";
                }
                return Boolean.toString(pvEnum.toString().equals(Conversion.BOOLEAN_TEXT_TRUE));
            case BuiltInDataType.BOOLEAN_VECTOR_NAME:
                final StrVectorValue pv = (StrVectorValue) entityPropertyValue.getPropValue();
                if (pv == null) {
                    return "null";
                }
                final List<Boolean> bools = pv.getStrVectorValue().stream().
                    map(b -> Boolean.valueOf(b.equals(Conversion.BOOLEAN_TEXT_TRUE))).collect(Collectors.toList());
                return Objects.toString(Arrays.toString(bools.toArray(new Boolean[] {})));
            default:
                return Objects.toString(entityPropertyValue.getPropValue());
        }
    }

    static List<SlotReferenceXml> getRelatedSlots(final Stream<SlotPair> relatedSlotPairs,
            final SlotRelationName relationName,
            final RelatedSlotExtractor extractor) {
        List<SlotPair> filteredSlotPairs =  relatedSlotPairs.
                sorted(Comparator.comparingLong(SlotPair::getId)).
                filter(slotPair -> relationName.equals(slotPair.getSlotRelation().getName())).
                collect(Collectors.toList());
        List<SlotReferenceXml> results = new ArrayList<>();
        for (SlotPair slotPair : filteredSlotPairs) {
            Slot slot = extractor.getRelatedSlot(slotPair);
            results.add(extractSlotReference(slot, slotPair.getRelationshipLabel()));
        }
        return emptyToNull(results);
    }

    private static SlotReferenceXml extractSlotReference(Slot slot, SlotRelationshipLabel relationshipLabel) {
        SlotReferenceXml slotReference = new SlotReferenceXml();
        slotReference.setId(slot.getId());
        slotReference.setNameId(slot.getNameUuid());
        slotReference.setName(slot.getName());
        if (relationshipLabel != null) {
            slotReference.setLabel(relationshipLabel.getLabel());
        }
        return slotReference;
    }

    private static SlotReferenceXml extractSlotReference(Slot slot) {
        return extractSlotReference(slot, null);
    }

    static List<SlotReferenceXml> getSlotNames(final List<Slot> slots) {
        if (slots == null) {
            return null;
        }
        return Utils.emptyToNull(slots.stream()
                .map(Utils::extractSlotReference)
                .collect(Collectors.toList()));
    }
}
