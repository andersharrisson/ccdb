/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.webservice;


import java.util.List;
import java.util.Map;

import org.openepics.discs.conf.ejb.SlotPairEJB;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotRelationName;

/**
 * This loads complete slot hierarchy information in memory and allows to query it through its methods.  
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class SlotHieararchyCache {
    private SlotPairEJB slotPairEJB;

    private Map<Long, List<Slot>> parentMap;
    private Map<Long, List<Slot>> childrenMap;
    private Map<Long, List<Slot>> poweredByMap;
    private Map<Long, List<Slot>> powersMap;
    private Map<Long, List<Slot>> controlledByMap;
    private Map<Long, List<Slot>> controlsMap;

    /**
     * Constructs the object and loads the hierarchy.
	 * @param slotPairEJB slot pair EJB
	 */
	public SlotHieararchyCache(SlotPairEJB slotPairEJB) {
		super();
		this.slotPairEJB = slotPairEJB;
		refresh();
	}

	/** Reloads slot hierarchy information. */
    public void refresh() {
        parentMap = slotPairEJB.getRelationMap(SlotRelationName.CONTAINS, false);
        childrenMap = slotPairEJB.getRelationMap(SlotRelationName.CONTAINS, true);
        poweredByMap = slotPairEJB.getRelationMap(SlotRelationName.POWERS, false);
        powersMap = slotPairEJB.getRelationMap(SlotRelationName.POWERS, true);
        controlledByMap = slotPairEJB.getRelationMap(SlotRelationName.CONTROLS, false);
        controlsMap = slotPairEJB.getRelationMap(SlotRelationName.CONTROLS, true);
    }

	/**
	 * Returns slots that are children of the slot with the given id
	 * @param id the slot id
	 * @return list of slots
	 */
    public List<Slot> getChildren(Long id) {
		return childrenMap.get(id);
	}

	/**
	 * Returns slots that are parents of the slot with the given id
	 * @param id the slot id
	 * @return list of slots
	 */
	public List<Slot> getParents(Long id) {
		return parentMap.get(id);
	}

	/**
	 * Returns slots that power the slot with the given id
	 * @param id the slot id
	 * @return list of slots
	 */
	public List<Slot> getPoweredBy(Long id) {
		return poweredByMap.get(id);
	}

	/**
	 * Returns slots that are powered by slot with the given id
	 * @param id the slot id
	 * @return list of slots
	 */
	public List<Slot> getPowers(Long id) {
		return powersMap.get(id);
	}

	/**
	 * Returns slots that are control the slot with the given id
	 * @param id the slot id
	 * @return list of slots
	 */
	public List<Slot> getControlledBy(Long id) {
		return controlledByMap.get(id);
	}

	/**
	 * Returns slots that are controlled by the slot with the given id
	 * @param id the slot id
	 * @return list of slots
	 */
	public List<Slot> getControls(Long id) {
		return controlsMap.get(id);
	}
}
