/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.webservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import joptsimple.internal.Strings;

import org.openepics.discs.conf.ejb.ComptypeEJB;
import org.openepics.discs.conf.ejb.InstallationEJB;
import org.openepics.discs.conf.ejb.SlotEJB;
import org.openepics.discs.conf.ejb.SlotPairEJB;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.InstallationRecord;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotPair;
import org.openepics.discs.conf.ent.SlotRelationName;
import org.openepics.discs.conf.jaxb.Artifact;
import org.openepics.discs.conf.jaxb.InstallationSlot;
import org.openepics.discs.conf.jaxb.PropertyValue;
import org.openepics.discs.conf.jaxb.SlotType;
import org.openepics.discs.conf.jaxb.lists.InstallationSlotList;
import org.openepics.discs.conf.jaxrs.InstallationSlotResource;
import org.openepics.discs.conf.util.BlobStore;
import org.openepics.discs.conf.util.Utility;

/**
 * An implementation of the InstallationSlotResource interface.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
public class InstallationSlotResourceImpl implements InstallationSlotResource, Serializable {
    private static final long serialVersionUID = 3125813190423087085L;

    private static final Logger LOG = Logger.getLogger(InstallationSlotResourceImpl.class.getCanonicalName());

    @Inject private SlotEJB slotEJB;
    @Inject private SlotPairEJB slotPairEJB;
    @Inject private ComptypeEJB compTypeEJB;
    @Inject private InstallationEJB installationEJB;
    @Inject private BlobStore blobStore;

    private SlotHieararchyCache slotHieararchyCache;
    
    @Override
    public InstallationSlotList getInstallationSlots(String deviceType) {
    	try
    	{
	        slotHieararchyCache = new SlotHieararchyCache(slotPairEJB);

	        // Get all slots
	        if ("undefined".equals(deviceType)) {
	            return new InstallationSlotList(slotEJB.findAll().stream().
	                        filter(slot -> slot!=null 
	                        && !slot.getComponentType().getName().equals(SlotEJB.ROOT_COMPONENT_TYPE)).map(slot -> createInstallationSlot(slot)).
	                        collect(Collectors.toList()));
	        } else {
	            // Get them filtered by deviceType
	            return new InstallationSlotList(getInstallationSlotsForType(deviceType));
	        }
    	} finally {
	        slotHieararchyCache = null;
    	}
    }

    @Override
    public InstallationSlot getInstallationSlot(String name) {
        Slot slot = getSlotByName(name);
        if (slot == null) {
            return null;
        }
        return createInstallationSlot(slot);
    }

    private Slot getSlotByName(String name) {
        Slot slot = slotEJB.findByNameAndIsHostingSlot(name, true);
        // if hosting slot not found, alternatively return a container
        if (slot == null) {
            slot = slotEJB.findByName(name);
        }
        return slot;
    }

    @Override
    public Response getAttachment(String name, String fileName) {
        final Slot slot = getSlotByName(name);
        return GetAttachmentResourceBase.getFileForSlot(slot, installationEJB.getActiveInstallationRecordForSlot(slot),
                                                                                            name, fileName, blobStore);
    }

    private List<InstallationSlot> getInstallationSlotsForType(String deviceType) {
        if (Strings.isNullOrEmpty(deviceType)) {
            return new ArrayList<>();
        }

        final ComponentType ct = compTypeEJB.findByName(deviceType);
        if (ct == null) {
            return new ArrayList<>();
        }

        return slotEJB.findByComponentType(ct).stream().
                map(slot -> createInstallationSlot(slot)).
                collect(Collectors.toList());
    }

    private InstallationSlot createInstallationSlot(final Slot slot) {
        if (slot == null) {
            return null;
        }

        final InstallationSlot installationSlot = new InstallationSlot();
        installationSlot.setName(slot.getName());
        installationSlot.setSlotType(slot.isHostingSlot() ? SlotType.SLOT : SlotType.CONTAINER);
        installationSlot.setDescription(slot.getDescription());
        installationSlot.setDeviceType(slot.getComponentType().getName());
        installationSlot.setArtifacts(Utils.emptyToNull(getArtifacts(slot)));

        if (slotHieararchyCache != null) {
            final Long id = slot.getId();
            installationSlot.setParents(Utils.getSlotNames(slotHieararchyCache.getParents(id)));
            installationSlot.setChildren(Utils.getSlotNames(slotHieararchyCache.getChildren(id)));
            installationSlot.setPoweredBy(Utils.getSlotNames(slotHieararchyCache.getPoweredBy(id)));
            installationSlot.setPowers(Utils.getSlotNames(slotHieararchyCache.getPowers(id)));
            installationSlot.setControlledBy(Utils.getSlotNames(slotHieararchyCache.getControlledBy(id)));
            installationSlot.setControls(Utils.getSlotNames(slotHieararchyCache.getControls(id)));
        } else {
	        installationSlot.setParents(Utils.getRelatedSlots(slot.getPairsInWhichThisSlotIsAChildList().stream(),
	                        SlotRelationName.CONTAINS, pair -> pair.getParentSlot()));
	        installationSlot.setChildren(Utils.getRelatedSlots(slot.getPairsInWhichThisSlotIsAParentList().stream(),
	                        SlotRelationName.CONTAINS, pair -> pair.getChildSlot()));
	
	        installationSlot.setPoweredBy(Utils.getRelatedSlots(slot.getPairsInWhichThisSlotIsAChildList().stream(),
	                        SlotRelationName.POWERS, pair -> pair.getParentSlot()));
	        installationSlot.setPowers(Utils.getRelatedSlots(slot.getPairsInWhichThisSlotIsAParentList().stream(),
	                        SlotRelationName.POWERS, pair -> pair.getChildSlot()));
	
	        installationSlot.setControlledBy(Utils.getRelatedSlots(slot.getPairsInWhichThisSlotIsAChildList().stream(),
	                        SlotRelationName.CONTROLS, pair -> pair.getParentSlot()));
	        installationSlot.setControls(Utils.getRelatedSlots(slot.getPairsInWhichThisSlotIsAParentList().stream(),
	                        SlotRelationName.CONTROLS, pair -> pair.getChildSlot()));
        }
        
        installationSlot.setProperties(getPropertyValues(slot));
        return installationSlot;
    }

    private List<PropertyValue> getPropertyValues(final Slot slot) {
        final InstallationRecord record = installationEJB.getActiveInstallationRecordForSlot(slot);

        final Stream<? extends PropertyValue> externalProps = Stream.concat(
                            slot.getComponentType().getComptypePropertyList().stream().
                                filter(propValue -> !propValue.isPropertyDefinition()).
                                map(propValue -> Utils.createPropertyValue(propValue)),
                            record == null ? Stream.empty() :
                                record.getDevice().getDevicePropertyList().stream().
                                    map(propValue -> Utils.createPropertyValue(propValue)));

        return Stream.concat(slot.getSlotPropertyList().stream().map(propValue -> Utils.createPropertyValue(propValue)),
                                externalProps).
                        collect(Collectors.toList());
    }

    private List<Artifact> getArtifacts(final Slot slot) {
        final InstallationRecord record = installationEJB .getLastInstallationRecordForSlot(slot);

        final List<Artifact> externalArtifacts = Utils.getArtifacts(slot.getComponentType());
        if (record != null) {
            externalArtifacts.addAll(Utils.getArtifacts(record.getSlot()));
        }

        final List<Artifact> deviceArtifacts = Utils.getArtifacts(slot);
        deviceArtifacts.addAll(externalArtifacts);

        return deviceArtifacts;
    }

    @Override
    public Response getControlsChildren(String name, boolean transitive, List<String> properties) {
    	try
    	{
	        slotHieararchyCache = new SlotHieararchyCache(slotPairEJB);

	    	final Slot slot = getSlotByName(name);
	        if (slot == null) {
	            return Response.status(Status.NOT_FOUND).build();
	        }

	        final List<InstallationSlot> slots = new ArrayList<>();
	        try {
	            buildControlsSlotList(slots, slot, transitive, properties, new HashSet<Long>());
	        } catch (Exception e) {
	            LOG.log(Level.WARNING, "Error buidling slot list.", e);
	            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
	        }

	        return Response.status(Status.OK).entity(new InstallationSlotList(slots)).build();
	        
    	} finally {
	        slotHieararchyCache = null;
    	}
    }

    private void buildControlsSlotList(final List<InstallationSlot> slots, final Slot parent, final boolean transitive,
                                                                final List<String> properties, Set<Long> addedSlots) {
        for (final SlotPair pair : parent.getPairsInWhichThisSlotIsAParentList()) {
            if ((pair.getSlotRelation().getName() == SlotRelationName.CONTROLS)) {
                final Slot slot = pair.getChildSlot();
                if (isSlotCandidate(slot, properties)) {
                    final InstallationSlot iSlot = createInstallationSlot(slot);

                    if (!addedSlots.contains(slot.getId())) {
                        slots.add(iSlot);
                        addedSlots.add(slot.getId());
                    }
                }
                if (transitive) {
                    buildControlsSlotList(slots, slot, transitive, properties, addedSlots);
                }
            }
        }
    }

    private boolean isSlotCandidate(final Slot slot, final List<String> properties) {
        if (Utility.isNullOrEmpty(properties)) {
            return true;
        }
        final List<String> propCopy = new ArrayList<>(properties);
        // check slot properties
        checkProperties(slot.getSlotPropertyList(), propCopy);
        if (!propCopy.isEmpty()) {
            // check device type properties
            checkProperties(slot.getComponentType().getComptypePropertyList(), propCopy);
        }
        if (!propCopy.isEmpty()) {
            // check installed device properties
            final InstallationRecord rec = installationEJB.getActiveInstallationRecordForSlot(slot);
            if (rec != null) {
                checkProperties(rec.getDevice().getDevicePropertyList(), propCopy);
            }
        }
        return propCopy.isEmpty();
    }

    private <T extends org.openepics.discs.conf.ent.PropertyValue> void checkProperties(final List<T> propVals,
            final List<String> properties) {
        for (T val : propVals) {
            final String propName = val.getProperty().getName();
            if (properties.contains(propName)) properties.remove(propName);
            if (properties.isEmpty()) return;
        }
    }
}
