/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.webservice;

import io.swagger.jaxrs.config.BeanConfig;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * This represents the JAX-RS application which hosts all REST resources of the CCDB.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
@ApplicationPath("/")
public class CcdbRestService extends Application implements Serializable {
    private static final long serialVersionUID = 6813705813230928697L;

    public CcdbRestService() {
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.0");
        beanConfig.setSchemes(System.getProperty("ccdb.swagger.schemes", "https").split(","));
        beanConfig.setBasePath(System.getProperty("ccdb.swagger.basepath", "/rest"));
        beanConfig.setResourcePackage("org.openepics.discs.conf.jaxrs");
        beanConfig.setScan(true);

    }

    @Override
    public Set<Class<?>> getClasses() { // NOSONAR generic wildcard types part of the framework
        return getRestResourceClasses();
    }

    private Set<Class<?>> getRestResourceClasses() {  // NOSONAR generic wildcard types part of the framework
        return new java.util.HashSet<Class<?>>(
                Arrays.asList(
                        DeviceResourceImpl.class,
                        DeviceTypeResourceImpl.class,
                        HealthcheckResourceImpl.class,
                        SlotNameResourceImpl.class,
                        SlotResourceImpl.class,
                        io.swagger.jaxrs.listing.ApiListingResource.class,
                        io.swagger.jaxrs.listing.SwaggerSerializers.class,
                        SwaggerUIResource.class));
    }
}
