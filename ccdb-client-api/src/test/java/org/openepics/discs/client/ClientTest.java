package org.openepics.discs.client;

import java.util.Arrays;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.openepics.discs.conf.jaxb.Device;
import org.openepics.discs.conf.jaxb.DeviceType;
import org.openepics.discs.conf.jaxb.InstallationSlot;
import org.openepics.discs.conf.jaxrs.client.DeviceClient;
import org.openepics.discs.conf.jaxrs.client.DeviceTypeClient;
import org.openepics.discs.conf.jaxrs.client.InstallationSlotClient;

import com.google.common.collect.Lists;

@Ignore
public class ClientTest {

    private void dumpList(final String name, final List<?> list) {
        if (list != null) {
            System.out.print(name + ": ");
            System.out.println(Arrays.toString(list.toArray()));
        }
    }

    @Test
    public void testSlots() {
        final InstallationSlotClient client = CCDBClient.createInstallationSlotClient();

        final List<InstallationSlot> slots =  client.getInstallationSlots();
        dumpSlots(slots);
    }

    private void dumpSlots(final List<InstallationSlot> slots) {
        if (slots == null) {
            System.out.println("slots == null");
            return;
        }
        System.out.println("= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = ");
        System.out.println("slots: " + slots.size());
        for (final InstallationSlot slot : slots) {
            System.out.println();
            System.out.println("name: " + slot.getName());
            System.out.println("description: " + slot.getDescription());
            System.out.println("deviceType: " + slot.getDeviceType());
            dumpList("artifacts", slot.getArtifacts());
            dumpList("parents", slot.getParents());
            dumpList("children", slot.getChildren());
            dumpList("powers", slot.getPowers());
            dumpList("poweredBy", slot.getPoweredBy());
            dumpList("controls", slot.getControls());
            dumpList("controlledBy", slot.getControlledBy());
            dumpList("properties", slot.getProperties());
        }
    }

    @Test
    public void testTypes() {
        final DeviceTypeClient client =CCDBClient.createDeviceTypeClient();

        final List<DeviceType> types =  client.getAllDeviceTypes();
        dumpTypes(types);
    }

    private void dumpTypes(final List<DeviceType> types) {
        if (types == null) {
            System.out.println("types == null");
            return;
        }
        System.out.println("= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = ");
        System.out.println("types: " + types.size());
        dumpList("Types", types);
    }

    @Test
    public void testDevices() {
        final DeviceClient client = CCDBClient.createDeviceClient();

        final List<Device> devs =  client.getAllDevices();
        dumpDevices(devs);
    }

    @Test
    public void testControlsSlots() {
        final InstallationSlotClient client = CCDBClient.createInstallationSlotClient();

        final List<InstallationSlot> slots = client.getControlsChildren("LEBT-00:Ctrl-IOC-1X", true,
                                                                    Lists.newArrayList("EPICSSnippet","EPICSModule"));
        dumpSlots(slots);
    }

    private void dumpDevices(final List<Device> devices) {
        if (devices == null) {
            System.out.println("devices == null");
            return;
        }
        System.out.println("= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = ");
        System.out.println("devices: " + devices.size());
        dumpList("Devices", devices);
    }
}
