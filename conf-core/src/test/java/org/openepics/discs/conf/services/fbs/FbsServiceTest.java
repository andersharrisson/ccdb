/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.services.fbs;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.openepics.discs.conf.util.AppProperties;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link FbsService} class (using MockServer, details: {@link FbsServiceMock}).
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 *
 * @see FbsService
 **/
@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"javax.net.ssl.*"})
public class FbsServiceTest extends FbsServiceMock {
    private FbsService fbsService;

    @Mock
    private AppProperties appProperties;

    @Before
    public void setup() {
        super.setup();
        fbsService = new FbsService();
        Whitebox.setInternalState(fbsService, "properties", appProperties);
        when(appProperties.getProperty(AppProperties.FBS_APPLICATION_URL)).thenReturn(FbsServiceMock.TEST_FBS_URL);
    }

    @Test
    public void getAllFbsTags() {
        final List<FbsElement> fbsElements = fbsService.getAllFbsTags();
        Map<String, String> tagMappings = FbsUtil.readTagMappings(fbsElements);
        assertNotNull(fbsElements);
        assertFalse(fbsElements.isEmpty());
        assertNotNull(tagMappings);
        assertFalse(tagMappings.isEmpty());
    }

    @Test
    public void getFbsTagForEssName() {
        final String essName = "HBL-110RFC:RFS-ADR-11002";
        final String tag = fbsService.getFbsTagForEssName(essName);
        assertNotNull(tag);
        assertEquals(tag, "=ESS.ACC.A05.A12.E01.K01.K01.KF01.K02");
    }

    @Test
    public void getFbsTagForEssName_noMatch() {
        final String essName = "noMatch";
        final String tag = fbsService.getFbsTagForEssName(essName);
        assertNotNull(tag);
        assertTrue(tag.isEmpty());
    }

    @Test
    public void getFbsTagForEssName_ambiguous() {
        final String essName = "MBL-010RFC:RFS-SIM-410";
        final String tag = fbsService.getFbsTagForEssName(essName);
        assertNotNull(tag);
        assertTrue(tag.isEmpty());
    }

    @Test
    public void getFbsTagsForFbsTagQuery() {
        final String tagQuery = "=ESS.ACC.A05.A19.E03";
        final int max_results = 5;
        final List<String> tags = fbsService.getFbsTagsForFbsTagQuery(tagQuery, max_results);

        assertNotNull(tags);
        assertEquals(tags.size(), max_results);

        for (String value : tags) {
            assertNotNull(value);
            assertTrue(value.contains(tagQuery));
        }
    }

    @Test
    public void getFbsTagsForFbsTagQuery_noMatch() {
        final String tagQuery = "noMatch";
        final int max_results = 5;
        final List<String> tags = fbsService.getFbsTagsForFbsTagQuery(tagQuery, max_results);

        assertNotNull(tags);
        assertEquals(tags.size(), 0);
    }

    @Test
    public void getFbsTagsForFbsTagQuery_invalidMaxResults() {
        final String tagQuery = "=ESS.ACC.A05.A19.E03";
        final int max_results = -5;
        final List<String> tags = fbsService.getFbsTagsForFbsTagQuery(tagQuery, max_results);

        assertNotNull(tags);
        assertEquals(tags.size(), 0);
    }
}
