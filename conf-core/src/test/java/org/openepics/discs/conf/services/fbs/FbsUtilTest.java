/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.services.fbs;

import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for {@link FbsUtil} class.
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 *
 * @see FbsUtil
 **/

public class FbsUtilTest {

    @Test
    public void readTags(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("ess1", "tag1"),
                createElement("ess2", "tag2"));
        List<String> tags = FbsUtil.readTags(fbsElements);
        assertEquals(tags.size(), 2);
        assertEquals(tags.get(0), "tag1");
        assertEquals(tags.get(1), "tag2");
    }

    @Test
    public void readTags_nullAndEmpty(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("ess1", "tag1"),
                createElement("ess2", "tag2"),
                createElement("ess3", ""),
                createElement("ess4", null));
        List<String> tags = FbsUtil.readTags(fbsElements);
        assertEquals(tags.size(), 2);
        assertEquals(tags.get(0), "tag1");
        assertEquals(tags.get(1), "tag2");
    }

    @Test
    public void readTagMappingsCableNameToFbsTag(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("ess1", "tag1"),
                createElement("ess2", "tag2"));
        Map<String, String> map = FbsUtil.readTagMappings(fbsElements);
        assertEquals(map.size(), 2);
        assertEquals(map.get("ess1"), "tag1");
        assertEquals(map.get("ess2"), "tag2");
    }

    @Test
    public void readTagMappingsCableNameToFbsTag_ambiguous(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("ess1", "tag1"),
                createElement("ess1", "tag2"));
        Map<String, String> map = FbsUtil.readTagMappings(fbsElements);
        assertEquals(map.size(), 1);
        assertEquals(map.get("ess1"), "Ambiguous name: tag1, tag2");
    }

    private FbsElement createElement(final String essName, final String fbsTag) {
        FbsElement fbsElement = new FbsElement();
        fbsElement.essName = essName;
        fbsElement.tag = fbsTag;
        return fbsElement;
    }
}
