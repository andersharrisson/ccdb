/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.services.fbs;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.mockserver.client.MockServerClient;
import org.mockserver.configuration.ConfigurationProperties;
import org.mockserver.integration.ClientAndServer;

import java.io.IOException;

import org.mockserver.matchers.Times;
import org.mockserver.model.Header;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.model.Parameter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Base class which uses MockServer framework to mock CHESS/ITIP service response. Calls for webservice from any
 * subclasses will be redirected to mock server running on localhost and response will be mocked.
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public abstract class FbsServiceMock {

    private static ClientAndServer mockServer;
    protected static final String TEST_FBS_URL = "http://localhost:1080/fbs.json";

    @BeforeClass
    public static void startServer() {
        ConfigurationProperties.logLevel("ERROR");
        ConfigurationProperties.disableSystemOut(true);
        mockServer = ClientAndServer.startClientAndServer(1080);
    }

    @AfterClass
    public static void stopServer() {
        mockServer.stop();
    }

    @Before
    public void setup() {
        createExpectationSuccess(Collections.singletonList(new Parameter("ESSName", "noMatch")),
                "no_match.json");
        createExpectationSuccess(Collections.singletonList(new Parameter("ESSName", "MBL-010RFC:RFS-SIM-410")),
                "duplicated.json");
        createExpectationSuccess(Collections.singletonList(new Parameter("ESSName", "HBL-110RFC:RFS-ADR-11002")),
                "ess_name.json");
        createExpectationSuccess(Arrays.asList(new Parameter("tag", "%=ESS.ACC.A05.A19.E03%"),
                new Parameter("max_results", "5")),
                "tag_search.json");
        createExpectationSuccess(Arrays.asList(new Parameter("tag", "%noMatch%"),
                new Parameter("max_results", "5")),
                "no_match.json");
        createExpectationSuccess(Collections.emptyList(), "all.json");
    }

    @After
    public void clean() {
        new MockServerClient("localhost", 1080).reset();
    }

    protected void createExpectationFail(final List<Parameter> parameters) {
        new MockServerClient("localhost", 1080)
                .when(
                        HttpRequest.request()
                                .withMethod("GET")
                                .withPath("/fbs.json")
                                .withQueryStringParameters(parameters), Times.exactly(1))
                .respond(
                        HttpResponse.response()
                                .withStatusCode(404)
                                .withDelay(TimeUnit.MILLISECONDS,1)
                );
    }

    private void createExpectationSuccess(final List<Parameter> parameters, final String resource) {
        new MockServerClient("localhost", 1080)
                .when(
                        HttpRequest.request()
                                .withMethod("GET")
                                .withPath("/fbs.json")
                                .withQueryStringParameters(parameters), Times.exactly(1))
                .respond(
                        HttpResponse.response()
                                .withStatusCode(200)
                                .withHeaders(
                                        new Header("Content-Type", "application/json; charset=utf-8"))
                                .withBody(getResponseBody(resource))
                                .withDelay(TimeUnit.MILLISECONDS,1)
                );
    }

    private String getResponseBody(String fileName) {
        try {
            return IOUtils.toString(
                    this.getClass().getResourceAsStream("/chess.response/" + fileName),
                    "UTF-8"
            );
        } catch (IOException e) {
            return null;
        }
    }
}

