/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

// PrimeFaces Shift-select workaround. Event is triggered on the last node. This is not a proper solution!
// Workaround suggestion: https://forum.primefaces.org/viewtopic.php?t=42788
PrimeFaces.widget.TreeTable.prototype.selectNodesInRange = function(node) {
    if (this.cursorNode) {
        this.unselectAllNodes();

        const currentNodeIndex = node.index(),
            cursorNodeIndex = this.cursorNode.index(),
            startIndex = (currentNodeIndex > cursorNodeIndex) ? cursorNodeIndex : currentNodeIndex,
            endIndex = (currentNodeIndex > cursorNodeIndex) ? (currentNodeIndex + 1) : (cursorNodeIndex + 1),
            nodes = this.tbody.children();

        for(let i = startIndex ; i < endIndex; i++) {
            const lastRow = endIndex - 1 === i;
            // fire select ajax event for the last node
            this.selectNode(nodes.eq(i), !lastRow);
        }
    }
    else {
        this.selectNode(node);
    }
};