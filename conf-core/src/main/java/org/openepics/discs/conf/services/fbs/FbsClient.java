/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.services.fbs;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringUtils;
import org.openepics.cable.client.impl.ClosableResponse;
import org.openepics.cable.client.impl.ResponseException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Client to ITIP integration platform for retrieval data from FBS webservice and extraction of information
 * such as ESS name, FBS tag.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class FbsClient implements AutoCloseable {
    private static final Logger LOGGER = Logger.getLogger(FbsClient.class.getName());
    private static final String COULD_NOT_RETRIEVE_RESPONSE = "Couldn't retrieve data from service at %s.";

    private static final String PARAM_ESS_NAME = "ESSName";
    private static final String TAG = "tag";
    private static final String MAX_RESULTS = "max_results";

    @Nonnull
    private final Client client = ClientBuilder.newClient();

    private final String url;

    /**
     * Constructor.
     * @param url URL where the Fbs xml is retrieved from.
     */
    public FbsClient(String url) {
        this.url = url;
    }

    /**
     * Read FBS data and deserializes it into a FBS element list.
     * @return parsed data.
     */
    public List<FbsElement> getFbsListing() {
        return getFbsListing(Collections.emptyMap());
    }

    /**
     * Get FBS element list for exact match on ESS name.
     *
     * @param essName ESS Name
     * @return List of matching FBS tags
     */
    public List<FbsElement> getFbsListingForEssName(final String essName) {
        return getFbsListing(ImmutableMap.of(PARAM_ESS_NAME, essName));
    }

    /**
     * Read FBS data for FBS tag and deserializes it into a FbsElement list.
     *
     * @param tag query, FBS tag part
     * @param maxResults maximum number of results
     * @return {@link List} of matching {@link FbsElement}
     */
    public List<FbsElement> getFbsListingForTagQuery(final String tag, final int maxResults) {
        if (StringUtils.isEmpty(StringUtils.trim(tag)) || maxResults < 0) {
            return Collections.emptyList();
        }

        return getFbsListing(ImmutableMap.of(TAG, encodeUrl(StringUtils.trim(tag)),
                MAX_RESULTS, String.valueOf(maxResults)));
    }

    @Override
    public void close() {
        client.close();
    }

    //--------------------------------------------------------------------------------------------------

    private List<FbsElement> getFbsListing(final Map<String, String> params) {
        if (StringUtils.isEmpty(StringUtils.trim(url))) {
            return null;
        }
        UriBuilder uriBuilder = getUriBuilder(url, params);

        long time = System.nanoTime();

        try (final ClosableResponse response = new ClosableResponse(
                client.target(uriBuilder).request(new MediaType[]{MediaType.APPLICATION_JSON_TYPE}).get())) {
            if (response.getStatus() == 200) {
                List<FbsElement> fbsElements = response.readEntity(new GenericType<List<FbsElement>>() {
                });
                if (fbsElements != null) {
                    LOGGER.log(Level.INFO, "FBS service call, count " + fbsElements.size() + ", runtime: "
                            + (System.nanoTime() - time) / 1_000_000 + " ms (URL: " + (uriBuilder != null ?
                            uriBuilder.build().toString() : url) + ")");
                }

                return fbsElements;
            } else {
                throw new ResponseException(String.format(COULD_NOT_RETRIEVE_RESPONSE, url) + " "
                        + response.getStatus() + " " + response.getStatusInfo().getReasonPhrase());
            }
        } catch (Exception e) {
            LOGGER.log(Level.INFO, String.format(COULD_NOT_RETRIEVE_RESPONSE, url), e);
            throw new ResponseException(String.format(COULD_NOT_RETRIEVE_RESPONSE, url), e);
        }
    }

    private UriBuilder getUriBuilder(final String url,
                                           @Nullable final Map<String, String> queryParameters) {
        if (StringUtils.isEmpty(StringUtils.trim(url))) {
            return null;
        }

        UriBuilder ub = UriBuilder.fromUri(url.trim());
        if (queryParameters != null) {
            for (Map.Entry<String, String> entry : queryParameters.entrySet()) {
                ub.queryParam(entry.getKey(), entry.getValue());
            }
        }

        return ub;
    }

    private String encodeUrl(final String url) {
        try {
            return URLEncoder.encode(url, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return url;
        }
    }
}
