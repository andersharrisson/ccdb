/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.openepics.discs.conf.services.names;

import java.util.HashSet;
import java.util.Set;

import javax.ejb.Stateless;

import org.openepics.names.client.NamesClient;
import org.openepics.names.jaxb.DeviceNameElement;

/**
 * This is a service layer that handles accessing naming service through {@link NamesClient}.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class NamesService {

    /**
     * Returns a set of all devices currently registered in the naming service.
     *
     * @return the set
     */
    public Set<DeviceNameElement> getAllDevices() {
        final Set<DeviceNameElement> devices = new HashSet<DeviceNameElement>();
        try {
            final NamesClient client = new NamesClient();
            devices.addAll(client.getAllDeviceNames());
        } catch (RuntimeException e) {
            throw new RuntimeException("There was an error retriving data from the naming service.", e);
        }
        return devices;
    }
}
