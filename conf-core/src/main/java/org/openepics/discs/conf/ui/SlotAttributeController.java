/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.ui;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;
import org.omnifaces.cdi.ViewScoped;
import org.openepics.discs.conf.ejb.InstallationEJB;
import org.openepics.discs.conf.ejb.PropertyEJB;
import org.openepics.discs.conf.ejb.SlotEJB;
import org.openepics.discs.conf.ent.Artifact;
import org.openepics.discs.conf.ent.ComptypePropertyValue;
import org.openepics.discs.conf.ent.DevicePropertyValue;
import org.openepics.discs.conf.ent.ExternalLink;
import org.openepics.discs.conf.ent.InstallationRecord;
import org.openepics.discs.conf.ent.Property;
import org.openepics.discs.conf.ent.PropertyValue;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotArtifact;
import org.openepics.discs.conf.ent.SlotPropertyValue;
import org.openepics.discs.conf.ent.Tag;
import org.openepics.discs.conf.security.SecurityPolicy;
import org.openepics.discs.conf.ui.common.AbstractAttributesController;
import org.openepics.discs.conf.ui.util.Action;
import org.openepics.discs.conf.ui.util.Entity;
import org.openepics.discs.conf.ui.util.UiUtility;
import org.openepics.discs.conf.util.PropertyValueNotUniqueException;
import org.openepics.discs.conf.views.EntityAttrArtifactView;
import org.openepics.discs.conf.views.EntityAttrExternalLinkView;
import org.openepics.discs.conf.views.EntityAttrPropertyValueView;
import org.openepics.discs.conf.views.EntityAttrTagView;
import org.openepics.discs.conf.views.EntityAttributeView;
import org.openepics.discs.conf.util.EntityAttributeKind;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

/**
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
@Named
@ViewScoped
@FacesConverter("slotPropertyConverter")
public class SlotAttributeController extends AbstractAttributesController<Slot, SlotPropertyValue, SlotArtifact>
    implements Converter {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = Logger.getLogger(SlotAttributeController.class.getCanonicalName());
    private static final int ALIAS_AUTOCOMPLETE_MAX_RESULTS = 20;

    @Inject
    private SlotEJB slotEJB;
    @Inject
    private InstallationEJB installationEJB;
    @Inject
    private PropertyEJB propertyEJB;
    @Inject
    private SecurityPolicy securityPolicy;

    @Inject
    private HierarchiesController hierarchiesController;

    // ------ variables for attribute manipulation ------
    private List<Property> filteredProperties;

    private String slotRefName;

    private SlotPropertyValue aliasSlotPropertyValue;
    private String originalAliasSlot;
    private List<SlotPropertyValue> lastSlotPropertySelection;
    private Slot referencedSlot;
    private static final ImmutableList<EntityAttributeKind> DISPLAYED_SLOT_KINDS =
            ImmutableList.of(
                    EntityAttributeKind.DEVICE_TYPE_PROPERTY,
                    EntityAttributeKind.DEVICE_TYPE_ARTIFACT,
                    EntityAttributeKind.DEVICE_TYPE_EXTERNAL_LINK,
                    EntityAttributeKind.DEVICE_TYPE_TAG,
                    EntityAttributeKind.SLOT_PROPERTY,
                    EntityAttributeKind.SLOT_ARTIFACT,
                    EntityAttributeKind.SLOT_EXTERNAL_LINK,
                    EntityAttributeKind.SLOT_TAG
            );
    private static final ImmutableList<EntityAttributeKind> DISPLAYED_CONTAINER_KINDS =
            ImmutableList.of(
                    EntityAttributeKind.CONTAINER_ARTIFACT,
                    EntityAttributeKind.CONTAINER_EXTERNAL_LINK,
                    EntityAttributeKind.CONTAINER_TAG
            );

    public SlotAttributeController() {
    }

    /** Java EE post construct life-cycle method. */
    @PostConstruct
    public void init() {
        setDao(slotEJB);
        slotRefName = null;
    }

    /**
     * The main method that prepares the fields for any of the following
     * dialogs in slot property case when alias properties should be treat as well:
     * <ul>
     * <li>the dialog to modify a property value</li>
     * <li>the dialog to modify an artifact data</li>
     * </ul>
     *
     * If property is a SlotPropertyValue, it gets the refered slot name for the UI before popup gets opened
     */
    @Override
    public void prepareModifyPropertyPopUp() {
        super.prepareModifyPropertyPopUp();

        if (dialogAttribute instanceof EntityAttrPropertyValueView &&
                ((EntityAttrPropertyValueView)dialogAttribute).getEntity() instanceof SlotPropertyValue) {

            aliasSlotPropertyValue = ((SlotPropertyValue) ((EntityAttrPropertyValueView)dialogAttribute)
                    .getEntity()).getAliasOf();

            SlotPropertyValue propValue = (SlotPropertyValue) dialogAttribute.getEntity();

            if(propValue.getReferenceToSlot() != null) {
                slotRefName = propValue.getReferenceToSlot().getName();
                referencedSlot = propValue.getReferenceToSlot();
            } else {
                slotRefName = null;
                referencedSlot = null;
            }
        }
    }

    @Override
    public void populateAttributesList() {
        List<Slot> slots = hierarchiesController.getSelectedSlots();

        Preconditions.checkNotNull(slots);
        attributes = Lists.newArrayList();
        for (final Slot slot : slots) {
            addPropertyValues(slot);
            addArtifacts(slot);
            addTags(slot);
            addExternalLinks(slot);
        }
    }

    private void addPropertyValues(final Slot slot) {
        final InstallationRecord activeInstallationRecord = installationEJB.getActiveInstallationRecordForSlot(slot);

        List<EntityAttrPropertyValueView<Slot>> propertyAttributes = new ArrayList<>();
        for (final ComptypePropertyValue value : slot.getComponentType().getComptypePropertyList()) {
            if (!value.isPropertyDefinition()) {
                propertyAttributes.add(new EntityAttrPropertyValueView<Slot>(value, slot, slot.getComponentType()));
            }
        }

        for (final SlotPropertyValue value : slot.getSlotPropertyList()) {
            propertyAttributes.add(new EntityAttrPropertyValueView<Slot>(value, slot));
        }

        if (activeInstallationRecord != null) {
            for (final DevicePropertyValue devicePropertyValue : activeInstallationRecord.getDevice()
                    .getDevicePropertyList()) {
                propertyAttributes.add(new EntityAttrPropertyValueView<Slot>(devicePropertyValue,
                        EntityAttributeKind.DEVICE_PROPERTY, slot, activeInstallationRecord.getDevice()));
            }
        } else {
            // show defaults for slots without devices.
            for (final ComptypePropertyValue value : slot.getComponentType().getComptypePropertyList()) {
                if (value.isPropertyDefinition() && value.isDefinitionTargetDevice()) {
                    propertyAttributes.add(new EntityAttrPropertyValueView<Slot>(value, slot, slot.getComponentType()));
                }
            }
        }

        propertyAttributes.sort(Comparator.comparing(a -> a.getProperty().getName()));
        attributes.addAll(propertyAttributes);
    }

    private void addArtifacts(final Slot slot) {
        final InstallationRecord activeInstallationRecord = installationEJB.getActiveInstallationRecordForSlot(slot);

        for (final Artifact artifact : slot.getComponentType().getEntityArtifactList()) {
            attributes.add(new EntityAttrArtifactView<>(artifact, slot, slot.getComponentType()));
        }

        for (final Artifact artifact : slot.getEntityArtifactList()) {
            attributes.add(new EntityAttrArtifactView<>(artifact, slot));
        }

        if (activeInstallationRecord != null) {
            for (final Artifact deviceArtifact : activeInstallationRecord.getDevice().getEntityArtifactList()) {
                attributes
                        .add(new EntityAttrArtifactView<>(deviceArtifact, slot, activeInstallationRecord.getDevice()));
            }
        }
    }

    private void addExternalLinks(final Slot slot) {

        slot.getComponentType().getExternalLinkList().stream().forEach(externalLink -> attributes
                .add(new EntityAttrExternalLinkView<>(externalLink, slot, slot.getComponentType())));

        slot.getExternalLinkList().stream()
                .forEach(externalLink -> attributes.add(new EntityAttrExternalLinkView<>(externalLink, slot)));

        final InstallationRecord activeInstallationRecord = installationEJB.getActiveInstallationRecordForSlot(slot);

        if (activeInstallationRecord != null) {
            activeInstallationRecord.getDevice().getExternalLinkList().stream().forEach(externalLink -> attributes
                    .add(new EntityAttrExternalLinkView<>(externalLink, slot, activeInstallationRecord.getDevice())));
        }

    }

    private void addTags(final Slot slot) {
        final InstallationRecord activeInstallationRecord = installationEJB.getActiveInstallationRecordForSlot(slot);

        for (final Tag tagInstance : slot.getComponentType().getTags()) {
            attributes.add(new EntityAttrTagView<Slot>(tagInstance, slot, slot.getComponentType()));
        }

        for (final Tag tagInstance : slot.getTags()) {
            attributes.add(new EntityAttrTagView<Slot>(tagInstance, slot));
        }

        if (activeInstallationRecord != null) {
            for (final Tag tagInstance : activeInstallationRecord.getDevice().getTags()) {
                attributes.add(new EntityAttrTagView<Slot>(tagInstance, slot, activeInstallationRecord.getDevice()));
            }
        }
    }

    /**
     * @return <code>true</code> if the attribute "Delete" button can be enabled,
     *         <code>false</code> otherwise
     */
    public boolean canDeleteAttributes() {
        if (selectedAttribute == null) {
            return false;
        }

        return canDelete(selectedAttribute);
    }

    @Override
    protected boolean canDelete(EntityAttributeView<Slot> attributeView) {
        switch (attributeView.getKind()) {
            case SLOT_ARTIFACT:
            case SLOT_EXTERNAL_LINK:
            case SLOT_TAG:
            case CONTAINER_ARTIFACT:
            case CONTAINER_EXTERNAL_LINK:
            case CONTAINER_TAG:
                return true;
            case SLOT_PROPERTY:
                return attributeView.isValueSet();
            default:
                return false;
        }
    }

    /**
     * The handler called from the "Delete confirmation" dialog. This actually
     * deletes an attribute
     */
    @Override
    public void deleteAttributes() {
        Preconditions.checkNotNull(selectedAttribute);
        int props = 0;

        final Slot slot = slotEJB.findById(selectedAttribute.getParentId());
        switch (selectedAttribute.getKind()) {
            case SLOT_EXTERNAL_LINK:
            case CONTAINER_EXTERNAL_LINK:
                slotEJB.deleteExternalLink(slot, (ExternalLink) selectedAttribute.getEntity());
                hierarchiesController.refreshSlot(slot);
                break;
            case SLOT_ARTIFACT:
            case CONTAINER_ARTIFACT:
                slotEJB.deleteChild(selectedAttribute.getEntity());
                hierarchiesController.refreshSlot(slot);
                break;
            case SLOT_TAG:
            case CONTAINER_TAG:
                slot.getTags().remove(selectedAttribute.getEntity());
                hierarchiesController.saveSlotAndRefresh(slot);
                break;
            case SLOT_PROPERTY:
                SlotPropertyValue prop = ((SlotPropertyValue) selectedAttribute.getEntity());
                prop.setAliasOf(null);
                prop.setReferenceToSlot(null);
                prop.setPropValue(null);
                updateSlotProperty(prop);
                hierarchiesController.refreshSlot(slot);
                break;
            default:
                throw new RuntimeException("Trying to delete an attribute that cannot be removed on home screen.");
        }
        ++props;

        UiUtility.showInfoMessage(Entity.PROPERTIES, Action.DELETE, props);
        clearRelatedAttributeInformation();
        populateAttributesList();
    }

    @Override
    public boolean hasEditPermission() {
        return securityPolicy.getUIHint(SecurityPolicy.UI_HINT_HIERARCHY_MODIFY);
    }

    @Override
    public boolean canEdit(EntityAttributeView<Slot> attributeView) {
        switch (attributeView.getKind()) {
            case SLOT_ARTIFACT:
            case SLOT_EXTERNAL_LINK:
            case SLOT_PROPERTY:
            case CONTAINER_ARTIFACT:
            case CONTAINER_EXTERNAL_LINK:
                return true;
            default:
                return false;
        }
    }

    @Override
    protected void propertyNameChangeOverride(final EntityAttrPropertyValueView<Slot> propertyValueView) {
        propertyValueView.setPropertyNameChangeDisabled(propertyValueView.getParentEntity().isHostingSlot());
    }

    /**
     * Prepares data for addition of {@link PropertyValue}. Only valid for
     * containers.
     */
    public void prepareForPropertyValueAdd() {
        selectedAttribute = null;
        final SlotPropertyValue slotValueInstance = new SlotPropertyValue(false);
        slotValueInstance.setPropertiesParent(hierarchiesController.getSelectedNodeSlot());
        dialogAttribute = new EntityAttrPropertyValueView<Slot>(slotValueInstance,
                hierarchiesController.getSelectedEntity());
        filterProperties();
    }

    /**
     * A method to add a {@link PropertyValue} to a container.
     *
     * @param event the event
     */
    public void addPropertyValue(ActionEvent event) {
        Preconditions.checkNotNull(dialogAttribute);

        boolean reportedError = false;
        try {
            final EntityAttrPropertyValueView<Slot> view = getDialogAttrPropertyValue();
            final PropertyValue slotValueInstance = view.getEntity();

            slotEJB.addChild(slotValueInstance, true);
            UiUtility.showInfoMessage(Entity.PROPERTY, slotValueInstance.getProperty().getName(),
                    Action.CREATE);
            hierarchiesController.refreshSlot(view.getParentEntity());
        } catch (EJBException e) {
            if (UiUtility.causedBySpecifiedExceptionClass(e, PropertyValueNotUniqueException.class)) {
                reportedError = true;
                final String formName = getFormId(event.getComponent());
                FacesContext.getCurrentInstance().validationFailed();
                FacesContext.getCurrentInstance().addMessage(formName + "propertyValidationMessage", new FacesMessage(
                        FacesMessage.SEVERITY_ERROR, UiUtility.ERROR_MESSAGE, "Value must be unique."));
            } else {
                throw e;
            }
        } finally {
            if (!reportedError) {
                resetFields();
                populateAttributesList();
            }
        }
    }

    @Override
    protected void filterProperties() {
        final List<Property> propertyCandidates = propertyEJB.findAllOrderedByName();

        final Property dialogProperty = getDialogAttrPropertyValue() != null
                ? getDialogAttrPropertyValue().getProperty()
                : null;

        // remove all properties that are already defined.
        if (hierarchiesController.getSelectedNodeSlot() != null) {
            for (final SlotPropertyValue slotPropertyValue : hierarchiesController.getSelectedNodeSlot()
                    .getSlotPropertyList()) {
                if (!slotPropertyValue.getProperty().equals(dialogProperty)) {
                    propertyCandidates.remove(slotPropertyValue.getProperty());
                }
            }
        }
        filteredProperties = propertyCandidates;
    }

    @Override
    protected void refreshParentEntity(EntityAttributeView<Slot> attributeView) {
        hierarchiesController.refreshSlot(attributeView.getParentEntity());
    }

    @Override
    protected Slot getSelectedEntity() {
        return hierarchiesController.getSelectedEntity();
    }

    public void setAttrbutes(List<EntityAttributeView<Slot>> attributes) {
        this.attributes = attributes;
    }

    /** @return the filteredProperties */
    public List<Property> getFilteredProperties() {
        return filteredProperties;
    }

    @Override
    protected SlotPropertyValue newPropertyValue() {
        return new SlotPropertyValue();
    }

    @Override
    protected Artifact newArtifact() {
        return new SlotArtifact();
    }

    /**
     *
     * @return The name of the device type for the selected slot or slots (having the same device type), if applicable.
     * If no slot or multiple slots having different device types are selected, an empty string is returned.
     */
    public String getDeviceType() {
        String commonDeviceType = hierarchiesController.getSelectedSlotsCommonDeviceType();
        if (StringUtils.isNotEmpty(commonDeviceType)) {
            return commonDeviceType;
        }

        try {
            Slot slot = hierarchiesController.getSelectedEntity();
            return slot.getComponentType().getName();
        } catch (IllegalArgumentException e) {
            return "";
        }
    }

    /**
     * @return Aliased slot property value if slot property value is an alias
     */
    public SlotPropertyValue getAliasSlotPropertyValue() {
        return aliasSlotPropertyValue;
    }

    public void setAliasSlotPropertyValue(SlotPropertyValue aliasSlotPropertyValue) {
        if (originalAliasSlot == null) {
            originalAliasSlot =  this.aliasSlotPropertyValue != null
                    ? this.aliasSlotPropertyValue.getSlot().getName() : "";
        }

        this.aliasSlotPropertyValue = aliasSlotPropertyValue;
        if (dialogAttribute.getEntity() instanceof SlotPropertyValue) {
            ((SlotPropertyValue)dialogAttribute.getEntity()).setAliasOf(aliasSlotPropertyValue);
        }
    }

    /**
     * This method resets the fields related to the dialog that was just shown
     * to the user.
     */
    @Override
    public void resetFields() {
        super.resetFields();
        aliasSlotPropertyValue = null;
        originalAliasSlot = null;
    }

    /**
     * This method resets dialog attribute's property value to original when Edit property dialog is cancelled.
     */
    @Override
    public void resetProperty() {
        super.resetProperty();
        if(originalAliasSlot != null) {
            setAliasSlotPropertyValue(StringUtils.isNotEmpty(originalAliasSlot)
                    ? getSlotPropertyValueByName(originalAliasSlot) : null);
        }

        if(StringUtils.isNotEmpty(slotRefName)) {
            if (dialogAttribute != null && dialogAttribute.getEntity() instanceof SlotPropertyValue){
                ((SlotPropertyValue)dialogAttribute.getEntity()).setReferenceToSlot(referencedSlot);
            }
        }
    }

    /**
     * Returns possible slot property values for slot property value alias autocomplete.
     *
     * @param query query string, part of the slot name
     * @return the {@link List} of the possible {@link SlotPropertyValue}s
     */
    public List<SlotPropertyValue> autocompleteAliasedSlot(final String query) {
        lastSlotPropertySelection = new ArrayList<>();
        if (dialogAttribute instanceof EntityAttrPropertyValueView) {
            Property aliasOf = ((EntityAttrPropertyValueView)dialogAttribute).getProperty().getAliasOf();
            try {
                final List<Slot> slot = hierarchiesController.getRestrictedSelectedSlots();
                if(aliasOf != null) {
                    lastSlotPropertySelection =
                            slotEJB.getSlotsWithProperty(aliasOf, query, slot.get(0).getId(),
                                    ALIAS_AUTOCOMPLETE_MAX_RESULTS + 1);
                }
            } catch (IllegalArgumentException e) {
                LOGGER.warning("Error in alias auto complete function. " + e.getMessage());
            }
        }
        return lastSlotPropertySelection;
    }

    /**
     * @return Maximum length of alias aoutcomplete list on UI
     */
    public int getAliasAutocompleteMaxResults() {
        return ALIAS_AUTOCOMPLETE_MAX_RESULTS;
    }


    /**
     * Finds the selected slot property value by slot name
     */
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if(value != null && value.trim().length() > 0 && lastSlotPropertySelection != null) {
            return getSlotPropertyValueByName(value);
        }

        return null;
    }

    /**
     * Returns the selected slot property value's slot name
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof SlotPropertyValue) {
            return ((SlotPropertyValue) value).getSlot().getName();
        }
        return null;
    }

    private SlotPropertyValue getSlotPropertyValueByName(final String slotName) {
        if (lastSlotPropertySelection == null) {
            return null;
        }
        return lastSlotPropertySelection.stream().filter(sp -> sp.getSlot().getName().equals(slotName))
                .findFirst().orElse(null);
    }

    /**
     * Gets the referred slot name
     * @return the referred slot name, if property is a SlotPropertyValue
     */
    public String getSlotRefName() {
        return slotRefName;
    }

    /**
     * Sets the referred slot in DB by it's name
     *
     * @param slotRefName
     *          the desired Slot reference name
     */
    public void setSlotRefName(String slotRefName) {
        this.slotRefName = slotRefName;

        if (dialogAttribute.getEntity() instanceof SlotPropertyValue) {
            SlotPropertyValue propertyValue = (SlotPropertyValue) dialogAttribute.getEntity();
            if(propertyValue != null) {

                Slot toSlot;
                if (Strings.isNullOrEmpty(slotRefName)) {
                    toSlot = null;
                } else {
                    toSlot = slotEJB.findByName(slotRefName);
                }

                 propertyValue.setReferenceToSlot(toSlot);
            }
        }
    }

    /**
     *  It autocompletes the slot names if user enters some characters on UI
     *  It invokes the hierarchiesController autocomplete method
     *
     * @param query
     *          The search string entered by user for getting a SlotName
     * @return
     *          A list of SlotName-strings, that matches the user-entered value
     */
    public List<String> slotNameAutocompleteText(String query) {
        return slotEJB.findSlotByNameContainingString(query, getNameAutocompleteMaxResults() + 1)
                .stream().map(Slot::getName).collect(Collectors.toList());
    }

    /**
     * It modifies the refered Slot if type is a SlotPropertyValue
     * If user deletes/leaves the SlotName empty, the slotReference will be set to <code>NULL</code>
     * In other case it searches the refered slot by name, and sets the reference-value to it
     *
     * @param event the event
     */
    public void modifyPropertyValue(ActionEvent event) {

        SlotPropertyValue propertyValue = null;

        if(dialogAttribute.getEntity() instanceof SlotPropertyValue) {
            propertyValue = (SlotPropertyValue) dialogAttribute.getEntity();
        }

        super.modifyPropertyValue(event);

        if(propertyValue != null) {

            Slot toSlot;
            if (Strings.isNullOrEmpty(slotRefName)) {
                toSlot = null;
                referencedSlot = null;
            } else {
                toSlot = slotEJB.findByName(slotRefName);
                referencedSlot = toSlot;
            }

            slotEJB.updateSlotReference(propertyValue, toSlot);
        }

    }

    @Override
    protected void checkAndUpdateAttribute(final PropertyValue propVal, final String newValueStr) {
        if (propVal instanceof SlotPropertyValue) {
            SlotPropertyValue editedPropVal = (SlotPropertyValue)propVal;
            if (newValueStr == null) {
                editedPropVal.setPropValue(null);
            }
            if(slotEJB.checkPropertyValueUnique(editedPropVal)) {
                updateSlotProperty(editedPropVal);
            } else {
                throw new PropertyValueNotUniqueException();
            }
        }
    }

    /**
     * Refreshes slot object from database which modified property belongs to. After that slot object's
     * property list is updated with new property value from modifiedProperty. (This method is used for avoiding
     * OptimisticLockException at save when property value is modified via data table cell edit and using
     * 'Edit Property' dialog as well.)
     *
     * @param modifiedProperty the modified slot property object
     */
    private void updateSlotProperty(final SlotPropertyValue modifiedProperty) {
        Slot slot = slotEJB.findById(modifiedProperty.getSlot().getId());
        SlotPropertyValue freshPropValue = slot.getSlotPropertyList().stream().filter(p -> p.getId().equals(modifiedProperty.getId())).findFirst().orElse(null);
        if (freshPropValue != null) {
            freshPropValue.setPropValue(modifiedProperty.getPropValue());
            freshPropValue.setAliasOf(modifiedProperty.getAliasOf());
            freshPropValue.setReferenceToSlot(modifiedProperty.getReferenceToSlot());
            slotEJB.save(slot);
        }
    }

    /**
     * Used to determine the autocomplete-list maximum size
     * It calls, and returns by the HierarchiesController autocomplete max result size that is stored in property
     *
     * @return
     *      The size-limit of the autocomplete-list, that has to be shown on the UI
     */
    public int getNameAutocompleteMaxResults() {
        return hierarchiesController.getNameAutocompleteMaxResults();
    }

    @Override
    public List<SelectItem> getAttributeKinds() {
        List<Slot> slots = hierarchiesController.getSelectedSlots();
        Set<EntityAttributeKind> attributeKinds = new LinkedHashSet<>();
        if(slots.stream().anyMatch(Slot::isHostingSlot)) {
            attributeKinds.addAll(DISPLAYED_SLOT_KINDS);
        }
        if(slots.stream().anyMatch(slot -> !slot.isHostingSlot())) {
            attributeKinds.addAll(DISPLAYED_CONTAINER_KINDS);
        }
        return UiUtility.buildAttributeKinds(Lists.newArrayList(attributeKinds));
    }
}
