/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import com.google.common.collect.ImmutableList;
import org.omnifaces.cdi.ViewScoped;
import org.openepics.discs.conf.ejb.DeviceEJB;
import org.openepics.discs.conf.ejb.InstallationEJB;
import org.openepics.discs.conf.ent.Artifact;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.ComptypePropertyValue;
import org.openepics.discs.conf.ent.Device;
import org.openepics.discs.conf.ent.DeviceArtifact;
import org.openepics.discs.conf.ent.DevicePropertyValue;
import org.openepics.discs.conf.ent.InstallationRecord;
import org.openepics.discs.conf.ent.PropertyValue;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotPropertyValue;
import org.openepics.discs.conf.ent.Tag;
import org.openepics.discs.conf.ui.common.AbstractAttributesController;
import org.openepics.discs.conf.ui.util.UiUtility;
import org.openepics.discs.conf.util.CCDBRuntimeException;
import org.openepics.discs.conf.views.DeviceView;
import org.openepics.discs.conf.views.EntityAttrArtifactView;
import org.openepics.discs.conf.views.EntityAttrExternalLinkView;
import org.openepics.discs.conf.views.EntityAttrPropertyValueView;
import org.openepics.discs.conf.views.EntityAttrTagView;
import org.openepics.discs.conf.views.EntityAttributeView;
import org.openepics.discs.conf.util.EntityAttributeKind;

/**
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
@Named
@ViewScoped
public class DeviceAttributesController
        extends AbstractAttributesController<Device, DevicePropertyValue, DeviceArtifact> {
    private static final long serialVersionUID = 1L;

    @Inject private DeviceEJB deviceEJB;
    @Inject private InstallationEJB installationEJB;

    @Inject private DevicesController devicesController;

    private final List<SelectItem> attributeKinds = UiUtility.buildAttributeKinds(DISPLAYED_KINDS);
    private static final ImmutableList<EntityAttributeKind> DISPLAYED_KINDS =
            ImmutableList.of(
                    EntityAttributeKind.DEVICE_TYPE_PROPERTY,
                    EntityAttributeKind.DEVICE_TYPE_ARTIFACT,
                    EntityAttributeKind.DEVICE_TYPE_EXTERNAL_LINK,
                    EntityAttributeKind.DEVICE_TYPE_TAG,
                    EntityAttributeKind.DEVICE_PROPERTY,
                    EntityAttributeKind.DEVICE_ARTIFACT,
                    EntityAttributeKind.DEVICE_EXTERNAL_LINK,
                    EntityAttributeKind.DEVICE_TAG
            );

    /** Java EE post construct life-cycle method. */
    @PostConstruct
    public void init() {
        setDao(deviceEJB);
    }

    @Override
    protected void filterProperties() {
        // nothing to do
    }

    @Override
    public void populateAttributesList() {
        attributes = new ArrayList<>();

        for (final DeviceView deviceView : devicesController.getSelectedDevices()) {
            final Device attrDevice = deviceEJB.refreshEntity(deviceView.getDevice());
            final ComponentType parent = attrDevice.getComponentType();

            for (final ComptypePropertyValue parentProp : parent.getComptypePropertyList()) {
                if (!parentProp.isPropertyDefinition()) {
                    attributes.add(new EntityAttrPropertyValueView<Device>(parentProp, attrDevice, parent));
                }
            }

            for (final Artifact parentArtifact : parent.getEntityArtifactList()) {
                attributes.add(new EntityAttrArtifactView<>(parentArtifact, attrDevice, parent));
            }


            parent.getExternalLinkList().stream()
                .forEach(
                        parentArtifact -> attributes.add(
                                new EntityAttrExternalLinkView<>(parentArtifact, attrDevice)));

            for (final Tag parentTag : parent.getTags()) {
                attributes.add(new EntityAttrTagView<>(parentTag, attrDevice, parent));
            }

            for (final DevicePropertyValue propVal : attrDevice.getDevicePropertyList()) {
                attributes.add(new EntityAttrPropertyValueView<Device>(propVal, attrDevice));
            }

            for (final Artifact artf : attrDevice.getEntityArtifactList()) {
                attributes.add(new EntityAttrArtifactView<>(artf, attrDevice));
            }

            attrDevice.getExternalLinkList().stream()
                .forEach(
                        externalLink -> attributes.add(
                                new EntityAttrExternalLinkView<>(externalLink, attrDevice)));

            for (final Tag tagAttr : attrDevice.getTags()) {
                attributes.add(new EntityAttrTagView<Device>(tagAttr, attrDevice));
            }

            final InstallationRecord installationRecord =
                    installationEJB.getActiveInstallationRecordForDevice(attrDevice);
            final Slot slot = installationRecord != null ? installationRecord.getSlot() : null;

            if (slot != null) {
                for (final SlotPropertyValue value : slot.getSlotPropertyList()) {
                    attributes.add(new EntityAttrPropertyValueView<Device>(value, attrDevice, slot));
                }
                for (final Artifact value : slot.getEntityArtifactList()) {
                    attributes.add(new EntityAttrArtifactView<>(value, attrDevice, slot));
                }
                for (final Tag tag : slot.getTags()) {
                    attributes.add(new EntityAttrTagView<>(tag, attrDevice, slot));
                }
            } else {
                for (final ComptypePropertyValue parentProp : parent.getComptypePropertyList()) {
                    if (parentProp.isDefinitionTargetSlot())
                        attributes.add(new EntityAttrPropertyValueView<Device>(parentProp,
                                                                    EntityAttributeKind.SLOT_PROPERTY,
                                                                    attrDevice, parent));
                }
            }
        }
    }

    @Override
    protected void checkAndUpdateAttribute(PropertyValue propVal, String newValueStr) {
        throw new CCDBRuntimeException("Editable Properties datatable is not implemented for Devices");
    }

    @Override
    public boolean hasEditPermission() {
        return false;
    }

    @Override
    public boolean canEdit(EntityAttributeView<Device> attributeView) {
        final EntityAttributeKind attributeKind = attributeView.getKind();
        return EntityAttributeKind.DEVICE_PROPERTY.equals(attributeKind)
                || EntityAttributeKind.DEVICE_ARTIFACT.equals(attributeKind)
                || EntityAttributeKind.DEVICE_EXTERNAL_LINK.equals(attributeKind);
    }

    @Override
    protected boolean canDelete(EntityAttributeView<Device> attributeView) {
        final EntityAttributeKind attributeKind = attributeView.getKind();
        return EntityAttributeKind.DEVICE_PROPERTY.equals(attributeKind)
                || EntityAttributeKind.DEVICE_ARTIFACT.equals(attributeKind)
                || EntityAttributeKind.DEVICE_EXTERNAL_LINK.equals(attributeKind)
                || EntityAttributeKind.DEVICE_TAG.equals(attributeKind);
    }

    @Override
    protected Device getSelectedEntity() {
        if (devicesController.isSingleDeviceSelected()) {
            return deviceEJB.refreshEntity(devicesController.getSelectedDevices().get(0).getDevice());
        }
        throw new IllegalArgumentException("No device selected");
    }

    @Override
    protected DevicePropertyValue newPropertyValue() {
        return new DevicePropertyValue();
    }

    @Override
    protected Artifact newArtifact() {
        return new DeviceArtifact();
    }

    @Override
    public List<SelectItem> getAttributeKinds() {
        return attributeKinds;
    }
}
