/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.ui;

import org.openepics.discs.conf.ejb.ComptypeEJB;
import org.openepics.discs.conf.ui.common.UIException;
import org.openepics.discs.conf.ui.trees.DeviceTypeTree;
import org.primefaces.model.TreeNode;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Named
@SessionScoped
public class DeviceTypeTreeController implements Serializable {
    @Inject private ComptypeEJB comptypeEJB;
    private transient DeviceTypeTree deviceTypeTree;
    private boolean showOnlyUnreferenced = false;
    private String nameFilter;
    private String descriptionFilter;

    public void setSelectedDeviceTypeNodes(TreeNode[] selectedDeviceTypeNodes) {
        this.selectedDeviceTypeNodes = selectedDeviceTypeNodes;
    }

    private transient TreeNode[] selectedDeviceTypeNodes;

    /** Java EE post construct life-cycle method. */
    @PostConstruct
    public void init() {
        try {
            deviceTypeTree = new DeviceTypeTree(comptypeEJB);
        } catch (Exception e) {
            throw new UIException("Device type tree controller initialization failed: " + e.getMessage(), e);
        }
    }

    public DeviceTypeTree getDeviceTypeTree() {
        return deviceTypeTree;
    }

    public TreeNode[] getSelectedDeviceTypeNodes() {
        return selectedDeviceTypeNodes;
    }

    public boolean isShowOnlyUnreferenced() {
        return showOnlyUnreferenced;
    }

    public void setShowOnlyUnreferenced(boolean showOnlyUnreferenced) {
        this.showOnlyUnreferenced = showOnlyUnreferenced;
        deviceTypeTree.setOnlyUnreferenced(showOnlyUnreferenced);
    }

    public String getNameFilter() {
        return nameFilter;
    }

    public void setNameFilter(String nameFilter) {
        this.nameFilter = nameFilter;
        deviceTypeTree.setFilter(DeviceTypeTree.NAME, nameFilter);
    }

    public String getDescriptionFilter() {
        return descriptionFilter;
    }

    public void setDescriptionFilter(String descriptionFilter) {
        this.descriptionFilter = descriptionFilter;
        deviceTypeTree.setFilter(DeviceTypeTree.DESCRIPTION, descriptionFilter);
    }

    public void resetState() {
        deviceTypeTree.unselectAllNodes();
        setNameFilter(null);
        setDescriptionFilter(null);
        setShowOnlyUnreferenced(false);
    }
}
