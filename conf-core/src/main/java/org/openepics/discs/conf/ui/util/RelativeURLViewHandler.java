/*
 * Copyright (c) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.ui.util;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.ViewHandler;
import javax.faces.application.ViewHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

public class RelativeURLViewHandler extends ViewHandlerWrapper {

    private static final Logger LOGGER = Logger.getLogger(RelativeURLViewHandler.class.getName());
    private ViewHandler baseViewHandler;

    public RelativeURLViewHandler(ViewHandler baseViewHandler) {
        this.baseViewHandler = baseViewHandler;
    }

    @Override
    public String getActionURL(FacesContext context, String viewId) {
        return convertToRelativeURL(context, baseViewHandler.getActionURL(context, viewId));
    }

    @Override
    public String getResourceURL(FacesContext context, String path) {
        return convertToRelativeURL(context, baseViewHandler.getResourceURL(context, path));
    }

    @Override
    public ViewHandler getWrapped() {
        return baseViewHandler;
    }

    private String convertToRelativeURL(FacesContext context, String url) {
        final HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
            final URI uri = new URI(request.getRequestURI());
            String path = uri.getPath().replace("//", "/");
            if (url.startsWith("/")) {
                StringBuilder prefix = new StringBuilder(".");
                int count = path.length() - path.replace("/", "").length();
                for (int i = 0; i < (count - 1); i++) {
                    prefix.append("./.");
                }
                return (prefix.toString() + url);
            }
            return url;
        } catch (URISyntaxException e) {
            LOGGER.log(Level.SEVERE, "Malformed URI while converting to relative path", e);
            return url;
        }
    }
}
