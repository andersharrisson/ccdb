/*
 * Copyright (C) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.ui;

import org.openepics.discs.conf.util.AppProperties;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Zoltan Runyo <zoltan.runyo@esss.se>
 */
@Named
@ViewScoped
public class MailManager implements Serializable {
    private static final Logger LOGGER = Logger.getLogger(MailManager.class.getCanonicalName());

    @Inject
    private AppProperties properties;

    /** @return the support email */
    public String getSupportEmail() {
        final String supportEmail = properties.getProperty(AppProperties.SUPPORT_EMAIL);
        LOGGER.log(Level.FINE, "Support email: {0}", supportEmail);
        return supportEmail;
    }
}
