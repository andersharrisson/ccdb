/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.omnifaces.cdi.ViewScoped;
import org.openepics.discs.conf.ejb.ComptypeEJB;
import org.openepics.discs.conf.ejb.DAO;
import org.openepics.discs.conf.ejb.DeviceEJB;
import org.openepics.discs.conf.ejb.InstallationEJB;
import org.openepics.discs.conf.ejb.PropertyEJB;
import org.openepics.discs.conf.ejb.SlotEJB;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.ComptypeArtifact;
import org.openepics.discs.conf.ent.ComptypePropertyValue;
import org.openepics.discs.conf.ent.DataType;
import org.openepics.discs.conf.ent.Device;
import org.openepics.discs.conf.ent.DevicePropertyValue;
import org.openepics.discs.conf.ent.InstallationRecord;
import org.openepics.discs.conf.ent.Property;
import org.openepics.discs.conf.ent.PropertyValue;
import org.openepics.discs.conf.ent.PropertyValueUniqueness;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotPropertyValue;
import org.openepics.discs.conf.ent.Tag;
import org.openepics.discs.conf.ent.values.Value;
import org.openepics.discs.conf.security.SecurityPolicy;
import org.openepics.discs.conf.ui.common.AbstractAttributesController;
import org.openepics.discs.conf.ui.util.Action;
import org.openepics.discs.conf.ui.util.Entity;
import org.openepics.discs.conf.ui.util.UiUtility;
import org.openepics.discs.conf.util.Conversion;
import org.openepics.discs.conf.util.DefaultValueNotAllowedException;
import org.openepics.discs.conf.util.PropertyValueNotUniqueException;
import org.openepics.discs.conf.util.UnhandledCaseException;
import org.openepics.discs.conf.util.Utility;
import org.openepics.discs.conf.views.ComponentTypeView;
import org.openepics.discs.conf.views.EntityAttrArtifactView;
import org.openepics.discs.conf.views.EntityAttrPropertyValueView;
import org.openepics.discs.conf.views.EntityAttrTagView;
import org.openepics.discs.conf.views.EntityAttributeView;
import org.openepics.discs.conf.util.EntityAttributeKind;
import org.openepics.discs.conf.views.MultiPropertyValueView;
import org.primefaces.PrimeFaces;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.event.CellEditEvent;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.openepics.discs.conf.ent.Artifact;
import org.openepics.discs.conf.views.EntityAttrExternalLinkView;

/**
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
@Named
@ViewScoped
public class ComptypeAttributesController
        extends AbstractAttributesController<ComponentType, ComptypePropertyValue, ComptypeArtifact> {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = Logger.getLogger(ComptypeAttributesController.class.getCanonicalName());

    private static final String INPUT_VALIDATION_FAIL = "inputValidationFail";

    private enum DefinitionTarget { SLOT, DEVICE }

    @Inject private ComptypeEJB comptypeEJB;
    @Inject private PropertyEJB propertyEJB;
    @Inject private SlotEJB slotEJB;
    @Inject private DeviceEJB deviceEJB;
    @Inject private InstallationEJB installationEJB;
    @Inject private ComponentTypeManager componentTypeManager;
    @Inject private SecurityPolicy securityPolicy;

    @Resource
    UserTransaction devicePropToSlotPropTx;

    private List<Property> filteredProperties;
    private List<Property> selectedProperties;
    private List<Property> selectionPropertiesFiltered;
    private boolean isPropertyDefinition;
    private DefinitionTarget definitionTarget = null;

    private List<MultiPropertyValueView> filteredPropertyValues;
    private List<MultiPropertyValueView> selectedPropertyValues;
    private List<MultiPropertyValueView> selectionPropertyValuesFiltered;
    private boolean selectAllRows;
    private final List<SelectItem> attributeKinds = UiUtility.buildAttributeKinds(DISPLAYED_KINDS);
    private static final ImmutableList<EntityAttributeKind> DISPLAYED_KINDS =
            ImmutableList.of(
                    EntityAttributeKind.DEVICE_TYPE_PROPERTY,
                    EntityAttributeKind.DEVICE_TYPE_ARTIFACT,
                    EntityAttributeKind.DEVICE_TYPE_EXTERNAL_LINK,
                    EntityAttributeKind.DEVICE_TYPE_TAG,
                    EntityAttributeKind.SLOT_PROPERTY
            );

    /** Java EE post construct life-cycle method. */
    @PostConstruct
    public void init() {
        setDao(comptypeEJB);
        resetFields();
    }

    @Override
    public void clearRelatedAttributeInformation() {
        super.clearRelatedAttributeInformation();
        filteredProperties = null;
        selectedProperties = null;
    }

    @Override
    protected void deletePropertyValue(final ComptypePropertyValue propValueToDelete) {
        if (propValueToDelete.isPropertyDefinition()) {
            if (propValueToDelete.isDefinitionTargetSlot()) {
                for (final Slot slot : slotEJB.findByComponentType(componentTypeManager.getSelectedComponent()
                        .getComponentType())) {
                    removeUndefinedProperty(slot.getSlotPropertyList(), propValueToDelete.getProperty(), slotEJB);
                }
            }

            if (propValueToDelete.isDefinitionTargetDevice()) {
                for (final Device device
                        : deviceEJB.findDevicesByComponentType(componentTypeManager.getSelectedComponent()
                                .getComponentType())) {
                    removeUndefinedProperty(device.getDevicePropertyList(), propValueToDelete.getProperty(), deviceEJB);
                }
            }
        }
        super.deletePropertyValue(propValueToDelete);
    }

    private <T extends PropertyValue> void removeUndefinedProperty(final List<T> entityProperties,
            final Property propertyToDelete, final DAO<?> daoEJB) {
        T propValueToDelete = null;
        for (final T entityPropValue : entityProperties) {
                if (entityPropValue.getProperty().equals(propertyToDelete)) {
                    propValueToDelete = entityPropValue;
                    break;
                }
        }
        if (propValueToDelete != null) {
            daoEJB.deleteChild(propValueToDelete);
        }
    }

    @Override
    public void populateAttributesList() {
        Preconditions.checkNotNull(componentTypeManager.getSelectedDeviceTypes());
        attributes = Lists.newArrayList();

        for (final ComponentTypeView selectedMember : componentTypeManager.getSelectedDeviceTypes()) {
            // if leaf node in device type tree
            if(selectedMember.getComponentType().getId() != null) {

                // refresh the component type from database. This refreshes all related collections as well.
                final ComponentType freshComponentType = comptypeEJB.refreshEntity(selectedMember.getComponentType());

                for (final ComptypePropertyValue prop : freshComponentType.getComptypePropertyList()) {
                    attributes.add(new EntityAttrPropertyValueView<>(prop, freshComponentType));
                }

                for (final Artifact art : freshComponentType.getEntityArtifactList()) {
                    attributes.add(new EntityAttrArtifactView<>(art, freshComponentType));
                }

                freshComponentType.getExternalLinkList().stream()
                        .forEach(
                                externalLink -> attributes.add(
                                        new EntityAttrExternalLinkView<>(externalLink, freshComponentType)));

                for (final Tag tagAttr : freshComponentType.getTags()) {
                    attributes.add(new EntityAttrTagView<>(tagAttr, freshComponentType));
                }
            }
        }
    }

    @Override
    protected void filterProperties() {
        if ((componentTypeManager.getSelectedDeviceTypes() == null)
                || (componentTypeManager.getSelectedDeviceTypes().size() != 1)) {
            filteredProperties = null;
            return;
        }

        List<Property> propertyCandidates = propertyEJB.findAllOrderedByName();
        // Collect property aliases and slot references only for slot properties
        if (definitionTarget == null || !definitionTarget.equals(DefinitionTarget.SLOT)) {
            propertyCandidates = propertyCandidates.stream().filter(p -> !p.isAlias()).filter(
                    p -> p.getSlotReference() == null || !p.getSlotReference()).collect(Collectors.toList());
        }

        final Property dialogProperty =
                getDialogAttrPropertyValue() != null ? getDialogAttrPropertyValue().getProperty() : null;

        for (ComponentTypeView compType : componentTypeManager.getSelectedDeviceTypes()) {
            ComponentType refreshedCompType = comptypeEJB.findById(compType.getId());
            for (final ComptypePropertyValue comptypePropertyValue
                    : refreshedCompType.getComptypePropertyList()) {
                final Property currentProperty = comptypePropertyValue.getProperty();
                if (!currentProperty.equals(dialogProperty)) {
                    propertyCandidates.remove(currentProperty);
                }
            }
        }
        filteredProperties = propertyCandidates;
    }

    @Override
    public void resetFields() {
        super.resetFields();
        filteredProperties = null;
        selectedProperties = null;
        selectionPropertiesFiltered = null;
    }

    @Override
    public boolean hasEditPermission() {
        return securityPolicy.getUIHint(SecurityPolicy.UI_HINT_DEVTYPE_MODIFY);
    }

    @Override
    public boolean canEdit(EntityAttributeView<ComponentType> attribute) {
        if (attribute == null) {
            return false;
        }

        boolean isAlias = false;
        if (attribute instanceof EntityAttrPropertyValueView
                && ((EntityAttrPropertyValueView)attribute).getProperty().getAliasOf() != null) {
            isAlias = true;
        }

        boolean isSlotReference = false;
        if (attribute instanceof EntityAttrPropertyValueView
                && (Boolean.TRUE.equals(((EntityAttrPropertyValueView)attribute).getProperty().getSlotReference()))) {
            isSlotReference = true;
        }

        // Property alias and slot reference cannot have default value so cannot be edited
        return attribute.getKind() != EntityAttributeKind.DEVICE_TYPE_TAG
                && !isAlias && !isSlotReference;
    }

    /**
     * Checks if selected attributes list contains only device properties.
     *
     * @return <code>true</code> if if selected attributes list contains only device properties otherwise
     * <code>false</code>
     */
    public boolean isDeviceProperty() {
        if (selectedAttribute == null) {
            return false;
        }
        return EntityAttributeKind.DEVICE_PROPERTY.equals(selectedAttribute.getKind());
    }

    /**
     * Transforms all selected properties to slot property in a user transaction.
     */
    public void transformSelectedToSlotProperties() {
        List<ComptypePropertyValue> allComptypeAttributeValues = Collections.singletonList((ComptypePropertyValue) selectedAttribute.getEntity());

        // Collects selected property values belonging to a specific device type into a map of
        // device type ids - property value lists
        Map<Long, List<ComptypePropertyValue>> compTypePropMap = new HashMap<>();
        for (ComptypePropertyValue propertyValue : allComptypeAttributeValues) {
            if (!compTypePropMap.containsKey(propertyValue.getComponentType().getId())) {
                compTypePropMap.put(propertyValue.getComponentType().getId(),
                        new ArrayList<>(Collections.singletonList(propertyValue)));
            } else {
                compTypePropMap.get(propertyValue.getComponentType().getId()).add(propertyValue);
            }
        }

        try {
            devicePropToSlotPropTx.begin();
            // Iterates all property value lists from the map
            for (List<ComptypePropertyValue> comptypeAttributeValues : compTypePropMap.values()) {
                final ComponentType deviceType = comptypeAttributeValues.get(0).getComponentType();
                // Iterates all property value from the lists belonging to a specific device type
                for (ComptypePropertyValue propertyValue : comptypeAttributeValues) {
                    // Modifies property value to be a slot property
                    propertyValue.setDefinitionTargetDevice(false);
                    propertyValue.setDefinitionTargetSlot(true);

                    // Looks for related slots and devices (having the current device type)
                    final List<Slot> relatedSlots = slotEJB.findByComponentType(deviceType);
                    final List<Device> relatedDevices = deviceEJB.findDevicesByComponentType(deviceType);

                    // Adds property as a new slot property instead of device property to every related slot and
                    // removes the device property. Keep value of device property (copy it to new slot property)
                    // if slot has installed device.
                    for (Slot slot : relatedSlots) {
                        if (canAddProperty(slot.getSlotPropertyList(), propertyValue.getProperty())) {
                            final InstallationRecord activeInstallationRecord =
                                    installationEJB.getActiveInstallationRecordForSlot(slot);
                            final SlotPropertyValue newSlotProperty = new SlotPropertyValue();
                            newSlotProperty.setProperty(propertyValue.getProperty());
                            newSlotProperty.setSlot(slot);
                            newSlotProperty.setPropValue(propertyValue.getPropValue());
                            if (activeInstallationRecord != null) {
                                DevicePropertyValue devicePropertyValue = activeInstallationRecord.getDevice()
                                        .getDevicePropertyList().stream().filter(devicePropValue
                                                -> propertyValue.getProperty().getId()
                                                .equals(devicePropValue.getProperty()
                                                        .getId())).findFirst().orElse(null);
                                if (devicePropertyValue != null) {
                                    Value propValue = devicePropertyValue.getPropValue();
                                    deviceEJB.deleteChild(devicePropertyValue);
                                    newSlotProperty.setPropValue(propValue);
                                }
                            }
                            slotEJB.addChild(newSlotProperty, true);
                        } else {
                            LOGGER.log(Level.FINE, "Type: " + componentTypeManager.getSelectedComponent()
                                    .getName() + "; Slot: " + slot.getName()
                                    + ";  Trying to add the same property value again: "
                                    + propertyValue.getProperty()
                                    .getName());
                        }
                    }

                    final List<DevicePropertyValue> devicePropertyValuesToDelete = new ArrayList<>();
                    // Removes device property value from every related devices.
                    for (Device device : relatedDevices) {
                        device.getDevicePropertyList().stream().filter(devicePropValue
                                -> propertyValue.getProperty().getId().equals(devicePropValue.getProperty()
                                .getId())).forEach(devicePropertyValuesToDelete::add);
                    }
                    for (DevicePropertyValue devicePropertyValue : devicePropertyValuesToDelete) {
                        deviceEJB.deleteChild(devicePropertyValue);
                    }
                }
                comptypeEJB.save(deviceType);
            }
            devicePropToSlotPropTx.commit();
        } catch (Exception ex) {
            try {
                devicePropToSlotPropTx.rollback();
                UiUtility.showErrorMessage(UiUtility.ErrorType.UNHANDLED,
                        "Error during transforming device property to slot property",
                        ExceptionUtils.getStackTrace(ex));
                LOGGER.log(Level.SEVERE, "Error during transforming device property to slot property: " +
                        ex.getMessage(), ex);
            } catch (IllegalStateException | SecurityException | SystemException ex1) {
                UiUtility.showErrorMessage(UiUtility.ErrorType.UNHANDLED,
                        "Error during transforming device property to slot property: " +
                                "failed to rollback transaction",
                        ExceptionUtils.getStackTrace(ex));
                LOGGER.log(Level.SEVERE,
                        "Error during transforming device property to slot property, " +
                                "failed to rollback transaction: "
                                + ex1.getMessage(), ex1);
            }
        }
    }

    /**
     * Recalculates and refreshes attribute/property list
     */
    public void refreshAttributes() {
        clearRelatedAttributeInformation();
        populateAttributesList();
    }

    /** Prepares the data for device type property creation */
    public void prepareForPropertyValueAdd() {
        filterProperties();
        filteredPropertyValues = filteredProperties.stream().map(MultiPropertyValueView::new).
                                    collect(Collectors.toList());
        selectedPropertyValues = Lists.newArrayList();
        selectionPropertyValuesFiltered = null;
        selectAllRows = false;
    }

    /** Prepares the data for slot property (definition) creation */
    public void prepareForSlotPropertyAdd() {
        definitionTarget = DefinitionTarget.SLOT;
        isPropertyDefinition = true;
        prepareForPropertyValueAdd();
    }

    /** Prepares the data for device property (definition) creation */
    public void prepareForDevicePropertyAdd() {
        definitionTarget = DefinitionTarget.DEVICE;
        isPropertyDefinition = true;
        prepareForPropertyValueAdd();
    }

    /** The event handler for when user clicks on the check-box in the "Add property values" dialog.
     * @param prop the property value to handle the event for
     */
    public void rowSelectListener(final MultiPropertyValueView prop) {
        if (prop.isSelected()) {
            selectedPropertyValues.add(prop);
        } else {
            selectedPropertyValues.remove(prop);
        }
    }

    /** The function to handle the state of the "Select all" checkbox after the filter change */
    public void updateToggle() {
        final List<MultiPropertyValueView> pvList = selectionPropertyValuesFiltered == null
                                                        ? filteredPropertyValues : selectionPropertyValuesFiltered;
        if (pvList == null || pvList.isEmpty()) {
            selectAllRows = false;
            return;
        }
        for (final MultiPropertyValueView pv : pvList) {
            if (!pv.isSelected()) {
                selectAllRows = false;
                return;
            }
        }
        selectAllRows = true;
    }

    /**
     * The event handler for toggling selection of all property values
     *
     * @param e the {@link AjaxBehaviorEvent}
     */
    public void handleToggleAll(AjaxBehaviorEvent e) {
        final SelectBooleanCheckbox source = (SelectBooleanCheckbox) e.getSource();
        final boolean submittedValue = (Boolean) source.getSubmittedValue();

        final List<MultiPropertyValueView> pvList = selectionPropertyValuesFiltered == null
                ? filteredPropertyValues : selectionPropertyValuesFiltered;
        if (pvList.isEmpty())
            return;
        if (submittedValue) {
            selectAllFiltered(pvList);
        } else {
            unselectAllFiltered(pvList);
        }
        final List<String> checkboxes = Lists.newArrayList();
        for (int i = 0; i < pvList.size(); ++i) {
            checkboxes.add(getFormId(source) + "propertySelect:" + i + ":propSelection");
        }
        PrimeFaces.current().ajax().update(checkboxes);
    }

    private void selectAllFiltered(final List<MultiPropertyValueView> pvList) {
        for (final MultiPropertyValueView pv : pvList) {
            if (!pv.isSelected()) {
                pv.setSelected(true);
                selectedPropertyValues.add(pv);
            }
        }
    }

    private void unselectAllFiltered(final List<MultiPropertyValueView> pvList) {
        for (final MultiPropertyValueView pv : pvList) {
            if (pv.isSelected()) {
                pv.setSelected(false);
                selectedPropertyValues.remove(pv);
            }
        }
    }

    /** This method handled the value once the users is done putting it in. This method actually performs the
     * input validation.
     *
     * This is a workaround to use validation on cell editors without closing them.
     *
     * @param event the event
     */
    public void onEditCell(CellEditEvent event) {
        final Object newValue = event.getNewValue();
        final Object oldValue = event.getOldValue();

        if (newValue != null && !newValue.equals(oldValue)) {
            final MultiPropertyValueView editedPropVal = selectionPropertyValuesFiltered == null
                                                            ? filteredPropertyValues.get(event.getRowIndex())
                                                            : selectionPropertyValuesFiltered.get(event.getRowIndex());
            final DataType dataType = editedPropVal.getDataType();
            final String regexp = editedPropVal.getRegexp();
            final String newValueStr = getEditEventValue(newValue, editedPropVal.getPropertyValueUIElement());
            if ((editedPropVal.getProperty().getValueUniqueness() != PropertyValueUniqueness.NONE)
                    && !Strings.isNullOrEmpty(newValueStr)) {
                final String formName = getFormId(event.getComponent());
                FacesContext.getCurrentInstance().addMessage(formName + INPUT_VALIDATION_FAIL,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, UiUtility.ERROR_MESSAGE,
                                                                    "Unique property cannot have a default value."));
                FacesContext.getCurrentInstance().validationFailed();
                return;
            }
            try {
                switch (editedPropVal.getPropertyValueUIElement()) {
                    case INPUT:
                    case CALENDAR:
                    case CSENTRY_HOST_AUTOCOMPLETE:
                        EntityAttrPropertyValueView.validateSingleLine(newValueStr, dataType, regexp);
                        break;
                    case TEXT_AREA:
                        EntityAttrPropertyValueView.validateMultiLine(newValueStr, dataType, regexp);
                        break;
                    case SELECT_ONE_MENU:
                        if (Strings.isNullOrEmpty(newValueStr)) {
                            throw UiUtility.getValidationError("A value must be selected.");
                        }
                        break;
                    case NONE:
                    default:
                        throw new UnhandledCaseException();
                }
                final Value val = Conversion.stringToValue(newValueStr, dataType);
                comptypeEJB.checkPropertyValueUnique(createPropertyValue(editedPropVal.getProperty(), val));
                editedPropVal.setValue(val);
            } catch (ValidatorException e) {
                final String formName = getFormId(event.getComponent());
                editedPropVal.setUiValue(
                        oldValue == null ? null
                                : getEditEventValue(oldValue, editedPropVal.getPropertyValueUIElement()));
                FacesContext.getCurrentInstance().addMessage(formName + INPUT_VALIDATION_FAIL, e.getFacesMessage());
                FacesContext.getCurrentInstance().validationFailed();
            } catch (EJBException e) {
                if (UiUtility.causedBySpecifiedExceptionClass(e, PropertyValueNotUniqueException.class)) {
                    final String formName = getFormId(event.getComponent());
                    FacesContext.getCurrentInstance().addMessage(formName + INPUT_VALIDATION_FAIL,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, UiUtility.ERROR_MESSAGE,
                                    "Value is not unique."));
                    FacesContext.getCurrentInstance().validationFailed();
                } else {
                    throw e;
                }
            }
        }
    }

    /**
     * A method that adds either slot or device instance properties. It adds the definition to the device
     * type and property values to already existing slots or device instances.
     */
    public void addNewPropertyValueDefs() {
        Preconditions.checkState(isPropertyDefinition);
        int created = 0;
        for (final MultiPropertyValueView pv : selectedPropertyValues) {
            final ComptypePropertyValue newPropertyValueInstance = newPropertyValue();
            newPropertyValueInstance.setInRepository(false);
            newPropertyValueInstance.setProperty(pv.getProperty());
            newPropertyValueInstance.setPropertiesParent(getSelectedEntity());
            newPropertyValueInstance.setPropertyDefinition(true);
            if (definitionTarget == DefinitionTarget.SLOT) {
                newPropertyValueInstance.setDefinitionTargetSlot(true);
            } else {
                newPropertyValueInstance.setDefinitionTargetDevice(true);
            }

            // This should be checked in UI, but let's normalize in case of an error.
            if (pv.getProperty().getValueUniqueness() != PropertyValueUniqueness.NONE) {
                pv.setValue(null);
            }

            newPropertyValueInstance.setPropValue(pv.getValue());
            comptypeEJB.addChild(newPropertyValueInstance, true);
            componentTypeManager.refreshSelectedComponent();
            addPropertyValueBasedOnDef(newPropertyValueInstance);
            ++created;
        }
        UiUtility.showInfoMessage(definitionTarget == DefinitionTarget.SLOT ? Entity.SLOT_PROPERTIES
                                                                            : Entity.DEVICE_PROPERTIES,
                                        Action.CREATE, created);
        resetFields();
        populateAttributesList();
    }

    private void addPropertyValueBasedOnDef(ComptypePropertyValue definition) {
        if (definition.isDefinitionTargetSlot()) {
            for (final Slot slot
                    : slotEJB.findByComponentType(componentTypeManager.getSelectedComponent().getComponentType())) {
                if (canAddProperty(slot.getSlotPropertyList(), definition.getProperty())) {
                    final SlotPropertyValue newSlotProperty = new SlotPropertyValue();
                    newSlotProperty.setProperty(definition.getProperty());
                    newSlotProperty.setSlot(slot);
                    newSlotProperty.setPropValue(definition.getPropValue());
                    slotEJB.addChild(newSlotProperty, true);
                } else {
                    LOGGER.log(Level.FINE, "Type: " + componentTypeManager.getSelectedComponent().getName()
                            + "; Slot: " + slot.getName()
                            + ";  Trying to add the same property value again: "
                            + definition.getProperty().getName());
                }
            }
        }

        if (definition.isDefinitionTargetDevice()) {
            for (final Device device : deviceEJB.findDevicesByComponentType(componentTypeManager.getSelectedComponent().
                                                                                getComponentType())) {
                if (canAddProperty(device.getDevicePropertyList(), definition.getProperty())) {
                    final DevicePropertyValue newDeviceProperty = new DevicePropertyValue();
                    newDeviceProperty.setProperty(definition.getProperty());
                    newDeviceProperty.setDevice(device);
                    newDeviceProperty.setPropValue(definition.getPropValue());
                    deviceEJB.addChild(newDeviceProperty, true);
                } else {
                    LOGGER.log(Level.FINE, "Type: " + componentTypeManager.getSelectedComponent().getName()
                            + "; Device: " + device.getSerialNumber()
                            + ";  Trying to add the same property value again: "
                            + definition.getProperty().getName());
                }
            }
        }
    }

    @Override
    protected void checkAndUpdateAttribute(final PropertyValue propVal, final String newValueStr) {
        if (propVal instanceof ComptypePropertyValue) {
            ComptypePropertyValue editedPropVal = (ComptypePropertyValue)propVal;
            if (newValueStr == null) {
                editedPropVal.setPropValue(null);
            }
            Utility.checkCompTypePropertyDefaultValue(editedPropVal.getComponentType(), editedPropVal,
                    newValueStr);
            updateCompTypeProperty(editedPropVal);
        }
    }

    /**
     * Refreshes device type object from database which modified property belongs to. After that device type object's
     * property list is updated with new property value from modifiedProperty. (This method is used for avoiding
     * OptimisticLockException at save when property value is modified via data table cell edit and using
     * 'Edit Property' dialog as well.)
     *
     * @param modifiedProperty the modified device type property object
     */
    private void updateCompTypeProperty(final ComptypePropertyValue modifiedProperty) {
        ComponentType deviceType = comptypeEJB.findById(modifiedProperty.getComponentType().getId());
        ComptypePropertyValue freshPropValue = deviceType.getComptypePropertyList().stream().filter(p -> p.getId().equals(modifiedProperty.getId())).findFirst().orElse(null);
        if (freshPropValue != null) {
            freshPropValue.setPropValue(modifiedProperty.getPropValue());
            comptypeEJB.save(deviceType);
        }
    }

    @Override
    public void modifyPropertyValue(ActionEvent event) {
        final ComptypePropertyValue editedPropVal = (ComptypePropertyValue) dialogAttribute.getEntity();
        try {
            Utility.checkCompTypePropertyDefaultValue(dialogAttribute.getParentEntity(), editedPropVal,
                    dialogAttribute.getValue());
        } catch (final DefaultValueNotAllowedException e) {
            final EntityAttrPropertyValueView<?> propAttribute = (EntityAttrPropertyValueView<?>)dialogAttribute;
            propAttribute.setPropertyValue(null);
            final String formName = getFormId(event.getComponent());
            FacesContext.getCurrentInstance().validationFailed();
            FacesContext.getCurrentInstance().addMessage(formName + "propertyValidationMessage",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "", e.getMessage()));
            return;
        }

        super.modifyPropertyValue(event);
    }

    /** Checks whether it is safe to add a new property (definition) to the entity.
     * @param entityProperties the list of properties the entity already has
     * @param propertyToAdd the property we want to add
     * @return <code>true</code> if the property is safe to add, <code>false</code> otherwise (it already exists).
     */
    private <T extends PropertyValue> boolean canAddProperty(final List<T> entityProperties,
                                                                        final Property propertyToAdd) {
        for (final T entityProperty : entityProperties) {
            if (entityProperty.getProperty().equals(propertyToAdd)) {
                return false;
            }
        }
        return true;
    }


    @Override
    protected boolean canDelete(EntityAttributeView<ComponentType> attribute) {
        return true;
    }

    @Override
    protected ComponentType getSelectedEntity() {
        final ComponentTypeView selectedComponent = componentTypeManager.getSelectedComponent();
        if (selectedComponent != null) {
            final Long typeId = selectedComponent.getId();
            // for "Add new device type" the selectedComponent will contain a ComponentType which has not been persisted
            if (typeId != null)
                return comptypeEJB.refreshEntity(selectedComponent.getComponentType());
            else
                return selectedComponent.getComponentType();
        }
        throw new IllegalArgumentException("No device type selected");
    }

    @Override
    protected ComptypePropertyValue newPropertyValue() {
        return new ComptypePropertyValue();
    }

    @Override
    protected Artifact newArtifact() {
        return new ComptypeArtifact();
    }

    /** This method returns a String representation of the property value.
     * @param prop the value of the property to show value for
     * @return the string representation
     */
    public String displayPropertyValue(MultiPropertyValueView prop) {
        final Value val = prop.getValue();
        return val == null ? "<Please define>" : Conversion.valueToString(val);
    }

    private ComptypePropertyValue createPropertyValue(final Property prop, final Value value) {
        final ComptypePropertyValue pv = new ComptypePropertyValue();
        pv.setComponentType(getSelectedEntity());
        pv.setProperty(prop);
        pv.setPropValue(value);
        return pv;
    }

    /** The save action for adding multiple property values to a device type. */
    public void saveMultiplePropertyValues() {
        try {
            int created = 0;
            for (final MultiPropertyValueView pv : selectedPropertyValues) {
                ComptypePropertyValue newValue = createPropertyValue(pv.getProperty(), pv.getValue());
                comptypeEJB.addChild(newValue, true);
                ++created;
            }
            componentTypeManager.refreshSelectedComponent();
            UiUtility.showInfoMessage(Entity.DEVICE_TYPE_PROPERTIES, Action.CREATE, created);
        } finally {
            resetFields();
            populateAttributesList();
        }
    }

    /** @return the filteredPropertyValues */
    public List<MultiPropertyValueView> getFilteredPropertyValues() {
        return filteredPropertyValues;
    }

    /** @param filteredPropertyValues the filteredPropertyValues to set */
    public void setFilteredPropertyValues(List<MultiPropertyValueView> filteredPropertyValues) {
        this.filteredPropertyValues = filteredPropertyValues;
    }

    /** @return the selectedPropertyValues */
    public List<MultiPropertyValueView> getSelectedPropertyValues() {
        return selectedPropertyValues;
    }

    /** @param selectedPropertyValues the selectedPropertyValues to set */
    public void setSelectedPropertyValues(List<MultiPropertyValueView> selectedPropertyValues) {
        this.selectedPropertyValues = selectedPropertyValues;
    }

    /** @return the selectionPropertyValuesFiltered */
    public List<MultiPropertyValueView> getSelectionPropertyValuesFiltered() {
        return selectionPropertyValuesFiltered;
    }

    /** @param selectionPropertyValuesFiltered the selectionPropertyValuesFiltered to set */
    public void setSelectionPropertyValuesFiltered(List<MultiPropertyValueView> selectionPropertyValuesFiltered) {
        this.selectionPropertyValuesFiltered = selectionPropertyValuesFiltered;
    }

    /** @return the selectAllRows */
    public boolean isSelectAllRows() {
        return selectAllRows;
    }

    /** @param selectAllRows the selectAllRows to set */
    public void setSelectAllRows(boolean selectAllRows) {
        this.selectAllRows = selectAllRows;
    }

    /** @return The list of {@link Property} entities the user can select in the property table. */
    public List<Property> getFilteredProperties() {
        return filteredProperties;
    }

    /** @return the selectedProperties */
    public List<Property> getSelectedProperties() {
        return selectedProperties;
    }

    /** @param selectedProperties the selectedProperties to set */
    public void setSelectedProperties(List<Property> selectedProperties) {
        this.selectedProperties = selectedProperties;
    }

    /** @return the selectionPropertiesFiltered */
    public List<Property> getSelectionPropertiesFiltered() {
        return selectionPropertiesFiltered;
    }

    /** @param selectionPropertiesFiltered the selectionPropertiesFiltered to set */
    public void setSelectionPropertiesFiltered(List<Property> selectionPropertiesFiltered) {
        this.selectionPropertiesFiltered = selectionPropertiesFiltered;
    }

    public void resetPropertySelection(final String id){
        super.resetPropertySelection(id);
        filteredPropertyValues = Lists.newArrayList();
    }

    /**
     * Is used to enable/disable UI element for default value entering
     *
     * @param prop
     *          The property that may, or may not have a typed-in default value on UI
     * @return <code>true</code>, if type is a slot reference, or <code>false</code> en every other case
     */
    public boolean canHaveDefaultValue(MultiPropertyValueView prop) {
        return BooleanUtils.toBoolean(prop.getProperty().getSlotReference());
    }

    @Override
    public List<SelectItem> getAttributeKinds() {
        return attributeKinds;
    }
}
