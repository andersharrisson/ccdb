/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.ui;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.openepics.discs.conf.ejb.InstallationEJB;
import org.openepics.discs.conf.ejb.SlotEJB;
import org.openepics.discs.conf.ejb.SlotPairEJB;
import org.openepics.discs.conf.ejb.SlotRelationEJB;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotPair;
import org.openepics.discs.conf.ent.SlotRelation;
import org.openepics.discs.conf.ent.SlotRelationName;
import org.openepics.discs.conf.ui.trees.FilteredTreeNode;
import org.openepics.discs.conf.ui.trees.SlotRelationshipTree;
import org.openepics.discs.conf.ui.util.Action;
import org.openepics.discs.conf.ui.util.ConnectsManager;
import org.openepics.discs.conf.ui.util.Entity;
import org.openepics.discs.conf.ui.util.Summary;
import org.openepics.discs.conf.ui.util.UiUtility;
import org.openepics.discs.conf.util.Utility;
import org.openepics.discs.conf.views.SlotRelationshipView;
import org.openepics.discs.conf.views.SlotView;
import org.primefaces.context.RequestContext;
import org.primefaces.model.TreeNode;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.Lists;

/**
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
@Named
@ViewScoped
public class RelationshipController implements Serializable {
    private static final long serialVersionUID = 1L;

    private static final String RELATIONSHIP_SELF_ERROR = "The slot cannot be in relationship with itself.";
    private static final String RELATIONSHIP_EXISTS_ERROR = "This relationship already exists.";

    @Inject private SlotEJB slotEJB;
    @Inject private InstallationEJB installationEJB;
    @Inject private SlotRelationEJB slotRelationEJB;
    @Inject private SlotPairEJB slotPairEJB;
    @Inject private ConnectsManager connectsManager;
    @Inject private HierarchiesController hierarchiesController;

    private List<SlotRelationshipView> relationships;
    private List<SlotRelationshipView> filteredRelationships;
    private List<SelectItem> relationshipTypes;

    private List<SlotRelationshipView> selectedRelationships;

    private SlotRelationshipView editedRelationshipView;
    private List<String> relationshipTypesForDialog;
    private Map<String, SlotRelation> slotRelationBySlotRelationStringName;

    private SlotRelationshipTree containsTree;

    /**
     * Orders the {@link SlotPair}s according to the relationship type and parent {@link Slot}
     */
    private class SlotPairComparator implements Comparator<SlotPair> {
        private final Slot slot;

        public SlotPairComparator(final Slot slot) {
            this.slot = slot;
        }

        @Override
        public int compare(SlotPair o1, SlotPair o2) {
            if (o1 == null) return (o2 == null ? 0 : 1);
            if (o2 == null) return -1;
            if (o1.getSlotRelation().getName() == SlotRelationName.CONTAINS) {
                // o1: CONTAINS
                if (o2.getSlotRelation().getName() == SlotRelationName.CONTAINS) {
                    // o2: CONTAINS
                    if (o1.getParentSlot().equals(slot)) {
                        // o1 is "CONTAINS" (not "CONTAINED IN")
                        if (o2.getParentSlot().equals(slot)) {
                            // o2 is also "CONTAINS"
                            return creationDifference(o1, o2);
                        } else {
                            // o2 is "CONTAINED IN", which means o1 comes after o2
                            return 1;
                        }
                    } else {
                        // o1 is "CONTAINED IN"
                        if (o2.getParentSlot().equals(slot)) {
                            // o2 is "CONTAINS"
                            return -1;
                        } else {
                            // o2 is also "CONTAINED IN"
                            return creationDifference(o1, o2);
                        }
                    }
                } else {
                    // only o1 is "CONTAINS", o2 isn't
                    return -1;
                }
            } else if (o2.getSlotRelation().getName()  == SlotRelationName.CONTAINS) {
                // only o2 is "CONTAINS"
                return 1;
            } else {
                // no relationship is "CONTAINS"
                return creationDifference(o1, o2);
            }
        }

        private int creationDifference(final SlotPair o1, final SlotPair o2) {
            return Long.compare(o1.getId(), o2.getId());
        }
    }

    private boolean editExistingRelationship;

    public RelationshipController() {}

    /** Java EE post construct life-cycle method. */
    @PostConstruct
    public void init() {
        relationshipTypes = buildRelationshipTypeList();

    	final SlotView rootView = new SlotView(slotEJB.getRootNode(), null, 1, slotEJB);
    	containsTree = new SlotRelationshipTree(SlotRelationName.CONTAINS, slotEJB, installationEJB, slotRelationEJB);
    	containsTree.setRootNode(new FilteredTreeNode<SlotView>(rootView, null, containsTree));
    }

    /** Tell this bean which {@link HierarchiesController} is its "master"
     * @param hierarchiesController the {@link HierarchiesController} master bean
     */
    protected void setUIParent(HierarchiesController hierarchiesController) {
        this.hierarchiesController = hierarchiesController;
    }

    private List<SelectItem> buildRelationshipTypeList() {
        Builder<SelectItem> immutableListBuilder = ImmutableList.builder();
        immutableListBuilder.add(new SelectItem("", "Select one"));

        final List<SlotRelation> slotRelations = slotRelationEJB.findAll();
        slotRelations.sort(new Comparator<SlotRelation>() {
            @Override
            public int compare(SlotRelation o1, SlotRelation o2) {
                return o1.getNameAsString().compareTo(o2.getNameAsString());
            }});
        // LinkedHashMap preserves the ordering. Important for UI.
        slotRelationBySlotRelationStringName = new LinkedHashMap<>();
        for (final SlotRelation slotRelation : slotRelations) {
            immutableListBuilder.add(new SelectItem(slotRelation.getNameAsString(), slotRelation.getNameAsString()));
            immutableListBuilder.add(new SelectItem(slotRelation.getIname(), slotRelation.getIname()));
            slotRelationBySlotRelationStringName.put(slotRelation.getNameAsString(), slotRelation);
            slotRelationBySlotRelationStringName.put(slotRelation.getIname(), slotRelation);
        }
        if (connectsManager.getCableDBStatus()) {
            immutableListBuilder.add(new SelectItem(connectsManager.getRelationshipName(),
                    connectsManager.getRelationshipName()));
        }

        relationshipTypesForDialog = ImmutableList.copyOf(slotRelationBySlotRelationStringName.keySet().iterator());
        return immutableListBuilder.build();
    }

    /** Add the relationship information connected to one {@link Slot} to the relationship list.
     * @param slot the {@link Slot} to add the information for
     * @param forceInit if <code>true</code> recreate the relationship list even if it already contains some information
     * and add just the one {@link Slot} info to the new list. If <code>false</code> add the relationship information to
     * the already existing list.
     */
    protected void initRelationshipList(final Slot slot, final boolean forceInit) {
        if (forceInit || relationships == null) {
            relationships = Lists.newArrayList();
        }
        addToRelationshipList(slot);
    }

    private void addToRelationshipList(final Slot slot) {
        final Slot rootSlot = slotEJB.getRootNode();
        final SlotPairComparator comparator = new SlotPairComparator(slot);
        final List<SlotPair> slotPairs = slotPairEJB.getSlotRelations(slot);
        slotPairs.sort(comparator);

        for (final SlotPair slotPair : slotPairs) {
            if (!slotPair.getParentSlot().equals(rootSlot)) {
                relationships.add(new SlotRelationshipView(slotPair, slot));
            }
        }

        final List<Slot> connectedSlots = connectsManager.getSlotConnects(slot);
        for (final Slot targetSlot : connectedSlots) {
            relationships.add(new SlotRelationshipView(slot.getId()+"c"+targetSlot.getId(), slot, targetSlot,
                    connectsManager.getRelationshipName()));
        }
    }

    /** Remove the relationships connected to one {@link Slot} from the relationship list and the
     * list of selected relationships if necessary.
     * @param slot the {@link Slot} to remove the information for
     */
    protected void removeRelatedRelationships(final Slot slot) {
        final ListIterator<SlotRelationshipView> relationsList = relationships.listIterator();
        while (relationsList.hasNext()) {
            final SlotRelationshipView relationshipView = relationsList.next();
            if (slot.getName().equals(relationshipView.getSourceSlotName())) {
                relationsList.remove();
            }
        }
        if (selectedRelationships!= null) {
            Iterator<SlotRelationshipView> i = selectedRelationships.iterator();
            while (i.hasNext()) {
                SlotRelationshipView selectedRelationship = i.next();
                if (selectedRelationship.getSourceSlotName().equals(slot.getName())) i.remove();
            }
        }
    }

    /** Called from {@link HierarchiesController} when the user deselects everything. */
    protected void clearRelationshipInformation() {
        relationships = null;
        filteredRelationships = null;
        selectedRelationships = null;
    }

    /**
     * This method gets called when the relationship processing is complete or dialog is simply closed. It properly
     * refreshes the selected node state so that the hierarchy behaves consistently.
     * The currently selected slot is also refreshed, so that new relationship data is displayed.
     */
    public void onRelationshipPopupClose() {
        // clear the previous dialog selection for the next use
        if (editedRelationshipView != null) {
            if (editedRelationshipView.getTargetNode() != null) {
                editedRelationshipView.getTargetNode().setSelected(false);
            }

            editedRelationshipView = null;
        }
    }

    /** Called when button to delete relationship is clicked */
    public void onRelationshipDelete() {
        if (canRelationshipBeDeleted()) {
            int deletedRelationships = 0;
            for (SlotRelationshipView selectedRelationship : selectedRelationships) {
                final SlotPair slotPairToBeRemoved = selectedRelationship.getSlotPair();
                final Long parentSlotId = slotPairToBeRemoved.getParentSlot().getId();
                final Long childSlotId = slotPairToBeRemoved.getChildSlot().getId();
                relationships.remove(selectedRelationship);
                // update contained slots, because removal needs to be logged
                slotPairToBeRemoved.getParentSlot().getPairsInWhichThisSlotIsAParentList().remove(slotPairToBeRemoved);
                slotPairToBeRemoved.getChildSlot().getPairsInWhichThisSlotIsAChildList().remove(slotPairToBeRemoved);
                slotPairEJB.delete(slotPairToBeRemoved);
                selectedRelationship = null;
                hierarchiesController.refreshTrees(new HashSet<>(Arrays.asList(childSlotId, parentSlotId)));
                ++deletedRelationships;
            }
            UiUtility.showUniformMessage(Summary.SUCCESS, Entity.RELATIONSHIPS, Action.DELETE, deletedRelationships);
            selectedRelationships = null;
        } else {
            RequestContext.getCurrentInstance().execute("PF('cantDeleteRelation').show();");
        }
    }

    /** Tells the UI whether the relationship "Edit" button can be enabled.
     * @return <code>true</code> if only one relationship is selected and is of correct type,
     * <code>false</code> otherwise.
     */
    public boolean canRelationshipBeEdited() {
        if (selectedRelationships == null || selectedRelationships.size() != 1) return false;
        if (selectedRelationships.get(0).getSlotPair() == null) return false;
        if (selectedRelationships.get(0).getSlotPair().getSlotRelation().getName() == SlotRelationName.CONTAINS) {
            return false;
        }
        return true;
    }

    private boolean canRelationshipBeDeleted() {
        if (selectedRelationships == null || selectedRelationships.size() == 0) return false;
        for (SlotRelationshipView selectedRelationship : selectedRelationships) {
            if (selectedRelationship.getSlotPair() == null) return false;
            if (selectedRelationship.getSlotPair().getSlotRelation().getName().equals(SlotRelationName.CONTAINS)
                && !slotPairEJB.slotHasMoreThanOneContainsRelation(selectedRelationship.getSlotPair().getChildSlot())) return false;
        }
        return true;
    }

    /** Prepares data for editing new relationship */
    public void prepareEditRelationshipPopup() {
        Preconditions.checkState((selectedRelationships != null) && (selectedRelationships.size() == 1));

        containsTree.unselectAllTreeNodes();
        editExistingRelationship = true;
        // setups the dialog
        final SlotRelationshipView v = selectedRelationships.get(0);
        editedRelationshipView = new SlotRelationshipView(v.getSlotPair(), v.getSourceSlot());

        final FilteredTreeNode<SlotView> node = containsTree.findNode(editedRelationshipView.getTargetSlot());
        node.setSelected(true);
        editedRelationshipView.setTargetNode(node);

        // modify relationship types drop down menu
        relationshipTypesForDialog = slotRelationBySlotRelationStringName.entrySet().stream()
                .filter(e -> !e.getValue().getName().equals(SlotRelationName.CONTAINS)).map(Entry::getKey).collect(Collectors.toList());
    }

    /** Prepares data for adding new relationship(s) */
    public void prepareAddRelationshipPopup() {
        Preconditions.checkNotNull(hierarchiesController.getSelectedNodeSlot());
        Preconditions.checkState(hierarchiesController.isSingleNodeSelected());

        containsTree.unselectAllTreeNodes();
        editExistingRelationship = false;
        // clear the previous dialog selection in case the dialog was already used before
        editedRelationshipView = new SlotRelationshipView(null, hierarchiesController.getSelectedNodeSlot());

        // modify relationship types drop down menu
        if (hierarchiesController.getSelectedNodeSlot().isHostingSlot()) {
            relationshipTypesForDialog = ImmutableList.copyOf(slotRelationBySlotRelationStringName.keySet().iterator());
            editedRelationshipView.setRelationshipName(SlotRelationName.CONTROLS.toString());
        } else {
            relationshipTypesForDialog = ImmutableList.of(SlotRelationName.CONTAINS.toString(),
                    SlotRelationName.CONTAINS.inverseName());
            editedRelationshipView.setRelationshipName(SlotRelationName.CONTAINS.toString());
        }
    }

    /**
     * Called when user clicks add button to add new relationship. Relationship is added if this does not
     * cause a loop on CONTAINS relationships
     */
    public void onRelationshipAdd() {
        try {
            if (editExistingRelationship) {
                editExistingRelationship();
            } else {
                addNewRelationships();
                clearRelationshipInformation();
                initRelationshipList(hierarchiesController.getSelectedNodeSlot(), true);
            }
        } finally {
            onRelationshipPopupClose();
        }
    }

    private void editExistingRelationship() {
        Preconditions.checkNotNull(editedRelationshipView.getSlotPair());
        final SlotRelation newSlotRelation =
                slotRelationBySlotRelationStringName.get(editedRelationshipView.getRelationshipName());
        final Slot parentSlot;
        final Slot childSlot;
        // if the user selects a new target slot the previous target is considered "lost"
        final Slot lostSlot;
        if (newSlotRelation.getNameAsString().equals(editedRelationshipView.getRelationshipName())) {
            childSlot = editedRelationshipView.getTargetNode().getData().getSlot();
            parentSlot = editedRelationshipView.getSourceSlot();
            lostSlot = getLostSlot(editedRelationshipView.getSlotPair(), parentSlot, childSlot);
        } else {
            childSlot = editedRelationshipView.getSourceSlot();
            parentSlot = editedRelationshipView.getTargetNode().getData().getSlot();
            lostSlot = getLostSlot(editedRelationshipView.getSlotPair(), childSlot, parentSlot);
        }

        if (!canEditRelationshipProceed(parentSlot, childSlot, newSlotRelation)) {
            return;
        }

        // before creating a new relationship, we need to find the original one in the parent and the child
        final SlotPair editedSlotPair = editedRelationshipView.getSlotPair();
        final int parentRelationshipIndex = parentSlot.getPairsInWhichThisSlotIsAParentList().indexOf(editedSlotPair);
        final int childRelationshipIndex = childSlot.getPairsInWhichThisSlotIsAChildList().indexOf(editedSlotPair);

        if (lostSlot != null) {
            // update the "lost" slot. We don't know in which list the relationship was, so we try both.
            lostSlot.getPairsInWhichThisSlotIsAChildList().remove(editedSlotPair);
            lostSlot.getPairsInWhichThisSlotIsAParentList().remove(editedSlotPair);
        }

        // change the relationship
        editedSlotPair.setChildSlot(childSlot);
        editedSlotPair.setParentSlot(parentSlot);
        editedSlotPair.setSlotRelation(newSlotRelation);

        // update the relationship parent and child slots for logging
        if (parentRelationshipIndex > -1) {
            parentSlot.getPairsInWhichThisSlotIsAParentList().set(parentRelationshipIndex, editedSlotPair);
        } else {
            parentSlot.getPairsInWhichThisSlotIsAParentList().add(editedSlotPair);
        }

        if (childRelationshipIndex > -1) {
            childSlot.getPairsInWhichThisSlotIsAChildList().set(childRelationshipIndex, editedSlotPair);
        } else {
            childSlot.getPairsInWhichThisSlotIsAChildList().add(editedSlotPair);
        }

        // check the new relationship before committing everything
        if (slotPairEJB.slotPairCreatesLoop(editedSlotPair, childSlot)) {
            RequestContext.getCurrentInstance().execute("PF('slotPairLoopNotification').show();");
            return;
        }

        slotPairEJB.save(editedSlotPair);

        // save the lost slot for logging
        if (lostSlot != null) {
            slotEJB.save(lostSlot);
        }

        relationships.remove(selectedRelationships.get(0));
        relationships.add(new SlotRelationshipView(slotPairEJB.refreshEntity(editedSlotPair),
                                                selectedRelationships.get(0).getSourceSlot()));
        UiUtility.showUniformMessage(Summary.SUCCESS, Entity.RELATIONSHIP,
                                                    selectedRelationships.get(0).getRelationshipName(), Action.UPDATE);
        selectedRelationships = null;

        final boolean isContainsAdded = (newSlotRelation.getName() == SlotRelationName.CONTAINS);
        hierarchiesController.refreshTrees(new HashSet<>(Arrays.asList(childSlot.getId(), parentSlot.getId())));

        if (isContainsAdded && (parentSlot == hierarchiesController.getSelectedNodeSlot())) {
            hierarchiesController.expandFirstSelectedNode();
        }
    }

    /* This method returns the Slot that will be replaced by a new target, in case the user selected a new target.
     * If the user did not select a new target Slot but just changed the relationship type, this method return null.
     * The "source" is the Slot that cannot change.
     */
    private Slot getLostSlot(final SlotPair originalPair, final Slot source, final Slot newTarget) {
        final Long targetId = newTarget.getId();
        final Long sourceId = source.getId();
        if (sourceId.equals(originalPair.getParentSlot().getId())) {
            // source is parent
            if (targetId.equals(originalPair.getChildSlot().getId())) {
                // target did not change
                return null;
            } else {
                return originalPair.getChildSlot();
            }
        } else {
            // source is child
            if (targetId.equals(originalPair.getParentSlot().getId())) {
                // target did not change
                return null;
            } else {
                return originalPair.getParentSlot();
            }
        }
    }

    /* Performs various edit relationship checks, prints an error message. Returns false if the the checks fail,
     * true if the edit operation can proceed. If one of the checks fails, the method prints an error message.
     */
    private boolean canEditRelationshipProceed(final Slot parentSlot, final Slot childSlot,
                                                                                final SlotRelation newSlotRelation) {
        if (childSlot.equals(parentSlot)) {
            UiUtility.showUniformMessage(Summary.ERROR, Entity.RELATIONSHIP,
                    editedRelationshipView.getRelationshipName(), Action.UPDATE, RELATIONSHIP_SELF_ERROR);
            return false;
        }

        final SlotPair editedSlotPair = editedRelationshipView.getSlotPair();
        if (editedSlotPair.getChildSlot().equals(childSlot) &&
                editedSlotPair.getParentSlot().equals(parentSlot) &&
                editedSlotPair.getSlotRelation().equals(newSlotRelation)) {
            // nothing to do, relationship not modified
            return false;
        }

        if (!slotPairEJB.findSlotPairsByParentChildRelation(childSlot.getName(), parentSlot.getName(),
                newSlotRelation.getName()).isEmpty()) {
            UiUtility.showUniformMessage(Summary.ERROR, Entity.RELATIONSHIP,
                    editedRelationshipView.getRelationshipName(), Action.UPDATE,
                    RELATIONSHIP_EXISTS_ERROR);   // XXX why is this message not shown?!
            return false;
        }
        return true;
    }

    private void addNewRelationships() {
        // we will create new entities in memory and check all of them. Then we will add them in a single transaction
        Preconditions.checkArgument(!containsTree.getSelectedNodes().isEmpty());
        final SlotRelation slotRelation =
                slotRelationBySlotRelationStringName.get(editedRelationshipView.getRelationshipName());
        final boolean isForwardRelation = slotRelation.getNameAsString().equals(editedRelationshipView.getRelationshipName());

        final Slot sourceSlot = hierarchiesController.getSelectedNodeSlot();

        final List<SlotPair> newRelationships = Lists.newArrayList();
        final HashSet<Long> refreshList = new HashSet<Long>();
        refreshList.add(editedRelationshipView.getSourceSlot().getId());

        for (final TreeNode selectedNode : containsTree.getSelectedNodes()) {
            final Slot parentSlot;
            final Slot childSlot;
            if (isForwardRelation) {
                childSlot = ((SlotView) selectedNode.getData()).getSlot();
                parentSlot = sourceSlot;
                refreshList.add(childSlot.getId());
            } else {
                childSlot = sourceSlot;
                parentSlot = ((SlotView) selectedNode.getData()).getSlot();
                refreshList.add(parentSlot.getId());
            }

            if (childSlot.equals(parentSlot)) {
                UiUtility.showUniformMessage(Summary.ERROR, Entity.RELATIONSHIP, "", Action.CREATE, RELATIONSHIP_SELF_ERROR);
                return;
            }

            if (!slotPairEJB.findSlotPairsByParentChildRelation(childSlot.getName(), parentSlot.getName(),
                    slotRelation.getName()).isEmpty()) {
                UiUtility.showUniformMessage(Summary.ERROR, Entity.RELATIONSHIP, "", Action.CREATE,
                        RELATIONSHIP_EXISTS_ERROR);  // XXX why is this message not shown?!
                return;
            }
            final SlotPair newSlotPair = new SlotPair();

            newSlotPair.setChildSlot(childSlot);
            newSlotPair.setParentSlot(parentSlot);
            newSlotPair.setSlotRelation(slotRelation);

            if (slotPairEJB.slotPairCreatesLoop(newSlotPair, childSlot)) {
                RequestContext.getCurrentInstance().execute("PF('slotPairLoopNotification').show();");
                return;
               }

            newRelationships.add(newSlotPair);
        }
        slotPairEJB.add(newRelationships);

        relationships.addAll(newRelationships.stream().map(e -> new SlotRelationshipView(e, sourceSlot)).
                collect(Collectors.toList()));
        UiUtility.showUniformMessage(Summary.SUCCESS, Entity.RELATIONSHIPS, Action.CREATE, newRelationships.size());

        hierarchiesController.refreshTrees(refreshList);

        // expand tree if a single node gained new CONTAINS child(ren)
        final boolean isContainsAdded = (slotRelation.getName() == SlotRelationName.CONTAINS);
        if (isContainsAdded && (newRelationships.size() == 1) && isForwardRelation) {
            hierarchiesController.expandFirstSelectedNode();
        }

        containsTree.unselectAllTreeNodes();
    }

    /** Expands the selected node or entire tree */
    public void expandTreeNodes() {
        if (editExistingRelationship) {
            final FilteredTreeNode<SlotView> selectedNode = editedRelationshipView.getTargetNode();
            if (selectedNode == null) {
                expandOrCollapseNode(containsTree.getRootNode(), true);
            } else {
                expandOrCollapseNode(selectedNode, true);
            }
        } else if (!Utility.isNullOrEmpty(containsTree.getSelectedNodes())) {
            for (final FilteredTreeNode<SlotView> selectedSlotView : containsTree.getSelectedNodes()) {
                expandOrCollapseNode(selectedSlotView, true);
            }
        } else {
            expandOrCollapseNode(containsTree.getRootNode(), true);
        }
    }

    /** Collapses the selected node or entire tree */
    public void collapseTreeNodes() {
        if (editExistingRelationship) {
            final FilteredTreeNode<SlotView> selectedNode = editedRelationshipView.getTargetNode();
            if (selectedNode == null) {
                expandOrCollapseNode(containsTree.getRootNode(), false);
            } else {
                expandOrCollapseNode(selectedNode, false);
            }
        } else if (!Utility.isNullOrEmpty(containsTree.getSelectedNodes())) {
            for (final FilteredTreeNode<SlotView> selectedSlotView : containsTree.getSelectedNodes()) {
                expandOrCollapseNode(selectedSlotView, false);
            }
        } else {
            expandOrCollapseNode(containsTree.getRootNode(), false);
        }
    }

    private void expandOrCollapseNode(final FilteredTreeNode<SlotView> parent, final boolean expand) {
        parent.setExpanded(expand);
        for (final  FilteredTreeNode<SlotView> node : parent.getFilteredChildren()) {
            expandOrCollapseNode(node, expand);
        }
    }

    /** @return The list of relationships for the currently selected slot. */
    public List<SlotRelationshipView> getRelationships() {
        return relationships;
    }
    public void setRelationships(List<SlotRelationshipView> relationships) {
        this.relationships = relationships;
    }

    /** @return the {@link List} of relationship types to display in the filter drop down selection. */
    public List<SelectItem> getRelationshipTypes() {
        return relationshipTypes;
    }

    /** @return the filteredRelationships */
    public List<SlotRelationshipView> getFilteredRelationships() {
        return filteredRelationships;
    }
    /** @param filteredRelationships the filteredRelationships to set */
    public void setFilteredRelationships(List<SlotRelationshipView> filteredRelationships) {
        this.filteredRelationships = filteredRelationships;
    }

    /** @return the selectedRelationships */
    public List<SlotRelationshipView> getSelectedRelationships() {
        return selectedRelationships;
    }
    /** @param selectedRelationships the selectedRelationships to set */
    public void setSelectedRelationships(List<SlotRelationshipView> selectedRelationships) {
        this.selectedRelationships = selectedRelationships;
    }

    /** @return the relationshipTypesForDialog */
    public List<String> getRelationshipTypesForDialog() {
        return relationshipTypesForDialog;
    }
    /** @param relationshipTypesForDialog the relationshipTypesForDialog to set */
    public void setRelationshipTypesForDialog(List<String> relationshipTypesForDialog) {
        this.relationshipTypesForDialog = relationshipTypesForDialog;
    }

    /** @return the editedRelationshipView */
    public SlotRelationshipView getEditedRelationshipView() {
        return editedRelationshipView;
    }

    /**@return the contains tree */
	public SlotRelationshipTree getContainsTree() {
		return containsTree;
	}

	/** @return is existing relationship being edited */
	public boolean isEditExistingRelationship() {
	    return editExistingRelationship;
	}
}
