/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.services.fbs;

import com.google.common.base.Preconditions;
import org.apache.commons.lang.StringUtils;
import org.openepics.discs.conf.util.AppProperties;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Service bean class for classes which need access to CHESS/ITIP webservice
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Stateless
public class FbsService {
    private static final String COULD_NOT_RETRIEVE_INFORMATION_FROM_ITIP = "Could not retrieve information from ITIP.";
    private static final Logger LOGGER = Logger.getLogger(FbsService.class.getName());

    @Inject
    private AppProperties properties;

    public List<FbsElement> getAllFbsTags() {
        try (FbsClient client = createFbsClient()) {
            return client.getFbsListing();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, COULD_NOT_RETRIEVE_INFORMATION_FROM_ITIP, e);
        }
        return Collections.emptyList();
    }

    /**
     * Get FBS tag for exact match for ESS name. If ESS Name does not match or match is ambiguous (more then 1 FBS tags
     * are matching), method returns empty string.
     *
     * @param essName ESS Name
     * @return FBS tag
     */
    public String getFbsTagForEssName(final String essName) {
        Preconditions.checkNotNull(essName);

        try (FbsClient client = createFbsClient()) {
            final List<FbsElement> fbsListing = client.getFbsListingForEssName(essName);
            if (fbsListing != null && fbsListing.size() == 1) {
                return fbsListing.get(0).tag;
            }
        } catch (Exception e) {
            LOGGER.log(Level.INFO, COULD_NOT_RETRIEVE_INFORMATION_FROM_ITIP, e);
        }
        return "";
    }

    /**
     * Queries FBS data for FBS tag and deserializes it into a {@link FbsElement} list.
     *
     * @param tagPart query, FBS tag part
     * @param maxResults maximum number of results
     * @return {@link List} of matching {@link FbsElement}
     */
    public List<String> getFbsTagsForFbsTagQuery(final String tagPart, final int maxResults) {
        if (StringUtils.isNotEmpty(StringUtils.trim(tagPart))) {
            try (FbsClient client = createFbsClient()) {
                String tagSearch = "%" + StringUtils.trim(tagPart).toUpperCase() + "%";
                final List<FbsElement> fbsElements = client.getFbsListingForTagQuery(tagSearch, maxResults);
                return FbsUtil.readTags(fbsElements);
            } catch (Exception e) {
                LOGGER.log(Level.INFO,COULD_NOT_RETRIEVE_INFORMATION_FROM_ITIP + e.getMessage());
            }
        }
        return Collections.emptyList();
    }

    //---------------------------------------------------------------------------------------------------------

    private FbsClient createFbsClient() {
        return new FbsClient(properties.getProperty(AppProperties.FBS_APPLICATION_URL));
    }
}
