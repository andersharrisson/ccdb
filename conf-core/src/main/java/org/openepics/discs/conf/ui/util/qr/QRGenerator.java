/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.ui.util.qr;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.util.Matrix;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

/**
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
public class QRGenerator {

    private QRGenerator() { }

    /**
     * Creates a PDDocument containing QRcodes generated from dataForQR.
     *
     * @param dataForQR
     *              list of QRData from which to generate QR codes
     * @param columns
     *              amount of QR codes there should be horizontally on one page
     * @param rows
     *              amount of QR codes there should be vertically on one page
     * @param isLandscape
     *              <code>true</code> to generate pages in landscape, <code>false</code> for portrait
     * @return PDDocument containing QR codes
     * @throws IOException error in writing data to the PDF stream
     * @throws WriterException error in generating QR code
     */
    public static PDDocument generate(List<? extends QRData> dataForQR, int columns, int rows, boolean isLandscape)
            throws IOException, WriterException {
        return isLandscape ? generateLandscapeDocument(dataForQR, columns, rows)
                                    : generatePortraitDocument(dataForQR, columns, rows);
    }

    private static PDDocument generateLandscapeDocument(List<? extends QRData> dataForQR, int columns, int rows)
            throws IOException, WriterException {
        final PDDocument document = new PDDocument();
        PDPage page = new PDPage(PDRectangle.A4);
        page.setRotation(90);
        PDPageContentStream contentStream = new PDPageContentStream(document, page, AppendMode.APPEND, true);
        final PDFont font = PDType1Font.TIMES_ROMAN;

        // Calculating sizes
        final float width = (page.getCropBox().getUpperRightY() / (columns + 1));
        final float height = (page.getCropBox().getUpperRightX() / (rows + 1));
        // We want it to be a square so we take the smaller measure
        final float size = Math.min(width, height);
        // Get all whitespace remaining after we draw all QR codes
        final float whitespaceY = page.getCropBox().getUpperRightY() - columns * size;
        final float whitespaceX = page.getCropBox().getUpperRightX() - rows * size;

        // Keep track of how many QR's are on the current page
        int col = 0;
        int row = 0;

        // Start drawing QR codes
        for (final QRData data : dataForQR) {
            // If page is full we create another.
            if (col == columns) {
                ++row;
                col = 0;
            }
            if (row == rows) {
                contentStream.close();
                document.addPage(page);
                page = new PDPage(PDRectangle.A4);
                page.setRotation(90);
                contentStream = new PDPageContentStream(document, page, AppendMode.APPEND, true);
                col = 0;
                row = 0;
            }
            PDImageXObject pdImage = LosslessFactory.createFromImage(document, generateSingleQR(data.getURL(), (int) size, true));

            // Calculate position of QRcodes
            float y = (pdImage.getHeight() + whitespaceY / (columns + 1)) * col + whitespaceY / (columns + 1);
            float x = (pdImage.getWidth() + whitespaceX / (rows + 1)) * row + whitespaceX / (rows + 1);
            // Draw QRCode
            contentStream.drawImage(pdImage, x, y, size, size);

            // QR title setting
            contentStream.beginText();
            contentStream.setFont(font, calculateFontSize(size));
            final float textWidth = font.getStringWidth(data.getTitle());
            final float textHeight = font.getFontDescriptor().getFontBoundingBox().getHeight();
            contentStream.setTextMatrix(getLandscapeTransform(x, y, (int) size, textWidth, textHeight));
            // Draw QR title
            contentStream.newLineAtOffset(0, 0);
            contentStream.showText(data.getTitle());
            contentStream.endText();

            ++col;
        }
        contentStream.close();
        document.addPage(page);
        return document;
    }

    private static Matrix getLandscapeTransform(final float qrPosX, final float qrPosY, final int qrSize,
                    final float textWidth, final float textHeight) {
        AffineTransform transform = new AffineTransform();
        //
        // Because of rotation, x actually means the vertical offset, and y means horizontal offset
        //
        // x - is the upper edge of QR, so we move it to its bottom
        final double textXPosition = qrPosX + qrSize
                // then we additionally move it my half the height of the font (0.5 line)
                + (textHeight / 1000 * qrSize / 10) / 2;
        // y - is the left edge of QR, so we move it to its middle
        final double textYPosition = qrPosY + qrSize / 2
                // then we move it start by half the text width to the left
                - (textWidth / 1000 * qrSize / 10) / 2;
        transform.translate(textXPosition, textYPosition);
        transform.rotate(Math.PI / 2);

        return new Matrix(transform);
    }

    private static PDDocument generatePortraitDocument(List<? extends QRData> dataForQR, int columns, int rows)
            throws IOException, WriterException {
        final PDDocument document = new PDDocument();
        PDPage page = new PDPage(PDRectangle.A4);
        PDPageContentStream contentStream = new PDPageContentStream(document, page, AppendMode.APPEND, true);
        final PDFont font = PDType1Font.TIMES_ROMAN;

        // Calculating sizes
        final float width = (page.getCropBox().getUpperRightX() / (columns + 1));
        final float height = (page.getCropBox().getUpperRightY() / (rows + 1));
        // We want it to be a square so we take the smaller measure
        final float size = Math.min(width, height);
        // Get all whitespace remaining after we draw all QR codes
        final float whitespaceX = page.getCropBox().getUpperRightX() - columns * size;
        final float whitespaceY = page.getCropBox().getUpperRightY() - rows * size;

        // Keep track of how many QR's are on the current page
        int col = 0;
        int row = 0;

        // Start drawing QR codes
        for (final QRData data : dataForQR) {
            // If page is full we create another.
            if (col == columns) {
                ++row;
                col = 0;
            }
            if (row == rows) {
                contentStream.close();
                document.addPage(page);
                page = new PDPage(PDRectangle.A4);
                contentStream = new PDPageContentStream(document, page, AppendMode.APPEND, true);
                col = 0;
                row = 0;
            }
            PDImageXObject pdImage = LosslessFactory.createFromImage(document, generateSingleQR(data.getURL(), (int) size, false));

            // Calculate position of QRcodes
            float x = (pdImage.getWidth() + whitespaceX / (columns + 1)) * col + whitespaceX / (columns + 1);
            float y = (pdImage.getHeight() + whitespaceY / (rows + 1)) * (rows - row - 1) + whitespaceY / (rows + 1);
            // Draw QRCode
            contentStream.drawImage(pdImage, x, y, size, size);

            // QR title setting
            contentStream.beginText();
            contentStream.setFont(font, calculateFontSize(size));
            final float textWidth = font.getStringWidth(data.getTitle());
            final float textHeight = font.getFontDescriptor().getFontBoundingBox().getHeight();
            contentStream.setTextMatrix(getPortraitTransform(x, y, (int) size, textWidth, textHeight));
            // Draw QR title
            contentStream.newLineAtOffset(0, 0);
            contentStream.showText(data.getTitle());
            contentStream.endText();

            ++col;
        }
        contentStream.close();
        document.addPage(page);
        return document;
    }

    private static Matrix getPortraitTransform(final float qrPosX, final float qrPosY, final int qrSize,
            final float textWidth, final float textHeight) {
        AffineTransform transform = new AffineTransform();
        final double textXPosition = qrPosX + qrSize / 2
                - (textWidth / 1000 * qrSize / 10) / 2;
        final double textYPosition = qrPosY
                - (textHeight / 1000 * qrSize / 10) / 2;
        transform.translate(textXPosition, textYPosition);

        return new Matrix(transform);
    }

    private static BufferedImage generateSingleQR(String myCodeText, int size, boolean rotated) throws WriterException {
        Map<EncodeHintType, Object> hintMap = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
        hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");

        // Now with zxing version 3.2.1 you could change border size (white border size to just 1)
        hintMap.put(EncodeHintType.MARGIN, 1); /* default = 4 */
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = null;
        bitMatrix = qrCodeWriter.encode(myCodeText, BarcodeFormat.QR_CODE, size, size, hintMap);
        int crunchifyWidth = bitMatrix.getWidth();
        BufferedImage image = new BufferedImage(crunchifyWidth, crunchifyWidth, BufferedImage.TYPE_INT_RGB);
        image.createGraphics();

        Graphics2D graphics = (Graphics2D) image.getGraphics();
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, crunchifyWidth, crunchifyWidth);
        graphics.setColor(Color.BLACK);

        for (int x = 0; x < crunchifyWidth; x++) {
            for (int y = 0; y < crunchifyWidth; y++) {
                if (bitMatrix.get(x, y)) {
                    if (rotated) {
                        graphics.fillRect(y, (crunchifyWidth - x - 1), 1, 1);
                    } else {
                        graphics.fillRect(x, y, 1, 1);
                    }
                }
            }
        }
        return image;
    }

    /**
     * Calculate font size based on the size of the QR code.
     *
     * @param size
     * @return
     */
    private static float calculateFontSize(float size) {
        return (size / 10) - 1;
    }
}
