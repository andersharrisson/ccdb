/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.ui;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.PostActivate;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.omnifaces.cdi.ViewScoped;
import org.openepics.discs.conf.dl.annotations.SignalsLoader;
import org.openepics.discs.conf.dl.annotations.SlotsLoader;
import org.openepics.discs.conf.dl.common.DataLoader;
import org.openepics.discs.conf.dl.common.DataLoaderResult;
import org.openepics.discs.conf.ejb.ComptypeEJB;
import org.openepics.discs.conf.ejb.InstallationEJB;
import org.openepics.discs.conf.ejb.SlotEJB;
import org.openepics.discs.conf.ejb.SlotPairEJB;
import org.openepics.discs.conf.ejb.SlotRelationEJB;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.InstallationRecord;
import org.openepics.discs.conf.ent.Property;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotPair;
import org.openepics.discs.conf.ent.SlotRelationName;
import org.openepics.discs.conf.ui.common.AbstractExcelSingleFileImportUI;
import org.openepics.discs.conf.ui.common.DataLoaderHandler;
import org.openepics.discs.conf.ui.common.UIException;
import org.openepics.discs.conf.ui.export.ExportSimpleTableDialog;
import org.openepics.discs.conf.ui.export.SimpleTableExporter;
import org.openepics.discs.conf.ui.trees.BasicTreeNode;
import org.openepics.discs.conf.ui.trees.ConnectsTree;
import org.openepics.discs.conf.ui.trees.FilterMatchMode;
import org.openepics.discs.conf.ui.trees.FilteredTreeNode;
import org.openepics.discs.conf.ui.trees.SlotRelationshipTree;
import org.openepics.discs.conf.ui.trees.Tree;
import org.openepics.discs.conf.ui.util.Action;
import org.openepics.discs.conf.ui.util.ConnectsManager;
import org.openepics.discs.conf.ui.util.Entity;
import org.openepics.discs.conf.ui.util.ExportSimpleSlotsTableDialog;
import org.openepics.discs.conf.ui.util.SlotRelationshipManager;
import org.openepics.discs.conf.ui.util.UiUtility;
import org.openepics.discs.conf.ui.util.qr.QRData;
import org.openepics.discs.conf.ui.util.qr.QRGenerator;
import org.openepics.discs.conf.ui.util.qr.QRSlotData;
import org.openepics.discs.conf.util.AppProperties;
import org.openepics.discs.conf.util.AutoCompleteQuery;
import org.openepics.discs.conf.util.AutoCompleteWsQuery;
import org.openepics.discs.conf.util.CCDBRuntimeException;
import org.openepics.discs.conf.util.DeleteOnCloseFileInputStream;
import org.openepics.discs.conf.util.PropertyValueNotUniqueException;
import org.openepics.discs.conf.services.slot.SlotValidationService;
import org.openepics.discs.conf.util.Utility;
import org.openepics.discs.conf.services.fbs.FbsCache;
import org.openepics.discs.conf.services.fbs.FbsService;
import org.openepics.discs.conf.services.names.Names;
import org.openepics.discs.conf.services.names.Names.NameStatus;
import org.openepics.discs.conf.views.SlotView;
import org.openepics.names.jaxb.DeviceNameElement;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.json.JSONArray;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.net.MediaType;
import com.google.zxing.WriterException;

import joptsimple.internal.Strings;
import org.primefaces.PrimeFaces;

/**
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
@Named
@ViewScoped
public class HierarchiesController extends AbstractExcelSingleFileImportUI
        implements SimpleTableExporter, SlotRelationshipManager, Serializable {
    private static final long serialVersionUID = 2743408661782529373L;

    private static final int NAME_AUTOCOMPLETE_MAX_RESULTS = 20;

    private static final Logger LOGGER = Logger.getLogger(HierarchiesController.class.getCanonicalName());

    private static final String CANNOT_PASTE_INTO_ROOT = "The following slots cannot be made hierarchy roots:";
    private static final String CANNOT_PASTE_INTO_SLOT = "The following containers cannot become children of slot:";
    private static final String CANNOT_PASTE_INTO_SELF = ""
            + "The following containers cannot become children of themselves:";

    /** The device page part of the URL containing all the required parameters already. */
    private static final String NAMING_DEVICE_PAGE = "devices.xhtml?i=2&deviceName=";

    private static final String CABLEDB_DEVICE_PAGE = "cables.xhtml?cableName=";

    @Inject
    private SlotEJB slotEJB;
    @Inject
    private SlotPairEJB slotPairEJB;
    @Inject
    private InstallationEJB installationEJB;
    @Inject
    private SlotRelationEJB slotRelationEJB;
    @Inject
    private ComptypeEJB comptypeEJB;
    @Inject
    private Names names;
    @Inject
    private ConnectsManager connectsManager;
    @Inject
    private InstallationController installationController;
    @Inject
    private RelationshipController relationshipController;
    @Inject
    private SlotAttributeController slotAttributeController;
    @Inject
    private SlotValidationService slotValidationService;
    @Inject
    private FbsService fbsService;
    @Inject
    private FbsCache fbsCache;

    @Inject
    private DataLoaderHandler dataLoaderHandler;
    @Inject
    @SignalsLoader
    private DataLoader signalsDataLoader;
    @Inject
    @SlotsLoader
    private DataLoader slotsDataLoader;

    @Inject
    private AppProperties properties;

    @Resource
    UserTransaction utx;

    private enum ActiveTab {
        INCLUDES,
        POWERS,
        CONTROLS,
        CONNECTS,
    }

    private enum ClipboardOperations {
        COPY,
        CUT
    }

    public enum DisplayedNames {
        NAMES,
        FBSNAMES,
        BOTH
    }

    private HashSet<Long> selectedNodeIds;
    private HashSet<Long> expandedNodeIds = new HashSet<>();
    private HashSet<Long> displayedAttributeNodeIds;
    private String requestedSlot;

    // ---- variables for hierarchies and tabs --------
    private Tree<SlotView> selectedTree;

    private transient SlotRelationshipTree containsTree;
    private transient SlotRelationshipTree powersTree;
    private transient SlotRelationshipTree controlsTree;
    private transient ConnectsTree connectsTree;

    /** <code>selectedSlot</code> is only initialized when there is only one node in the tree selected */
    private Slot selectedSlot;
    /** <code>selectedSlotView</code> is only initialized when there is only one node in the tree selected */
    private SlotView selectedSlotView;
    private String selectedSlotsCommonDeviceType;
    private ActiveTab activeTab;
    private transient List<FilteredTreeNode<SlotView>> clipboardSlots;
    private List<SlotView> pasteErrors;
    private ClipboardOperations clipboardOperation;
    private String pasteErrorReason;
    private transient List<FilteredTreeNode<SlotView>> nodesToDelete;
    private List<SlotView> slotsToDelete;
    private List<SlotView> filteredSlotsToDelete;
    private boolean restrictToConventionNames;

    // variables from the slot / containers editing merger.
    private String name;
    private String fbsName;
    private DisplayedNames displayedNames = DisplayedNames.NAMES;
    private String originalName;
    private String originalFbsName;
    private String previousName;
    private String description;
    /** Used in "add child to parent" operations. This usually reflects the <code>selectedNode</code>. */
    private boolean isInstallationSlot;
    private boolean hasDevice;
    private String deviceType;
    private String parentName;
    private boolean isNewSlot;

    private String namingRedirectionUrl;
    private String cableRedirectionUrl;

    private SlotView linkSlot;

    private transient ExportSimpleSlotsTableDialog simpleTableExporterDialog;

    private transient String containsModeTriState;

    private transient QrPdfDialog qrPdfDialog;

    // CCDB-572: store for common slot properties selected for retain by user
    private transient List<PropertySelection> deviceTypesCommonProperties;

    // CCDB-572: class for selected properties
    public static class PropertySelection {
        private Property property;
        private Boolean included;

        public PropertySelection(Property property, Boolean included) {
            this.property = property;
            this.included = included;
        }

        public Property getProperty() {
            return property;
        }

        public void setProperty(Property property) {
            this.property = property;
        }

        public Boolean getIncluded() {
            return included;
        }

        public void setIncluded(Boolean included) {
            this.included = included;
        }
    }

    private final transient AutoCompleteQuery<String> nameAutoComplete = new AutoCompleteWsQuery() {
        @Override
        public List<String> getData() {
            return Lists.newArrayList(names.getActiveNames());
        }
    };

    private static class FbsTagSearchAction implements AutoCompleteWsQuery {
        private final FbsService service;
        private List<String> validFbsNames = Collections.emptyList();

        public FbsTagSearchAction(final FbsService fbsService) {
            this.service = fbsService;
        }

        @Override
        public List<String> getData() {
            return validFbsNames;
        }

        @Override
        public List<String> autoCompleteResult(Map<String, Object> params, Integer limit) {
            String query = params.get(QUERY_PARAM).toString();
            validFbsNames = service.getFbsTagsForFbsTagQuery(query, limit);
            return AutoCompleteWsQuery.super.autoCompleteResult(ImmutableMap.of(AutoCompleteQuery.QUERY_PARAM, query),
                    limit);
        }
    }

    public class QrPdfDialog {
        private int rows;
        private int columns;
        private boolean isLandscape;
        private final List<String> qrList;

        public QrPdfDialog(final List<String> qrList) {
            rows = 3;
            columns = 3;
            isLandscape = true;
            this.qrList = qrList;
        }

        @NotNull
        @Min(value = 1, message = "The number of rows cannot be less than 1.")
        @Max(value = 8, message = "The number of rows cannot be greater than 8.")
        public int getRows() {
            return rows;
        }

        public void setRows(int rows) {
            this.rows = rows;
        }

        @NotNull
        @Min(value = 1, message = "The number of columns cannot be less than 1.")
        @Max(value = 8, message = "The number of columns cannot be greater than 8.")
        public int getColumns() {
            return columns;
        }

        public void setColumns(int columns) {
            this.columns = columns;
        }

        @NotNull
        public boolean isLandscape() {
            return isLandscape;
        }

        public void setLandscape(boolean isLandscape) {
            this.isLandscape = isLandscape;
        }

        public StreamedContent getDownload() {
            LOGGER.log(Level.FINE, "Generating the PDF.");
            try {
                final String tempFolderPath = System.getProperty("java.io.tmpdir");
                final File tempFile = File.createTempFile("ccdbQRs", ".pdf", new File(tempFolderPath));

                List<QRData> qrData = qrList.stream().map(QRSlotData::new).collect(Collectors.toList());

                final PDDocument pdfDocument = QRGenerator.generate(qrData, columns, rows, isLandscape);
                pdfDocument.save(tempFile);
                pdfDocument.close();

                final DeleteOnCloseFileInputStream pdfStream = new DeleteOnCloseFileInputStream(tempFile);
                return new DefaultStreamedContent(pdfStream, MediaType.PDF.toString(), "ccdb_qrcode.pdf");
            } catch (IOException | WriterException e) {
                UiUtility.showErrorMessage(Entity.QR_PDF, "", Action.GENERATE, "", e);
                LOGGER.log(Level.WARNING, "Cannot produce QR PDF.", e);
                return null;
            } finally {
                qrPdfDialog = null;
            }
        }
    }

    private Long paramId;

    public Long getParamId() {
        return paramId;
    }

    public void setParamId(final Long paramId) {
        this.paramId = paramId;
        requestedSlot = "id:" + paramId;
        final Slot slot = slotEJB.findById(paramId);
        selectSlot(slot);
    }

    private String paramName;

    public String getParamName() {
        return paramName;
    }

    public void setParamName(final String paramName) {
        this.paramName = paramName;
        if (paramName != null) {
            requestedSlot = paramName;
            final Slot slot = getSlotFromName(paramName);
            selectSlot(slot);
        }
    }

    /** Java EE post construct life-cycle method. */
    @Override
    @PostConstruct
    public void init() {
        try {
            super.init();
            activeTab = ActiveTab.INCLUDES;
            containsModeTriState = "0";

            initHierarchies();
            initNamingInformation(false);

            simpleTableExporterDialog = new ExportSimpleSlotsTableDialog(this, slotEJB.getRootNode(), installationEJB);
            qrPdfDialog = new QrPdfDialog(null);
        } catch (Exception e) {
            throw new UIException("Hierarchies display initialization failed: " + e.getMessage(), e);
        }
    }

    @PostActivate
    public void postActivate() {
        containsModeTriState = "0";
        initNamingInformation(false);
        initHierarchies();
        simpleTableExporterDialog = new ExportSimpleSlotsTableDialog(this, slotEJB.getRootNode(), installationEJB);
    }

    private void initNamingInformation(boolean refresh) {
        boolean detectNamingStatus = names.isEnabled();

        restrictToConventionNames = detectNamingStatus
                && properties.getBooleanPropety(AppProperties.RESTRICT_TO_CONVENTION_NAMES);

        LOGGER.log(Level.FINE, "detectNamingStatus: " + Boolean.toString(detectNamingStatus));
        LOGGER.log(Level.FINE, "restrictToConventionNames: " + Boolean.toString(restrictToConventionNames));

        if(refresh){
            refreshNamingInformation();
        }
        namingRedirectionUrl = null;

        if (!detectNamingStatus) {
            return;
        }

        final String namingUrl = properties.getProperty(AppProperties.NAMING_APPLICATION_URL);

        if (Strings.isNullOrEmpty(namingUrl)) {
            if (detectNamingStatus) {
                LOGGER.log(Level.WARNING, AppProperties.NAMING_APPLICATION_URL + " not defined.");
            }
        } else {
            final StringBuilder redirectionUrl = new StringBuilder(namingUrl);
            if (redirectionUrl.charAt(redirectionUrl.length() - 1) != '/') {
                redirectionUrl.append('/');
            }
            redirectionUrl.append(NAMING_DEVICE_PAGE);
            namingRedirectionUrl = redirectionUrl.toString();
            LOGGER.log(Level.FINE, String.format("Naming url: %s", namingRedirectionUrl));
        }
    }

    /**
     * This method resets Add/Edit slot dialog properties when dialog is closed.
     */
    public void resetProperties() {
        deviceTypesCommonProperties = null;
    }

    public void refreshNamingInformation() {
        LOGGER.log(Level.FINER, "refreshNamingInformation() :: Refreshing names list.");
        if (names.isEnabled()) {
            names.refresh();
            if (selectedSlotView != null) {
                Slot slot = selectedSlotView.getSlot();
                if (slot != null) {
                    slotValidationService.validateSlot(slot);
                    name = slot.getName();
                }
            }
        }
    }

    protected void saveSlotAndRefresh(final Slot slot) {
        slotEJB.save(slot);
        refreshSlot(slot);
    }

    protected void refreshSlot(final Slot slot) {
        final Slot freshSlot = slotEJB.refreshEntity(slot);
        if (selectedSlot != null) {
            selectedSlot = freshSlot;
        }
    }

    protected void refreshTrees(Set<Long> ids) {
        containsTree.refreshIds(ids);
        if (selectedTree == controlsTree) {
            controlsTree.refreshIds(ids);
        } else {
            controlsTree.reset();
        }

        if (selectedTree == powersTree) {
            powersTree.refreshIds(ids);
        } else {
            powersTree.reset();
        }

        if (selectedTree == connectsTree) {
            connectsTree.refreshIds(ids);
        } else {
            connectsTree.reset();
        }
    }

    private void selectSlot(final Slot slot) {
        if (slot != null) {
            selectSingleNode(slot);
        } else {
            UiUtility.showErrorMessage("The requested slot '" + requestedSlot
                    + "' could not be located in the database.");
        }
    }

    private String getNamingDescription(String name) {
        if (!names.isEnabled()) {
            return null;
        }

        if (name == null) {
            return null;
        }
        DeviceNameElement element = names.getAllNames().get(name);
        return (element != null) ? element.getDescription() : null;
    }

    public boolean linkToNaming(final SlotView slot) {
        return (namingRedirectionUrl != null) && names.isEnabled() && (slot != null)
                && (NameStatus.NONE != names.getNameStatus(slot.getName()));
    }

    /*
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Above
     * : bean initialization section and global private utility methods.
     *
     * Below: Screen population methods. These methods prepare the data to be displayed on the main UI screen. * * * * *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    private void updateDisplayedSlotInformation() {
        selectedSlotView = null;
        selectedSlot = null;
        if (Utility.isNullOrEmpty(selectedTree.getSelectedNodes())) {
            selectedNodeIds = null;
            displayedAttributeNodeIds = null;
            clearRelatedInformation();
        } else {
            initNodeIds();
            if (displayedAttributeNodeIds != null && !displayedAttributeNodeIds.isEmpty()) {
                // there are attributes displayed, remove the ones that are no longer selected
                removeUnselectedRelatedInformation();
            }
            // initialize the list of node IDs that have the related information displayed (if not already)
            if (displayedAttributeNodeIds == null) {
                displayedAttributeNodeIds = new HashSet<>();
            }
            // the related tables are ready for new items
            addRelatedInformationForNewSlots();

            if (selectedTree.getSelectedNodes().size() == 1) {
                selectSingleNode(selectedTree.getSelectedNodes().get(0));
            }
            // workaround for the PrimeFaces bug where multiple selection does not properly count the rows it must
            // display.
            PrimeFaces.current()
                    .executeScript("initializeDataTable(" + slotAttributeController.getAttributes().size() + ");");
        }
    }

    /* Remove attributes, relationships and installation information for slots no longer selected */
    private void removeUnselectedRelatedInformation() {
        for (final Iterator<Long> iter = displayedAttributeNodeIds.iterator(); iter.hasNext();) {
            final Long id = iter.next();
            if (!selectedNodeIds.contains(id)) {
                final Slot unselectedSlot = slotEJB.findById(id);
                if (unselectedSlot != null) {
                    slotAttributeController.clearRelatedAttributeInformation();
                    slotAttributeController.populateAttributesList();
                    relationshipController.removeRelatedRelationships(unselectedSlot);
                    installationController.removeRelatedInstallationRecord(unselectedSlot);
                    iter.remove();
                }
            }
        }
    }

    /* Add attributes, relationships and installation information for slots that are missing */
    private void addRelatedInformationForNewSlots() {
        for (final Long selectedId : selectedNodeIds) {
            if (!displayedAttributeNodeIds.contains(selectedId)) {
                // this slot doesn't have information in the related tables yet
                final Slot slotToAdd = slotEJB.findById(selectedId);
                relationshipController.initRelationshipList(slotToAdd, false);
                installationController.initInstallationRecordList(slotToAdd, false);
                displayedAttributeNodeIds.add(selectedId);
            }
        }

        slotAttributeController.clearRelatedAttributeInformation();
        slotAttributeController.populateAttributesList();
    }

    private void initNodeIds() {
        selectedNodeIds = new HashSet<>();
        for (final FilteredTreeNode<SlotView> node : selectedTree.getSelectedNodes()) {
            selectedNodeIds.add(node.getData().getId());
        }
    }

    /*
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Above: Screen population methods. These methods prepare the data to be displayed on the main UI screen.
     *
     * Below: Callback methods called from the main UI screen. E.g.: methods that are called when user user selects a
     * line in a table. * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * * * * * *
     */
    /* Clears all slot related information when user deselects the slots in the hierarchy. */
    private void clearRelatedInformation() {
        selectedSlotView = null;
        selectedSlot = null;
        selectedNodeIds = null;
        displayedAttributeNodeIds = null;
        slotAttributeController.clearRelatedAttributeInformation();
        installationController.clearInstallationInformation();
        relationshipController.clearRelationshipInformation();
    }

    private void selectSingleNode(final FilteredTreeNode<SlotView> selectedNode) {
        selectedSlotView = selectedNode.getData();
        selectedSlot = selectedSlotView.getSlot();
    }

    /**
     * The function to select a different node in the TreeTable by clicking on the link in the relationship table.
     *
     * @param slot
     *            the slot we want to switch to
     */
    public void selectSingleNode(final Slot slot) {
        FilteredTreeNode<SlotView> node = containsTree.findNode(slot);

        // the final slot found
        containsTree.unselectAllTreeNodes();
        clearRelatedInformation();
        fakeUISelection(node);
    }

    /**
     * The function to add a node to selection
     *
     * @param slot
     *            the slot we want to add to selection
     */
    public void selectNode(final Slot slot) {
        FilteredTreeNode<SlotView> node = containsTree.findNode(slot);
        fakeUISelection(node);
    }

    private void fakeUISelection(final FilteredTreeNode<SlotView> node) {
        selectedTree.getSelectedNodes().add(node);
        node.setSelected(true);
        updateDisplayedSlotInformation();
    }

    /**
     * Called when a user selects a new node in one of the hierarchy trees. This event is also triggered once if a range
     * of nodes is selected using the Shift+Click action.
     *
     * @param event
     *            Event triggered on node selection action
     */
    public void onNodeSelect(NodeSelectEvent event) {
        updateDisplayedSlotInformation();
        if (activeTab == ActiveTab.INCLUDES) {
            removeTreeData();
        }
    }

    /**
     * Called when a user deselects a new node in one of the hierarchy trees.
     *
     * @param event
     *            Event triggered on node deselection action
     */
    public void onNodeUnselect(NodeUnselectEvent event) {
        // in the callback, the selectedNodes no longer contains the unselected node
        updateDisplayedSlotInformation();
        if (activeTab == ActiveTab.INCLUDES) {
            removeTreeData();
        }
    }

    public void onNodeExpand(NodeExpandEvent event) {
        try {
            FilteredTreeNode<SlotView> node = (FilteredTreeNode<SlotView>) event.getTreeNode();
            expandedNodeIds.add(node.getData().getId());
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Expanded node id cannot be retrieved", e);
            return;
        }
    }

    public void onNodeCollapse(NodeCollapseEvent event) {
        try {
            FilteredTreeNode<SlotView> node = (FilteredTreeNode<SlotView>) event.getTreeNode();
            expandOrCollapseNode(node,  false);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Collapsed node id cannot be retrieved", e);
            return;
        }
    }

    /**
     * The event is triggered when the hierarchy tab is changed.
     *
     * @param event
     *            the event
     */
    public void onTabChange(TabChangeEvent event) {
        final List<FilteredTreeNode<SlotView>> masterNodes = !containsTree.getSelectedNodes().isEmpty()
                ? containsTree.getSelectedNodes() : Arrays.asList(containsTree.getRootNode());

        ActiveTab newActiveTab = ActiveTab.valueOf(event.getTab().getId());

        switch (newActiveTab) {
        case INCLUDES:
            selectedTree = containsTree;
            break;
        case POWERS:
            powersTree.initHierarchy(masterNodes);
            selectedTree = powersTree;
            break;
        case CONTROLS:
            controlsTree.initHierarchy(masterNodes);
            selectedTree = controlsTree;
            break;
        case CONNECTS:
            connectsTree.initHierarchy(masterNodes);
            selectedTree = connectsTree;
            break;
        }

        activeTab = newActiveTab;
        updateDisplayedSlotInformation();
    }

    private void removeTreeData() {
        // remove other trees
        powersTree.reset();
        controlsTree.reset();
        connectsTree.reset();
    }

    @Override
    public void setDataLoader() {
        dataLoader = signalsDataLoader;
    }

    @Override
    public void doImport() {
        try (InputStream inputStream = new ByteArrayInputStream(importData)) {
            DataLoaderResult loaderResult = dataLoaderHandler.loadData(inputStream, dataLoader);
            setLoaderResult(loaderResult != null ? loaderResult : new DataLoaderResult());
            initHierarchies();
            initNamingInformation(loaderResult != null);
            clearRelatedInformation();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected boolean isIdenticalRecordInstalled(String name, String installation) {
        Slot slot = slotEJB.findByName(name);

        InstallationRecord installationRecord = installationEJB.getActiveInstallationRecordForSlot(slot);

        if (installationRecord != null) {
            final String installationRecordName = installationRecord.getDevice().getName();
            final Date installDate = installationRecord.getInstallDate();
            final Date uninstallDate = installationRecord.getUninstallDate();
            return name.equals(slot.getName())
                    && installation.equals(installationRecordName)
                    && installDate != null
                    && uninstallDate == null;
        } else {
            return false;
        }
    }

    /**
     * Returns the database id of an {@link Slot} based on its name.
     *
     * @param slotName
     *            the name of the slot to search for
     * @return the slot, <code>null</code> if such slot was not found or if it is not an installation slot.
     */
    private Slot getSlotFromName(final String slotName) {
        if (Strings.isNullOrEmpty(slotName)) {
            return null;
        }

        final Slot slot = slotEJB.findByNameAndIsHostingSlot(slotName, true);

        return (slot == null || !slot.isHostingSlot()) ? null : slot;
    }

    /**
     * Calculates the CSS class name for the slot in question.
     *
     * @param slot
     *            the {@link Slot} to calculate the CSS class name for
     * @return the name of the CSS class
     */
    public String calcNameClass(final SlotView slot) {
        Preconditions.checkNotNull(slot);
        if (!slot.isHostingSlot()) {
            return "nameContainer";
        }

        switch (names.getNameStatus(slot.getName())) {
        case ACTIVE:
            return "nameActive";
        case NONE:
            return "nameMissing";
        case OBSOLETE:
            return "nameObsolete";
        case DELETED:
            return "nameDeleted";
        default:
            return "nameMissing";
        }
    }


    /**
     * Calculates the CSS class fbsName for the slot in question.
     *
     * @param slot
     *            the {@link Slot} to calculate the CSS class fbsName for
     * @return the fbsName of the CSS class
     */
    public String calcFbsNameClass(final SlotView slot) {
        Preconditions.checkNotNull(slot);

        if (getValidFbsNames().contains(slot.getFbsName())) {
            if (!slotEJB.isSlotWithFbsNameUnique(slot.getSlot())) {
                return "fbsNameDuplicated";
            }
            return "nameActive";
        }
        return "fbsNameInvalid";

    }

    public void prepareImportSignalPopup() {
        dataLoader = signalsDataLoader;
        prepareImportPopup();
    }

    public void prepareImportSlotPopup() {
        dataLoader = slotsDataLoader;
        prepareImportPopup();
    }

    public void expandTreeNodes() {
        if (selectedTree.getSelectedNodes().isEmpty()) {
            expandOrCollapseNode(selectedTree.getRootNode(), true);
        } else {
            for (final FilteredTreeNode<SlotView> node : selectedTree.getSelectedNodes()) {
                expandOrCollapseNode(node, true);
            }
        }
    }

    public void collapseTreeNodes() {
        if (selectedTree.getSelectedNodes().isEmpty()) {
            expandOrCollapseNode(selectedTree.getRootNode(), false);
        } else {
            for (final FilteredTreeNode<SlotView> node : selectedTree.getSelectedNodes()) {
                expandOrCollapseNode(node, false);
            }
        }
    }

    private void expandOrCollapseNode(final FilteredTreeNode<SlotView> parent, final boolean expand) {
        parent.setExpanded(expand);
        if (expand) {
            expandedNodeIds.add(parent.getData().getId());
        }else {
            expandedNodeIds.remove(parent.getData().getId());
            }
        for (final FilteredTreeNode<SlotView> node : parent.getFilteredChildren()) {
            expandOrCollapseNode(node, expand);
        }
    }

    public void updateContainsTreeFilterMatchMode() {
        switch (containsModeTriState) {
        case "1":
            containsTree.setFilterMatchMode(FilterMatchMode.SLOT);
            break;
        case "2":
            containsTree.setFilterMatchMode(FilterMatchMode.CONTAINER);
            break;
        default:
            containsTree.setFilterMatchMode(FilterMatchMode.BOTH);
            break;
        }
        containsTree.applyFilter();
    }

    /*
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Above: Callback methods called from the main UI screen. E.g.: methods that are called when user user selects a
     * line in a table.
     *
     * Below: Methods for manipulation, populating and editing the hierarchy tree of slots and containers. * * * * * * *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */
    /** Prepares back-end data used for container deletion */
    public void prepareDeletePopup() {
        Preconditions.checkNotNull(selectedTree.getSelectedNodes());

        nodesToDelete = Lists.newArrayList();
        for (final FilteredTreeNode<SlotView> nodeToDelete : selectedTree.getSelectedNodes()) {
            addSlotToDeleteWithChildren(nodeToDelete);
        }
        slotsToDelete = nodesToDelete.stream().map(FilteredTreeNode::getData).collect(Collectors.toList());
    }

    private void addSlotToDeleteWithChildren(final FilteredTreeNode<SlotView> nodeToDelete) {
        if (!nodesToDelete.contains(nodeToDelete)) {
            nodesToDelete.add(nodeToDelete);
        }

        // make sure that the tree children are properly initialized.
        for (final FilteredTreeNode<SlotView> child : nodeToDelete.getBufferedAllChildren()) {
            addSlotToDeleteWithChildren(child);
        }
    }

    /** Deletes selected container */
    public void onSlotsDelete() {
        Preconditions.checkNotNull(nodesToDelete);
        Preconditions.checkState(!nodesToDelete.isEmpty());

        List<Slot> slotsToUpdateAfterDelete = new ArrayList<>();
        for (FilteredTreeNode<SlotView> node : nodesToDelete) {
            Slot slotToDelete = node.getData().getSlot();
            for (SlotPair slotPair : slotPairEJB.getSlotRelations(slotToDelete)) {
                if (slotPair.getSlotRelation().getName().equals(SlotRelationName.CONTAINS)) {
                    slotsToUpdateAfterDelete.add(slotPair.getParentSlot());
                }
            }
        }

        final int numSlotsToDelete = nodesToDelete.size();
        while (!nodesToDelete.isEmpty()) {
            removeDeletedFromClipboard();
            deleteWithChildren(nodesToDelete.get(0));
        }

        final Set<Long> nodesToRefresh = slotsToUpdateAfterDelete.stream()
                .map(Slot::getId)
                .collect(Collectors.toSet());
        refreshTrees(nodesToRefresh);

        UiUtility.showInfoMessage(Entity.SLOTS, Action.DELETE, numSlotsToDelete);
        selectedTree.getSelectedNodes().clear();
        nodesToDelete = null;
        clearRelatedInformation();
    }

    private void removeDeletedFromClipboard() {
        if (!Utility.isNullOrEmpty(clipboardSlots)) {
            for (final FilteredTreeNode<SlotView> deleteCandidate : nodesToDelete) {
                clipboardSlots.remove(deleteCandidate);
            }
        }
    }

    private void deleteWithChildren(final FilteredTreeNode<SlotView> node) {
        while (!node.getBufferedAllChildren().isEmpty()) {
            deleteWithChildren(node.getBufferedAllChildren().get(0));
        }
        final FilteredTreeNode<SlotView> parentTreeNode = (FilteredTreeNode<SlotView>) node.getParent();
        final SlotView slotViewToDelete = node.getData();
        final Slot slotToDelete = slotViewToDelete.getSlot();
        // delete uninstalls device as well
        slotEJB.delete(slotToDelete);
        // update UI data as well
        parentTreeNode.refreshCache();
        nodesToDelete.remove(node);
    }

    private void initHierarchies() {
        SlotView rootView = new SlotView(slotEJB.getRootNode(), null, 1, slotEJB);

        containsTree = new SlotRelationshipTree(SlotRelationName.CONTAINS, slotEJB, installationEJB, slotRelationEJB);
        containsTree.setRootNode(new FilteredTreeNode<>(rootView, null, containsTree));

        controlsTree = new SlotRelationshipTree(SlotRelationName.CONTROLS, slotEJB, installationEJB, slotRelationEJB);
        controlsTree.setRootNode(new FilteredTreeNode<>(rootView, null, controlsTree));

        powersTree = new SlotRelationshipTree(SlotRelationName.POWERS, slotEJB, installationEJB, slotRelationEJB);
        powersTree.setRootNode(new FilteredTreeNode<>(rootView, null, powersTree));

        connectsTree = new ConnectsTree(slotEJB, slotPairEJB, connectsManager);
        connectsTree.setRootNode(new FilteredTreeNode<>(rootView, null, connectsTree));

        selectedTree = containsTree;
    }

    /**
     * @return cableDBStatus
     */
    public boolean getCableDBStatus() {
        return connectsManager.getCableDBStatus();
    }

    /**
     * The action event to be called when the user presses the "move up" action button. This action moves the current
     * container/slot up one space, if that is possible.
     */
    public void moveSlotUp() {
        Preconditions.checkState(isSingleNodeSelected());
        final FilteredTreeNode<SlotView> currentNode = selectedTree.getSelectedNodes().get(0);
        final FilteredTreeNode<SlotView> parent = (FilteredTreeNode<SlotView>) currentNode.getParent();
        slotPairEJB.moveUp(parent.getData().getSlot(), currentNode.getData().getSlot());
        parent.refreshCache();
    }

    /**
     * The action event to be called when the user presses the "move down" action button. This action moves the current
     * container/slot down one space, if that is possible.
     */
    public void moveSlotDown() {
        Preconditions.checkState(isSingleNodeSelected());
        final FilteredTreeNode<SlotView> currentNode = selectedTree.getSelectedNodes().get(0);
        final FilteredTreeNode<SlotView> parent = (FilteredTreeNode<SlotView>) currentNode.getParent();
        slotPairEJB.moveDown(parent.getData().getSlot(), currentNode.getData().getSlot());
        parent.refreshCache();
    }

    /** Prepares fields that are used in pop up for editing an existing container */
    public void prepareEditPopup() {
        Preconditions.checkState(isSelectionEditable());
        selectedSlotsCommonDeviceType = null;
        if (isSingleNodeSelected()) {
            isNewSlot = false;
            isInstallationSlot = selectedSlotView.isHostingSlot();
            name = selectedSlotView.getName();
            fbsName = selectedSlotView.getFbsName();
            originalName = selectedSlotView.getName();
            originalFbsName = selectedSlotView.getFbsName();
            description = selectedSlotView.getDescription();
            deviceType = selectedSlotView.getSlot().getComponentType().getName();
            parentName = selectedSlotView.getParentNode().getParentNode() == null ? ""
                    : selectedSlotView.getParentNode().getName();
            hasDevice = selectedSlotView.getInstalledDevice() != null;
        } else if (isMultipleHostingSlotsWithCommonDeviceTypeSelected()) {
            isNewSlot = false;
            isInstallationSlot = true;
            String deviceTypeName = null;
            for (String devType : selectedTree.getSelectedNodes().stream()
                    .map(BasicTreeNode::getData).map(SlotView::getDeviceTypeName).collect(Collectors.toList())) {
                if (deviceTypeName == null) {
                    deviceTypeName = devType;
                } else if (!deviceTypeName.equals(devType)) {
                    deviceTypeName = null;
                    break;
                }
            }
            if (StringUtils.isEmpty(deviceTypeName)) {
                throw new CCDBRuntimeException(
                        "Multiple slots batch device type modification is allowed only in case " +
                        "when selected slots have the same device type!");
            }
            deviceType = deviceTypeName;
            selectedSlotsCommonDeviceType = deviceType;
        }

        if (deviceTypesCommonProperties != null) {
            deviceTypesCommonProperties.clear();
        }
    }

    /** Prepares fields that are used in pop up for adding a new container */
    public void prepareContainerAddPopup() {
        isNewSlot = true;
        isInstallationSlot = false;
        initAddInputFields();
    }

    /** Prepares fields that are used in pop up for adding a new installation slot */
    public void prepareInstallationSlotPopup() {
        isInstallationSlot = true;
        isNewSlot = true;
        initAddInputFields();
    }

    private void initAddInputFields() {
        name = null;
        fbsName = null;
        description = null;
        deviceType = null;
        parentName = (selectedSlot == null) ? "" : selectedSlot.getName();
        hasDevice = false;
    }

    /** Called to save modified slot/container information */
    public void onSlotModify() {
        Slot modifiedSlot = selectedSlotView.getSlot();
        modifiedSlot.setName(name);
        modifiedSlot.setFbsName(fbsName);
        modifiedSlot.setNameUuid(null);
        slotValidationService.validateSlot(modifiedSlot);
        modifiedSlot.setDescription(description);

        try {
            saveModifiedSlot(modifiedSlot, selectedSlotView);
            clearRelatedInformation();
            updateDisplayedSlotInformation();
        } catch (EJBException e) {
            // Check for PropertyValueNotUniqueException, device type change may lead to this error
            if (UiUtility.causedBySpecifiedExceptionClass(e, PropertyValueNotUniqueException.class)) {
                Throwable throwable =
                        UiUtility.getCauseBySpecifiedExceptionClass(e, PropertyValueNotUniqueException.class);
                String propName = null;
                if (throwable != null) {
                    propName = ((PropertyValueNotUniqueException) throwable).getProperty().getFullName();
                }
                FacesContext.getCurrentInstance().validationFailed();
                FacesContext.getCurrentInstance().addMessage("editSlotForm:propertyValidationMessage", new FacesMessage(
                        FacesMessage.SEVERITY_ERROR, UiUtility.ERROR_MESSAGE, "Value must be unique" +
                        (StringUtils.isEmpty(propName) ? "." : ": " + propName)));
            } else {
                throw e;
            }
        }
    }

    private void saveModifiedSlot(Slot modifiedSlot, SlotView selectedSlotView) {
        if (modifiedSlot.isHostingSlot() && installationEJB.getActiveInstallationRecordForSlot(modifiedSlot) == null) {
            // changeSlotType only saves if the actual device type changes, otherwise it returns the slot unmodified
            List<Property> propertiesToKeep = deviceTypesCommonProperties == null ? Collections.emptyList() :
                    deviceTypesCommonProperties.stream()
                    .filter(PropertySelection::getIncluded)
                    .map(PropertySelection::getProperty).collect(Collectors.toList());
            modifiedSlot = slotEJB.changeSlotType(modifiedSlot, comptypeEJB.findByName(deviceType), propertiesToKeep);
        }
        slotEJB.save(modifiedSlot);
        selectedSlotView.setSlot(modifiedSlot);
        UiUtility.showInfoMessage(Entity.SLOT, modifiedSlot.getName(), Action.UPDATE);
    }

    public void onMultipleSlotsModify() {
        try {
            utx.begin();
            for (SlotView slotView : selectedTree.getSelectedNodes().
                    stream().map(BasicTreeNode::getData).collect(Collectors.toList())) {
                Slot slot = slotView.getSlot();
                try {
                    saveModifiedSlot(slot, slotView);
                } catch (EJBException e) {
                    utx.rollback();
                    throw e;
                }
            }
            utx.commit();
            clearRelatedInformation();
            updateDisplayedSlotInformation();
        } catch (IllegalStateException
                | SecurityException
                | HeuristicMixedException
                | HeuristicRollbackException
                | NotSupportedException
                | RollbackException
                | SystemException ex) {
            try {
                utx.rollback();
                LOGGER.log(Level.SEVERE, "Transaction Error", ex.getMessage());
            } catch (IllegalStateException | SecurityException | SystemException ex1) {
                LOGGER.log(Level.SEVERE, ex1.getMessage(), ex1);
            }
        }
    }

    /** Called to add a new installation slot / container to the database */
    public void onSlotAdd() {
        Preconditions.checkState(selectedTree.getSelectedNodes().size() <= 1);
        final Slot newSlot = new Slot(name, isInstallationSlot);
        newSlot.setFbsName(fbsName);
        slotValidationService.validateSlot(newSlot);
        newSlot.setDescription(description);
        if (isInstallationSlot) {
            newSlot.setComponentType(comptypeEJB.findByName(deviceType));
        } else {
            newSlot.setComponentType(comptypeEJB.findByName(SlotEJB.GRP_COMPONENT_TYPE));
        }
        final FilteredTreeNode<SlotView> parentNode = selectedTree.getSelectedNodes().size() == 1
                ? selectedTree.getSelectedNodes().get(0) : selectedTree.getRootNode();
        final Slot parentSlot = parentNode.getData().getSlot();
        slotEJB.addSlotToParentWithPropertyDefs(newSlot, parentSlot, false, true);

        // first update the back-end data
        parentNode.refreshCache();
        parentNode.setExpanded(true);
        UiUtility.showInfoMessage(Entity.SLOT, newSlot.getName(), Action.CREATE);
        selectSingleNode(newSlot);
    }

    /**
     * Helper method for auto complete when entering a name for new installation {@link Slot}.
     *
     * @param query
     *            Text that was entered so far
     * @return {@link List} of strings with suggestions
     */
    public List<String> nameAutocompleteText(String query) {
        final List<String> currentValue = new ArrayList<>();

        // if no filter and current name valid, add it to drop-down to allow to set back original name by clearing
        // filter
        if (query.isEmpty() && !isNewSlot) {
            final String currentName = selectedSlotView.getName();
            if (names.getNameStatus(currentName).equals(NameStatus.ACTIVE)) {
                currentValue.add(currentName);
            }
        }

        final int limit = getNameAutocompleteMaxResults() + 1;
        return ListUtils.union(currentValue, nameAutoComplete.autoCompleteResult(
                ImmutableMap.of(AutoCompleteQuery.QUERY_PARAM, query), limit));
    }

    public int getNameAutocompleteMaxResults() {
        return NAME_AUTOCOMPLETE_MAX_RESULTS;
    }

    /**
     * Helper method for auto complete when entering a fbs name for new installation {@link Slot}.
     *
     * @param query
     *            Text that was entered so far
     * @return {@link List} of strings with suggestions
     */
    public List<String> fbsNameAutocompleteText(String query) {
        final List<String> resultList = new ArrayList<>();
        List<String> validFbsNames = new FbsTagSearchAction(fbsService).autoCompleteResult(
                ImmutableMap.of(AutoCompleteQuery.QUERY_PARAM, query), getNameAutocompleteMaxResults() + 1);

        // if no filter and current name valid, add it to drop-down to allow to set back original name by clearing
        // filter
        if (query.isEmpty() && !isNewSlot) {
            final String currentName = selectedSlotView.getFbsName();
            if (currentName != null && ! currentName.isEmpty()) {
                resultList.add(currentName);
            }
        }

        resultList.addAll(validFbsNames);
        return resultList;
    }

    private List<String> getValidFbsNames() {
        return fbsCache.getTagNames();
    }

    /**
     * Clear and (possibly) fill FBS Name with value corresponding to selected Slot Name,
     * and tries to get description from naming
     */
    public void clearFillSlotFbsName() {
        if (!StringUtils.isEmpty(name)) {
            String fbsTag = fbsService.getFbsTagForEssName(name);
            if (!StringUtils.isEmpty(fbsTag)) {
                fbsName = fbsTag;
            } else {
                fbsName = null;
            }

            searchSlotInNaming();
        }
    }

    /**
     * This method places the currently selected tree nodes into the clipboard and marks for moving.
     */
    public void cutTreeNodes() {
        Preconditions.checkState(isIncludesActive());
        Preconditions.checkState(!selectedTree.getSelectedNodes().isEmpty());

        clipboardOperation = ClipboardOperations.CUT;
        putSelectedNodesOntoClipboard();
    }

    /**
     * This method places the currently selected tree nodes into the clipboard and marks for copying.
     */
    public void copyTreeNodes() {
        Preconditions.checkState(isIncludesActive());
        Preconditions.checkState(!selectedTree.getSelectedNodes().isEmpty());

        clipboardOperation = ClipboardOperations.COPY;
        putSelectedNodesOntoClipboard();
    }

    public ClipboardOperations getCliboardOperation() {
        return clipboardOperation;
    }

    /**
     * This method tells whether a parent of the {@link BasicTreeNode} is in the clipboard.
     *
     * @param node
     *            the {@link BasicTreeNode} to check for
     * @return <code>true</code> if the node's parent is in the clipboard, <code>false</code> otherwise
     */
    public boolean isAncestorNodeInClipboard(final BasicTreeNode<SlotView> node) {
        if (Utility.isNullOrEmpty(clipboardSlots) || (node == null)
                || node.equals(selectedTree.getRootNode().getData())) {
            return false;
        }
        if (clipboardSlots.contains(node)) {
            return true;
        } else {
            return isAncestorNodeInClipboard(node.getParent());
        }
    }

    private void putSelectedNodesOntoClipboard() {
        clipboardSlots = new ArrayList<>();

        // 2. We put the selected nodes into the clipboard
        for (final FilteredTreeNode<SlotView> node : selectedTree.getSelectedNodes()) {
            clipboardSlots.add(node);
        }

        // 3. We remove all the nodes that have their parents in the clipboard
        for (final Iterator<FilteredTreeNode<SlotView>> nodesIterator = clipboardSlots.iterator(); nodesIterator
                .hasNext();) {
            final FilteredTreeNode<SlotView> removalCandidate = nodesIterator.next();
            if (isAncestorNodeInList(clipboardSlots, removalCandidate)) {
                nodesIterator.remove();
            }
        }
    }

    /**
     * @param possibleAscendant
     * @param candidate
     * @return <code>true</code> if the <code>candidate</code> is a descendant of <code>possibleAscendant</code>,
     *         <code>false</code> otherwise
     */
    private boolean isNodeDescendant(final FilteredTreeNode<SlotView> possibleAscendant,
            final FilteredTreeNode<SlotView> candidate) {
        return isAncestorNodeInList(Arrays.asList(possibleAscendant), candidate);
    }

    private boolean isAncestorNodeInList(final List<FilteredTreeNode<SlotView>> candidates,
            final FilteredTreeNode<SlotView> node) {
        BasicTreeNode<SlotView> parentNode = node.getParent();
        while (parentNode != null) {
            if (candidates.contains(parentNode)) {
                return true;
            }
            parentNode = parentNode.getParent();
        }
        return false;
    }

    /**
     * The method checks whether the paste operation is legal. If not, it fills the <code>pasteErrors</code>
     * {@link List} and the <code>pasteErrorReason</code>. If the paste operation is legal the <code>pasteErrors</code>
     * {@link List} will remain empty.
     */
    public void checkPasteAction() {
        Preconditions.checkState(!isClipboardEmpty());
        Preconditions.checkState(selectedTree.getSelectedNodes().size() < 2);

        final boolean makeRoots = Utility.isNullOrEmpty(selectedTree.getSelectedNodes());
        final boolean isTargetInstallationSlot = !makeRoots && selectedSlot.isHostingSlot();
        if (makeRoots) {
            pasteErrorReason = CANNOT_PASTE_INTO_ROOT;
        } else {
            pasteErrorReason = CANNOT_PASTE_INTO_SLOT;
        }

        pasteErrors = Lists.newArrayList();
        for (final FilteredTreeNode<SlotView> node : clipboardSlots) {
            if (makeRoots && node.getData().isHostingSlot()) {
                pasteErrors.add(node.getData());
            } else if (isTargetInstallationSlot && !node.getData().isHostingSlot()) {
                pasteErrors.add(node.getData());
            }
        }

        if ((pasteErrors.isEmpty()) && (selectedSlot != null)) {
            pasteErrorReason = CANNOT_PASTE_INTO_SELF;
            BasicTreeNode<SlotView> current = selectedTree.getSelectedNodes().get(0);
            while (current != null) {
                for (final FilteredTreeNode<SlotView> node : clipboardSlots) {
                    if (node.equals(current)) {
                        pasteErrors.add(node.getData());
                    }
                }
                current = current.getParent();
            }
        }
    }

    /**
     * This method is called when the user selects the "Paste" action. The method either moves the nodes that are in the
     * clipboard, or creates new copies.
     */
    public void pasteTreeNodes() {
        Preconditions.checkState(isIncludesActive());
        Preconditions.checkState(!isClipboardEmpty());
        Preconditions.checkState(selectedTree.getSelectedNodes().size() < 2);
        Preconditions.checkNotNull(pasteErrors);
        Preconditions.checkState(pasteErrors.isEmpty());

        if (clipboardOperation == ClipboardOperations.CUT) {
            moveSlotsToNewParent();
            UiUtility.showInfoMessage(Entity.SLOT, "", Action.MOVE);
        } else {
            final FilteredTreeNode<SlotView> parentNode = (!selectedTree.getSelectedNodes().isEmpty())
                    ? selectedTree.getSelectedNodes().get(0) : selectedTree.getRootNode();
            copySlotsToParent(clipboardSlots.stream().map(FilteredTreeNode<SlotView>::getData).map(SlotView::getSlot)
                    .collect(Collectors.toList()), parentNode);
            UiUtility.showInfoMessage(Entity.SLOTS, Action.COPY, 0);
        }
    }

    private void moveSlotsToNewParent() {
        final FilteredTreeNode<SlotView> newParent = Utility.isNullOrEmpty(selectedTree.getSelectedNodes())
                ? selectedTree.getRootNode() : selectedTree.getSelectedNodes().get(0);

        // remove the nodes that do not get moved or are moved to their own descendant
        final List<FilteredTreeNode<SlotView>> candidates = clipboardSlots.stream()
                .filter(e -> !(e.getParent().equals(newParent) || isNodeDescendant(e, newParent)))
                .collect(Collectors.toList());
        final List<BasicTreeNode<SlotView>> oldParents = candidates.stream().map(FilteredTreeNode<SlotView>::getParent)
                .collect(Collectors.toList());

        final List<SlotPair> moveCandidatesByRelationship = candidates.stream().map(FilteredTreeNode<SlotView>::getData)
                .map(SlotView::getParentRelationship).collect(Collectors.toList());
        slotPairEJB.moveSlotsToNewParent(moveCandidatesByRelationship, newParent.getData().getSlot());

        clipboardSlots = null;
        // Refresh the information about the affected slots in all the hierarchy trees
        newParent.refreshCache();
        newParent.setExpanded(true);

        for (BasicTreeNode<SlotView> node : oldParents) {
            ((FilteredTreeNode<SlotView>) node).refreshCache();
        }
    }

    private void copySlotsToParent(final List<Slot> sourceSlots, final FilteredTreeNode<SlotView> parentNode) {
        final SlotView newParentSlotView = parentNode.getData();
        final Slot newParentSlot = newParentSlotView.getSlot();
        slotEJB.copySlotsToParent(sourceSlots, newParentSlot);

        parentNode.refreshCache();
        parentNode.setExpanded(true);
    }

    public void prepareQrDialog() {
        final HashSet<String> includedSlots = new HashSet<>();
        final List<String> qrSlots = Lists.newArrayList();

        List<? extends FilteredTreeNode<SlotView>> selectedNodes = selectedTree.getSelectedNodes();
        if (selectedNodes.isEmpty()) {
            selectedNodes = selectedTree.getRootNode().getFilteredChildren();
        }

        for (final FilteredTreeNode<SlotView> node : selectedNodes) {
            addAllSlotsForQR(includedSlots, qrSlots, node, false);
        }

        qrPdfDialog = new QrPdfDialog(qrSlots);
    }

    /**
     * @param includedSlots
     *            a set of all slots that were already added, to avoid adding the same one twice
     * @param allSlots
     *            a list of slots to convert to QR codes (set does not maintain order)
     * @param node
     *            the node we're working on
     * @param addAllChildren
     *            whether to all children if installation slot, or just itself
     */
    private void addAllSlotsForQR(final HashSet<String> includedSlots, final List<String> allSlots,
            final FilteredTreeNode<SlotView> node, final boolean addAllChildren) {
        final SlotView nodeSlot = node.getData();
        final String slotName = nodeSlot.getName();
        if (nodeSlot.isHostingSlot()) {
            // installation slot
            // first let's add it
            if (!includedSlots.contains(slotName)) {
                includedSlots.add(slotName);
                allSlots.add(slotName);
            }
            // do we need to include all it's children?
            if (!node.isLeaf() && addAllChildren) {
                for (final FilteredTreeNode<SlotView> childNode : node.getFilteredChildren()) {
                    addAllSlotsForQR(includedSlots, allSlots, childNode, addAllChildren);
                }
            }
        } else {
            // this is a container
            if (!node.isLeaf()) {
                for (final FilteredTreeNode<SlotView> childNode : node.getFilteredChildren()) {
                    addAllSlotsForQR(includedSlots, allSlots, childNode, true);
                }
            }
        }
    }

    /*
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Above: Methods for manipulation, populating and editing the hierarchy tree of slots and containers.
     *
     * Below: Input field validators regardless of the dialog they are used in. * * * * * * * * * * * * * * * * * * * *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */
    /**
     * Throws a validation error exception if the slot name is not unique.
     *
     * @param ctx
     *            {@link javax.faces.context.FacesContext}
     * @param component
     *            {@link javax.faces.component.UIComponent}
     * @param value
     *            The value
     * @throws ValidatorException
     *             validation failed
     */
    public void validateInstallationSlot(FacesContext ctx, UIComponent component, Object value) {
        /*
         * This parameter is checked so that validation is optionally performed depending on the ajax or final form
         * submit call.
         */
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (!params.containsKey("validate")) {
            return;
        }

        final String valueStr = value.toString();

        if (restrictToConventionNames && isInstallationSlot
                && !names.getActiveNames().contains(valueStr)) {
                throw UiUtility.getValidationError("The slot name not found in the naming tool.");
        }
        if (isNewSlot) {
            // add dialog
            if (isInstallationSlot) {
                // check uniqueness across whole database
                if (!slotEJB.isInstallationSlotNameUnique(valueStr))
                    throw UiUtility.getValidationError("The slot name must be unique.");
            } else {
                // check uniqueness only for the parent
                final Slot slotParent = selectedSlotView != null ? selectedSlotView.getSlot() : slotEJB.getRootNode();
                if (!slotEJB.isContainerNameUnique(valueStr, slotParent, null))
                    throw UiUtility.getValidationError("Parent alread contains equally named child.");
            }
        } else {
            // edit dialog
            if (isInstallationSlot) {
                // check uniqueness across whole database
                if (!originalName.equals(valueStr) && !slotEJB.isInstallationSlotNameUnique(valueStr))
                    throw UiUtility.getValidationError("The slot name must be unique.");
            } else {
                // check uniqueness only for the parent
                if (!originalName.equals(valueStr) && !slotEJB.isContainerNameUnique(valueStr,
                        selectedSlotView.getParentNode().getSlot(), selectedSlotView.getSlot()))
                    throw UiUtility.getValidationError("Parent alread contains equally named child.");
            }
        }
    }

    /**
     * Throws a validation error exception if the fbs name is not unique.
     *
     * @param ctx
     *            {@link javax.faces.context.FacesContext}
     * @param component
     *            {@link javax.faces.component.UIComponent}
     * @param value
     *            The value
     * @throws ValidatorException
     *             validation failed
     */
    public void validateFbsName(FacesContext ctx, UIComponent component, Object value) {
        /*
         * This parameter is checked so that validation is optionally performed depending on the ajax or final form
         * submit call.
         */
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (!params.containsKey("validate")) {
            return;
        }

        // for compatibility with old slots without fbsName
        final String valueStr = value!=null ? value.toString() : "";

        if (valueStr.isEmpty()) {
            return;
        }

        //if (fbsNameAutocompleteText(valueStr).get(0) != valueStr) {
        //      throw UiUtility.getValidationError("The fbs name not found in the <placeholder>.");
        //}

        // edit dialog
        // check uniqueness across whole database


        if (!valueStr.equals(originalFbsName) && !slotEJB.isFbsNameUnique(valueStr))
            throw UiUtility.getValidationError("The fbs name must be unique.");

    }

    /*
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Above: Input field validators regardless of the dialog they are used in.
     *
     * Below: Getters and setter all logically grouped based on where they are used. All getters and setters are usually
     * called from the UI dialogs. * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * * * * * * * * * * *
     */

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Main screen
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /** @return The slot/container that is currently selected in the tree. */
    public Slot getSelectedNodeSlot() {
        return selectedSlot;
    }

    /**
     * @return the {@link SlotView} of the slot, if there is only one node selected in the tree. <code>null</code>
     *         otherwise.
     */
    public SlotView getSingleSelectedSlotView() {
        return selectedSlotView;
    }

    protected void expandSelectedNodes() {
        selectedTree.getSelectedNodes().stream().forEach(selectedNode -> selectedNode.setExpanded(true));
    }

    public Tree<SlotView> getContainsTree() {
        return containsTree;
    }

    public Tree<SlotView> getControlsTree() {
        return controlsTree;
    }

    public Tree<SlotView> getPowersTree() {
        return powersTree;
    }

    public Tree<SlotView> getConnectsTree() {
        return connectsTree;
    }

    /** @return <code>true</code> if the UI is currently showing the <code>INCLUDES</code> hierarchy */
    public boolean isIncludesActive() {
        return activeTab == ActiveTab.INCLUDES;
    }

    /**
     * @return <code>true</code> if the currently shown hierarchy tree has no nodes under the current filter,
     *         <code>false</code> otherwise.
     */
    public boolean isDisplayedTreeEmpty() {
        return (activeTab == ActiveTab.INCLUDES
                && (containsTree == null || containsTree.getRootNode().getFilteredChildren().isEmpty()))
                || (activeTab == ActiveTab.CONTROLS
                        && (controlsTree == null || controlsTree.getRootNode().getFilteredChildren().isEmpty()))
                || (activeTab == ActiveTab.POWERS
                        && (powersTree == null || powersTree.getRootNode().getFilteredChildren().isEmpty()))
                || (activeTab == ActiveTab.CONNECTS
                        && (connectsTree == null || connectsTree.getRootNode().getFilteredChildren().isEmpty()));
    }

    public boolean isSingleNodeSelected() {
        return (selectedTree.getSelectedNodes() != null) && (selectedTree.getSelectedNodes().size() == 1);
    }

    public boolean isAtLeastOneNodeSelected() {
        return (selectedTree.getSelectedNodes() != null) && (!selectedTree.getSelectedNodes().isEmpty());
    }

    public boolean isMultipleNodesSelected() {
        return (selectedTree.getSelectedNodes() != null) && (selectedTree.getSelectedNodes().size() > 1);
    }

    /**
     * @return <code>true</code> if the selected slots are hosting slots and have the same device type,
     * <code>false</code> otherwise.
     */
    public boolean isMultipleHostingSlotsWithCommonDeviceTypeSelected() {
        return StringUtils.isNotEmpty(getSelectedSlotsCommonDeviceType());
    }

    /**
     * Get common device type name for selected slots if they are all hosting slots and have the same device type.
     * @return Common device type's name
     */
    public String getSelectedSlotsCommonDeviceType() {
        if ((selectedTree.getSelectedNodes() != null) && (selectedTree.getSelectedNodes().size() > 1)) {
            String firstSlotsDeviceType = selectedTree.getSelectedNodes().get(0).getData().getDeviceTypeName();
            long selectedContainersCount = selectedTree.getSelectedNodes().stream().map(BasicTreeNode::getData)
                    .filter(s -> !s.isHostingSlot() || !s.getDeviceTypeName().equals(firstSlotsDeviceType)).count();
            if(selectedContainersCount == 0) {
                return firstSlotsDeviceType;
            }
        }
        return null;
    }

    public boolean isSelectionEditable() {
        return isSingleNodeSelected() || isMultipleHostingSlotsWithCommonDeviceTypeSelected();
    }

    public boolean isClipboardEmpty() {
        return Utility.isNullOrEmpty(clipboardSlots);
    }

    public List<SlotView> getSlotsToDelete() {
        return slotsToDelete;
    }

    public void setSlotsToDelete(List<SlotView> slotsToDelete) {
        this.slotsToDelete = slotsToDelete;
    }

    public List<SlotView> getFilteredSlotsToDelete() {
        return filteredSlotsToDelete;
    }

    public void setFilteredSlotsToDelete(List<SlotView> filteredSlotsToDelete) {
        this.filteredSlotsToDelete = filteredSlotsToDelete;
    }

    public int getNumberOfSlotsToDelete() {
        return nodesToDelete != null ? nodesToDelete.size() : 0;
    }

    public String getPasteErrorReason() {
        return pasteErrorReason;
    }

    public List<SlotView> getPasteErrors() {
        return pasteErrors;
    }

    public List<SlotView> getClipboardSlots() {
        return clipboardSlots == null ? null
                : clipboardSlots.stream().map(FilteredTreeNode<SlotView>::getData).collect(Collectors.toList());
    }

    public String getRequestedSlot() {
        return requestedSlot;
    }

    public String getContainsModeTriState() {
        return containsModeTriState;
    }

    public void setContainsModeTriState(String containsModeTriState) {
        this.containsModeTriState = containsModeTriState;
    }

    public QrPdfDialog getQrPdfDialog() {
        return qrPdfDialog;
    }

    public boolean isNamingEnabled() {
        return names.isEnabled();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Slot dialog
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /** @return the name */
    @NotNull
    @Size(min = 1, max = 128, message = "Name can have at most 128 characters.")
    public String getName() {
        return name;
    }

    /** @return the fbs name */
    @Size(max = 255, message = "FbsName can have at most 255 characters.")
    public String getFbsName() {
        return fbsName;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        if (!name.equals(this.name)) {
            previousName = this.name;
            this.name = name;
        }
    }

    /**
     * @param fbsName
     *            the fbs name to set
     */
    public void setFbsName(String fbsName) {
        if (fbsName == null) {
            this.fbsName = "";
        } else {
            this.fbsName = fbsName;
        }
    }


    public String getDisplayedNames() {
        return displayedNames.toString();
    }

    public void setDisplayedNames(String displayedContainerNames) {
        if ("NAMES".equals(displayedContainerNames)){
            this.displayedNames = DisplayedNames.NAMES;
        } else if ("FBSNAMES".equals(displayedContainerNames)){
            this.displayedNames = DisplayedNames.FBSNAMES;
        } else if ("BOTH".equals(displayedContainerNames)){
            this.displayedNames = DisplayedNames.BOTH;
        }

        containsTree.setDisplayedNames(this.displayedNames);
        containsTree.applyFilter();
    }

    public boolean isShowNames() {
        return displayedNames == DisplayedNames.BOTH || displayedNames ==  DisplayedNames.NAMES;
    }

    public boolean isShowFbsNames() {
        return displayedNames == DisplayedNames.BOTH || displayedNames == DisplayedNames.FBSNAMES;
    }

    /** @return the description */
    @NotNull
    @Size(max = 255, message = "Description can have at most 255 characters.")
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /** @return the isInstallationSlot */
    public boolean isInstallationSlot() {
        return isInstallationSlot;
    }

    // CCDB-572: listener for device type change on Edit slot dialog
    public void onDeviceTypeChanged() {
        deviceTypesCommonProperties = new ArrayList<>();
        if (selectedSlotView != null) {
            ComponentType compType = comptypeEJB.findByName(deviceType);
            if (compType != null) {
                deviceTypesCommonProperties = slotEJB.getCommonProperties(
                        selectedSlotView.getSlot(), compType).stream()
                        .map(p -> new PropertySelection(p, true)).collect(Collectors.toList());
            }
        } else if (StringUtils.isNotEmpty(selectedSlotsCommonDeviceType)) {
            ComponentType commonCompType = comptypeEJB.findByName(selectedSlotsCommonDeviceType);
            ComponentType compType = comptypeEJB.findByName(deviceType);
            if (commonCompType != null && compType != null) {
                deviceTypesCommonProperties = slotEJB.getCommonProperties(
                        commonCompType, compType).stream()
                        .map(p -> new PropertySelection(p, true)).collect(Collectors.toList());
            }
        }
    }

    public List<PropertySelection> getDeviceTypesCommonProperties() {
        return deviceTypesCommonProperties;
    }

    /** @return the deviceType */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * @param deviceType
     *            the deviceType to set
     */
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    /** @return the isNewSlot */
    public boolean isNewSlot() {
        return isNewSlot;
    }

    /** @return the parent slot name */
    public String getParentName() {
        return parentName;
    }

    /** @return hasDevice */
    public boolean getHasDevice() {
        return hasDevice;
    }

    /** @return the namingRedirectionUrl */
    public String getNamingRedirectionUrl() {
        return namingRedirectionUrl;
    }

    /** @return the linkSlot */
    public SlotView getLinkSlot() {
        return linkSlot;
    }

    /**
     * @param linkSlot
     *            the linkSlot to set
     */
    public void setLinkSlot(SlotView linkSlot) {
        this.linkSlot = linkSlot;
    }

    /** @return the cableRedirectionUrl */
    public String getCableRedirectionUrl() {
        if (cableRedirectionUrl == null) {
            final String cableRedirectionUrl = properties.getProperty(AppProperties.CABLEDB_APPLICATION_URL);

            if (!Strings.isNullOrEmpty(cableRedirectionUrl)) {
                final StringBuilder redirectionUrl = new StringBuilder(cableRedirectionUrl);
                if (redirectionUrl.charAt(redirectionUrl.length() - 1) != '/') {
                    redirectionUrl.append('/');
                }
                redirectionUrl.append(CABLEDB_DEVICE_PAGE);
                this.cableRedirectionUrl = redirectionUrl.toString();
            }
        }
        return cableRedirectionUrl;
    }

    /** @return the restrictToConventionNames */
    public boolean isRestrictToConventionNames() {
        return restrictToConventionNames;
    }

    protected Slot getSelectedEntity() {
        if (selectedSlot != null) {
            return slotEJB.refreshEntity(selectedSlot);
        }
        throw new IllegalArgumentException("No slot selected");
    }

    /**
     * Gets selected slots, or empty list
     * @return selected slots, or empty list
     */
    protected List<Slot> getSelectedSlots() {
        if (selectedTree.getSelectedNodes().isEmpty())
            return Collections.emptyList();
        else
            return selectedTree.getSelectedNodes().stream().map(e -> e.getData().getSlot())
                    .collect(Collectors.toList());
    }

    /**
     * Gets selected slots, or throws IllegalArgumentException
     * @return list of selected slots, or throws exception
     */
    protected List<Slot> getRestrictedSelectedSlots(){
        List<Slot> result = getSelectedSlots();

        if((result == null) || (result.isEmpty())) {
            throw new IllegalArgumentException("No slots selected");
        }

        return result;
    }

    @Override
    public ExportSimpleTableDialog getSimpleTableDialog() {
        return simpleTableExporterDialog;
    }

    public boolean isContainsEmpy() {
        return containsTree.getRootNode().getBufferedAllChildren().isEmpty();
    }

    @Override
    public List<FilteredTreeNode<SlotView>> getSelectedNodes() {
        return selectedTree.getSelectedNodes().isEmpty()
                ? Arrays.asList(selectedTree.getRootNode())
                : selectedTree.getSelectedNodes();
    }

    /**
     * returns ids of selected nodes to frontend
     */
    public void getContainsSelection() {
        JSONArray selectedIds = new org.primefaces.json.JSONArray(selectedNodeIds);
        PrimeFaces.current().ajax().addCallbackParam("selectedIds", selectedIds);
    }

    public String getContainsSelectionAsString() {
        if (selectedNodeIds == null) {
            return "";
        }

        return Joiner.on(',').join(selectedNodeIds);
    }

    public void setContainsSelection() {
        Map<String, String[]> paramMap =
                FacesContext.getCurrentInstance().getExternalContext().getRequestParameterValuesMap();
        String[] selections = paramMap.get("selections");

        if (selections == null || selections.length == 0) {
            // Skip if nothing is to be selected.
            return;
        }

        //First select only one to clear any current selection
        int firstValidSelectedIdx;
        for (firstValidSelectedIdx = 0; firstValidSelectedIdx < selections.length; firstValidSelectedIdx++) {
            // After slot or container deletion from Contains tree value in "selections" param which comes from
            // ccdb-common.js can be invalid so null check is needed.
            String id = selections[firstValidSelectedIdx];
            Slot slot = slotEJB.findById(Long.valueOf(id));
            if (slot != null) {
                selectSingleNode(slot);
                break;
            }
        }

        // now select all others
        for (int i = firstValidSelectedIdx + 1; i < selections.length; i++){
            String id = selections[i];
            Slot slot = slotEJB.findById(Long.valueOf(id));
            // After slot or container deletion from Contains tree value in "selections" param which comes from
            // ccdb-common.js can be invalid so null check is needed.
            if (slot != null) {
                selectNode(slot);
            }
        }
    }

    /**
     * returns ids of expanded nodes to frontend
     */
    public void getContainsExpanded() {
        JSONArray expandedIds = new org.primefaces.json.JSONArray(expandedNodeIds);
        PrimeFaces.current().ajax().addCallbackParam("expandedIds", expandedIds);
    }

    public void setContainsExpanded() {
        Map<String, String[]> paramMap =
                FacesContext.getCurrentInstance().getExternalContext().getRequestParameterValuesMap();
        String[] expandedIds = paramMap.get("expandedIds");

        if (expandedIds == null) {
            // Skip if nothing is to be expanded.
            return;
        }

        expandedNodeIds = new HashSet<>();
        for (int i = 0; i < expandedIds.length; i++){
            long id = Long.parseLong(expandedIds[i]);
            expandedNodeIds.add(id);
            Slot slot = slotEJB.findById(id);
            // After slot or container deletion from Contains tree value in "selections" param which comes from
            // ccdb-common.js can be invalid so null check is needed.
            if (slot != null) {
                FilteredTreeNode<SlotView> node = containsTree.findNode(slot);
                node.setExpanded(true);
            }
        }
    }

    public void restoreViewSettings() {
        Map<String, String> paramMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String displayedNames = paramMap.get("displayedNames");
        setDisplayedNames(displayedNames);
    }

    /**
     * If description is without user modifications (equals default description of the previously set installation
     * slot or is not set at all), replace it with the default description of the new slot.
     */
    private void searchSlotInNaming() {

        String previousName = this.previousName != null ? this.previousName : name;
        if (this.description == null || this.description.isEmpty()
                || this.description.equals(getNamingDescription(previousName))) {
            this.description = getNamingDescription(name);
        }
    }
}
