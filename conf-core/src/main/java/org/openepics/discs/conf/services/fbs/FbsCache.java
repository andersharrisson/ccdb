/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.services.fbs;

import com.google.common.base.Preconditions;
import org.openepics.discs.conf.ejb.SlotEJB;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.services.ConcurrentOperationServiceBase;
import org.openepics.discs.conf.util.BuiltInDataType;

import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Performs periodic FBS cache update.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@Singleton
@Startup
@Lock(LockType.READ)
@DependsOn("DatabaseMigration")
public class FbsCache extends ConcurrentOperationServiceBase implements Serializable {

    private static final long serialVersionUID = 5156929453297777314L;
    private static final Logger LOGGER = Logger.getLogger(FbsCache.class.getName());

    @Inject
    private FbsService fbsService;

    @Inject
    private SlotEJB slotEJB;

    private List<String> tagNames;
    private Map<String, String> tagMappings;

    @PostConstruct
    protected void initialise() {
        super.initialise();
    }

    /**
     * @return the list of cached Fbs tags.
     */
    public List<String> getTagNames() {
        if (!initialized) {
            runOperationBlocking();
        }

        return tagNames;
    }

    /** Refresh cache. */
    public void refresh() {
        runOperationBlocking();
    }

    @Override
    protected String getServiceName() {
        return "Fbs cache";
    }

    @Override
    protected String getOperation() {
        return "Fbs cache update";
    }

    /** Performs periodic update of the Fbs cache every hour. */
    @Schedule(hour = "*", persistent = false)
    public void update() {
        runOperationNonBlocking();
    }

    /**
     * Update cache (Fbs). Note <tt>@Lock(LockType.WRITE)</tt> for update of shared resources.
     */
    @Lock(LockType.WRITE)
    @Override
    protected boolean runOperation(String... vargs) {
        Preconditions.checkArgument(vargs.length == 0);

        final List<FbsElement> fbsElements = fbsService.getAllFbsTags();
        if (!fbsElements.isEmpty()) {
            tagNames = FbsUtil.readTags(fbsElements);
            tagMappings = FbsUtil.readTagMappings(fbsElements);
            LOGGER.fine("FBS Cache update finished, got " + tagNames.size() + " tags and " + tagMappings.size() +
                    " tag mappings.");

            updateInstallationSlots();
            return true;
        }

        return false;
    }

    private void updateInstallationSlots() {
        List<Slot> allSlots = Collections.emptyList();
        try {
            allSlots = slotEJB.findAll();
        } catch (Exception e) {
            LOGGER.warning("Failed to extract slots from database.");
        }
        allSlots.stream()
                .filter(slot -> tagMappings.containsKey(slot.getName()))
                .forEach(slot -> slot.setFbsName(tagMappings.get(slot.getName())));
    }
}
