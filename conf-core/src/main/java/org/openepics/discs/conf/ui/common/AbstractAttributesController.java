/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.ui.common;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIForm;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

import com.google.common.base.Strings;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.openepics.discs.conf.ejb.DAO;
import org.openepics.discs.conf.ejb.TagEJB;
import org.openepics.discs.conf.ent.Artifact;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.ComptypePropertyValue;
import org.openepics.discs.conf.ent.ConfigurationEntity;
import org.openepics.discs.conf.ent.DataType;
import org.openepics.discs.conf.ent.Device;
import org.openepics.discs.conf.ent.DevicePropertyValue;
import org.openepics.discs.conf.ent.EntityWithArtifacts;
import org.openepics.discs.conf.ent.EntityWithProperties;
import org.openepics.discs.conf.ent.EntityWithTags;
import org.openepics.discs.conf.ent.NamedEntity;
import org.openepics.discs.conf.ent.Property;
import org.openepics.discs.conf.ent.PropertyValue;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotPropertyValue;
import org.openepics.discs.conf.ent.Tag;
import org.openepics.discs.conf.ui.util.Action;
import org.openepics.discs.conf.ui.util.Entity;
import org.openepics.discs.conf.ui.util.UiUtility;
import org.openepics.discs.conf.util.BuiltInDataType;
import org.openepics.discs.conf.services.csentry.CSEntryHostValidationService;
import org.openepics.discs.conf.util.Conversion;
import org.openepics.discs.conf.util.DefaultValueNotAllowedException;
import org.openepics.discs.conf.util.EntityAttributeKind;
import org.openepics.discs.conf.util.PropertyValueNotUniqueException;
import org.openepics.discs.conf.util.PropertyValueUIElement;
import org.openepics.discs.conf.util.UnhandledCaseException;
import org.openepics.discs.conf.util.Utility;
import org.openepics.discs.conf.util.csentry.CSEntryService;
import org.openepics.discs.conf.views.EntityAttrArtifactView;
import org.openepics.discs.conf.views.EntityAttrPropertyValueView;
import org.openepics.discs.conf.views.EntityAttrTagView;
import org.openepics.discs.conf.views.EntityAttributeView;
import org.primefaces.PrimeFaces;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.openepics.discs.conf.ent.EntityWithExternalLinks;
import org.openepics.discs.conf.ent.ExternalLink;
import org.openepics.discs.conf.views.EntityAttrExternalLinkView;

/**
 * Parent class for all classes that handle entity attributes manipulation
 *
 * @param <C> There are 3 entities in the database having Property values, Tags
 * and Artifacts with the same interface.
 * @param <T> There are 4 property value tables in the database, but all have
 * the same columns and interface.
 * @param <S> There are 5 artifact value tables in the database, but all have
 * the same columns and interface.
 *
 * @author <a href="mailto:andraz.pozar@cosylab.com">Andraž Požar</a>
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
public abstract class AbstractAttributesController<C extends ConfigurationEntity & NamedEntity & EntityWithTags &
    EntityWithArtifacts, T extends PropertyValue, S extends Artifact> implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final int TAG_AUTOCOMPLETE_MAX_RESULTS = 20;

    @Inject
    protected TagEJB tagEJB;
    @Inject
    private CSEntryHostValidationService csEntryValidationService;
    @Inject
    private CSEntryService csEntryService;

    protected List<EntityAttributeView<C>> attributes;
    protected List<EntityAttributeView<C>> filteredAttributes;
    protected EntityAttributeView<C> selectedAttribute;
    protected List<EntityAttributeView<C>> nonDeletableAttributes;
    private List<EntityAttributeView<C>> filteredDialogAttributes;

    protected EntityAttributeView<C> dialogAttribute;
    protected EntityAttrArtifactView<C> downloadArtifactView;
    protected EntityAttrExternalLinkView<C> externalLinkView;

    protected OverlayValueProvider overlay = new OverlayValueProvider();

    private DAO<C> dao;

    private String originalPropertyValue;

    /**
     * This method resets the fields related to the dialog that was just shown
     * to the user.
     */
    public void resetFields() {
        dialogAttribute = null;
        originalPropertyValue = null;
    }

    /**
     * This method resets dialog attribute's property value to original when Edit property dialog is cancelled.
     */
    public void resetProperty() {
        if (dialogAttribute instanceof EntityAttrPropertyValueView<?> && originalPropertyValue != null ) {
                ((EntityAttrPropertyValueView<C>) dialogAttribute).setPropertyValue(
                        StringUtils.isNotEmpty(originalPropertyValue) ? originalPropertyValue : null);
        }
    }

    /**
     * Adds or modifies an artifact
     *
     * @param event the PrimeFaces provided event
     */
    public void modifyArtifact(ActionEvent event) {

        final EntityAttrArtifactView<C> artifactView = getDialogAttrArtifact();

        if (isArtifactNameUnique(artifactView)) {

            if (artifactView.isArtifactBeingModified()) {
                dao.saveChild(artifactView.getEntity(), true);
            } else {
                dao.addChild(artifactView.getEntity(), true);
            }

            UiUtility.showInfoMessage(Entity.ARTIFACT, artifactView.getArtifactName(),
                    artifactView.isArtifactBeingModified() ? Action.UPDATE : Action.CREATE);
            refreshParentEntity(dialogAttribute);
            resetFields();
            clearRelatedAttributeInformation();
            populateAttributesList();
        } else {
            final String formName = getFormId(event.getComponent());
            FacesContext.getCurrentInstance().validationFailed();
            FacesContext.getCurrentInstance().addMessage(formName + "nameValidationMessage", new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, UiUtility.ERROR_MESSAGE,
                    "File name must be unique."));
        }
    }

    public void modifyExternalLink(ActionEvent event) {
        final EntityAttrExternalLinkView<C> currentExternalLinkView = getDialogExternalLink();
        Set<ExternalLink> existingExternalLinks = ((EntityWithExternalLinks) getSelectedEntity()).getExternalLinkList();
        ExternalLink newOrModifiedExternalLink = currentExternalLinkView.getEntity();

        if (isExternalLinkNameUnique(existingExternalLinks, newOrModifiedExternalLink)) {

            if (currentExternalLinkView.isExternalLinkBeingModified()) {
                dao.updateExternalLink((EntityWithExternalLinks) getSelectedEntity(), newOrModifiedExternalLink);
            } else {
                dao.addExternalLink((EntityWithExternalLinks) getSelectedEntity(), newOrModifiedExternalLink);
            }

            UiUtility.showInfoMessage(Entity.EXTERNAL_LINK, newOrModifiedExternalLink.getName(),
                    currentExternalLinkView.isExternalLinkBeingModified() ? Action.UPDATE : Action.CREATE);
            refreshParentEntity(currentExternalLinkView);
            resetFields();
            clearRelatedAttributeInformation();
            populateAttributesList();

        } else {
            final String formName = getFormId(event.getComponent());
            FacesContext.getCurrentInstance().validationFailed();
            FacesContext.getCurrentInstance().addMessage(formName + "nameValidationMessage", new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, UiUtility.ERROR_MESSAGE,
                    "External link name must be unique."));
        }
    }

    private boolean isArtifactNameUnique(final EntityAttrArtifactView<C> artifactView) {
        for (final Artifact existingArtifact : artifactView.getParentEntity().getEntityArtifactList()) {
            if (!existingArtifact.getId().equals(artifactView.getEntity().getId())
                    && existingArtifact.getName().equals(artifactView.getEntity().getName())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if the name of the {@link ExternalLink} as specified by the user
     * is unique. The algorithm must be able to deal with the following use
     * cases:
     * <ol>
     * <li>User adds a new {@link ExternalLink}</li>
     * <li>User modifies an existing {@link ExternalLink} without changing its
     * name</li>
     * <li>User modifies an existing {@link ExternalLink} including an update to
     * the name</li>
     * </ol>
     * In all the above use cases this method must determine of the name of the
     * {@link ExternalLink} is unique among the list of {@link ExternalLink}s
     * associated by the parent entity.
     *
     * This method is made static to facilitate unit testing.
     *
     * @param existingExternalLinks The list of {@link ExternalLink}s already
     * present with the parent.
     * @param newOrModifiedExternalLink The new or modified {@link ExternalLink}
     * that is added or edited.
     * @return <code>true</code> if the name of the {@link ExternalLink} is
     * unique and hence allowed, <code>false</code> otherwise.
     */
    protected static boolean isExternalLinkNameUnique(
            final Set<ExternalLink> existingExternalLinks, final ExternalLink newOrModifiedExternalLink) {

        for (ExternalLink existingExternalLink : existingExternalLinks) {
            if (existingExternalLink.getName().equals(newOrModifiedExternalLink.getName())) {
                if (existingExternalLink.getId().equals(newOrModifiedExternalLink.getId())) {
                    continue;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    protected String getFormId(final UIComponent input) {
        UIComponent cmp = input;
        while ((cmp != null) && !(cmp instanceof UIForm)) {
            cmp = cmp.getParent();
        }
        return cmp != null ? cmp.getId() + ":" : "";
    }

    /**
     * Adds new {@link Tag} to parent {@link ConfigurationEntity}
     */
    public void addNewTag() {
        try {
            final EntityAttrTagView<C> tagView = getDialogAttrTag();
            Tag tag = tagEJB.findById(tagView.getTag());
            if (tag == null) {
                tag = tagView.getEntity();
            }

            C ent = tagView.getParentEntity();
            final Set<Tag> existingTags = ent.getTags();
            if (!existingTags.contains(tag)) {
                existingTags.add(tag);
                dao.save(ent);
                UiUtility.showInfoMessage(Entity.TAG, tag.getName(), Action.CREATE);
            }
        } finally {
            refreshParentEntity(dialogAttribute);
            resetFields();
            clearRelatedAttributeInformation();
            populateAttributesList();
        }
    }

    /**
     * The method builds a list of units that are already used. If the list is
     * not empty, it is displayed to the user and the user is prevented from
     * deleting them.
     */
    public void checkAttributesForDeletion() {
        Preconditions.checkNotNull(selectedAttribute);

        filteredDialogAttributes = Lists.newArrayList();
        nonDeletableAttributes = Lists.newArrayList();
        if (!canDelete(selectedAttribute)) {
            nonDeletableAttributes.add(selectedAttribute);
        }
    }

    /**
     * Deletes selected attributes from parent {@link ConfigurationEntity}.
     * These attributes can be {@link Tag},
     * {@link PropertyValue}, {@link Artifact} or {@link ExternalLink}
     *
     * @throws IOException attribute deletion failure
     */
    @SuppressWarnings("unchecked")
    public void deleteAttributes() throws IOException {
        Preconditions.checkNotNull(selectedAttribute);
        Preconditions.checkNotNull(dao);
        Preconditions.checkNotNull(nonDeletableAttributes);
        Preconditions.checkState(nonDeletableAttributes.isEmpty());

        int deletedAttributes = 0;
        if (selectedAttribute instanceof EntityAttrPropertyValueView<?>) {
            deletePropertyValue((T) selectedAttribute.getEntity());
        } else if (selectedAttribute instanceof EntityAttrArtifactView<?>) {
            deleteArtifact((Artifact) selectedAttribute.getEntity());
        } else if (selectedAttribute instanceof EntityAttrTagView<?>) {
            final Tag tagAttr = (Tag) selectedAttribute.getEntity();
            deleteTagFromParent(selectedAttribute.getParentEntity(), tagAttr);
        } else if (selectedAttribute instanceof EntityAttrExternalLinkView<?>) {
            deleteExternalLink((ExternalLink) selectedAttribute.getEntity());
        } else {
            throw new UnhandledCaseException();
        }

        ++deletedAttributes;

        nonDeletableAttributes = null;
        refreshParentEntity(dialogAttribute);
        clearRelatedAttributeInformation();
        populateAttributesList();
        UiUtility.showInfoMessage(Entity.PROPERTIES, Action.DELETE, deletedAttributes);
    }

    /**
     * A callback that gives the descendants opportunity to refresh the entity
     * that is related to the attribute that was just processed.
     *
     * @param attributeView the attributeView
     */
    protected void refreshParentEntity(final EntityAttributeView<C> attributeView) {
        // no refresh by default
    }

    protected void deletePropertyValue(final T propValueToDelete) {
        if (!isPropertyValueInherited(propValueToDelete)) {
            dao.deleteChild(propValueToDelete);
        } else {
            propValueToDelete.setPropValue(null);
            dao.saveChild(propValueToDelete);
        }
    }

    protected void validateAttribute(PropertyValue editedPropVal, String newValueStr) {
        final DataType dataType = editedPropVal.getProperty().getDataType();
        final String regexp = editedPropVal.getProperty().getRegexp();
        switch (Conversion.getUIElementFromProperty(editedPropVal.getProperty())) {
            case INPUT:
            case CALENDAR:
                EntityAttrPropertyValueView.validateSingleLine(newValueStr, dataType, regexp);
                break;
            case CSENTRY_HOST_AUTOCOMPLETE:
                if (csEntryService.isHostnameInvalid(newValueStr.trim())) {
                    throw UiUtility.getValidationError("Invalid host name");
                }
                break;
            case TEXT_AREA:
                EntityAttrPropertyValueView.validateMultiLine(newValueStr, dataType, regexp);
                break;
            case SELECT_ONE_MENU:
                if (Strings.isNullOrEmpty(newValueStr)) {
                    throw UiUtility.getValidationError("A value must be selected.");
                }
                break;
            case NONE:
            default:
                throw new UnhandledCaseException();
        }
    }

    private void deleteArtifact(final Artifact artifactToDelete) {
        dao.deleteChild(artifactToDelete, true);
    }

    private void deleteExternalLink(final ExternalLink externalLink) {
        dao.deleteExternalLink((EntityWithExternalLinks) getSelectedEntity(),
                externalLink);
    }

    /**
     * The main method that prepares the fields for any of the following
     * dialogs:
     * <ul>
     * <li>the dialog to modify a property value</li>
     * <li>the dialog to modify an artifact data</li>
     * </ul>
     */
    public void prepareModifyPropertyPopUp() {
        Preconditions.checkNotNull(selectedAttribute);
        //at least one property has to be selected
        dialogAttribute = selectedAttribute;

        if (getDialogAttrPropertyValue() != null) {
            final EntityAttrPropertyValueView<C> propertyValueView = getDialogAttrPropertyValue();
            PropertyValue propertyValue = propertyValueView.getEntity();
            propertyValueView.setPropertyNameChangeDisabled(propertyValue instanceof DevicePropertyValue
                    || propertyValue instanceof SlotPropertyValue || isPropertyValueInherited(propertyValue));
            propertyNameChangeOverride(propertyValueView);
            filterProperties();

            PrimeFaces.current().ajax().update("modifyPropertyValueForm:modifyPropertyValue");
            PrimeFaces.current().executeScript("PF('modifyPropertyValue').show();");
        }

        if (getDialogAttrArtifact() != null) {
            PrimeFaces.current().ajax().update("modifyArtifactForm:modifyArtifact");
            PrimeFaces.current().executeScript("PF('modifyArtifact').show();");
        } else if (getDialogExternalLink() != null) {
            PrimeFaces.current().ajax().update("modifyExternalLinkForm:modifyExternalLink");
            PrimeFaces.current().executeScript("PF('modifyExternalLink').show();");
        }
    }

    /**
     * This method is called after the {@link #prepareModifyPropertyPopUp()}
     * calculates the <code>isPropertyNameChangeDisabled</code> value and can be
     * used to override it from the descendant classes.
     *
     * @param propertyValueView the attribute (property value) for which this
     * callback is called
     */
    protected void propertyNameChangeOverride(final EntityAttrPropertyValueView<C> propertyValueView) {
        // no override by default
    }

    /**
     * Finds slot or device type object from database which selectedAttribute belongs to. After that refreshes selected
     * attribute from slot or device type object's property list. (This method is used for avoiding
     * OptimisticLockException at save when property value is modified via data table cell edit and using
     * 'Edit Property' dialog as well.)
     */
    public void updateSelectedAttribute() {
        // Refreshing selected attribute from database
        if (selectedAttribute instanceof EntityAttrPropertyValueView) {
            PropertyValue pv = ((EntityAttrPropertyValueView)selectedAttribute).getEntity();
            PropertyValue pvFresh = null;
            if (pv instanceof SlotPropertyValue) {
                Slot freshSlot = (Slot) dao.findById(((SlotPropertyValue)pv).getSlot().getId());
                pvFresh = freshSlot.getSlotPropertyList().stream().
                        filter(sp ->  pv.getId().equals(sp.getId())).findFirst().orElse(null);
            } else if (pv instanceof ComptypePropertyValue) {
                ComponentType freshDeviceType = (ComponentType) dao.findById(((ComptypePropertyValue)pv).getComponentType().getId());
                pvFresh = freshDeviceType.getComptypePropertyList().stream().
                        filter(sp ->  pv.getId().equals(sp.getId())).findFirst().orElse(null);
            }

            if (pvFresh != null) {
                ((EntityAttrPropertyValueView) selectedAttribute).refreshEntity(pvFresh);
            }
        }
    }

    /**
     * Modifies {@link PropertyValue}
     *
     * @param event the event
     */
    public void modifyPropertyValue(ActionEvent event) {
        Preconditions.checkNotNull(dialogAttribute);
        Preconditions.checkState(isSingleAttributeSelected());

        boolean reportedError = false;
        try {
            //setting value for all selected properties, and save them into DB
            SlotPropertyValue entity = null;
            if (dialogAttribute.getEntity() instanceof SlotPropertyValue) {
                entity = (SlotPropertyValue) dialogAttribute.getEntity();
            }

            //set the value for slot reference if it is slotProperty
            if ((entity != null) && (((EntityAttrPropertyValueView) dialogAttribute).isSlotReference())) {
                ((SlotPropertyValue) selectedAttribute.getEntity()).setReferenceToSlot(entity.getReferenceToSlot());
            } else {
                //set the value for not slot reference
                ((EntityAttrPropertyValueView) selectedAttribute).setPropertyValue(
                        ((EntityAttrPropertyValueView) dialogAttribute).getPropertyValue());

                //set value if slotProperty is alias
                if ((entity != null) && (entity.getAliasOf() != null)) {
                    ((SlotPropertyValue) selectedAttribute.getEntity()).setAliasOf(
                            ((SlotPropertyValue) dialogAttribute.getEntity()).getAliasOf());
                }
            }
            dao.saveChild(selectedAttribute.getEntity());

            UiUtility.showInfoMessage(Entity.PROPERTY, dialogAttribute.getName(), Action.UPDATE);
        } catch (EJBException e) {
            if (UiUtility.causedBySpecifiedExceptionClass(e, PropertyValueNotUniqueException.class)) {
                reportedError = true;
                final String formName = getFormId(event.getComponent());
                FacesContext.getCurrentInstance().validationFailed();
                FacesContext.getCurrentInstance().addMessage(formName + "propertyValidationMessage", new FacesMessage(
                        FacesMessage.SEVERITY_ERROR, UiUtility.ERROR_MESSAGE, "Value must be unique."));
            } else {
                throw e;
            }
        } finally {
            if (!reportedError) {
                refreshParentEntity(dialogAttribute);
                resetFields();
                clearRelatedAttributeInformation();
                populateAttributesList();
            }
        }
    }

    /**
     * Finds artifact file that was uploaded on the file system and returns it
     * to be downloaded
     *
     * @return Artifact file to be downloaded
     * @throws FileNotFoundException Thrown if file was not found on file system
     */
    @SuppressWarnings("unchecked")
    public StreamedContent getDownloadFile() throws FileNotFoundException {
        final S selectedArtifact = (S) downloadArtifactView.getEntity();
        // guess mime type based on the original file name, not on the name of the blob (UUID).
        final String contentType = FacesContext.getCurrentInstance().getExternalContext()
                .getMimeType(selectedArtifact.getName());

        byte[] content = selectedArtifact.getBinaryData().getContent();

        // The null check is needed as some uploaded artifacts were missing from the file system
        // during migration of artifacts from file system to blobs.
        if (content == null) {
            throw new FileNotFoundException("Artifact content is missing.");
        }

        return new DefaultStreamedContent(new ByteArrayInputStream(content), contentType, selectedArtifact.getName());
    }

    public EntityAttributeView<C> getDownloadArtifact() {
        return downloadArtifactView;
    }

    public void setDownloadArtifact(EntityAttrArtifactView<C> downloadArtifact) {
        this.downloadArtifactView = downloadArtifact;
    }

    public void setExternalLinkView(EntityAttrExternalLinkView<C> externalLinkView) {
        this.externalLinkView = externalLinkView;
    }

    /**
     * This method determines whether the user has privilege to edit attributes.
     *
     * @return <code>true</code> if the user has privilege to edit attributes,
     * <code>false</code> otherwise.
     */
    public abstract boolean hasEditPermission();

    /**
     * This method determines whether the entity attribute should have the
     * "pencil" icon displayed in the UI.
     *
     * @param attributeView The object containing the UI info for the attribute
     * table row.
     * @return <code>true</code> if the attribute can be edited,
     * <code>false</code> otherwise.
     */
    public abstract boolean canEdit(EntityAttributeView<C> attributeView);

    /**
     * This method determines whether the entity attribute can be deleted - is
     * not used anywhere.
     *
     * @param attributeView The object containing the UI info for the attribute
     * table row.
     * @return <code>true</code> if the attribute can be deleted,
     * <code>false</code> otherwise.
     */
    protected abstract boolean canDelete(EntityAttributeView<C> attributeView);

    private boolean isPropertyValueInherited(PropertyValue propValue) {
        List<ComptypePropertyValue> parentProperties = null;
        EntityWithProperties parentEntity = propValue.getPropertiesParent();
        if (parentEntity != null) {
            if (parentEntity instanceof Slot) {
                if (((Slot) parentEntity).isHostingSlot()) {
                    parentProperties = ((Slot) parentEntity).getComponentType().getComptypePropertyList();
                } else {
                    return false;
                }
            } else if (parentEntity instanceof Device) {
                parentProperties = ((Device) parentEntity).getComponentType().getComptypePropertyList();
            } else if (parentEntity instanceof ComponentType) {
                return false;
            } else {
                throw new UnhandledCaseException();
            }
        }

        if (Utility.isNullOrEmpty(parentProperties)) {
            return false;
        }

        final String propertyName = propValue.getProperty().getName();
        for (final ComptypePropertyValue inheritedPropVal : parentProperties) {
            if (inheritedPropVal.isPropertyDefinition()
                    && ((inheritedPropVal.isDefinitionTargetDevice() && parentEntity instanceof Device)
                    || (inheritedPropVal.isDefinitionTargetSlot() && parentEntity instanceof Slot))
                    && propertyName.equals(inheritedPropVal.getProperty().getName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * This method is called when there is no main entity selected and the
     * attributes tables must be made empty
     */
    public void clearRelatedAttributeInformation() {
        attributes = null;
        filteredAttributes = null;
        selectedAttribute = null;
    }

    protected abstract C getSelectedEntity();

    protected abstract T newPropertyValue();

    protected abstract Artifact newArtifact();

    protected void deleteTagFromParent(C parent, Tag tag) {
        Preconditions.checkNotNull(parent);
        parent.getTags().remove(tag);
        dao.save(parent);
    }

    /**
     * Filters a list of possible properties to attach to the entity based on
     * the association type.
     */
    protected abstract void filterProperties();

    /**
     * Populates attributes specified by id. If id is null, populates all
     * attributes.
     */
    public abstract void populateAttributesList();

    /**
     * Returns list of all attributes for current {@link ConfigurationEntity}
     *
     * @return the list of attributes
     */
    public List<EntityAttributeView<C>> getAttributes() {
        return attributes;
    }

    /**
     * Prepares the UI data for addition of {@link Tag}
     */
    public void prepareForTagAdd() {
        dialogAttribute = new EntityAttrTagView<>(getSelectedEntity());
    }

    /**
     * Prepares the UI data for addition of {@link Artifact}
     */
    public void prepareForArtifactAdd() {
        final Artifact artifact = newArtifact();
        final C selectedEntity = getSelectedEntity();
        artifact.setArtifactsParent(selectedEntity);
        dialogAttribute = new EntityAttrArtifactView<>(artifact, selectedEntity);
    }

    public void prepareForExternalLinkAdd() {
        final ExternalLink externalLink = new ExternalLink();
        final C selectedEntity = getSelectedEntity();
        dialogAttribute = new EntityAttrExternalLinkView<>(externalLink, selectedEntity);
    }

    /**
     * @return the selected table row (UI view presentation)
     */
    public EntityAttributeView<C> getSelectedAttribute() {
        return selectedAttribute;
    }

    /**
     * @param selectedAttribute the selected property value, tag or artifact
     */
    public void setSelectedAttribute(EntityAttributeView<C> selectedAttribute) {
        this.selectedAttribute = selectedAttribute;
    }

    protected void setDao(DAO<C> dao) {
        this.dao = dao;
    }

    /**
     * Used by the {@link Tag} input value control to display the list of
     * auto-complete suggestions. The list contains the tags already stored in
     * the database.
     *
     * @param query The text the user typed so far.
     * @return The list of auto-complete suggestions.
     */
    public List<String> tagAutocompleteText(String query) {
        return tagEJB.findByNamePart(query, getTagAutocompleteMaxResults() + 1)
                .stream()
                .map(Tag::getName)
                .collect(Collectors.toList());
    }

    /**
     * @return max result set size for tag autocomplete component
     */
    public int getTagAutocompleteMaxResults() {
        return TAG_AUTOCOMPLETE_MAX_RESULTS;
    }

    /**
     * @return the filteredAttributes
     */
    public List<EntityAttributeView<C>> getFilteredAttributes() {
        return filteredAttributes;
    }

    /**
     * @param filteredAttributes the filteredAttributes to set
     */
    public void setFilteredAttributes(List<EntityAttributeView<C>> filteredAttributes) {
        this.filteredAttributes = filteredAttributes;
    }

    /**
     * @return the attributeKinds
     */
    public abstract List<SelectItem> getAttributeKinds();

    /**
     * @return <code>true</code> if an attribute is selected,
     * <code>false</code> otherwise.
     */
    public boolean isSingleAttributeSelected() {
        return (selectedAttribute != null);
    }

    /**
     * Checks if parameter is EntityPropertyValueView instance
     *
     * @param entityAttributeView the parameter that type must be checked
     * @return true if parameter is instance of EntityPropertyValueView
     */
    private boolean isEntityAttrProvValue(EntityAttributeView<C> entityAttributeView) {

        return entityAttributeView instanceof EntityAttrPropertyValueView;
    }

    /**
     * This method is called from the UI and resets a table with the implicit ID
     * "propertySelect" in the form indicated by the parameter.
     *
     * @param id the ID of the from containing a table #propertySelect
     */
    public void resetPropertySelection(final String id) {
        final DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
                .findComponent(id + ":propertySelect");
        dataTable.setSortBy(null);
        dataTable.setFirst(0);
        dataTable.setFilteredValue(null);
        dataTable.setFilters(null);
    }

    /**
     * @return the nonDeletableAttributes
     */
    public List<EntityAttributeView<C>> getNonDeletableAttributes() {
        return nonDeletableAttributes;
    }

    /**
     * @return the filteredDialogAttributes
     */
    public List<EntityAttributeView<C>> getFilteredDialogAttributes() {
        return filteredDialogAttributes;
    }

    /**
     * @param filteredDialogAttributes the filteredDialogAttributes to set
     */
    public void setFilteredDialogAttributes(List<EntityAttributeView<C>> filteredDialogAttributes) {
        this.filteredDialogAttributes = filteredDialogAttributes;
    }

    /**
     * @return the dialogAttribute
     */
    public EntityAttrTagView<C> getDialogAttrTag() {
        if (dialogAttribute instanceof EntityAttrTagView<?>) {
            return (EntityAttrTagView<C>) dialogAttribute;
        }
        return null;
    }

    /**
     * @return the dialogAttribute
     */
    public EntityAttrPropertyValueView<C> getDialogAttrPropertyValue() {
        if (dialogAttribute instanceof EntityAttrPropertyValueView<?>) {
            if (originalPropertyValue == null) {
                String propValue = ((EntityAttrPropertyValueView<C>) dialogAttribute).getPropertyValue();
                originalPropertyValue = propValue != null ? propValue : "";
            }
            return (EntityAttrPropertyValueView<C>) dialogAttribute;
        }
        return null;
    }

    /**
     * This method resets dialog attribute's property value to default value if it default value was set.
     */
    public void resetDialogAttrPropertyValue() {
        if (dialogAttribute instanceof EntityAttrPropertyValueView<?>) {
            String defaultPropertyValue = getDefaultDialogAttrPropertyValue();
            if (defaultPropertyValue != null) {
                if (originalPropertyValue == null) {
                    originalPropertyValue = ((EntityAttrPropertyValueView<C>) dialogAttribute).getPropertyValue();
                }
                ((EntityAttrPropertyValueView<C>) dialogAttribute).setPropertyValue(defaultPropertyValue);
            }
        }
    }

    /**
     * @return if dialog attribute's property has default value.
     */
    public boolean isDialogAttrPropertyValueResetable() {
        if (dialogAttribute instanceof EntityAttrPropertyValueView<?>) {
            String defaultPropertyValue = getDefaultDialogAttrPropertyValue();
            if (defaultPropertyValue != null) {
                return true;
            }
        }

        return false;
    }

    private String getDefaultDialogAttrPropertyValue() {
        if (dialogAttribute instanceof EntityAttrPropertyValueView<?>) {
            if(((EntityAttrPropertyValueView<C>) dialogAttribute).getEntity() instanceof SlotPropertyValue) {
                SlotPropertyValue slotPropertyValue =
                        (SlotPropertyValue) ((EntityAttrPropertyValueView<C>) dialogAttribute).getEntity();
                // Looking for default value of the slot property value in device type properties
                ComptypePropertyValue pv =  slotPropertyValue.getSlot().getComponentType().getComptypePropertyList()
                        .stream()
                        .filter(ComptypePropertyValue::isDefinitionTargetSlot)
                        .filter(p -> p.getProperty().getId()
                                .equals(((EntityAttrPropertyValueView<C>) dialogAttribute).getProperty().getId()))
                        .findAny().orElse(null);
                if (pv != null && pv.getPropValue() != null) {
                    return new EntityAttrPropertyValueView<>(
                            pv, slotPropertyValue.getSlot().getComponentType()).getValue();
                }
            }
        }

        return null;
    }

    /**
     * @return the dialogAttribute
     */
    public EntityAttrArtifactView<C> getDialogAttrArtifact() {
        if (dialogAttribute instanceof EntityAttrArtifactView<?>) {
            return (EntityAttrArtifactView<C>) dialogAttribute;
        }
        return null;
    }

    /**
     * @return the dialogExternalLink
     */
    public EntityAttrExternalLinkView<C> getDialogExternalLink() {
        if (dialogAttribute instanceof EntityAttrExternalLinkView<?>) {
            return (EntityAttrExternalLinkView<C>) dialogAttribute;
        }
        return null;
    }

    /**
     * @return the overlay
     */
    public OverlayValueProvider getOverlay() {
        return overlay;
    }

    /**
     * Converts multiline text for HTML representation,
     * replaces all new line characters to HTML line break (&lt;br/&gt;).
     *
     * @param text Multiline input text
     * @return HTML representation
     */
    public String createDescription(String text) {
        return text.replaceAll("\\r\\n|\\r|\\n", "<br/>");
    }

    /**
     * @return if dialog's {@link PropertyValue} is alias
     */
    public boolean isDialogAttributeAlias() {
        return isDialogAttributeAlias(dialogAttribute);
    }

    public boolean isDialogAttributeAlias(final EntityAttributeView<C> attributeView) {
        if (attributeView instanceof EntityAttrPropertyValueView) {
            Property property = ((EntityAttrPropertyValueView) attributeView).getProperty();
            return property != null && property.getAliasOf() != null;
        }

        return false;
    }

    public boolean isSlotReference(final EntityAttributeView<C> attributeView) {
        if (attributeView instanceof EntityAttrPropertyValueView) {
            return  ((EntityAttrPropertyValueView) attributeView).isSlotReference();
        }

        return false;
    }

    /**
     * Calculates the CSS class name for the entity attribute.
     *
     * @param attributeView {@link EntityAttributeView} belonging to attribute
     * @return the name of the CSS class
     */
    public String calcValueClass(final EntityAttributeView<C> attributeView) {
        Preconditions.checkNotNull(attributeView);
        if (attributeView instanceof EntityAttrPropertyValueView) {
            EntityAttrPropertyValueView propertyValueView = (EntityAttrPropertyValueView)attributeView;
            if (BuiltInDataType.CSENTRY_HOST.equals(
                    Conversion.getBuiltInDataType(propertyValueView.getProperty().getDataType()))) {
                if (csEntryValidationService.isHostNameInvalid(propertyValueView.getPropertyValue())) {
                    return "attributeHostnameInvalid";
                }
            }
        }
        return "attributeValueValid";
    }

    /**
     * Calculates the CSS class name for the entity attribute name and kind, highlighting device properties.
     *
     * @param attributeView {@link EntityAttributeView} belonging to attribute
     * @return the name of the CSS class
     */
    public String calcPropertyColumnClass(final EntityAttributeView<C> attributeView) {
        Preconditions.checkNotNull(attributeView);
        if (attributeView instanceof EntityAttrPropertyValueView) {
            EntityAttrPropertyValueView propertyValueView = (EntityAttrPropertyValueView) attributeView;
            if (EntityAttributeKind.DEVICE_PROPERTY.equals(propertyValueView.getKind())) {
                return "attributeDeviceProperty";
            }
        }
        return "attributeNonDeviceProperty";
    }

    /**
     * This method is called when edition of cell is finished in Properties data table. Validates input and saving new
     * property value is initiated from here.
     *
     * @param event cell edit event object
     */
    public void onAttributeCellEdit(CellEditEvent event) {
        final Object oldValue = event.getOldValue();

        List<EntityAttributeView<C>> attributeViews = getAttributes();
        String entityId = event.getRowKey();
        List<EntityAttributeView<C>> modifiedAttributes =
                attributeViews.stream().filter(av -> entityId.equals(av.getId())).collect(Collectors.toList());
        if (modifiedAttributes.size() == 1) {
            EntityAttrPropertyValueView view = (EntityAttrPropertyValueView) modifiedAttributes.get(0);
            final String newValueStr = StringUtils.isNotEmpty(view.getUiValue()) ? view.getUiValue() : null;
            PropertyValue editedPropVal = view.getEntity();
            final String oldValueStr = oldValue == null ? null
                    : getEditEventValue(oldValue, Conversion.getUIElementFromProperty(editedPropVal.getProperty()));
            try {
                if (!StringUtils.equals(prepareForComparison(newValueStr), prepareForComparison(oldValueStr))) {
                    if (newValueStr == null) {
                        throw new ValidatorException(new FacesMessage("Error",
                                "Please enter a property value"));
                    }
                    validateAttribute(editedPropVal, newValueStr);
                    checkAndUpdateAttribute(editedPropVal, newValueStr);
                }
            } catch (final DefaultValueNotAllowedException e) {
                if (modifiedAttributes.get(0) instanceof EntityAttrPropertyValueView) {
                    view.setPropertyValue(null);
                }
                UiUtility.showErrorMessage(valueForErrorMessage(newValueStr) + ": " +
                                Utility.DEFAULT_VALUE_NEVER_UNIQUE_ERROR,
                        ExceptionUtils.getStackTrace(e));
            } catch (final ValidatorException e) {
                view.setValue(oldValueStr);
                FacesContext.getCurrentInstance().validationFailed();
                UiUtility.showErrorMessage(valueForErrorMessage(newValueStr) + ": " + e.getFacesMessage().getDetail(),
                        ExceptionUtils.getStackTrace(e));
            } catch (PropertyValueNotUniqueException e) {
                if (modifiedAttributes.get(0) instanceof EntityAttrPropertyValueView) {
                    view.setPropertyValue(oldValueStr);
                }
                UiUtility.showErrorMessage(valueForErrorMessage(newValueStr) + ": " +
                                Utility.VALUE_MUST_BE_UNIQUE_ERROR,
                        ExceptionUtils.getStackTrace(e));
            }
        }
    }

    private String prepareForComparison(final String value) {
        return value == null ? "" : value.trim().replace("\r", "");
    }

    private String valueForErrorMessage(final String value) {
        return StringUtils.isEmpty(value) ? "Empty value" : "\"" + value + "\"";
    }

    protected abstract void checkAndUpdateAttribute(final PropertyValue propVal, final String newValueStr);

    /**
     * Extract value from {@link CellEditEvent} object. Which value to extract depends on the used UI element.
     *
     * The object is usually a list with 4 elements. The first 3 contain data, the last one contains a timestamp
     * indicating when the value was set.
     *
     * @param val {@link CellEditEvent} object
     * @param propValueUIElement which UI element was used for input.
     * @return extracted value.
     */
    protected String getEditEventValue(final Object val, final PropertyValueUIElement propValueUIElement) {
        if ((val == null) || (val instanceof String)){
            return (String)val;
        }
        if (val instanceof List<?>) {
            final List<?> valList = (List<?>)val;
            if (propValueUIElement == null) {
                for (final Object v : valList) {
                    if (v != null) {
                        return v.toString();
                    }
                }
                return null;
            } else {
                switch (propValueUIElement) {
                    case INPUT:
                    case CALENDAR:
                    case CSENTRY_HOST_AUTOCOMPLETE:
                        return Objects.toString(valList.get(0), null);
                    case TEXT_AREA:
                        return Objects.toString(valList.get(1), null);
                    case SELECT_ONE_MENU:
                        return Objects.toString(valList.get(2), null);
                    case NONE:
                    default:
                        throw new UnhandledCaseException();
                }
            }
        }
        throw new RuntimeException("MultiPropertyValue: UI string value cannot be extracted.");
    }

    /**
     * The validator for the UI input field in case of data table cell editor when host name is added and validated via
     * CSEntry service.
     *
     * @param ctx {@link javax.faces.context.FacesContext}
     * @param component {@link javax.faces.component.UIComponent}
     * @param value The value
     * @throws ValidatorException {@link javax.faces.validator.ValidatorException}
     */
    public void hostnameValidator(FacesContext ctx, UIComponent component, Object value) throws ValidatorException {
        if (value == null) {
            throw UiUtility.getValidationError("No value to parse.");
        }

        if (csEntryService.isHostnameInvalid(value.toString().trim())) {
            throw UiUtility.getValidationError("Invalid host name");
        }
    }
}
