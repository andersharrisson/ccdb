/*
 * Copyright (C) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.ui.trees;

import org.apache.commons.lang.StringUtils;
import org.openepics.discs.conf.ejb.ComptypeEJB;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.fields.DeviceTypeFields;
import org.openepics.discs.conf.ui.util.UiUtility;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.SortOrder;
import org.primefaces.model.TreeNode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Representing device type tree data structure.
 *
 * @author Zoltan Runyo <zoltan.runyo@ess.eu>
 */
public class DeviceTypeTree {
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";

    private final ComptypeEJB comptypeEJB;
    private TreeNode root;
    private boolean onlyUnreferenced;

    private String sortField;
    private final SortOrder sortOrder = SortOrder.ASCENDING;
    private final Map<String, String> filters = new HashMap<>();
    private final List<String> expandedNodes = new ArrayList<>();

    public DeviceTypeTree(ComptypeEJB comptypeEJB) {
        this.comptypeEJB = comptypeEJB;
    }

    private void createTree() {
        List<ComponentType> deviceTypes = loadAll();
        root = new DefaultTreeNode(new ComponentType(), null);
        String currentFirstLetter = "";
        TreeNode currentFirstLetterNode = null;
        for (ComponentType componentType : deviceTypes) {
            if(!onlyUnreferenced || !inUse(componentType)) {
                String firstLetter = componentType.getName().toUpperCase().substring(0, 1);
                if (!currentFirstLetter.equals(firstLetter)) {
                    currentFirstLetter = firstLetter;
                    currentFirstLetterNode = new DefaultTreeNode(new ComponentType(currentFirstLetter), root);
                    currentFirstLetterNode.setSelectable(false);
                    currentFirstLetterNode.setExpanded(expandedNodes.contains(currentFirstLetter));
                }
                new DefaultTreeNode(componentType, currentFirstLetterNode);
            }
        }
    }

    /**
     * Loads all device types from database.
     * @return {@link List} of device types
     */
    public List<ComponentType> loadAll() {
        final String name = filters.getOrDefault(NAME, null);
        final String description = filters.getOrDefault(DESCRIPTION, null);

        // sort by name is the default
        return comptypeEJB.findLazy(0, Integer.MAX_VALUE,
                selectSortField(sortField), (sortField == null) ? org.openepics.discs.conf.util.SortOrder.ASCENDING :
                        UiUtility.translateToCCDBSortOrder(sortOrder),
                name, description);
    }

    private DeviceTypeFields selectSortField(final String sortField) {
        if (sortField == null)
            return DeviceTypeFields.NAME;

        switch (sortField) {
            case NAME:
                return DeviceTypeFields.NAME;
            case DESCRIPTION:
                return DeviceTypeFields.DESCRIPTION;
            default:
                return null;
        }
    }

    private boolean inUse(ComponentType componentType) {
        return !comptypeEJB.findWhereIsComponentTypeUsed(componentType, 1).isEmpty();
    }

    /**
     * Optionally filters tree for unreferenced device types
     * @param onlyUnreferenced if <code>true<code/> then tree will contain only unreferenced device types, otherwise
     *                         will contain all device types
     */
    public void setOnlyUnreferenced(boolean onlyUnreferenced) {
        if(this.onlyUnreferenced != onlyUnreferenced) {
            this.onlyUnreferenced = onlyUnreferenced;
            resetTree();
        }
    }

    /**
     * @return Root tree node
     */
    public TreeNode getRoot() {
        if(root == null) {
            createTree();
        }
        return root;
    }

    /**
     * Finds tree node having device type with id
     * @param id database id of device type we are looking for
     * @return {@link TreeNode} object if found otherwise null
     */
    public TreeNode findAndSelectById(long id) {
        for (TreeNode indexNode : root.getChildren()) {
            for (TreeNode leafNode : indexNode.getChildren()) {
                if(Long.valueOf(id).equals(((ComponentType)leafNode.getData()).getId())) {
                    indexNode.setExpanded(true);
                    leafNode.setSelected(true);
                    return leafNode;
                }
            }
        }

        return null;
    }

    /**
     * Finds tree node having device type with name
     * @param name name of device type in database we are looking for
     * @return {@link TreeNode} object if found otherwise null
     */
    public TreeNode findAndSelectByName(final String name) {
        for (TreeNode indexNode : root.getChildren()) {
            for (TreeNode leafNode : indexNode.getChildren()) {
                if(name.equals(((ComponentType)leafNode.getData()).getName())) {
                    indexNode.setExpanded(true);
                    leafNode.setSelected(true);
                    return leafNode;
                }
            }
        }

        return null;
    }

    /**
     * @return <code>true<code/> if device tree is empty otherwise <code>false<code/>
     */
    public boolean isEmpty() {
        return root == null || root.getChildCount() == 0;
    }

    /**
     * Sets a filter
     * @param key field name
     * @param value filter value
     */
    public void setFilter(String key, String value) {
        String newValue = !StringUtils.isEmpty(value) ? value : null;
        String oldValue = filters.getOrDefault(key, null);
        if((oldValue == null && newValue != null) || (oldValue != null && newValue == null)
                || (oldValue != null && !oldValue.equals(newValue))) {
            filters.put(key, newValue);
            resetTree();
        }
    }

    /**
     * Sets root node to null (tree will be recreated at next {@link #getRoot() getRoot} method call)
     */
    public void resetTree() {
        saveNodeStates();
        root = null;
    }

    /**
     * Refreshes the list of expanded nodes.
     */
    public void saveNodeStates() {
        expandedNodes.clear();
        if (root != null && root.getChildren() != null) {
            for (TreeNode node : root.getChildren()) {
                if (node.isExpanded()) {
                    expandedNodes.add(((ComponentType) node.getData()).getName());
                }
            }
        }
    }

    /**
     * Deselect all leaf nodes and collapse all index nodes
     */
    public void unselectAllNodes() {
        if (root != null && root.getChildren() != null) {
            for (TreeNode indexNode : root.getChildren()) {
                for (TreeNode leafNode : indexNode.getChildren()) {
                    leafNode.setSelected(false);
                }
                indexNode.setExpanded(false);
            }
        }
    }
}
