/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.ui.trees;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.openepics.discs.conf.ejb.SlotEJB;
import org.openepics.discs.conf.ejb.SlotPairEJB;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotPair;
import org.openepics.discs.conf.ent.SlotRelationName;
import org.openepics.discs.conf.ui.util.ConnectsManager;
import org.openepics.discs.conf.views.SlotView;

/**
 * Implements extrinsic method, that returns tree node's children based on connects database.
 * Takes care of removing cycles.
 *
 * @author ilist
 *
 */
public class ConnectsTree extends SlotTree {
    private final ConnectsManager connectsManager;
    private final SlotPairEJB slotPairEJB;

    /**
     * Constructs the connects tree.
     *
     * @param slotEJB slotEJB
     * @param slotPairEJB slotPairEJB
     * @param connectsManager connects manager
     */
    public ConnectsTree(SlotEJB slotEJB, SlotPairEJB slotPairEJB, ConnectsManager connectsManager) {
        super(slotEJB);
        this.slotPairEJB = slotPairEJB;
        this.connectsManager = connectsManager;
    }

    /**
     * Containers and nodes containing the filter string are present.
     * @param node the node
     * @return should the node be displayed
     */
    @Override
    public boolean isNodeInFilter(BasicTreeNode<SlotView> node) {
        final SlotView slotView = node.getData();
        return !slotView.isHostingSlot() || slotView.getName().toUpperCase().contains(getAppliedFilter());
    }

    /**
     * Builds the connects hierarchy.
     * This is done as follows:
     *  1. Loading all {@link SlotPair}s that are already ordered by parent and pair ordering.
     *  2. For each selected node recursively traversing the contains hierarchy to find all children of the selected
     *     nodes in the contains hierarchy.
     *  3. The connects tree is build with the data from the connects hierarchy.
     *  4. Redundant roots are removed.
     *
     * @param selectedNodes The nodes from "contains" hierarchy to be searched for in "connects" hierarchy.
     */
    private void buildHierarchy(List<FilteredTreeNode<SlotView>> selectedNodes) {
        // Loading slot pairs.
        final Map<Slot, List<Slot>> containsHierarchy = new HashMap<>();
        slotPairEJB.findAllOrdered().stream()
             .filter(slotPair -> slotPair.getSlotRelation().getName().equals(SlotRelationName.CONTAINS))
             .forEach(slotPair -> addElementToListInMap(
                     containsHierarchy, slotPair.getParentSlot(), slotPair.getChildSlot()));

        // Finding all related slots in the contains hierarchy.
        final List<Slot> slotsInSubtree = new LinkedList<>();
        selectedNodes.stream()
                     .forEach(node -> addChildren(containsHierarchy, slotsInSubtree, node.getData().getCachedSlot()));

        // Building tree.
        AtomicInteger order = new AtomicInteger(0);
        bufferedAllChildren = slotsInSubtree.stream()
                             .map(slot -> new SlotView(slot, getRootNode().getData(), order.getAndIncrement(), slotEJB))
                             .map(slotView -> new FilteredTreeNode<SlotView>(slotView, getRootNode(), this))
                             .filter(slotViewFilteredTreeNode -> !getAllChildren(slotViewFilteredTreeNode).isEmpty())
                             .collect(Collectors.toList());

        // Removing redundant nodes.
        removeRedundantRoots();

        resetState();
        initialized = true;
    }

    private void addChildren(Map<Slot, List<Slot>> containsHierarchy, List<Slot> slotsInSubtree, Slot slot) {
        slotsInSubtree.add(slot);

        final List<Slot> children = containsHierarchy.get(slot);
        if (children != null) {
            children.stream()
                    .forEach(child -> addChildren(containsHierarchy, slotsInSubtree, child));
        }
    }

    private void addElementToListInMap(Map<Slot, List<Slot>> map, Slot key, Slot value) {
        if (!map.containsKey(key)) {
            map.put(key, new ArrayList<>());
        }

        map.get(key).add(value);
    }

    /**
     * Initializes the connects tree hierarchy.
     * @param selectedNodes The nodes from "contains" hierarchy to be searched for in "connects" hierarchy.
     */
    @Override
    public void initHierarchy(List<FilteredTreeNode<SlotView>> selectedNodes) {
        try {
            connectsManager.startCaching();
            buildHierarchy(selectedNodes);
        } finally {
            connectsManager.stopCaching();
        }
    }

    /**
     * Returns "cable" children of current tree node. Takes care of cycles.
     * Takes care of cable numbers.
     *
     * @param parent the parent node
     * @return it children
     */
    @Override
    public List<? extends BasicTreeNode<SlotView>> getDefaultAllChildren(BasicTreeNode<SlotView> parent) {
        final SlotView parentSlotView = parent.getData();
        final Slot parentSlot = parentSlotView.getSlot();

        List<Slot> childSlots = connectsManager.getSlotConnects(parentSlotView.getSlot());

        final List<BasicTreeNode<SlotView>> allChildren = new ArrayList<>();

        for (Slot child : childSlots) {
            if (hasCycle(parentSlotView, child.getId())) {
                // This sets the default for parent if the only connection from the slot is to itself.
                // If there are any other connections, this will get set to correct value after the loop.
                parentSlotView.setCableName(connectsManager.getCables(parentSlot,
                                                                      child).get(0).getName());
                continue;
            }
            final SlotView childSlotView = new SlotView(child, parentSlotView, 0, slotEJB);
            allChildren.add(new FilteredTreeNode<SlotView>(childSlotView, parent, this));

            // points to parent
            childSlotView.setCableName(connectsManager.getCables(parentSlot, child).get(0).getName());
        }

        // points to first child
        // XXX lazy loading may introduce bugs here
        if (!allChildren.isEmpty()) {
            parentSlotView.setCableName(connectsManager.getCables(parentSlot,
                                                    allChildren.get(0).getData().getSlot()).get(0).getName());
        }
        return allChildren;
    }

    /**
     * Checks if there is a cycle formed with node ID.
     *
     * @param parentSlotView the parent slot view
     * @param id to search for
     * @return true if there is a cycle
     */
    private boolean hasCycle(SlotView parentSlotView, Long id) {
        while (parentSlotView != null) {
            if (id.equals(parentSlotView.getId())) {
                return true;
            }
            parentSlotView = parentSlotView.getParentNode();
        }
        return false;
    }
}
