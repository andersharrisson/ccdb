/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */

package org.openepics.discs.conf.ui;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.EJBException;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.omnifaces.cdi.ViewScoped;
import org.openepics.discs.conf.dl.annotations.ComponentTypesLoader;
import org.openepics.discs.conf.dl.common.DataLoader;
import org.openepics.discs.conf.dl.common.DataLoaderResult;
import org.openepics.discs.conf.ejb.ComptypeEJB;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.ComptypePropertyValue;
import org.openepics.discs.conf.ent.ExternalLink;
import org.openepics.discs.conf.export.ExportTable;
import org.openepics.discs.conf.ui.common.AbstractExcelSingleFileImportUI;
import org.openepics.discs.conf.ui.common.DataLoaderHandler;
import org.openepics.discs.conf.ui.common.ExcelSingleFileImportUIHandlers;
import org.openepics.discs.conf.ui.common.UIException;
import org.openepics.discs.conf.ui.export.ExportSimpleTableDialog;
import org.openepics.discs.conf.ui.export.SimpleTableExporter;
import org.openepics.discs.conf.ui.util.Action;
import org.openepics.discs.conf.ui.util.Entity;
import org.openepics.discs.conf.ui.util.UiUtility;
import org.openepics.discs.conf.util.ImportFileStatistics;
import org.openepics.discs.conf.util.Utility;
import org.openepics.discs.conf.views.ComponentTypeView;
import org.primefaces.PrimeFaces;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.event.FileUploadEvent;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.primefaces.model.TreeNode;

/**
 * Controller bean for manipulation of {@link ComponentType} attributes
 *
 * @author vuppala
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 * @author <a href="mailto:andraz.pozar@cosylab.com">Andraž Požar</a>
 */
@Named
@ViewScoped
public class ComponentTypeManager implements SimpleTableExporter, ExcelSingleFileImportUIHandlers, Serializable {
    private static final long serialVersionUID = 1156974438243970794L;

    private static final Logger LOGGER = Logger.getLogger(ComponentTypeManager.class.getCanonicalName());

    @Inject
    private ComptypeEJB comptypeEJB;
    @Inject
    private ComptypeAttributesController comptypeAttributesController;
    @Inject
    private DataLoaderHandler dataLoaderHandler;
    @Inject
    @ComponentTypesLoader
    private DataLoader compTypesDataLoader;
    @Inject
    private DeviceTypeTreeController deviceTypeTreeController;

    private transient ExcelSingleFileImportUI excelSingleFileImportUI;

    private ComponentTypeView selectedComponent;

    private List<DuplicatePair> duplicateDeviceTypes;
    private List<ComponentTypeView> selectedDeviceTypes;
    private List<ComponentTypeView> usedDeviceTypes;
    private List<ComponentTypeView> filteredDialogTypes;

    private transient ExportSimpleTableDialog simpleTableExporterDialog;

    private Long paramId;
    private String paramName;
    private boolean isNewDeviceType;

    private class ExcelSingleFileImportUI extends AbstractExcelSingleFileImportUI {
        /** Construct the file import UI for the device type data loader. */
        public ExcelSingleFileImportUI() {
            super.init();
        }

        @Override
        public void setDataLoader() {
            dataLoader = compTypesDataLoader;
        }

        @Override
        public void doImport() {
            try (InputStream inputStream = new ByteArrayInputStream(importData)) {
                setLoaderResult(dataLoaderHandler.loadData(inputStream, compTypesDataLoader));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private class ExportSimpleDevTypeTableDialog extends ExportSimpleTableDialog {
        @Override
        protected String getTableName() {
            return "Device types";
        }

        @Override
        protected String getFileName() {
            return "ccdb_device_types";
        }

        @Override
        protected void addHeaderRow(ExportTable exportTable) {
            exportTable.addHeaderRow("Operation", "Device Type Name", "Device Type Description", "Property Type",
                    "Property Name", "Property Value");
        }

        @Override
        protected void addData(ExportTable exportTable) {
            final List<ComponentType> exportData = deviceTypeTreeController.getDeviceTypeTree().loadAll();
            for (final ComponentType devType : exportData) {
                exportTable.addDataRow(DataLoader.CMD_UPDATE_DEVICE_TYPE, devType.getName(), devType.getDescription());
                for (final ComptypePropertyValue pv : devType.getComptypePropertyList()) {
                    exportTable.addDataRow(DataLoader.CMD_UPDATE_PROPERTY, devType.getName(), null, getPropertyType(pv),
                            pv.getProperty().getName(), pv.getPropValue());
                }
                for (final ExternalLink externalLink : devType.getExternalLinkList()) {
                    exportTable.addDataRow(DataLoader.CMD_UPDATE_PROPERTY, devType.getName(), null,
                            DataLoader.PROP_EXTERNAL_LINK, externalLink.getName(), externalLink.getUri());
                }
            }
        }

        private String getPropertyType(final ComptypePropertyValue pv) {
            if (!pv.isPropertyDefinition()) {
                return DataLoader.PROP_TYPE_DEV_TYPE;
            } else if (pv.isDefinitionTargetSlot()) {
                return DataLoader.PROP_TYPE_SLOT;
            } else {
                // Should be removed after Device and Device property will be taken out completely
                return DataLoader.PROP_TYPE_DEV_INSTANCE;
            }
        }

        @Override
        protected String getExcelTemplatePath() {
            return "/resources/templates/ccdb_device_types.xlsx";
        }

        @Override
        protected int getExcelDataStartRow() {
            return 10;
        }
    }

    /** Java EE post construct life-cycle method. */
    @PostConstruct
    public void init() {
        try {
            excelSingleFileImportUI = new ExcelSingleFileImportUI();
            simpleTableExporterDialog = new ExportSimpleDevTypeTableDialog();
            resetFields();
        } catch (Exception e) {
            throw new UIException("Device type display initialization failed: " + e.getMessage(), e);
        }
    }

    /**
     * Restores view state (after page loaded)
     */
    public void restoreViewState() {
        setSelectedDeviceTypeNodes(deviceTypeTreeController.getSelectedDeviceTypeNodes());
        onRowSelect();
    }

    /**
     * Refreshes data behind device type tree and updates UI component
     */
    public void updateDeviceTypesTree() {
        deviceTypeTreeController.getDeviceTypeTree().resetTree();
        PrimeFaces.current().ajax().update("deviceTypesForm:deviceTypeTree");
    }

    /** @see org.openepics.discs.conf.ui.common.ExcelImportUIHandlers#doImport() */
    @Override
    public void doImport() {
        excelSingleFileImportUI.doImport();
        clearDeviceTypeRelatedInformation();
    }

    /** @see org.openepics.discs.conf.ui.common.ExcelImportUIHandlers#prepareImportPopup() */
    @Override
    public void prepareImportPopup() {
        excelSingleFileImportUI.prepareImportPopup();
    }

    /** @see org.openepics.discs.conf.ui.common.ExcelImportUIHandlers#setDataLoader() */
    @Override
    public void setDataLoader() {
        excelSingleFileImportUI.setDataLoader();
    }

    /** org.openepics.discs.conf.ui.common.ExcelImportUIHandlers#getLoaderResult() */
    @Override
    public DataLoaderResult getLoaderResult() {
        return excelSingleFileImportUI.getLoaderResult();
    }

    /**
     * @see org.openepics.discs.conf.ui.common.ExcelSingleFileImportUIHandlers#handleImportFileUpload(FileUploadEvent)
     */
    @Override
    public void handleImportFileUpload(FileUploadEvent event) {
        excelSingleFileImportUI.handleImportFileUpload(event);
    }

    /** @see org.openepics.discs.conf.ui.common.ExcelSingleFileImportUIHandlers#getExcelImportFileName() */
    @Override
    public String getExcelImportFileName() {
        return excelSingleFileImportUI.getExcelImportFileName();
    }

    /** @see org.openepics.discs.conf.ui.common.ExcelImportUIHandlers#getImportedFileStatistics() */
    @Override
    public ImportFileStatistics getImportedFileStatistics() {
        return excelSingleFileImportUI.getImportedFileStatistics();
    }

    /**
     * @return the import statistics for the imported file
     * @see org.openepics.discs.conf.ui.common.AbstractExcelSingleFileImportUI#getImportFileStatistics()
     */
    public ImportFileStatistics getImportFileStatistics() {
        return excelSingleFileImportUI.getImportFileStatistics();
    }

    /**
     * @return the dialog containing a simple error message
     * @see org.openepics.discs.conf.ui.common.AbstractExcelSingleFileImportUI#getSimpleErrorTableExportDialog()
     */
    public ExportSimpleTableDialog getSimpleErrorTableExportDialog() {
        return excelSingleFileImportUI.getSimpleErrorTableExportDialog();
    }

    /** Called when user selects a row */
    public void onRowSelect() {
        if (selectedDeviceTypes != null && !selectedDeviceTypes.isEmpty()) {
            if (selectedDeviceTypes.size() == 1) {
                selectedComponent = selectedDeviceTypes.get(0);
            } else {
                selectedComponent = null;
            }
            comptypeAttributesController.clearRelatedAttributeInformation();
            comptypeAttributesController.populateAttributesList();
            // workaround for the PrimeFaces bug where multiple selection does not properly count the rows it must
            // display.
            PrimeFaces.current()
                    .executeScript("initializeDataTable(" + comptypeAttributesController.getAttributes().size() + ");");

        } else {
            clearDeviceTypeRelatedInformation();
        }
    }

    /**
     * This method duplicates selected device types. This method actually copies selected device type name, description,
     * tags, artifacts and properties into new device type. If property has set universally unique value, copied
     * property value is set to null.
     */
    public void duplicate() {
        try {
            Preconditions.checkState(!Utility.isNullOrEmpty(selectedDeviceTypes));

            final int duplicated = comptypeEJB.duplicate(
                    duplicateDeviceTypes.stream().map(DuplicatePair::getComponentType).collect(Collectors.toList()),
                    duplicateDeviceTypes.stream().map(DuplicatePair::getNewName).collect(Collectors.toList()));
            UiUtility.showInfoMessage(Entity.DEVICE_TYPES, Action.DUPLICATE, duplicated);
            deviceTypeTreeController.getDeviceTypeTree().resetTree();
        } finally {
            clearDeviceTypeRelatedInformation();
        }
    }

    /**
     * Throws a validation error exception if the slot name is not unique.
     *
     * @param ctx
     *            {@link javax.faces.context.FacesContext}
     * @param component
     *            {@link javax.faces.component.UIComponent}
     * @param value
     *            The value
     * @throws ValidatorException
     *             validation failed
     */
    public void validateDeviceTypeName(FacesContext ctx, UIComponent component, Object value) {
        /*
         * This parameter is checked so that validation is optionally performed depending on the ajax or final form
         * submit call.
         */
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (!params.containsKey("validate")) {
            return;
        }
        final String valueStr = value.toString();

        //Check if name is unique across application
        if (comptypeEJB.findByName(valueStr) != null) {
            throw UiUtility.getValidationError("Name already exists.");
        }

        //Check if name is unique in current dialog
        int count = 0;
        for (DuplicatePair duplicate : duplicateDeviceTypes) {
            if (valueStr.equals(duplicate.getNewName())) {
                count++;
            }
        }
        if (count > 1) {
            throw UiUtility.getValidationError("Name has to be unique.");
        }
    }

    /** Validates the {@link ComponentType} name for uniqueness.
     * @param ctx {@link javax.faces.context.FacesContext}
     * @param component {@link javax.faces.component.UIComponent}
     * @param value The value
     * @throws ValidatorException validation failed
     */
    public void nameValidator(FacesContext ctx, UIComponent component, Object value) {
        if (value == null) {
            throw UiUtility.getValidationError("Please enter a name");
        }

        final String deviceTypeName = value.toString();
        if (isNewDeviceType && comptypeEJB.findByName(deviceTypeName) != null) {
            throw UiUtility.getValidationError("The property with this name already exists.");
        }
    }

    private void clearDeviceTypeRelatedInformation() {
        selectedDeviceTypes = null;
        selectedComponent = null;
        comptypeAttributesController.clearRelatedAttributeInformation();
        resetFields();
    }

    /** Prepares the UI data for the "Add a new device type" dialog. */
    public void prepareAddPopup() {
        isNewDeviceType = true;
        comptypeAttributesController.resetFields();
        selectedComponent = new ComponentTypeView();
        PrimeFaces.current().ajax().update("addDeviceTypeForm:addDeviceType");
    }

    /** This method resets the dialog fields related to the selected device type */
    public void resetFields() {
        if (selectedDeviceTypes != null && selectedDeviceTypes.size() == 1) {
            selectedComponent = selectedDeviceTypes.get(0);
        } else {
            selectedComponent = null;
        }
        comptypeAttributesController.resetFields();
    }

    /** Called when the user presses the "Save" button in the "Add a new device type" dialog. */
    public void onAdd() {
        try {
            final ComponentType devType = selectedComponent.getComponentType();
            comptypeEJB.add(devType);
            UiUtility.showInfoMessage(Entity.DEVICE_TYPE, devType.getName(), Action.CREATE);
            paramId = devType.getId();
            deviceTypeTreeController.getDeviceTypeTree().resetTree();
        } catch (Exception e) {
            if (UiUtility.causedByPersistenceException(e)) {
                UiUtility.showErrorMessage(Entity.DEVICE_TYPE,
                        selectedComponent.getComponentType().getName(), Action.CREATE,
                        "A device type instance with same name already exists.", e);
            } else {
                throw e;
            }
        } finally {
            clearDeviceTypeRelatedInformation();
        }
    }

    /**
     * Selects recently added device type in device type tree.
     */
    public void selectAddedDeviceType() {
        selectNodeForParamId();
    }

    /** Prepares the data for the device type editing dialog fields based on the selected device type. */
    public void prepareEditPopup() {
        Preconditions.checkState(isSingleDeviceTypeSelected());
        isNewDeviceType = false;
    }

    /** Saves the new device type data (name and/or description) */
    public void onChange() {
        Preconditions.checkNotNull(selectedComponent);
        try {
            comptypeEJB.save(selectedComponent.getComponentType());
            UiUtility.showInfoMessage(Entity.DEVICE_TYPE,
                    selectedComponent.getComponentType().getName(), Action.UPDATE);
            deviceTypeTreeController.getDeviceTypeTree().resetTree();
        } catch (EJBException ejbException) {
            if (causedByOptimisticLockException(ejbException)) {
                deviceTypeTreeController.getDeviceTypeTree().resetTree();
                UiUtility.showErrorMessage("Device type (" + selectedComponent.getName()
                                + ") was concurrently modified by another transaction, please try again!" ,
                        ExceptionUtils.getStackTrace(ejbException));
            } else {
                throw ejbException;
            }
        } catch (Exception e) {
            if (UiUtility.causedByPersistenceException(e)) {
                UiUtility.showErrorMessage(Entity.DEVICE_TYPE,
                        selectedComponent.getComponentType().getName(), Action.UPDATE,
                        "A device type instance with same name already exists.", e);
            } else {
                throw e;
            }
        } finally {
            refreshSelectedComponent();
        }
    }

    private boolean causedByOptimisticLockException(Throwable exception) {
        Throwable iterated = exception;
        while ((iterated != null) && (iterated.getCause() != null)) {
            if (iterated instanceof OptimisticLockException)
                return true;

            iterated = iterated.getCause();
        }

        return false;
    }

    /**
     * The method builds a list of device types that are already used. If the list is not empty, it is displayed to the
     * user and the user is prevented from deleting them.
     */
    public void checkDeviceTypesForDeletion() {
        Preconditions.checkNotNull(selectedDeviceTypes);
        Preconditions.checkState(!selectedDeviceTypes.isEmpty());

        usedDeviceTypes = Lists.newArrayList();
        for (final ComponentTypeView deviceTypeToDelete : selectedDeviceTypes) {
            // CCDB-574: No limitation for result length
            List<String> usedBy = comptypeEJB.findWhereIsComponentTypeUsed(deviceTypeToDelete.getComponentType(), 0);
            if (!usedBy.isEmpty()) {
                // CCDB-574: Show all devices which use this device type instead of first one.
                deviceTypeToDelete.setUsedBy(usedBy);
                usedDeviceTypes.add(deviceTypeToDelete);
            }
        }
    }

    /** Called when the user presses the "Delete" button under table listing the devices types. */
    public void onDelete() {
        try {
            Preconditions.checkNotNull(selectedDeviceTypes);
            Preconditions.checkState(!selectedDeviceTypes.isEmpty());
            Preconditions.checkNotNull(usedDeviceTypes);
            Preconditions.checkState(usedDeviceTypes.isEmpty());

            int deletedDeviceTypes = 0;
            for (final ComponentTypeView deviceTypeToDelete : selectedDeviceTypes) {
                final ComponentType freshEntity = comptypeEJB.refreshEntity(deviceTypeToDelete.getComponentType());
                freshEntity.getTags().clear();
                comptypeEJB.delete(freshEntity);
                ++deletedDeviceTypes;
            }
            UiUtility.showInfoMessage(Entity.DEVICE_TYPES, Action.DELETE, deletedDeviceTypes);
            deviceTypeTreeController.getDeviceTypeTree().resetTree();
        } finally {
            clearDeviceTypeRelatedInformation();
        }
    }

    /** @return <code>true</code> if a single device type is selected , <code>false</code> otherwise */
    public boolean isSingleDeviceTypeSelected() {
        return (selectedDeviceTypes != null) && (selectedDeviceTypes.size() == 1);
    }

    /** Pulls a fresh ComponentType entity from the database for the <code>selectedComponent</code>. */
    protected void refreshSelectedComponent() {
        selectedComponent.setComponentType(comptypeEJB.refreshEntity(selectedComponent.getComponentType()));
    }

    public void onNodeStateChanged() {
        deviceTypeTreeController.getDeviceTypeTree().saveNodeStates();
    }

    // -------------------- Getters and Setters ---------------------------------------

    public String getNameFilter() {
        return deviceTypeTreeController.getNameFilter();
    }

    public void setNameFilter(String nameFilter) {
        deviceTypeTreeController.setNameFilter(nameFilter);
    }

    public String getDescriptionFilter() {
        return deviceTypeTreeController.getDescriptionFilter();
    }

    public void setDescriptionFilter(String descriptionFilter) {
        deviceTypeTreeController.setDescriptionFilter(descriptionFilter);
    }

    public Long getParamId() {
        return paramId;
    }

    public void setParamId(Long paramId) {
        this.paramId = paramId;
        selectNodeForParamId();
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(final String name) {
        this.paramName = name;
        selectNodeForParamName();
    }

    private void selectNodeForParamId() {
        if(paramId != null) {
            deviceTypeTreeController.resetState();
            if (deviceTypeTreeController.getDeviceTypeTree().getRoot() != null) {
                TreeNode selection = deviceTypeTreeController.getDeviceTypeTree().findAndSelectById(paramId);
                if (selection != null) {
                    setSelectedDeviceTypeNodes(new TreeNode[]{selection});
                    onRowSelect();
                }
            }
        }
    }

    private void selectNodeForParamName() {
        boolean found = false;
        if(StringUtils.isNotEmpty(paramName)) {
            deviceTypeTreeController.resetState();
            if (deviceTypeTreeController.getDeviceTypeTree().getRoot() != null) {
                TreeNode selection = deviceTypeTreeController.getDeviceTypeTree().findAndSelectByName(paramName);
                if (selection != null) {
                    found = true;
                    setSelectedDeviceTypeNodes(new TreeNode[]{selection});
                    onRowSelect();
                }
            }
        }

        if (!found) {
            UiUtility.showErrorMessage("The requested device type '" + paramName
                    + "' could not be located in the database.");
        }
    }

    public TreeNode getTreeRoot() {
        return deviceTypeTreeController.getDeviceTypeTree().getRoot();
    }

    /** @return <code>true</code> if no data was found, <code>false</code> otherwise */
    public boolean isDataTableEmpty() {
        return deviceTypeTreeController.getDeviceTypeTree().isEmpty();
    }

    /** @return the selectedDeviceTypes */
    public List<ComponentTypeView> getSelectedDeviceTypes() {
        return selectedDeviceTypes;
    }

    public TreeNode[] getSelectedDeviceTypeNodes() {
        return deviceTypeTreeController.getSelectedDeviceTypeNodes() != null
                ? Arrays.copyOf(deviceTypeTreeController.getSelectedDeviceTypeNodes(),
                deviceTypeTreeController.getSelectedDeviceTypeNodes().length)
                : null;
    }

    public void setSelectedDeviceTypeNodes(final TreeNode[] selectedDeviceTypeNodes) {
        deviceTypeTreeController.setSelectedDeviceTypeNodes(selectedDeviceTypeNodes != null
                ? Arrays.copyOf(selectedDeviceTypeNodes, selectedDeviceTypeNodes.length)
                : null);
        setSelectedDeviceTypes(selectedDeviceTypeNodes != null ? Arrays.stream(selectedDeviceTypeNodes).map(n ->
                new ComponentTypeView((ComponentType) n.getData())).collect(Collectors.toList()) : null);
    }

    /**
     * @param selectedDeviceTypes
     *            the selectedDeviceTypes to set
     */
    public void setSelectedDeviceTypes(List<ComponentTypeView> selectedDeviceTypes) {
        this.selectedDeviceTypes = selectedDeviceTypes;
        if (this.selectedDeviceTypes == null || this.selectedDeviceTypes.isEmpty()) {
            clearDeviceTypeRelatedInformation();
        }
    }

    /** Prepares duplicate pairs for the duplicate dialog. */
    public void prepareDuplicateDialog() {
        duplicateDeviceTypes = new ArrayList<>();
        for (ComponentTypeView deviceType : selectedDeviceTypes) {
            duplicateDeviceTypes.add(
                    new DuplicatePair(deviceType, comptypeEJB.getFreeDeviceTypeName(deviceType.getComponentType())));
        }
    }

    /** @return the duplicateDeviceTypes */
    public List<DuplicatePair> getDuplicatePairs() {
        return duplicateDeviceTypes;
    }

    /** @return the {@link List} of used device types */
    public List<ComponentTypeView> getUsedDeviceTypes() {
        return usedDeviceTypes;
    }

    @Override
    public ExportSimpleTableDialog getSimpleTableDialog() {
        return simpleTableExporterDialog;
    }

    /** @return the filteredDialogTypes */
    public List<ComponentTypeView> getFilteredDialogTypes() {
        return filteredDialogTypes;
    }

    /**
     * @param filteredDialogTypes
     *            the filteredDialogTypes to set
     */
    public void setFilteredDialogTypes(List<ComponentTypeView> filteredDialogTypes) {
        this.filteredDialogTypes = filteredDialogTypes;
    }

    /** @return the selectedComponent */
    public ComponentTypeView getSelectedComponent() {
        return selectedComponent;
    }

    public boolean isShowOnlyUnreferenced() {
        return deviceTypeTreeController.isShowOnlyUnreferenced();
    }

    public void setShowOnlyUnreferenced(boolean unreferenced) {
        deviceTypeTreeController.setShowOnlyUnreferenced(unreferenced);
    }

    public void handleShowOnlyUnreferenced(AjaxBehaviorEvent e) {
        final SelectBooleanCheckbox source = (SelectBooleanCheckbox) e.getSource();
        final boolean submittedValue = (Boolean) source.getSubmittedValue();

        setShowOnlyUnreferenced(submittedValue);
    }
}
