/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.ui.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.omnifaces.cdi.ViewScoped;
import org.openepics.cable.jaxb.CableElement;
import org.openepics.cable.jaxb.EndpointElement;
import org.openepics.discs.conf.ejb.SlotEJB;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.services.cable.CableCache;

/**
 * Service for CableDB
 *
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovic</a>
 */
@Named
@ViewScoped
public class ConnectsManager implements Serializable  {
    private static final long serialVersionUID = 1615712215239730844L;

    @Inject private SlotEJB slotEJB;
    @Inject private CableCache cableCache;

    private boolean slotCachingEnabled = false;
    private Map<String, List<Slot>> slotCache;
    private Map<String, List<CableElement>> slotToCableMapping;

    /**
     * @param slot - the slot to use in query.
     * @return The list of all {@link Slot}s to which the slot is connected.
     */
    public List<Slot> getSlotConnects(Slot slot) {
        if (slotCachingEnabled) {
            buildConnectsHierarchyCacheIfNecessary();

            return getConnectedSlots(slot);
        } else {
            List<Slot> connects = new ArrayList<>();
            HashSet<String> uniqueEndpoints = new HashSet<>();

            for (CableElement cable : cableCache.getAllCables()) {
                Slot n2;
                if (isSlotCableEndpoint(slot, cable.getEndpointA())) {
                    n2 = getDeviceOfEndpoint(cable.getEndpointB());
                } else if (isSlotCableEndpoint(slot, cable.getEndpointB())) {
                    n2 = getDeviceOfEndpoint(cable.getEndpointA());
                } else continue;

                if (n2 != null && !uniqueEndpoints.contains(n2.getName())) {
                    connects.add(n2);
                    uniqueEndpoints.add(n2.getName());
                }
            }
            uniqueEndpoints.clear();

            return connects;
        }
    }

    private Slot getDeviceOfEndpoint(EndpointElement endpoint) {
        if (StringUtils.isNotEmpty(endpoint.getEssName())) {
            return slotEJB.findByName(endpoint.getEssName());
        }
        if (StringUtils.isNotEmpty(endpoint.getFbsTag())) {
            return slotEJB.findByFbsName(endpoint.getFbsTag());
        }
        return null;
    }

    /** Return all cables connecting two {@link Slot}s
     * @param slot1 slot 1
     * @param slot2 slot 2
     * @return A {@link List} of {@link CableElement}s connecting the two {@link Slot}s
     */
    public List<CableElement> getCables(Slot slot1, Slot slot2) {
        if (slotCachingEnabled) {
            buildConnectsHierarchyCacheIfNecessary();

            return getCablesConnectingSlots(slot1.getName(), slot2.getName());

        } else {
            List<CableElement> r = new ArrayList<>();

            for (CableElement cable : cableCache.getAllCables()) {
                if (isSlotCableEndpoint(slot1, cable.getEndpointA())
                        && isSlotCableEndpoint(slot2, cable.getEndpointB())
                        || isSlotCableEndpoint(slot1, cable.getEndpointB())
                        && isSlotCableEndpoint(slot2, cable.getEndpointA())) {
                    r.add(cable);
                }
            }
            return r;
        }
    }

    private boolean isSlotCableEndpoint(Slot slot, EndpointElement endpoint) {
        if(StringUtils.isNotEmpty(endpoint.getEssName())) {
            return endpoint.getEssName().equals(slot.getName());
        }
        if(StringUtils.isNotEmpty(endpoint.getFbsTag())) {
            return endpoint.getFbsTag().equals(slot.getFbsName());
        }
        return false;
    }

    private List<CableElement> getCablesConnectingSlots(String slotName1, String slotName2) {
        final List<CableElement> cablesOfSlot1 = slotToCableMapping.get(slotName1);
        final List<CableElement> cablesOfSlot2 = slotToCableMapping.get(slotName2);

        if (cablesOfSlot1 == null || cablesOfSlot2 == null) {
            return Collections.emptyList();
        } else {
            // Return only cables that are connected to both slots.
            return cablesOfSlot1.stream()
                                .filter(cablesOfSlot2::contains)
                                .collect(Collectors.toList());
        }
    }

    /** @return the cableDBStatus */
    public boolean getCableDBStatus() {
        return cableCache.getCableDBStatus();
    }

    public String getRelationshipName() {
        return "Connects";
    }

    /**
     * Enable caching of connects hierarchy.
     */
    public void startCaching() {
        slotCachingEnabled = true;
    }

    /**
     * Disable caching of connects hierarchy.
     */
    public void stopCaching() {
        slotCachingEnabled = false;
        slotCache = null;
        slotToCableMapping = null;
    }

    private void buildConnectsHierarchyCacheIfNecessary() {
        if (slotCache != null && slotToCableMapping != null) {
            return;
        }

        // Create mappings from Slot name, Slot FBS tag to Slot.
        slotCache = new HashMap<>();
        for (final Slot slot : slotEJB.findAll()) {
            if (StringUtils.isNotEmpty(slot.getName())) {
                addElementToListInMap(slotCache, slot.getName(), slot);
            }
            if (StringUtils.isNotEmpty(slot.getFbsName())) {
                addElementToListInMap(slotCache, slot.getFbsName(), slot);
            }
        }

        slotToCableMapping = new HashMap<>();

        for (final CableElement cable : cableCache.getAllCables()) {
            final List<Slot> slotA = getSlotFromCache(cable.getEndpointA());
            for (final Slot slot : slotA) {
                addElementToListInMap(slotToCableMapping, slot.getName(), cable);
            }

            final List<Slot> slotB = getSlotFromCache(cable.getEndpointB());
            for (final Slot slot : slotB) {
                addElementToListInMap(slotToCableMapping, slot.getName(), cable);
            }
        }
    }

    private List<Slot> getSlotFromCache(EndpointElement endpoint) {
        List<Slot> slots =  null;
        if (StringUtils.isNotEmpty(endpoint.getEssName())) {
            slots = slotCache.get(endpoint.getEssName());
        } else if (StringUtils.isNotEmpty(endpoint.getFbsTag())) {
            slots = slotCache.get(endpoint.getFbsTag());
        }
        return slots != null ? slots : Collections.emptyList();
    }

    private <K, V> void addElementToListInMap(Map<K, List<V>> map, K key, V value) {
        if (!map.containsKey(key)) {
            map.put(key, new ArrayList<>());
        }

        map.get(key).add(value);
    }

    private List<Slot> getConnectedSlots(Slot slot) {
        final List<CableElement> cableElements = slotToCableMapping.get(slot.getName());

        if (cableElements == null) {
            return Collections.emptyList();
        } else {
            return cableElements.stream()
                                .map(cable -> getSlotAtOtherEndOfCable(slot, cable))
                                .flatMap(List::stream)
                                .distinct()
                                .collect(Collectors.toList());
        }
    }

    private List<Slot> getSlotAtOtherEndOfCable(Slot slot, CableElement cable) {
        String nameEndpointA = cable.getEndpointA().getEssName();
        if (StringUtils.isNotEmpty(nameEndpointA)) {
            if (nameEndpointA.equals(slot.getName())) {
                return getSlotFromCache(cable.getEndpointB());
            }
        } else {
            String fbsEndpointA = cable.getEndpointA().getFbsTag();
            if (StringUtils.isNotEmpty(fbsEndpointA)) {
                if (fbsEndpointA.equals(slot.getFbsName())) {
                    return getSlotFromCache(cable.getEndpointB());
                }
            }
        }

        String nameEndpointB = cable.getEndpointB().getEssName();
        if (StringUtils.isNotEmpty(nameEndpointB)) {
            if (nameEndpointB.equals(slot.getName())) {
                return getSlotFromCache(cable.getEndpointA());
            }
        } else {
            String fbsEndpointB = cable.getEndpointB().getFbsTag();
            if (StringUtils.isNotEmpty(fbsEndpointB)) {
                if (fbsEndpointB.equals(slot.getFbsName())) {
                    return getSlotFromCache(cable.getEndpointA());
                }
            }
        }

        return Collections.emptyList();
    }
}
