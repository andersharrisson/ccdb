/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.openepics.discs.conf.services.names;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.enterprise.inject.Alternative;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.openepics.names.jaxb.DeviceNameElement;

/**
 * Provides an empty list of names for the purposes of development so that the application doesn't
 * have to connect to naming server every time.
 *
 * @author <a href="mailto:andraz.pozar@cosylab.com">Andraž Požar</a>
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
@Named("names")
@ViewScoped
@Alternative
public class DummyNames implements Names {
    private static final long serialVersionUID = 7974397392868166709L;

    @Override
    public boolean isEnabled() {
        return false;
    }

    @Override
    public boolean isError() {
        return false;
    }

    @Override
    public void refresh() {
        // do nothing
    }

    @Override
    public Map<String, DeviceNameElement> getAllNames() {
        return new HashMap<>();
    }

    @Override
    public Set<String> getActiveNames() {
        return null;
    }

    @Override
    public NameStatus getNameStatus(String name) {
        return NameStatus.NONE;
    }

    @Override
    public String getNameByUuid(String uuid) {
        return null;
    }

    @Override
    public String getUuidByName(String name) {
        return null;
    }
}
