/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.views;

import org.openepics.discs.conf.ent.ConfigurationEntity;
import org.openepics.discs.conf.ent.NamedEntity;
import javax.annotation.Nullable;

/**
 * The UI view class for the editable entity attributes.
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public abstract class EditableEntityAttributeView<E extends ConfigurationEntity & NamedEntity> extends EntityAttributeView<E> {

    protected String uiValue;
    protected EditableEntityAttributeView(@Nullable E viewParent, String usedBy) {
        super(viewParent, usedBy);
    }

    /**
     * Sets the value of the attribute
     *
     * @param value attribute value
     */
    public abstract void setValue(final String value);

    /**
     * @return Gives back the value added to input control by user (before validation, it may be invalid value for the
     * given data type).
     */
    public String getUiValue() {
        return uiValue;
    }
}
