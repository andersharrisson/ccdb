/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.ui;

import java.io.Serializable;

import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.views.ComponentTypeView;

/**
 * Represents the duplicate pair which is used while duplicating device types.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class DuplicatePair implements Serializable {
    private static final long serialVersionUID = 1L;

    private ComponentTypeView deviceType;
    private String newName;

    /**
     * Constructs the duplicate pair object that is used for device type duplication.
     *
     * @param deviceType
     *            the device to be duplicated
     * @param newName
     *            the new name of the duplicated device
     */
    public DuplicatePair(ComponentTypeView deviceType, String newName) {
        this.deviceType = deviceType;
        this.newName = newName;
    }

    /** @return the component type view for the device */
    public ComponentTypeView getDeviceType() {
        return deviceType;
    }

    /** @return the component type for the device */
    public ComponentType getComponentType() {
        return this.deviceType.getComponentType();
    }

    public void setDeviceType(ComponentTypeView deviceType) {
        this.deviceType = deviceType;
    }

    /** @return the new name for the device type */
    public String getNewName() {
        return newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }
}
