/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.services.fbs;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Utility class to get FBS tag and associated information.
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 *
 * @see FbsElement
 **/
public class FbsUtil {

    /**
     * This class is not to be instantiated.
     */
    private FbsUtil() {
        throw new IllegalStateException("Utility class");
    }


    /**
     * Converts an FBS element list into a map from slot name to FBS tags.
     *
     * @param fbsElements FBS element list retrieved from server.
     * @return map of slot name -> Fbs tag.
     */
    public static Map<String, String> readTagMappings(final List<FbsElement> fbsElements) {
        if (fbsElements == null || fbsElements.isEmpty()) {
            return Collections.emptyMap();
        }

        final Map<String, List<String>> mapWithMultipleValues = new HashMap<>();
        fbsElements.stream()
                        .filter(element -> !StringUtils.isEmpty(element.tag) && !StringUtils.isEmpty(element.essName))
                        .forEach(element -> addElementToListInMap(mapWithMultipleValues, element.essName, element.tag));

        final Map<String, String> mapWithOneValue = new HashMap<>();

        for (Map.Entry<String, List<String>> entry : mapWithMultipleValues.entrySet()) {
            String essName = entry.getKey();
            List<String> values = entry.getValue();

            String value;
            if (values.size() > 1) {
                value = "Ambiguous name: " + String.join(", ", values);
            } else {
                value = values.get(0);
            }

            mapWithOneValue.put(essName, value);
        }

        return mapWithOneValue;
    }

    /**
     * Converts an FBS element list into a list of all valid FBS tags.
     * @param fbsElements FBS element list retrieved from server.
     * @return list of valid Fbs tags.
     */
    public static List<String> readTags(final List<FbsElement> fbsElements) {
        if (fbsElements == null || fbsElements.isEmpty()) {
            return Collections.emptyList();
        }

        return fbsElements.stream().filter(element -> StringUtils.isNotEmpty(element.tag)).
                map(element -> element.tag).
                collect(Collectors.toList());
    }

    private static void addElementToListInMap(Map<String, List<String>> map, String key, String value) {
        if (!map.containsKey(key)) {
            map.put(key, new ArrayList<>());
        }

        map.get(key).add(value);
    }
}
