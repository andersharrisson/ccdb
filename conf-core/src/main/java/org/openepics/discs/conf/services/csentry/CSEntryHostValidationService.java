/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.services.csentry;

import org.apache.commons.lang3.StringUtils;
import org.openepics.discs.conf.ejb.ComptypeEJB;
import org.openepics.discs.conf.ejb.DataTypeEJB;
import org.openepics.discs.conf.ejb.DeviceEJB;
import org.openepics.discs.conf.ejb.SlotEJB;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.ComptypePropertyValue;
import org.openepics.discs.conf.ent.DataType;
import org.openepics.discs.conf.ent.Device;
import org.openepics.discs.conf.ent.DevicePropertyValue;
import org.openepics.discs.conf.ent.Property;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotPropertyValue;
import org.openepics.discs.conf.ent.values.StrValue;
import org.openepics.discs.conf.util.BuiltInDataType;
import org.openepics.discs.conf.services.ConcurrentOperationServiceBase;
import org.openepics.discs.conf.util.csentry.CSEntryClient;
import org.openepics.discs.conf.util.csentry.CSEntryService;

import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

/**
 * Performs periodic host name validation for host names from CSEntry service.
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Lock(LockType.READ)
@Singleton
@Startup
@DependsOn("DatabaseMigration")
public class CSEntryHostValidationService extends ConcurrentOperationServiceBase implements Serializable {
    private static final Logger LOGGER = Logger.getLogger(CSEntryHostValidationService.class.getName());

    @Inject private transient DataTypeEJB dataTypeEJB;
    @Inject private transient SlotEJB slotEJB;
    @Inject private transient DeviceEJB deviceEJB;
    @Inject private transient ComptypeEJB comptypeEJB;
    @Inject private transient CSEntryService csEntryService;

    // the time to wait before updating validity (20 min )
    private static final String UPDATE_INTERVAL = "*/20";

    private List<String> invalidHostNamesInDatabase = new ArrayList<>();

    @PostConstruct
    protected void initialise() {
        super.initialise();
    }

    /**
     * Updated name service cache.
     *
     */
    @Schedule(minute = UPDATE_INTERVAL, hour = "*", persistent = false)
    public void update() {
        runOperationNonBlocking();
    }

    private List<Property> findPropertiesHavingCSEntryHostDataType() {
        DataType csEntryHostType = null;
        try {
            csEntryHostType = dataTypeEJB.findByName(BuiltInDataType.CSENTRY_HOST.getDbName());
        } catch (Exception e) {
            LOGGER.warning("Failed to find " + BuiltInDataType.CSENTRY_HOST.getDbName() + " data type in database.");
        }

        return csEntryHostType != null ? dataTypeEJB.findNotAliasProperties(csEntryHostType) : Collections.emptyList();
    }

    private void validateSlotPropertyValues(final List<Property> propertiesHavingCSEntryHostType) {
        List<SlotPropertyValue> slotPropertyValues = slotEJB.findNotAliasByProperties(propertiesHavingCSEntryHostType);
        for (SlotPropertyValue propValue : slotPropertyValues) {
            if (propValue.getPropValue() != null && propValue.getPropValue() instanceof StrValue) {
                String hostname = ((StrValue)propValue.getPropValue()).getStrValue();
                CSEntryClient.HostCheckResult checkResult = csEntryService.checkHostname(hostname);
                if (checkResult.isInvalid()) {
                    if (StringUtils.isNotEmpty(checkResult.getSuggestedFQDN())) {
                        Slot slot = propValue.getSlot();
                        for (SlotPropertyValue slotPropertyValue : slot.getSlotPropertyList()) {
                            if (slotPropertyValue.getId().equals(propValue.getId())) {
                                slotPropertyValue.setPropValue(new StrValue(checkResult.getSuggestedFQDN()));
                                break;
                            }
                        }
                        slotEJB.saveInternal(slot, getServiceName());
                        LOGGER.warning("FIXED AUTOMATICALLY: Invalid CSEntry Host name for slot ("
                                + propValue.getSlot().getName() + "), property ("
                                + propValue.getProperty().getName() + "): " + hostname + ". Changed to: "
                                + checkResult.getSuggestedFQDN() + ". Last modified by: " + propValue.getModifiedBy());
                    } else {
                        invalidHostNamesInDatabase.add(hostname);
                        LOGGER.warning("Invalid CSEntry Host name for slot ("
                                + propValue.getSlot().getName() + "), property ("
                                + propValue.getProperty().getName() + "): " + hostname + ". Last modified by: "
                                + propValue.getModifiedBy());
                    }
                }
            }
        }
    }

    private void validateDevicePropertyValues(final List<Property> propertiesHavingCSEntryHostType) {
        List<DevicePropertyValue> devicePropertyValues = deviceEJB.findByProperties(propertiesHavingCSEntryHostType);
        for (DevicePropertyValue propValue : devicePropertyValues) {
            if (propValue.getPropValue() != null && propValue.getPropValue() instanceof StrValue) {
                String hostname = ((StrValue)propValue.getPropValue()).getStrValue();
                CSEntryClient.HostCheckResult checkResult = csEntryService.checkHostname(hostname);
                if (checkResult.isInvalid()) {
                    if (StringUtils.isNotEmpty(checkResult.getSuggestedFQDN())) {
                        Device device = propValue.getDevice();
                        for (DevicePropertyValue devicePropertyValue : device.getDevicePropertyList()) {
                            if (devicePropertyValue.getId().equals(propValue.getId())) {
                                devicePropertyValue.setPropValue(new StrValue(checkResult.getSuggestedFQDN()));
                                break;
                            }
                        }
                        deviceEJB.saveInternal(device, getServiceName());
                        LOGGER.warning("FIXED AUTOMATICALLY: Invalid CSEntry Host name for device ("
                                + propValue.getDevice().getName() + "), property ("
                                + propValue.getProperty().getName() + "): " + hostname + ". Suggestion: "
                                + checkResult.getSuggestedFQDN() + ". Last modified by: " + propValue.getModifiedBy());
                    } else {
                        invalidHostNamesInDatabase.add(hostname);
                        LOGGER.warning("Invalid CSEntry Host name for device (" + propValue.getDevice().getName()
                                + "), property ("
                                + propValue.getProperty().getName() + "): " + hostname
                                + ". Last modified by: " + propValue.getModifiedBy());
                    }
                }
            }
        }
    }

    private void validateDeviceTypePropertyValues(final List<Property> propertiesHavingCSEntryHostType) {
        List<ComptypePropertyValue> componentTypePropertyValues =
                comptypeEJB.findByProperties(propertiesHavingCSEntryHostType);
        for (ComptypePropertyValue propValue : componentTypePropertyValues) {
            if (propValue.getPropValue() != null && propValue.getPropValue() instanceof StrValue) {
                String hostname = ((StrValue)propValue.getPropValue()).getStrValue();
                CSEntryClient.HostCheckResult checkResult = csEntryService.checkHostname(hostname);
                if (checkResult.isInvalid()) {
                    if (StringUtils.isNotEmpty(checkResult.getSuggestedFQDN())) {
                        ComponentType componentType = propValue.getComponentType();
                        for (ComptypePropertyValue comptypePropertyValue : componentType.getComptypePropertyList()) {
                            if (comptypePropertyValue.getId().equals(propValue.getId())) {
                                comptypePropertyValue.setPropValue(new StrValue(checkResult.getSuggestedFQDN()));
                                break;
                            }
                        }
                        comptypeEJB.saveInternal(componentType, getServiceName());
                        LOGGER.warning("FIXED AUTOMATICALLY: Invalid CSEntry Host name for device type ("
                                + propValue.getComponentType().getName() + "), property ("
                                + propValue.getProperty().getName() + "): " + hostname + ". Suggestion: "
                                + checkResult.getSuggestedFQDN() + ". Last modified by: " + propValue.getModifiedBy());
                    } else {
                        invalidHostNamesInDatabase.add(hostname);
                        LOGGER.warning("Invalid CSEntry Host name for device type ("
                                + propValue.getComponentType().getName() + "), property ("
                                + propValue.getProperty().getName() + "): " + hostname + ". Last modified by: "
                                + propValue.getModifiedBy());
                    }
                }
            }
        }
    }

    @Override
    protected String getServiceName() {
        return "CSEntry Host Validation Service";
    }

    @Override
    protected String getOperation() {
        return "Host name validation";
    }

    /**
     * Performs validations
     */
    public void validate() {
        runOperationBlocking();
    }

    public boolean isHostNameInvalid(final String hostname) {
        return invalidHostNamesInDatabase.contains(hostname);
    }

    /**
     * Validates host name properties of slots, device types and devices
     *
     * @param vargs
     *            arguments to pass on to operation
     *
     * @return is operation successful
     */
    @Lock(LockType.WRITE)
    @Override
    protected boolean runOperation(String... vargs) {
        invalidHostNamesInDatabase = new ArrayList<>();
        List<Property> propertiesHavingCSEntryHostType = findPropertiesHavingCSEntryHostDataType();
        if (propertiesHavingCSEntryHostType != null && !propertiesHavingCSEntryHostType.isEmpty()) {
            validateDeviceTypePropertyValues(propertiesHavingCSEntryHostType);
            validateSlotPropertyValues(propertiesHavingCSEntryHostType);
            validateDevicePropertyValues(propertiesHavingCSEntryHostType);
        }
        return true;
    }
}
