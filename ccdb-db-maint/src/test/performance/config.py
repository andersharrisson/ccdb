#Settings for database stress testing
#
#####################################
#Amount of population data settings
# Minimum amount of any imported entity is 1.

deviceTypeCount = 10
#number of devices to add
deviceCount = 10000
#number of properties per device
devicePropertiesPerDevice = 10
slotCount = 100000
slotPairCount = slotCount 
slotPropertiesCount = 600000
slotPropertiesPerSlot = 100
propertyTypeCount = slotPropertiesPerSlot
propertyValueSize = 10 # String length
# of logs that will be inserted
logsCount = 250000
# length of variable part of log
textLength = 200
#Database connection settings
databaseUser = 'ccdb'
databasePassword = 'ccdb'
databaseHost = 'localhost'
databasePort = 5432
databaseName = 'ccdb'