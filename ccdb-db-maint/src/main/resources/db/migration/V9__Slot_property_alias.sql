ALTER TABLE property ADD alias_of int8 NULL DEFAULT NULL;
ALTER TABLE property ADD CONSTRAINT property_property_fk FOREIGN KEY (alias_of) REFERENCES property(id);
ALTER TABLE slot_property_value ADD alias_of int8 NULL DEFAULT NULL;
ALTER TABLE slot_property_value ADD CONSTRAINT slot_property_value_slot_property_value_fk FOREIGN KEY (alias_of) REFERENCES slot_property_value(id) ON DELETE SET NULL;
