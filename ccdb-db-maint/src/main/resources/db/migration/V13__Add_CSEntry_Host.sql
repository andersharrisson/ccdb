/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

INSERT INTO data_type (id, modified_at, modified_by, version, description, name, scalar) VALUES (nextval('hibernate_sequence'), CURRENT_TIMESTAMP, 'system', 0, 'CSEntry host name', 'CSEntryHost', TRUE);
UPDATE property SET (data_type) = (data_type.id) FROM data_type WHERE (property.name = 'Hostname' OR property.alias_of = (SELECT id FROM property WHERE property.name = 'Hostname')) AND data_type.name = 'CSEntryHost';