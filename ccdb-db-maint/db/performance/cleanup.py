#!/usr/bin/python
#
# Requirements to run this script : python and pg8000
# To install pg8000 : pip install pg8000

import pg8000 as pg
from config import *

connection = pg.connect(user = databaseUser, password = databasePassword, host = databaseHost, port = databasePort, database = databaseName)
cursor = connection.cursor()

lastId = 2000000

def getNamePattern():
    return '%%\\_'

cursor.execute("DELETE FROM slot_property_value WHERE id >= " + str(lastId))
cursor.execute("DELETE FROM device_property_value WHERE id >= " + str(lastId))
cursor.execute("DELETE FROM property WHERE id >= " + str(lastId))
cursor.execute("TRUNCATE slot_pair CASCADE")
# This is slower, can be used for preserving existing database content  
#cursor.execute("DELETE FROM slot_pair WHERE id >= '" + str(lastId) + "'")
cursor.execute("TRUNCATE slot CASCADE")
# This is slower, can be used for preserving existing database content  
#cursor.execute("DELETE FROM slot WHERE name LIKE '" + getNamePattern() + "'")
cursor.execute("INSERT INTO slot (id, modified_at, modified_by, version, description, is_hosting_slot, name, component_type) VALUES ('47','2016-07-13 15:03:58.93','system','0','Implicit CCDB type.','f','_ROOT','45')")
cursor.execute("TRUNCATE device CASCADE")
cursor.execute("DELETE FROM component_type WHERE name LIKE '" + getNamePattern() + "'")
# This can be used to repopulate default component types if clearing the whole component_type table, currently unused
#cursor.execute("INSERT INTO component_type (id, modified_at, modified_by, version, description, name) VALUES (45, 2016-07-13 15:03:58.929, system, 0, _ROOT, _ROOT), (46,2016-07-13 15:03:58.929,system,0,_GRP,_GRP)")

#Clear all logs
cursor.execute("TRUNCATE audit_record CASCADE")

connection.commit()
cursor.close()
connection.close()

print('Done')